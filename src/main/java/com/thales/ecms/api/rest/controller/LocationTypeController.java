package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.LocationTypeDTO;
import com.thales.ecms.service.LocationTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 * Controller for location type.
 *
 */
@Controller
@RequestMapping(value = "/locationtype")
public class LocationTypeController {

	@Autowired
	LocationTypeService locationTypeService;

	public static Logger logger = LoggerFactory.getLogger(LocationTypeController.class);

	/**
	 * Controller method for adding location type
	 * 
	 * @param request
	 * @param response
	 * @param locationType
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	@ApiOperation(value = "Adds LocationType as per given DTO")
	@RequestMapping(method = RequestMethod.POST, value = "/addlocationtype", consumes = "application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<?> addLocationType(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "DTO object for LocationType", required = true) @RequestBody LocationTypeDTO locationType) throws UnsupportedEncodingException, EmptyResponseException,
			ObjectNotFoundException, BadRequestException, ObjectExistsException {

		LocationTypeDTO locationTypeDTO = locationTypeService.addLocationType(locationType);
		if (locationTypeDTO != null) {
			return new ResponseEntity<>(locationTypeDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - Cannot create Location Type", HttpStatus.OK));
		}
	}

	/**
	 * Controller method for getting list of all location types
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/locationtypelist", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<?> getlocationTypeList(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {

		List<LocationTypeDTO> locationTypeDTO = locationTypeService.getlocationTypeList();
		if (locationTypeDTO != null && !locationTypeDTO.isEmpty()) {
			return new ResponseEntity<>(locationTypeDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - no Location Types exist", HttpStatus.OK));
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param locationId
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteLocationType/{locationTypeId}")
	@ResponseBody
	public ResponseEntity<?> deleteLocationById(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String locationTypeId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {

		String responseString = locationTypeService.deleteLocationTypeById(locationTypeId);
		if (responseString != null) {
			responseString = ObjectUtils.toJson(responseString);
			return new ResponseEntity<>(responseString, HttpStatus.OK);
		} else
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - Cannot delete LocationType", HttpStatus.OK));
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/updateLocationTypeById/{locationTypeId}")
	@ResponseBody
	public ResponseEntity<?> updateLocationTypeById(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String locationTypeId, @RequestBody LocationTypeDTO locationTypeDetails)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, ObjectExistsException {

		LocationTypeDTO locationTypeDTO = locationTypeService.updateLocationTypeById(locationTypeId, locationTypeDetails);
		if (locationTypeDTO != null) {
			return new ResponseEntity<>(locationTypeDTO, HttpStatus.OK);
		} else
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - Cannot update LocationType", HttpStatus.OK));
	}
	
	/*
	 * @RequestMapping(method = RequestMethod.PUT, value =
	 * "/updateLocation/{locationId}")
	 * 
	 * @ResponseBody public ResponseEntity<?>
	 * updateLocationById(HttpServletRequest request, HttpServletResponse
	 * response, @PathVariable String locationId,
	 * 
	 * @RequestBody LocationDTO airlineDetails)throws
	 * UnsupportedEncodingException, EmptyResponseException,
	 * ObjectNotFoundException, BadRequestException{
	 * 
	 * LocationDTO locationDTO = locationService.updateLocationById(locationId,
	 * airlineDetails); if(locationDTO != null){ return new
	 * ResponseEntity<>(locationDTO, HttpStatus.OK); } else{ throw new
	 * EmptyResponseException(new ErrorResponse(
	 * "Response returned was empty - Cannot create Airline Contact",
	 * HttpStatus.OK)); } }
	 */

}
