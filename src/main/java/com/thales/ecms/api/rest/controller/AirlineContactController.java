package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.AirlineContactDTO;
import com.thales.ecms.service.AirlineContactService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
public class AirlineContactController {

	@Autowired
	AirlineContactService airlineContactService;

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(AirlineContactController.class);
	
	/**
	 * Controller method to add an Airline Contact to an Airline
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param airlineContactDetails
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	@ApiOperation(value = "Add Airline Contacts as per given DTO")
	@RequestMapping(method = RequestMethod.POST, value = "/airlines/{airlineId}/contacts", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> addAirlineContact(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId,
			@ApiParam(value = "DTO Object for Airline Contact", required = true) @RequestBody AirlineContactDTO airlineContactDetails)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException,
			ObjectExistsException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		if(!EmailValidator.getInstance().isValid(airlineContactDetails.getEmail()))
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_EMAIL_INVALID,
							new Object[] { ECMSConstants.FIELD_EMAIL }, Locale.US), HttpStatus.BAD_REQUEST));
		PhoneNumberUtil util = PhoneNumberUtil.getInstance();
//		As It was printing exception stack trace, I've enclosed as below
		try{
			util.isValidNumber(util.parse(airlineContactDetails.getPhone(), request.getLocale().getCountry()));				
		}catch(NumberParseException e){
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_PHONE_INVALID,
					new Object[] { ECMSConstants.FIELD_PHONE }, Locale.US), HttpStatus.FORBIDDEN));
		}
		AirlineContactDTO airlineContactDTO = airlineContactService.addAirlineContact(airlineId, airlineContactDetails);
		if (airlineContactDTO != null) {
			return new ResponseEntity<>(airlineContactDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] { ECMSConstants.AIRLINE_CONTACT }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to return all Airline Contacts of an Airline
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	@ApiOperation(value = "Retrieve all Airline Contacts for an Airline")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/contacts", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getAirlineContacts(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		List<AirlineContactDTO> airlineContactDTOList = airlineContactService.getAirlineContacts(airlineId);
		if (airlineContactDTOList != null && !airlineContactDTOList.isEmpty()) {
			return new ResponseEntity<>(airlineContactDTOList, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller Method to return an Airline Contact for an Airline
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param contactId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	@ApiOperation(value = "Retrieve airline contact as per given Airport ID and Airline Contact ID")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/contacts/{contactId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getAirlineContact(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable("airlineId") String airlineId,
			@ApiParam(value = "Airline Contact ID", required = true) @PathVariable("contactId") String contactId)
			throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		contactId = URLDecoder.decode(contactId, ECMSConstants.UTF);
		AirlineContactDTO airlineContactDTO = airlineContactService.getAirlineContactById(airlineId, contactId);
		if (airlineContactDTO != null) {
			return new ResponseEntity<>(airlineContactDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}
	
	/**
	 * Controller Method to return an Airline Contact for an Airline
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param phone
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	@ApiOperation(value = "Retrieve airline contact as per given Airport ID and Airline Contact ID")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/phone/{phone}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getAirlineContactByPhone(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable("airlineId") String airlineId,
			@ApiParam(value = "Airline Contact ID", required = true) @PathVariable("phone") String phone)
			throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		phone = URLDecoder.decode(phone, ECMSConstants.UTF);
		AirlineContactDTO airlineContactDTO = airlineContactService.getAirlineContactByPhone(airlineId, phone);
		if (airlineContactDTO != null) {
			return new ResponseEntity<>(airlineContactDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}
	
	/**
	 * Controller Method to return an Airline Contact for an Airline
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param email
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	@ApiOperation(value = "Retrieve airline contact as per given Airport ID and Airline Contact Email ID")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/{email}/email", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getAirlineContactByEmail(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable("airlineId") String airlineId,
			@ApiParam(value = "Airline Contact Email ID", required = true) @PathVariable("email") String email)
			throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		email = URLDecoder.decode(email, ECMSConstants.UTF);
		AirlineContactDTO airlineContactDTO = airlineContactService.getAirlineContactByEmail(airlineId, email);
		if (airlineContactDTO != null) {
			return new ResponseEntity<>(airlineContactDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}

	
	/**
	 * Controller Method to return all Airline Contacts of an Airline
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	@ApiOperation(value = "Retrieve all Airline Contacts for all Airline")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/contacts", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getAllAirlineContacts(HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {		
		List<AirlineContactDTO> airlineContactDTOList = airlineContactService.getAllAirlineContacts();
		if (airlineContactDTOList != null && !airlineContactDTOList.isEmpty()) {
			return new ResponseEntity<>(airlineContactDTOList, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller Method to update an Airline Contact for an Airline
	 * 
	 * @param request
	 * @param response
	 * @param contactId
	 * @param airlineContactDetails
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	@ApiOperation(value = "Update Airline Contact as per given Airline Contact ID")
	@RequestMapping(method = RequestMethod.PUT, value = "/contacts/{contactId}", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> updateAirlineContact(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline Contact ID", required = true) @PathVariable("contactId") String contactId,
			@RequestBody AirlineContactDTO airlineContactDetails)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException, ObjectExistsException {
		if (airlineContactDetails == null)
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		if(!EmailValidator.getInstance().isValid(airlineContactDetails.getEmail()))
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_EMAIL_INVALID,
							new Object[] { ECMSConstants.FIELD_EMAIL }, Locale.US), HttpStatus.BAD_REQUEST));
		PhoneNumberUtil util = PhoneNumberUtil.getInstance();
//		As It was printing exception stack trace, I've enclosed as below
		try{
			util.isValidNumber(util.parse(airlineContactDetails.getPhone(), request.getLocale().getCountry()));				
		}catch(NumberParseException e){
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_PHONE_INVALID,
					new Object[] { ECMSConstants.FIELD_PHONE }, Locale.US), HttpStatus.FORBIDDEN));
		}
		contactId = URLDecoder.decode(contactId, ECMSConstants.UTF);
		AirlineContactDTO airlineContactDTO = airlineContactService.updateAirlineContact(contactId,airlineContactDetails);
		if (airlineContactDTO != null) {
			return new ResponseEntity<>(airlineContactDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
							new Object[] { ECMSConstants.AIRLINE_CONTACT }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to delete an Airline Contact for an Airline
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param contactId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	@ApiOperation(value = "Delete Airline Contact as per given Airline Id and Airline Contact Id")
	@RequestMapping(method = RequestMethod.DELETE, value = "/airlines/{airlineId}/contacts/{contactId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> deleteAirlineContact(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable("airlineId") String airlineId,
			@ApiParam(value = "Airline Contact ID", required = true) @PathVariable("contactId") String contactId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException {
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		contactId = URLDecoder.decode(contactId, ECMSConstants.UTF);
		Boolean state = airlineContactService.deleteAirlineContact(airlineId, contactId);
		if (state) {
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { ECMSConstants.AIRLINE_CONTACT }, Locale.US)), HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
							new Object[] { ECMSConstants.AIRLINE_CONTACT }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

}
