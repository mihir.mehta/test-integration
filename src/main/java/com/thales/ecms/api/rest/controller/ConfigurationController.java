package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.psddev.dari.db.ObjectType;
import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.dto.ConfigurationDTO;
import com.thales.ecms.model.dto.ConfigurationRequestDTO;
import com.thales.ecms.model.dto.OrderResponseDTO;
import com.thales.ecms.service.ConfigurationService;
import com.thales.ecms.service.OrderService;
import com.thales.ecms.utils.ECMSConstants;
import com.thales.ecms.views.ContentTypeListExcelView;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("configuration")
public class ConfigurationController {

	@Autowired
	ConfigurationService configurationService;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(RuleController.class);
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param configurationDTO
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 */
	@ApiOperation(value = "Add Configuration for an Airline") 
	@RequestMapping(method = RequestMethod.POST, value = "/addConfiguration/{airlineId}", consumes = ECMSConstants.CONSUME_FORMAT,produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> addConfiguration(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String airlineId,
			@ApiParam(value = "DTO Objects for Configuration", required = true) @RequestBody ConfigurationRequestDTO configurationDTO)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException, NoSuchMessageException, ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		ConfigurationRequestDTO configDTO = configurationService.addConfiguration(airlineId, configurationDTO);
		if (Objects.nonNull(configDTO)) {
			return new ResponseEntity<>(configDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}
	
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param configurationDTO
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 */
	@ApiOperation(value = "Update Configuration for an Airline")
	@RequestMapping(method = RequestMethod.PUT, value = "/updateConfiguration/{airlineId}/{configurationId}", consumes = ECMSConstants.CONSUME_FORMAT,produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> updateConfiguration(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String airlineId,@PathVariable String configurationId,
			@ApiParam(value = "DTO Objects for Configuration", required = true) @RequestBody ConfigurationRequestDTO configurationDTO)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException, NoSuchMessageException, ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		configurationId = URLDecoder.decode(configurationId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		if(StringUtils.isBlank(configurationId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.CONFIGURATION_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		ConfigurationRequestDTO configDTO = configurationService.updateConfiguration(airlineId, configurationId,configurationDTO);
		if (Objects.nonNull(configDTO)) {
			return new ResponseEntity<>(configDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
							new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param configurationDTO
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 */
	@ApiOperation(value = "Delete Configuration for an Airline")
	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteConfiguration/{airlineId}/{configurationId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> deleteConfiguration(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String airlineId,@PathVariable String configurationId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException, NoSuchMessageException, ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		configurationId = URLDecoder.decode(configurationId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		if(StringUtils.isBlank(configurationId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.CONFIGURATION_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		Boolean result = configurationService.deleteconfigurationById(airlineId, configurationId);
		if (result) {
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { ECMSConstants.CONFIGURATION }, Locale.US)), HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
							new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}


	/**
	 * Controller method for getting list of all contentTypeList
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/configurationList/{airlineId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getconfigurationList(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		List<ConfigurationRequestDTO> configurationDTOList = configurationService.getconfigurationList(airlineId);
		if (Objects.nonNull(configurationDTOList)) {
			if(configurationDTOList.isEmpty()){
				throw new EmptyResponseException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
								new Object[] { }, Locale.US), HttpStatus.FORBIDDEN));
			}
			return new ResponseEntity<>(configurationDTOList, HttpStatus.OK);
		} else {
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param configurationId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	@ApiOperation("Method to Get Configuration By configurationId")
	@RequestMapping(method = RequestMethod.GET, value = "/get/{airlineId}/{configurationId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getConfigurationById(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId, @PathVariable String configurationId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		configurationId = URLDecoder.decode(configurationId,"UTF-8").trim();
		if(StringUtils.isBlank(configurationId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.CONFIGURATION_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		ConfigurationRequestDTO configurationRequestDTO = configurationService.getConfigurationByCfgId(airlineId, configurationId);
		if (Objects.nonNull(configurationRequestDTO)) {			
			return new ResponseEntity<>(configurationRequestDTO, HttpStatus.OK);
		} else {
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}
	
	/**
	 * Controller method for deactivating a configuration
	 * @param request
	 * @param response
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/{airlineId}/{configurationId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> deactivateconfigurationById(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId ,@PathVariable String configurationId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException ,BadRequestException{		
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		configurationId = URLDecoder.decode(configurationId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		if(StringUtils.isBlank(configurationId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.CONFIGURATION_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		ConfigurationRequestDTO configurationDTO = configurationService.deactivateconfigurationById(airlineId, configurationId);
		if (Objects.nonNull(configurationDTO)) {
			return new ResponseEntity<>(configurationDTO, HttpStatus.OK);
		} else {
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller method returns XlsxView that populates an excel Workbook
	 * @param request
	 * @param response
	 * @return ContentTypeListExcelView
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * 
	 * @author arjun.p
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/exportContentTypeList/{airlineId}/{configurationId}", produces=ECMSConstants.PRODUCE_FORMAT)
    public ContentTypeListExcelView exportContentTypeList(Model model,HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId ,@PathVariable String configurationId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException ,BadRequestException{				
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		configurationId = URLDecoder.decode(configurationId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		if(StringUtils.isBlank(configurationId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.CONFIGURATION_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		Configuration configuration = configurationService.getConfigurationById(airlineId, configurationId);
		if(Objects.isNull(configuration)) 			
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));
		if(configuration.getContentTypeList()==null || configuration.getContentTypeList().isEmpty())			
			throw new EmptyResponseException(   
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
							new Object[] { }, Locale.US), HttpStatus.FORBIDDEN));		
        model.addAttribute("configuration",configuration);
        return new ContentTypeListExcelView();
    }
	
	/**
	 * Controller method returns XlsxView that populates an excel Workbook for a contentType
	 * @param request
	 * @param response
	 * @return ContentTypeListExcelView
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * 
	 * @author arjun.p
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/exportContentType/{airlineId}/{configurationId}/{contentTypeId}", produces=ECMSConstants.PRODUCE_FORMAT)	
    public ContentTypeListExcelView exportContentType(Model model,HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId ,@PathVariable String configurationId,@PathVariable String contentTypeId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException ,BadRequestException{				
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		configurationId = URLDecoder.decode(configurationId,"UTF-8").trim();
		contentTypeId = URLDecoder.decode(contentTypeId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		if(StringUtils.isBlank(configurationId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.CONFIGURATION_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		if(StringUtils.isBlank(contentTypeId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.CONTENT_TYPE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		Configuration configuration = configurationService.getConfigurationById(airlineId, configurationId);
		if(Objects.isNull(configuration))
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));
		if(configuration.getContentTypeList()==null || configuration.getContentTypeList().isEmpty())
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
							new Object[] {  }, Locale.US), HttpStatus.FORBIDDEN));
		ObjectType objectType = configurationService.getContentTypeById(contentTypeId);
		if(Objects.isNull(objectType))
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
							new Object[] { ECMSConstants.CONTENT_TYPE , ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));
        model.addAttribute("configuration",configuration);
        model.addAttribute("objectType",objectType);
        return new ContentTypeListExcelView();
    }
	
	/**
	 * Controller method returns XlsxView that populates an excel Workbook for a Configuration of an Order
	 * @param request
	 * @param response
	 * @return ContentTypeListExcelView
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * 
	 * @author arjun.p
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/exportOrderConfiguration/{orderId}/{airlineId}/{configurationId}", produces=ECMSConstants.PRODUCE_FORMAT)	
    public ContentTypeListExcelView exportOrderConfiguration(Model model,HttpServletRequest request, HttpServletResponse response, @PathVariable String orderId ,@PathVariable String airlineId ,@PathVariable String configurationId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException ,BadRequestException{			
		orderId = URLDecoder.decode(orderId,"UTF-8").trim();
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		configurationId = URLDecoder.decode(configurationId,"UTF-8").trim();
		if(StringUtils.isBlank(orderId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.ORDER_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		if(StringUtils.isBlank(configurationId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.CONFIGURATION_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		OrderResponseDTO orderDTO = orderService.getOrderById(airlineId,orderId);
		if(Objects.isNull(orderDTO))
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] { ECMSConstants.ORDER }, Locale.US), HttpStatus.FORBIDDEN));
		Configuration configuration = configurationService.getConfigurationById(orderDTO,airlineId, configurationId);
		if(Objects.isNull(configuration))
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));		
        model.addAttribute("configuration",configuration);
        model.addAttribute("DATA_EXPORT",true);
        return new ContentTypeListExcelView();
    }
}
