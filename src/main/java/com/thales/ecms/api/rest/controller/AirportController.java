package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.AirportDTO;
import com.thales.ecms.service.AirportService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Controller to control requests for CRUD operations on an Airport
 */

@Controller
public class AirportController {
	@Autowired
	AirportService airportService;

	@Autowired
	private MessageSource messageSource;

	/**
	 * Controller Method for adding an Airport
	 * 
	 * @param request
	 * @param response
	 * @param airportDetails
	 * @throws EmptyResponseException
	 * @throws ObjectExistsException
	 */

	@ApiOperation(value = "Add Airports as per given DTO")
	@RequestMapping(method = RequestMethod.POST, value = "/airports", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> addAirport(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "DTO Objects for Airport", required = true) @RequestBody AirportDTO airportDetails)
			throws ObjectExistsException, EmptyResponseException, BadRequestException {

		if (null == airportDetails) {
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		} else {
			AirportDTO airportDTO = airportService.addAirport(airportDetails);
			if (null != airportDTO) {
				return new ResponseEntity<>(airportDTO, HttpStatus.OK);
			} else {
				throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
						new Object[] { ECMSConstants.AIRPORT }, Locale.US), HttpStatus.FORBIDDEN));
			}
		}

	}

	/**
	 * Controller Method for return a list of all Airports
	 * 
	 * @param request
	 * @param response
	 * @throws EmptyResponseException
	 */

	@ApiOperation(value = "Retrieve all Airports")
	@RequestMapping(method = RequestMethod.GET, value = "/airports", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> getAirports(HttpServletRequest request, HttpServletResponse response)
			throws EmptyResponseException {

		List<AirportDTO> airportList = airportService.getAirport();
		if (null != airportList) {
			return new ResponseEntity<>(airportList, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US), HttpStatus.NOT_FOUND));
		}

	}

	/**
	 * Controller Method to update an Airport
	 * 
	 * @param request
	 * @param response
	 * @param airportId
	 * @param airportDetails
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 * @throws ObjectExistsException
	 * @throws BadRequestException
	 */

	@ApiOperation(value = "Update Airport as per given AirportID")
	@RequestMapping(method = RequestMethod.PUT, value = "/airports/{airportId}", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> updateAirport(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airport ID", required = true) @PathVariable String airportId,
			@RequestBody AirportDTO airportDetails) throws UnsupportedEncodingException, EmptyResponseException,
			ObjectNotFoundException, BadRequestException, ObjectExistsException {

		if (StringUtils.isNotBlank(airportId)) {
			airportId = URLDecoder.decode(airportId, ECMSConstants.UTF);
			AirportDTO airportDTO = airportService.updateAirport(airportId, airportDetails);
			if (airportDTO != null) {
				return new ResponseEntity<>(airportDTO, HttpStatus.OK);
			} else {
				throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
						new Object[] { ECMSConstants.AIRPORT }, Locale.US), HttpStatus.FORBIDDEN));
			}
		} else {
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}
	}

	/**
	 * Controller Method to delete an Airport
	 * 
	 * @param request
	 * @param response
	 * @param airportId
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 * @throws BadRequestException
	 */

	@ApiOperation(value = "Delete airport as per given AirportID")
	@RequestMapping(method = RequestMethod.DELETE, value = "/airports/{airportId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> deleteAirport(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airportId)
			throws UnsupportedEncodingException, ObjectNotFoundException, BadRequestException, EmptyResponseException {
		if (StringUtils.isNotBlank(airportId)) {
			airportId = URLDecoder.decode(airportId, ECMSConstants.UTF);
			if (airportService.deleteAirport(airportId)) {
				return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
						new Object[] { ECMSConstants.AIRPORT }, Locale.US)), HttpStatus.OK);
			} else {
				throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
						new Object[] { ECMSConstants.AIRPORT }, Locale.US), HttpStatus.FORBIDDEN));
			}
		} else {
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}
	}

	/**
	 * Controller Method to return an Airport by ID
	 * 
	 * @param request
	 * @param response
	 * @param airportId
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	@ApiOperation(value = "Retrieve airport as per given Airport ID")
	@RequestMapping(method = RequestMethod.GET, value = "/airports/{airportId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> getAirport(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airportId)
			throws EmptyResponseException, UnsupportedEncodingException, BadRequestException {
		if (StringUtils.isBlank(airportId)) {
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		} else {
			airportId = URLDecoder.decode(airportId, ECMSConstants.UTF);
			AirportDTO airportDTO = airportService.getAirportById(airportId);
			if (airportDTO != null) {
				return new ResponseEntity<>(airportDTO, HttpStatus.OK);
			} else {
				throw new EmptyResponseException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US), HttpStatus.NOT_FOUND));
			}
		}
	}

}
