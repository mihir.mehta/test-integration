package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.PaxguiLanguageDTO;
import com.thales.ecms.service.PaxguiLanguageService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
public class PaxguiLanguageController {

	@Autowired
	PaxguiLanguageService paxguiLanguageService;

	@Autowired
	private MessageSource messageSource;
	
	public static Logger logger = LoggerFactory.getLogger(PaxguiLanguageController.class);

	/**
	 * Controller method adding a language in Airline
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param paxguiLanguageDTO
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	
	@ApiOperation(value = "Add PaxguiLanguage as per given DTO")
	@RequestMapping(method = RequestMethod.POST, value = "/airlines/{airlineId}/paxguiLanguage", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> addPaxguiLanguage(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId, 
			@ApiParam(value = "DTO Objects for PaxguiLanguage", required = true)
			@RequestBody PaxguiLanguageDTO paxguiLanguageDTO)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException,ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		PaxguiLanguageDTO paxguiDTOs = paxguiLanguageService.addPaxguiLanguage(airlineId, paxguiLanguageDTO);

		if (paxguiDTOs != null) {
			return new ResponseEntity<>(paxguiDTOs, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE, new Object[] {ECMSConstants.PAXGUILANGUAGE},Locale.US), HttpStatus.FORBIDDEN));

		}

	}

	/**
	 * Controller method getting languages in Airline
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	
	@ApiOperation(value = "Retrieve all PaxguiLanguages")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/paxguilanguages", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> getPaxguiLanguages(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		List<PaxguiLanguageDTO> paxguiLanguageDTOs = paxguiLanguageService.getPaxguiLanguages(airlineId);
		if (paxguiLanguageDTOs != null) {
			return new ResponseEntity<>(paxguiLanguageDTOs, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null,Locale.US), HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller method deleting a language in Airline
	 * @param request
	 * @param response
	 * @param id
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	
	@ApiOperation(value = "Delete PaxguiLanguage as per given AirlineID and PaxguiLanguageID")
	@RequestMapping(method = RequestMethod.DELETE, value = "/airlines/{airlineId}/paxguilanguages/{id}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> deletePaxguiLanguage(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String id, @PathVariable("airlineId") String airlineId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		id = URLDecoder.decode(id, ECMSConstants.UTF);
		Boolean responseString = paxguiLanguageService.deletePaxguiLanguage(airlineId, id);
		if (responseString) {
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { ECMSConstants.PAXGUILANGUAGE }, Locale.US )), HttpStatus.OK);
		} else
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE, 
					new Object[] {ECMSConstants.PAXGUILANGUAGE},Locale.US), HttpStatus.FORBIDDEN));
	}

	/**
	 * Controller method updating a language in Airline
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param paxguiLanguageDetails
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	
	@ApiOperation(value = "Update PaxguiLanguage as per given AirlineID and PaxguiLanguageID")
	@RequestMapping(method = RequestMethod.PUT, value = "/airlines/{airlineId}/paxguilanguages/{id}", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT )
	@ResponseBody
	public ResponseEntity<?> updatePaxguiLanguage(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String id, @PathVariable("airlineId") String airlineId,
			@ApiParam(value = "DTO Objects for PaxguiLanguage", required = true)
			@RequestBody PaxguiLanguageDTO paxguiLanguageDetails) throws UnsupportedEncodingException,
			EmptyResponseException, ObjectNotFoundException, BadRequestException, ObjectExistsException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		id = URLDecoder.decode(id, ECMSConstants.UTF);
		PaxguiLanguageDTO paxguiLanguageDTO = paxguiLanguageService.updatePaxguiLanguage(airlineId, id,
				paxguiLanguageDetails);
		if (paxguiLanguageDTO != null)
			return new ResponseEntity<>(paxguiLanguageDTO, HttpStatus.OK);
		else
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE, 
							new Object[] {ECMSConstants.PAXGUILANGUAGE},Locale.US), HttpStatus.FORBIDDEN));

	}
}
