package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.RouteGroupDTO;
import com.thales.ecms.service.RouteGroupService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Controller class for CRUD operations on RouteGroup
 * 
 * @author arjun.p
 *
 */
@Controller
public class RouteGroupController {

	@Autowired
	private RouteGroupService routeGroupService;

	@Autowired
	private MessageSource messageSource;

	/**
	 * Controller Method to add a RouteGroup
	 * 
	 * @param request
	 * @param response
	 * @param routeGroupDetails
	 * @return ResponseEntity containing RouteGroupDTO
	 * @throws ObjectExistsException,
	 *             EmptyResponseException, BadRequestException,
	 *             ObjectNotFoundException
	 */
	@ApiOperation(value = "Add Route Group as per given DTO for an Airline")
	@RequestMapping(method = RequestMethod.POST, value = "/routeGroup/{airlineId}", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> addRouteGroup(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId,
			@ApiParam(value = "DTO Object for Route Group", required = true) @RequestBody RouteGroupDTO routeGroupDetails)
			throws UnsupportedEncodingException, ObjectExistsException, EmptyResponseException, BadRequestException,
			ObjectNotFoundException {
		if (StringUtils.isBlank(airlineId))
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		if (routeGroupDetails == null)
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		RouteGroupDTO routeGroupDTO = routeGroupService.addRouteGroup(airlineId, routeGroupDetails);
		if (routeGroupDTO != null) {
			return new ResponseEntity<>(routeGroupDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] { ECMSConstants.ROUTE_GROUP }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to return all RouteGroup
	 * 
	 * @param request
	 * @param response
	 * @return ResponseEntity containing list of RouteGroupDTO
	 * @throws ObjectNotFoundException
	 */
	@ApiOperation(value = "Retrieve all Route Groups for an Airline")
	@RequestMapping(method = RequestMethod.GET, value = "/routeGroup/{airlineId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getRouteGroups(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException {
		if (StringUtils.isBlank(airlineId))
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		List<RouteGroupDTO> routeGroupList = routeGroupService.getRouteGroups(airlineId);
		if (routeGroupList != null && !routeGroupList.isEmpty()) {
			return new ResponseEntity<>(routeGroupList, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller Method to return a RouteGroup
	 * 
	 * @param request
	 * @param response
	 * @return ResponseEntity containing a RouteGroupDTO
	 * @throws UnsupportedEncodingException
	 */
	@ApiOperation(value = "Retrieve route group as per given Airport ID and Route ID")
	@RequestMapping(method = RequestMethod.GET, value = "/routeGroup/{airlineId}/{routeGroupId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> getRouteGroupById(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId,
			@ApiParam(value = "Route Group ID", required = true) @PathVariable String routeGroupId)
			throws ObjectNotFoundException, BadRequestException, UnsupportedEncodingException {
		if (StringUtils.isBlank(airlineId))
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		if (StringUtils.isBlank(routeGroupId))
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		routeGroupId = URLDecoder.decode(routeGroupId, ECMSConstants.UTF);
		RouteGroupDTO routeGroup = routeGroupService.getRouteGroupById(airlineId, routeGroupId);
		if (routeGroup != null) {
			return new ResponseEntity<>(routeGroup, HttpStatus.OK);
		} else {
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller Method to update an RouteGroup
	 * 
	 * @param request
	 * @param response
	 * @param routeGroupId
	 * @return ResponseEntity containing RouteGroupDTO
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/routeGroup/{airlineId}/{routeGroupId}", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> updateRouteGroup(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId,
			@ApiParam(value = "Route Group ID", required = true) @PathVariable String routeGroupId,
			@RequestBody RouteGroupDTO routeGroupDetails)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException {
		if (StringUtils.isBlank(airlineId))
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US),
					HttpStatus.BAD_REQUEST));
		if (StringUtils.isBlank(routeGroupId))
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		if (routeGroupDetails == null)
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		routeGroupId = URLDecoder.decode(routeGroupId, ECMSConstants.UTF);
		RouteGroupDTO routeGroupDTO = routeGroupService.updateRouteGroup(airlineId, routeGroupId, routeGroupDetails);
		if (routeGroupDTO != null) {
			return new ResponseEntity<>(routeGroupDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
							new Object[] { ECMSConstants.ROUTE_GROUP }, Locale.US), HttpStatus.FORBIDDEN));
		}

	}

	/**
	 * Controller Method to delete an RouteGroup
	 * 
	 * @param request
	 * @param response
	 * @param routeGroupId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws EmptyResponseException
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/routeGroup/{airlineId}/{routeGroupId}", produces = ECMSConstants.PRODUCE_FORMAT)
	public ResponseEntity<?> deleteRouteGroup(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "Airline ID", required = true) @PathVariable String airlineId, @ApiParam(value = "Route Group ID", required = true) @PathVariable String routeGroupId)
			throws UnsupportedEncodingException, ObjectNotFoundException, BadRequestException, EmptyResponseException {
		if (StringUtils.isBlank(airlineId))
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US),
					HttpStatus.BAD_REQUEST));
		if (StringUtils.isBlank(routeGroupId))
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		routeGroupId = URLDecoder.decode(routeGroupId, ECMSConstants.UTF);
		Boolean status = routeGroupService.deleteRouteGroup(airlineId, routeGroupId);
		if (status) {
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { ECMSConstants.ROUTE_GROUP }, Locale.US)), HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
							new Object[] { ECMSConstants.ROUTE_GROUP }, Locale.US), HttpStatus.FORBIDDEN));
		}

	}

	public RouteGroupService getRouteGroupService() {
		return routeGroupService;
	}

	public void setRouteGroupService(RouteGroupService routeGroupService) {
		this.routeGroupService = routeGroupService;
	}

}
