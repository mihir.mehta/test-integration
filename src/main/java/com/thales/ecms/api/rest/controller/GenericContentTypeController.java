package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.GenericContentTypeRequestDTO;
import com.thales.ecms.model.dto.GenericContentTypeResponseDTO;
import com.thales.ecms.model.dto.SpecificContentTypeDTO;
import com.thales.ecms.service.GenericContentTypeService;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir
 * Controller Class to perform operations on Generic Content Types
 */
@Controller
public class GenericContentTypeController {

	public static Logger logger = LoggerFactory.getLogger(GenericContentTypeController.class);
	
	@Autowired
	GenericContentTypeService genericContentTypeService;
	
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * Controller Method to create a Generic Content Type
	 * @param request
	 * @param response
	 * @param genericContentTypeRequestDTO
	 * @return
	 * @throws NoSuchMessageException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 * @throws EmptyResponseException 
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/genericContentTypes", consumes = "application/json", produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> addGenericContentType(HttpServletRequest request, HttpServletResponse response, @RequestBody GenericContentTypeRequestDTO genericContentTypeRequestDTO) throws NoSuchMessageException, BadRequestException, ObjectExistsException, EmptyResponseException{
	
		GenericContentTypeResponseDTO genericContentTypeResponseDTO = genericContentTypeService.addGenericContentType(genericContentTypeRequestDTO);
		if(genericContentTypeResponseDTO != null){
			return new ResponseEntity<>(genericContentTypeResponseDTO, HttpStatus.OK);
		}
		else{
			logger.error("Response returned was empty - cannot create Generic Content Type");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE, new Object[]{ECMSConstants.GENERIC_CONTENT_TYPE}, Locale.US), HttpStatus.FORBIDDEN));
		}
		
	}
	
	/**
	 * Controller Method to return a list of Generic Content Types
	 * @param request
	 * @param response
	 * @return
	 * @throws NoSuchMessageException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/genericContentTypes", produces = "application/json")
	public ResponseEntity<?> getGenericContentTypes(HttpServletRequest request, HttpServletResponse response) throws NoSuchMessageException, EmptyResponseException, ObjectNotFoundException{
		
		List<GenericContentTypeResponseDTO> genericContentTypeDTOList = genericContentTypeService.getGenericContentTypes();
		if(!CollectionUtils.isEmpty(genericContentTypeDTOList)){
			Gson gson = new Gson();
			String responseString = gson.toJson(genericContentTypeDTOList);
			return new ResponseEntity<>(responseString, HttpStatus.OK);
		}
		else{
			logger.error("Empty response - no generic content types present");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller Method to return a Generic Content Type
	 * @param request
	 * @param response
	 * @param contentTypeId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchMessageException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/genericContentTypes/{contentTypeId}", produces = "application/json")
	public ResponseEntity<?> getGenericContentType(HttpServletRequest request, HttpServletResponse response, @PathVariable String contentTypeId) throws UnsupportedEncodingException, NoSuchMessageException, EmptyResponseException, ObjectNotFoundException{
		
		contentTypeId = URLDecoder.decode(contentTypeId, "UTF-8");
		GenericContentTypeResponseDTO genericContentTypeDTO = genericContentTypeService.getGenericContentTypeById(contentTypeId);
		if(genericContentTypeDTO != null){
			return new ResponseEntity<>(genericContentTypeDTO, HttpStatus.OK);
		}
		else{
			logger.error("Response returned was empty");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, new Object[]{ECMSConstants.GENERIC_CONTENT_TYPE}, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller Method to delete a Generic Content Type
	 * @param request
	 * @param response
	 * @param contentTypeId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchMessageException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/genericContentTypes/{contentTypeId}", produces = "application/json")
	public ResponseEntity<?> deleteGenericContentType(HttpServletRequest request, HttpServletResponse response, @PathVariable String contentTypeId) throws UnsupportedEncodingException, NoSuchMessageException, EmptyResponseException, ObjectNotFoundException{
		
		contentTypeId = URLDecoder.decode(contentTypeId, "UTF-8");
		if(genericContentTypeService.deleteGenericContentType(contentTypeId)){
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY, new Object[]{ECMSConstants.GENERIC_CONTENT_TYPE}, Locale.US)),
					HttpStatus.OK);
		}
		else{
			logger.error("Cannot delete Generic Content Type");
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE, new Object[]{ECMSConstants.GENERIC_CONTENT_TYPE}, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/airlines/{airlineId}/specificContentTypes", consumes = "application/json")
	public ResponseEntity<?> addSpecificContentTypes(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId, @RequestBody SpecificContentTypeDTO specificContentTypeDTO) throws NoSuchMessageException, ObjectExistsException, ObjectNotFoundException, BadRequestException, UnsupportedEncodingException{
		
		airlineId = URLDecoder.decode(airlineId, "UTF-8");
		String responseString = genericContentTypeService.addSpecificContentTypes(airlineId, specificContentTypeDTO);
		return new ResponseEntity<>(responseString, HttpStatus.OK);
	}
	
//	@RequestMapping(method = RequestMethod.POST, value = "/contentTypeInstances/{contentTypeId}", consumes = "application/json")
//	public ResponseEntity<?> createContentTypeInstances(HttpServletRequest request, HttpServletResponse response, @PathVariable String contentTypeId) throws UnsupportedEncodingException{
//		
//		contentTypeId = URLDecoder.decode(contentTypeId, "UTF-8");
//		String responseString = genericContentTypeService.createContentTypeInstances(contentTypeId);
//		return new ResponseEntity<>(responseString, HttpStatus.OK);
//	}
}
