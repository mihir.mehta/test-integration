/*package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectNotFoundException;
//import com.thales.ecms.model.GenericResponseDTO;
import com.thales.ecms.model.Location;
import com.thales.ecms.model.dto.AirlineContactDTO;
import com.thales.ecms.model.dto.LocationDTO;
import com.thales.ecms.service.AirlineContactService;
import com.thales.ecms.service.LocationService;

//Commenting this Controller , if required would modify and expose Apis for Location
@Controller
public class LocationController {

	@Autowired
	LocationService locationService;

	public static Logger logger = LoggerFactory.getLogger(LocationController.class);

	*//**
	 * Controller method for adding location
	 * 
	 * @param request
	 * @param response
	 * @param locationTypeId
	 * @param airlineId
	 * @param airlineLocationDetails
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 *//*
	@RequestMapping(method = RequestMethod.POST, value = "/airlines/{airlineId}/location", consumes = "application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<?> addLocation(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId,
			@RequestBody LocationDTO airlineLocationDetails)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException {

		LocationDTO genericResponseDTO = locationService.addLocation(airlineId, airlineLocationDetails);
		if (genericResponseDTO != null) {
			return new ResponseEntity<>(genericResponseDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - Cannot create Location", HttpStatus.OK));
		}
	}

	*//**
	 * Controller method for getting list of locations
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 *//*
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/locationList", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<?> getlocationList(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("airlineId") String airlineId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {
		
		airlineId = URLDecoder.decode(airlineId, "UTF-8");
		List<LocationDTO> locationDTO = locationService.getlocationList(airlineId);
		if (locationDTO != null && !locationDTO.isEmpty()) {
			return new ResponseEntity<>(locationDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - no Locations exist", HttpStatus.OK));
		}
	}

	*//**
	 * Controller method for deleting a location.
	 * @param request
	 * @param response
	 * @param locationId
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 *//*
	@RequestMapping(method = RequestMethod.DELETE, value = "/airlines/{airlineId}/deletelocation/{locationId}")
	@ResponseBody
	public ResponseEntity<?> deleteLocationById(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String locationId, @PathVariable("airlineId") String airlineId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {
		airlineId = URLDecoder.decode(airlineId, "UTF-8");
		locationId = URLDecoder.decode(locationId, "UTF-8");
		String responseString = locationService.deleteLocationById(airlineId, locationId);
		if (responseString != null) {
			responseString = ObjectUtils.toJson(responseString);
			return new ResponseEntity<>(responseString, HttpStatus.OK);
		} else
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - Cannot delete Location", HttpStatus.OK));
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/airlines/{airlineId}/updateLocation/{locationId}")
	@ResponseBody
	public ResponseEntity<?> updateLocationById(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String locationId, @PathVariable("airlineId") String airlineId, @RequestBody LocationDTO airlineDetails)
					throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException {
		
		airlineId = URLDecoder.decode(airlineId, "UTF-8");
		locationId = URLDecoder.decode(locationId, "UTF-8");
		LocationDTO locationDTO = locationService.updateLocationById(airlineId, locationId, airlineDetails);
		if (locationDTO != null) {
			return new ResponseEntity<>(locationDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse("Response returned was empty - Cannot update Location", HttpStatus.OK));
		}
	}

}
*/