package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.IngestionDetailDTO;
import com.thales.ecms.service.IngestionDetailService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author Mihir Controller Method for Ingestion Details
 */
@Controller
public class IngestionDetailController {

	@Autowired
	IngestionDetailService ingestionDetailService;

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(IngestionDetailController.class);

	/**
	 * Controller Method to get a list of Ingestion jobs
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws NoSuchMessageException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/ingestionDetails", produces = "application/json")
	public ResponseEntity<?> getIngestionDetails(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId, @ApiParam(allowableValues = "Uploaded, Imported") @RequestParam(required = true) String mode)
			throws ObjectNotFoundException, UnsupportedEncodingException, NoSuchMessageException,
			EmptyResponseException {

		airlineId = URLDecoder.decode(airlineId, "UTF-8");
		List<IngestionDetailDTO> ingestionDetailDTOList = ingestionDetailService.getIngestionDetails(airlineId, mode);
		if (!CollectionUtils.isEmpty(ingestionDetailDTOList)) {
			return new ResponseEntity<>(ingestionDetailDTOList, HttpStatus.OK);
		} else {
			logger.error("Response returned was empty");
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to get details about an Ingestion Job
	 * 
	 * @param request
	 * @param response
	 * @param ingestionDetailId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws NoSuchMessageException
	 * @throws EmptyResponseException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/ingestionDetails/{ingestionDetailId}", produces = "application/json")
	public ResponseEntity<?> getIngestionDetailsById(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String ingestionDetailId) throws UnsupportedEncodingException, ObjectNotFoundException,
			NoSuchMessageException, EmptyResponseException {

		ingestionDetailId = URLDecoder.decode(ingestionDetailId, "UTF-8");
		IngestionDetailDTO ingestionDetailDTO = ingestionDetailService.getIngestionDetailsById(ingestionDetailId);
		if (ingestionDetailDTO != null) {
			return new ResponseEntity<>(ingestionDetailDTO, HttpStatus.OK);
		} else {
			logger.error("Response returned was empty");
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method for adding an IngestionDetail
	 * 
	 * @param request
	 * @param response
	 * @param ingestionDetails
	 * @throws EmptyResponseException
	 * @throws ObjectExistsException
	 * @throws ObjectNotFoundException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchMessageException
	 */
	@ApiOperation(value = "Add ingestionDetails as per given DTO")
	@RequestMapping(method = RequestMethod.POST, value = "/airlines/{airlineId}/ingestionDetails", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> addIngestionDetailsByAirlineId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "AIRLINE ID", required = true) @PathVariable String airlineId,
			@RequestBody IngestionDetailDTO ingestionDetailDTO) throws ObjectExistsException, EmptyResponseException,
			BadRequestException, NoSuchMessageException, UnsupportedEncodingException, ObjectNotFoundException {

		if (StringUtils.isNotBlank(airlineId)) {
			IngestionDetailDTO ingestionDetail = ingestionDetailService.saveIngestionDetailsByAirlineId(airlineId,
					ingestionDetailDTO);
			if (null != ingestionDetail) {
				return new ResponseEntity<>(ingestionDetail, HttpStatus.OK);
			} else {
				throw new EmptyResponseException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
								new Object[] { ECMSConstants.AIRPORT }, Locale.US), HttpStatus.FORBIDDEN));
			}

		} else {
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}

	}

	/**
	 * Controller Method to update an ingestionDetails
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param ingestionDetails
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 * @throws ObjectExistsException
	 * @throws BadRequestException
	 */
	@ApiOperation(value = "Update Airport as per given AirportID")
	@RequestMapping(method = RequestMethod.PUT, value = "/airlines/{airlineId}/{ingestionDetailId}/ingestionDetails", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> updateIngestionDetailsByAirlineId(HttpServletRequest request, HttpServletResponse response,
			@ApiParam(value = "AIRLINE ID", required = true) @PathVariable String airlineId,
			@PathVariable String ingestionDetailId, @RequestBody IngestionDetailDTO ingestionDetailDTO)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException,
			ObjectExistsException {

		if (StringUtils.isNotBlank(airlineId)) {
			airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
			if (ingestionDetailDTO.getMode().equals(ECMSConstants.IMPORT)) {
				IngestionDetailDTO ingestionDetail = ingestionDetailService.updateIngestionDetailsByAirlineId(airlineId,
						ingestionDetailId, ingestionDetailDTO);
				if (ingestionDetail != null) {
					return new ResponseEntity<>(ingestionDetail, HttpStatus.OK);
				} else {
					throw new EmptyResponseException(
							new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
									new Object[] { ECMSConstants.AIRPORT }, Locale.US), HttpStatus.FORBIDDEN));
				}
			} else {
				throw new BadRequestException(new ErrorResponse("Mode should be Imported ", HttpStatus.BAD_REQUEST));
			}
		} else {
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}
	}

}
