package com.thales.ecms.api.rest.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thales.ecms.utils.ECMSConstants;
import com.thales.ecms.views.ResourceExcelView;

import io.swagger.annotations.ApiOperation;

/**
 * Controller to export all the resources/model objects used in ECMS
 * 
 * @author arjun.p
 *
 */
@Controller
@RequestMapping("export")
public class ResourceExportController {	
	
	@ApiOperation(value = "Export All Resources or Model Objects")
	@RequestMapping(method = RequestMethod.GET, value = "/resources", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResourceExcelView exportAllResource(HttpServletRequest request, HttpServletResponse response, Model model) {		
		return new ResourceExcelView();
	}
	
}
