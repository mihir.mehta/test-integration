package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.LruDTO;
import com.thales.ecms.service.LruService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author Mihir
 * Controller Class for CRUD Operations on LRUs
 */
@Controller
public class LruController {

	@Autowired
	LruService lruService;
	
	@Autowired
	private MessageSource messageSource;
	
	public static Logger logger = LoggerFactory.getLogger(LruController.class);
	
	/**
	 * Controller Method to add an LRU
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param lruDetails
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	
	@ApiOperation(value = "Add lru as per given DTO")
	@RequestMapping(method = RequestMethod.POST, value = "/airlines/{airlineId}/lrus", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> addLRU(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId, @ApiParam(value = "DTO Object for lru", required = true) @RequestBody LruDTO lruDetails) 
					throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException, ObjectExistsException{
		
		airlineId = URLDecoder.decode(airlineId,ECMSConstants.UTF);
		
		LruDTO lruDTO = lruService.addLRU(airlineId, lruDetails);
		if(lruDTO != null){
			return new ResponseEntity<>(lruDTO, HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
					new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller Method to return a list of all LRUs
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */
	
	@ApiOperation(value = "Retrieve all lrus as per given airlineID")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/lrus", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> getLRUs(HttpServletRequest request, HttpServletResponse response, @PathVariable String airlineId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException{
		
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		List<LruDTO> lruDTOList = lruService.getLRUs(airlineId);
		if(lruDTOList != null && !lruDTOList.isEmpty()){
			return new ResponseEntity<>(lruDTOList, HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
					new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}
	
	/**
	 * Controller Method to return an LRU
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param lruId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	
	@ApiOperation(value = "Retrieve  lru as per given airlineID and lruID")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/lrus/{lruId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> getLRU(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable("airlineId") String airlineId, 
			@PathVariable("lruId") String lruId) 
					throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException{
		
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		lruId = URLDecoder.decode(lruId, ECMSConstants.UTF);
		LruDTO lruDTO = lruService.getLRU(airlineId, lruId);
		if(lruDTO != null){
			return new ResponseEntity<>(lruDTO, HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
					new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}
	
	
	/**
	 * Controller Method to return an LRU by LRU Type Id ID
	 * @param request
	 * @param response
	 * @param lruTypeID
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException getLRUByLRUTypeId
	 */
	
	@ApiOperation(value = "Retrieve  lru as per given lruTypeID")
	@RequestMapping(method = RequestMethod.GET, value = "/lrus/lruTypes/{lruTypeId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> getLRUbyLRUTypeID(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable("lruTypeId") String lruTypeId) 
					throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException{
		
		lruTypeId = URLDecoder.decode(lruTypeId.trim(), ECMSConstants.UTF);
		LruDTO lruDTO = lruService.getLRUByLRUTypeId(lruTypeId);
		if(lruDTO != null){
 			return new ResponseEntity<>(lruDTO, HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
					new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}

	
	
	
	/**
	 * Controller Method to update an LRU
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param lruId
	 * @param lruDetails
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws EmptyResponseException 
	 * @throws ObjectNotFoundException 
	 * @throws BadRequestException 
	 * @throws ObjectExistsException 
	 */
	
	@ApiOperation(value = "Update lru as per given airlineID and lruID")
	@RequestMapping(method = RequestMethod.PUT, value = "/airlines/{airlineId}/lrus/{lruId}", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> updateLRU(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable("airlineId") String airlineId, 
			@PathVariable("lruId") String lruId, 
			@ApiParam(value = "DTO Object for lru", required = true) @RequestBody LruDTO lruDetails) 
					throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, BadRequestException, ObjectExistsException{
		
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		lruId = URLDecoder.decode(lruId, ECMSConstants.UTF);
		LruDTO lruDTO = lruService.updateLRU(airlineId, lruId, lruDetails);
		if(lruDTO != null){
			return new ResponseEntity<>(lruDTO, HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
					new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * Controller Method to delete an LRU
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param lruId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException 
	 * @throws EmptyResponseException 
	 */
	@ApiOperation(value = "Delete lru as per given airlineID and lruID")
	@RequestMapping(method = RequestMethod.DELETE, value = "/airlines/{airlineId}/lrus/{lruId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> deleteLRU(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable("airlineId") String airlineId, 
			@PathVariable("lruId") String lruId) 
					throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException{
		
		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		lruId = URLDecoder.decode(lruId, ECMSConstants.UTF);
		Boolean responseString = lruService.deleteLRU(airlineId, lruId);
		if(responseString){
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { ECMSConstants.LRU}, Locale.US )), HttpStatus.OK);
		}
		else{
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
					new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
}
