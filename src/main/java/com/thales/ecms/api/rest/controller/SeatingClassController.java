package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectAssociationExistsException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.SeatingClassDTO;
import com.thales.ecms.service.SeatingClassService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author Mihir Controller Class to perform CRUD operations on Class
 */
@Controller
public class SeatingClassController {

	@Autowired
	SeatingClassService seatingClassService;

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(SeatingClassController.class);

	/**
	 * Controller Method to add a Seating Class
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param classDetails
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws ObjectExistsException
	 * @throws BadRequestException
	 */

	@ApiOperation(value = "Add Seating class as per given DTO")
	@RequestMapping(method = RequestMethod.POST, value = "/airlines/{airlineId}/classes", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> addClass(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId,
			@ApiParam(value = "DTO Objects for Seating class", required = true) @RequestBody SeatingClassDTO classDetails)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, ObjectExistsException,
			BadRequestException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		SeatingClassDTO seatingClassDTO = seatingClassService.addClass(airlineId, classDetails);
		if (seatingClassDTO != null) {
			return new ResponseEntity<>(seatingClassDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] { ECMSConstants.SEATING_CLASS }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to return a list of all Seating Classes
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */

	@ApiOperation(value = "Retrieve seating class as per given airlineID")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/classes", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> getClasses(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		List<SeatingClassDTO> seatingClassDTOList = seatingClassService.getClasses(airlineId);
		if (seatingClassDTOList != null) {
			return new ResponseEntity<>(seatingClassDTOList, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller Method to return a Seating Class
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param seatingClassId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 */

	@ApiOperation(value = "Retrieve seating class as per given airlineID and seatingClassID ")
	@RequestMapping(method = RequestMethod.GET, value = "/airlines/{airlineId}/classes/{seatingClassId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> getClass(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("airlineId") String airlineId, @PathVariable("seatingClassId") String seatingClassId)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException {

		airlineId = URLDecoder.decode(airlineId, ECMSConstants.UTF);
		seatingClassId = URLDecoder.decode(seatingClassId, ECMSConstants.UTF);
		SeatingClassDTO seatingClassDTO = seatingClassService.getClassById(airlineId, seatingClassId);
		if (seatingClassDTO != null) {
			return new ResponseEntity<>(seatingClassDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ, null, Locale.US),
							HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Controller Method to update a Class
	 * 
	 * @param request
	 * @param response
	 * @param seatingClassId
	 * @param classDetails
	 * @return
	 * @throws EmptyResponseException
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 */

	@ApiOperation(value = "Update seating class as per given seatingClassID")
	@RequestMapping(method = RequestMethod.PUT, value = "/classes/{seatingClassId}", consumes = ECMSConstants.CONSUME_FORMAT, produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> updateClass(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("seatingClassId") String seatingClassId,
			@ApiParam(value = "DTO Objects for Seating class", required = true) @RequestBody SeatingClassDTO classDetails)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException, ObjectExistsException,
			BadRequestException {

		seatingClassId = URLDecoder.decode(seatingClassId, ECMSConstants.UTF);
		SeatingClassDTO seatingClassDTO = seatingClassService.updateClass(seatingClassId, classDetails);
		if (seatingClassDTO != null) {
			return new ResponseEntity<>(seatingClassDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
							new Object[] { ECMSConstants.SEATING_CLASS }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}

	/**
	 * Controller Method to delete a Class
	 * 
	 * @param request
	 * @param response
	 * @param classId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */

	@ApiOperation(value = "Delete seating class as per given seatingClassID")
	@RequestMapping(method = RequestMethod.DELETE, value = "/classes/{seatingClassId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> deleteClass(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("seatingClassId") String seatingClassId)
			throws UnsupportedEncodingException, ObjectNotFoundException, EmptyResponseException, ObjectExistsException,
			ObjectAssociationExistsException {

		
		seatingClassId = URLDecoder.decode(seatingClassId, ECMSConstants.UTF);
		Boolean responseString = seatingClassService.deleteClass(seatingClassId);
		if (responseString) {
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { ECMSConstants.SEATING_CLASS }, Locale.US)), HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
							new Object[] { ECMSConstants.SEATING_CLASS }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
}
