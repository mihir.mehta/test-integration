package com.thales.ecms.api.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.psddev.dari.util.ObjectUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.BundleForRouteGroupDTO;
import com.thales.ecms.service.BundleForRouteGroupService;
import com.thales.ecms.utils.ECMSConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Controller for BundleForRouteGroup
 * 
 * @author arjun.p
 *
 */
@Controller
@RequestMapping("bundleForRouteGroup")
public class BundleForRouteGroupController {

	@Autowired
	private BundleForRouteGroupService bundleForRouteGroupService;
	
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param bundleForRouteGroupDetails
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 */
	@ApiOperation(value = "Add BundleForRouteGroup for an Airline") 
	@RequestMapping(method = RequestMethod.POST, value = "/add/{airlineId}", consumes = ECMSConstants.CONSUME_FORMAT,produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> addBundleForRouteGroup(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String airlineId,
			@ApiParam(value = "DTO Object for BundleForRouteGroup", required = true) @RequestBody BundleForRouteGroupDTO bundleForRouteGroupDetails)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException, NoSuchMessageException, ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		BundleForRouteGroupDTO bundleForRouteGroupDTO = bundleForRouteGroupService.addBundleForRouteGroup(airlineId, bundleForRouteGroupDetails);
		if (Objects.nonNull(bundleForRouteGroupDTO)) {
			return new ResponseEntity<>(bundleForRouteGroupDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_CREATE,
							new Object[] { ECMSConstants.BUNDLE_FOR_ROUTE_GROUP }, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @return
	 * @throws EmptyResponseException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchMessageException
	 * @throws BadRequestException
	 * @throws ObjectNotFoundException
	 */
	@ApiOperation(value = "Get All BundleForRouteGroup as per given airlineId")
	@RequestMapping(method = RequestMethod.GET, value = "/getAll/{airlineId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> getAllBundleForRouteGroup(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId) throws EmptyResponseException, UnsupportedEncodingException, NoSuchMessageException, BadRequestException, ObjectNotFoundException{
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		List<BundleForRouteGroupDTO> list = bundleForRouteGroupService.getAll(airlineId);
		if(Objects.nonNull(list)){
			return new ResponseEntity<>(list, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_READ,
					new Object[] { ECMSConstants.BUNDLE_FOR_ROUTE_GROUP }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param bundleForRouteGroupId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchMessageException
	 * @throws BadRequestException
	 * @throws ObjectNotFoundException
	 */
	@ApiOperation(value = "Get BundleForRouteGroup as per given airlineId and bundleForRouteGroupId")
	@RequestMapping(method = RequestMethod.GET, value = "/get/{airlineId}/{bundleForRouteGroupId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> getBundleForRouteGroup(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId, @PathVariable String bundleForRouteGroupId) throws UnsupportedEncodingException, NoSuchMessageException, BadRequestException, ObjectNotFoundException{
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		bundleForRouteGroupId = URLDecoder.decode(bundleForRouteGroupId,"UTF-8").trim();
		if(StringUtils.isBlank(bundleForRouteGroupId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.BUNDLE_FOR_ROUTE_GROUP_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		BundleForRouteGroupDTO bundleForRouteGroupDTO= bundleForRouteGroupService.getBundleForRouteGroupById(airlineId, bundleForRouteGroupId);
		if(Objects.nonNull(bundleForRouteGroupDTO)){
			return new ResponseEntity<>(bundleForRouteGroupDTO, HttpStatus.OK);
		} else {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.BUNDLE_FOR_ROUTE_GROUP }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param bundleForRouteGroupId
	 * @param bundleForRouteGroupDetails
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws EmptyResponseException
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 */
	@ApiOperation(value = "Update BundleForRouteGroup for an Airline") 
	@RequestMapping(method = RequestMethod.PUT, value = "/update/{airlineId}/{bundleForRouteGroupId}", consumes = ECMSConstants.CONSUME_FORMAT,produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> updateBundleForRouteGroup(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String airlineId,@PathVariable String bundleForRouteGroupId,
			@ApiParam(value = "DTO Object for BundleForRouteGroup", required = true) @RequestBody BundleForRouteGroupDTO bundleForRouteGroupDetails)
			throws UnsupportedEncodingException, EmptyResponseException, ObjectNotFoundException,BadRequestException, NoSuchMessageException, ObjectExistsException {
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		bundleForRouteGroupId = URLDecoder.decode(bundleForRouteGroupId,"UTF-8").trim();
		if(StringUtils.isBlank(bundleForRouteGroupId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.BUNDLE_FOR_ROUTE_GROUP_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		BundleForRouteGroupDTO bundleForRouteGroupDTO = bundleForRouteGroupService.updateBundleForRouteGroup(airlineId, bundleForRouteGroupId,bundleForRouteGroupDetails);
		if (Objects.nonNull(bundleForRouteGroupDTO)) {
			return new ResponseEntity<>(bundleForRouteGroupDTO, HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_UPDATE,
							new Object[] { ECMSConstants.BUNDLE_FOR_ROUTE_GROUP }, Locale.US), HttpStatus.FORBIDDEN));			
		}
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param airlineId
	 * @param bundleForRouteGroupId
	 * @return
	 * @throws EmptyResponseException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchMessageException
	 * @throws BadRequestException
	 * @throws ObjectNotFoundException
	 */
	@ApiOperation(value = "Delete BundleForRouteGroup as per given airlineId and bundleForRouteGroupId")
	@RequestMapping(method = RequestMethod.DELETE, value = "/delete/{airlineId}/{bundleForRouteGroupId}", produces = ECMSConstants.PRODUCE_FORMAT)
	@ResponseBody
	public ResponseEntity<?> deleteBundleForRouteGroup(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String airlineId, @PathVariable String bundleForRouteGroupId) throws EmptyResponseException,UnsupportedEncodingException, NoSuchMessageException, BadRequestException, ObjectNotFoundException{
		airlineId = URLDecoder.decode(airlineId,"UTF-8").trim();
		if(StringUtils.isBlank(airlineId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.AIRLINE_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		bundleForRouteGroupId = URLDecoder.decode(bundleForRouteGroupId,"UTF-8").trim();
		if(StringUtils.isBlank(bundleForRouteGroupId))
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
							new Object[] { ECMSConstants.BUNDLE_FOR_ROUTE_GROUP_ID }, Locale.US), HttpStatus.BAD_REQUEST));
		Boolean result = bundleForRouteGroupService.deleteBundleForRouteGroup(airlineId, bundleForRouteGroupId);
		if (result) {
			return new ResponseEntity<>(ObjectUtils.toJson(messageSource.getMessage(ECMSConstants.DELETED_SUCCESSFULLY,
					new Object[] { ECMSConstants.BUNDLE_FOR_ROUTE_GROUP }, Locale.US)), HttpStatus.OK);
		} else {
			throw new EmptyResponseException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.EMPTY_RESPONSE_DELETE,
							new Object[] { ECMSConstants.BUNDLE_FOR_ROUTE_GROUP }, Locale.US), HttpStatus.FORBIDDEN));
		}
	}
	
	
}
