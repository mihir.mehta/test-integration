/**
 * 
 */
package com.thales.ecms.conversions;

import java.util.Objects;

import com.thales.ecms.model.Application;
import com.thales.ecms.model.AudioCollection;
import com.thales.ecms.model.BroadcastAudio;
import com.thales.ecms.model.ContentType;
import com.thales.ecms.model.Document;
import com.thales.ecms.model.FeatureVideo;
import com.thales.ecms.model.PRAM;
import com.thales.ecms.model.ShortSubjectVideo;

/**
 * @author kumarrajeev.r
 *
 */
public class ContentTypeFactory {

	/**
	 * This method type casts super class(ContentType) into the object of child
	 * class(broadcastAudio,Application etc.) and returns
	 * 
	 * @param contentType
	 * @return
	 */
	public static ContentType getContentType(ContentType contentType) {

		ContentType targetContentType = null;

		if (!Objects.isNull(contentType) && contentType instanceof ContentType) {

			Class<? extends ContentType> clazz = contentType.getClass();
			String className = convertClassNameWithUnderScore(clazz.getName().toString());
			ContentType.ContentTypeReferences contentTypeEnum = null;
			contentTypeEnum = ContentType.ContentTypeReferences.valueOf(className);

			switch (contentTypeEnum) {

			case Application: {
				Application application = (Application) contentType;
				targetContentType = application;
			}
				break;
			case Audio_Collection: {
				AudioCollection audioCollection = (AudioCollection) contentType;
				targetContentType = audioCollection;
			}
				break;
			case Broadcast_Audio: {
				BroadcastAudio broadcastAudio = (BroadcastAudio) contentType;
				targetContentType = broadcastAudio;
			}
				break;
			case Document: {
				Document audioCollection = (Document) contentType;
				targetContentType = audioCollection;
			}
				break;
			case Feature_Video: {
				FeatureVideo audioCollection = (FeatureVideo) contentType;
				targetContentType = audioCollection;
			}
				break;
			case PRAM: {
				PRAM audioCollection = (PRAM) contentType;
				targetContentType = audioCollection;
			}
				break;
			case Short_Subject_Video: {
				ShortSubjectVideo audioCollection = (ShortSubjectVideo) contentType;
				targetContentType = audioCollection;
			}
				break;
			}// end of switch
		} // end of if

		return targetContentType;
	}

	/**
	 * This method insert underscore before every capital letter of class name
	 * except first capital letter of class.
	 * 
	 * @param className
	 * @return
	 */
	public static String convertClassNameWithUnderScore(String className) {
		
		String tempClassName = null;
		if (!Objects.isNull(className)) {
			int index = className.lastIndexOf(".");
			tempClassName = className.substring(index + 1, className.length());
			tempClassName = tempClassName.replaceAll("(.)([A-Z])", "$1_$2").intern();
		}
		return tempClassName;
	}
}
