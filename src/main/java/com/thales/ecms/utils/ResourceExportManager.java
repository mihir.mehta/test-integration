package com.thales.ecms.utils;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;

import com.psddev.dari.db.ObjectType;
import com.thales.ecms.utils.ExcelCellStyleUtil.SECTION;

/**
 * A Simple Manager class used while exporting any Resource used in ECMS
 * 
 * @author arjun.p
 *
 */
public class ResourceExportManager {
	
	private Workbook workbook;
	private Map<SECTION,CellStyle> cellStyles;
		
	private List<Class> resources;
	
	private ResourceExportManager(Workbook workbook, Map<SECTION, CellStyle> cellStyles) {		
		this.workbook = workbook;
		this.cellStyles = cellStyles;
	}		
	
	public static ResourceExportManager newInstance(Workbook workbook,Map<SECTION, CellStyle> cellStyles){				
		return new ResourceExportManager(workbook, cellStyles);
	}
	
	public TemplateBuilder getResourceTemplateBuilder(){
		TemplateBuilder templateBuilder = null;		
		templateBuilder = new ResourceTemplateBuilderImpl(workbook, resources).cellStyles(cellStyles);
		return templateBuilder;
	}		
	
	public Workbook getWorkbook() {
		return workbook;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}

	public Map<SECTION, CellStyle> getCellStyles() {
		return cellStyles;
	}

	public void setCellStyles(Map<SECTION, CellStyle> cellStyles) {
		this.cellStyles = cellStyles;
	}

	public List<Class> getResources() {
		return resources;
	}

	public void setResources(List<Class> resources) {
		this.resources = resources;
	}



	/**
	 * This interface must be implemented by a class serving as a
	 * template builder for any resource while exporting a resource
	 * 
	 * @author arjun.p
	 * 
	 */
	public static interface TemplateBuilder {
		TemplateBuilder buildTemplate();
		TemplateBuilder buildSheet(ObjectType objectType);		
		TemplateBuilder cellStyles(Map<SECTION,CellStyle> cellStyles);				
	}

}
