package com.thales.ecms.utils;

public class ImportStatus {

	private int rowNumber;
	private boolean status;
	private String reason;
	private String userFriendlyMessage;

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public String getUserFriendlyMessage() {
		return userFriendlyMessage;
	}

	public void setUserFriendlyMessage(String userFriendlyMessage) {
		this.userFriendlyMessage = userFriendlyMessage;
	}

	@Override
	public String toString() {
		return "[ImportStatus] : Row Number " + this.getRowNumber() + ", status: " + this.isStatus() + ", reason: "
				+ this.getReason() + ", UserFriendly Message: " + this.getUserFriendlyMessage();

	}

}