package com.thales.ecms.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.psddev.dari.db.ObjectType;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.Configuration.ConfigurationTitle;
import com.thales.ecms.model.ContentType;
import com.thales.ecms.model.PaxguiLanguage;
import com.thales.ecms.model.ValidatorAttribute;
import com.thales.ecms.utils.ExcelCellStyleUtil.SECTION;
import com.thales.ecms.utils.ExcelTemplateManager.TemplateDataIntegrater;

/**
 * This class has responsibility to fill an excel sheet with data
 * It is the pure implementation of ExcelTemplateManager.TemplateDataIntegrater, uses
 * very convenient methods to fill excel sheet
 * 
 * @author arjun.p
 *
 */
public class TemplateDataIntegraterImpl implements ExcelTemplateManager.TemplateDataIntegrater {
	
	private Logger logger = Logger.getLogger(TemplateDataIntegraterImpl.class.getSimpleName());
	
	private final int ROW_COUNTER_INITIAL_INDEX = ExcelTemplateManager.DATA_ROW_COUNTER_INITIAL_INDEX;
	private final int COLUMN_COUNTER_INITIAL_INDEX= ExcelTemplateManager.DATA_COLUMN_COUNTER_INITIAL_INDEX;

	private Configuration configuration;
	
	private Workbook workbook;
	
	private Map<String, List<ConfigurationTitle>> mapOfConfigurationTitles;
	
	private int rowCounter,columnCounter;
	
	private List<LanguageSpecificCell> languageSpecificCells;
	
	private int selectedDataRow;
	private int dataRowStep;
	
	private Map<String, Map<String,ValidatorAttribute>> rules;
	
	private Map<SECTION,CellStyle> cellStyles;
	
	private Map<String, Integer> mapLanguageIndex;
	
	private Map<Integer, List<LanguageSpecificCell>>mapOfDataRowForLanguageSpecificCells;

	public TemplateDataIntegraterImpl(Configuration configuration, Workbook workbook) {
		this.configuration = configuration;
		this.workbook = workbook;
		init();
	}
	
	private void init(){
		mapOfConfigurationTitles=new HashMap<>();
		resetCounter();
		languageSpecificCells = new ArrayList<>();		
		prepareMapOfTitles();		
		dataRowStep = (configuration.getInterfaceLanguages()!=null ? configuration.getInterfaceLanguages().size() : 0);		
		prepareLanguageIndexing();
		mapOfDataRowForLanguageSpecificCells = new HashMap<>();
	}
	
	@Override	
	public TemplateDataIntegrater fillWorkbook() throws IllegalArgumentException, IllegalAccessException{
		for(ObjectType objectType : configuration.getContentTypeList()){
			resetCounter();
//			every time new
			languageSpecificCells = new ArrayList<>();
			mapOfDataRowForLanguageSpecificCells = new HashMap<>();
			
			fillSheet(objectType, workbook.getSheet(objectType.getDisplayName()));
		}
		return this;
	}
	
	@Override
	public TemplateDataIntegrater fillSheet(ObjectType objectType,Sheet sheet) throws IllegalArgumentException, IllegalAccessException{							
		List<ConfigurationTitle> configurationTitles = mapOfConfigurationTitles.get(objectType.getDisplayName());
		if(configurationTitles!=null && !configurationTitles.isEmpty()){
			for(ConfigurationTitle configurationTitle : configurationTitles){
//				It is the data row for one ConfigurationTitle
				Row row = null;
				if((row=sheet.getRow(selectedDataRow))==null)
					row=sheet.createRow(selectedDataRow);
				if(configurationTitle.getContent()!=null){
					ContentType contentType = configurationTitle.getContent();						
					for(Field field : contentType.getClass().getDeclaredFields()){
						ValidatorAttribute validatorAttribute = null;
						Map<String,ValidatorAttribute> map = rules.get(objectType.getInternalName());
						if(map!=null)
							validatorAttribute = map.get(field.getName());				
//						applying rules
						/*if(validatorAttribute!=null){
							if(validatorAttribute.isRequired())
								continue;
						}*/	
						field.setAccessible(true);
						if (field.get(contentType) instanceof Collection) {
							for (Object genericType : ((Collection<Object>)field.get(contentType))) {
//								Since the object may be of any type, we are analyzing its field
//								if any field type = PaxguiLanguage
//								we have to display the content from toString() method for this cell to respective language row
								boolean isLanguageSpecificField=false;
								boolean isCorrespondingLanguageFound=false;
								LanguageSpecificCell languageSpecificCell = null;
								int intendedRow = selectedDataRow;
								for(Field fieldGenericType : genericType.getClass().getDeclaredFields()){
									fieldGenericType.setAccessible(true);
									if(fieldGenericType.getType().getName().equals(PaxguiLanguage.class.getName())){										
										isLanguageSpecificField=true;
										if(fieldGenericType.get(genericType)!=null){
											PaxguiLanguage paxguiLanguage = (PaxguiLanguage)fieldGenericType.get(genericType);
											if(mapLanguageIndex.containsKey(paxguiLanguage.getPaxguiLanguageName())){
												intendedRow = selectedDataRow+mapLanguageIndex.get(paxguiLanguage.getPaxguiLanguageName());
												isCorrespondingLanguageFound=true;
											}
										}
										break;
									}
								}
								if(isLanguageSpecificField){
//									Decide on which language row
//									It may happen that the corresponding language is not present
									if(isCorrespondingLanguageFound){
										languageSpecificCell = new LanguageSpecificCell(intendedRow, columnCounter, genericType.toString());
										languageSpecificCells.add(languageSpecificCell);
										if(mapOfDataRowForLanguageSpecificCells.containsKey(selectedDataRow)){
											mapOfDataRowForLanguageSpecificCells.get(selectedDataRow).add(languageSpecificCell);
										}else{
											mapOfDataRowForLanguageSpecificCells.put(selectedDataRow, languageSpecificCells);
										}
									}									
//									createCell(row, columnCounter, genericType.toString());
								}else{
									createCell(row, columnCounter, genericType.toString());
								}
							}
						}else{
							if(field.getType().getName().equals(PaxguiLanguage.class.getName())){	
								if(field.get(contentType)!=null){
									PaxguiLanguage paxguiLanguage = (PaxguiLanguage)field.get(contentType);
									if(mapLanguageIndex.containsKey(paxguiLanguage.getPaxguiLanguageName())){
										int intendedRow = selectedDataRow+mapLanguageIndex.get(paxguiLanguage.getPaxguiLanguageName());
										LanguageSpecificCell languageSpecificCell = new LanguageSpecificCell(intendedRow, columnCounter, paxguiLanguage.toString());
										languageSpecificCells.add(languageSpecificCell);
										if(mapOfDataRowForLanguageSpecificCells.containsKey(selectedDataRow)){
											mapOfDataRowForLanguageSpecificCells.get(selectedDataRow).add(languageSpecificCell);
										}else{
											mapOfDataRowForLanguageSpecificCells.put(selectedDataRow, languageSpecificCells);
										}
									}
								}
							}else
								createCell(row, columnCounter, (field.get(contentType)!=null ? field.get(contentType).toString() : ""));
						}
//						log(objectType.getDisplayName()+".Field value: "+field.get(contentType));
						columnCounter++;
					}					
				}
				columnCounter=2;
//				Now create language specific rows
				insertLanguageRowsWithData(sheet, row);
//				Now clear everything that is specific to one ConfigurationTitle
				languageSpecificCells.clear();
				mapOfDataRowForLanguageSpecificCells.clear();
				selectedDataRow=selectedDataRow+(dataRowStep==0 ? 1 : dataRowStep)*2;
			}			
		}
		return this;
	}
	
	@Override
	public TemplateDataIntegrater rules(Map<String, Map<String, ValidatorAttribute>> rules) {
		this.rules=rules;		
		return this;
	}
	
	@Override
	public TemplateDataIntegrater cellStyles(Map<SECTION, CellStyle> cellStyles) {
		this.cellStyles=cellStyles;
		return this;
	}
	
	private void prepareMapOfTitles(){
		for(ObjectType objectType : configuration.getContentTypeList()){
			for(ConfigurationTitle configurationTitle : configuration.getTitleList()){
//				log("configurationTitle.Content Class:"+configurationTitle.getContent().getClass().getName()+", objectType.ObjectClassName:"+objectType.getObjectClassName());
				if(configurationTitle.getContent()!=null && configurationTitle.getContent().getClass().getName().equals(objectType.getObjectClassName())){					
					List<ConfigurationTitle> titles = (mapOfConfigurationTitles.containsKey(objectType.getDisplayName()) ? mapOfConfigurationTitles.get(objectType.getDisplayName()) : new ArrayList<>());
					titles.add(configurationTitle);
					mapOfConfigurationTitles.put(objectType.getDisplayName(), titles);
//					log("title Added for "+objectType.getDisplayName()+", title Size:"+titles.size());
				}				
			}
		}
	}
	
	public Row createRow(Sheet sheet){
		return sheet.createRow(rowCounter++);
	}
	
	public void createCell(Row row,int columnNumber,String value){
		Cell cell = row.createCell(columnNumber);
		cell.setCellValue(value);
	}	
	
	private void resetCounter(){
		rowCounter=ROW_COUNTER_INITIAL_INDEX;
		columnCounter=COLUMN_COUNTER_INITIAL_INDEX;
		selectedDataRow = ROW_COUNTER_INITIAL_INDEX;
	}
	
	public void createCell(Row row,int columnNumber,String value,SECTION section){
		Cell cell = row.createCell(columnNumber);
		cell.setCellValue(value);
		CellStyle style;		
		switch (section) {
		case HEADER:
			style=cellStyles.get(SECTION.HEADER);
			break;
		case TOP_LEVEL:
			style=cellStyles.get(SECTION.TOP_LEVEL);
			break;
		case TRACK:
			style=cellStyles.get(SECTION.TRACK);
			break;
		default:
			style=cellStyles.get(SECTION.TRACK);
			break;
		}
		cell.setCellStyle(style);
	}
	
	private void insertLanguageRowsWithData(Sheet sheet,Row startingRow){
//		###################################################################
//		PaxguiLanguage insertion on column index 1, from row 3
		Row languageRow=startingRow;// For first Iteration, first language has to on same row
		createCell(languageRow, 0, "Top Level",SECTION.TOP_LEVEL);
		if(configuration.getInterfaceLanguages()==null || configuration.getInterfaceLanguages().size()==0){
			createCell(languageRow, 1, "",SECTION.TOP_LEVEL);
			languageRow=sheet.createRow(languageRow.getRowNum()+1);
		}else{
			for(PaxguiLanguage paxguiLanguage : configuration.getInterfaceLanguages()){			
				if(languageRow.getRowNum() != startingRow.getRowNum())
					createCell(languageRow, 0, "",SECTION.TOP_LEVEL);			
				createCell(languageRow, 1, paxguiLanguage.getPaxguiLanguageName(),SECTION.TOP_LEVEL);			
//				every time create a new row
				languageRow=sheet.createRow(languageRow.getRowNum()+1);
			}
		}		
//		remove the last row as created by above loop
//		sheet.removeRow(languageRow);
//		rowCounter--;// Also remember to decrement rowCounter as one row has been removed
		if(mapOfDataRowForLanguageSpecificCells.containsKey(startingRow.getRowNum())){
			for(LanguageSpecificCell languageSpecificCell : mapOfDataRowForLanguageSpecificCells.get(startingRow.getRowNum())){
				Row row = null; 
				if((row=sheet.getRow(languageSpecificCell.getRow()))==null)
					row = sheet.createRow(languageSpecificCell.getRow());
				Cell cell = row.createCell(languageSpecificCell.getColumn());
				cell.setCellValue(languageSpecificCell.getValue());
			}
		}		
		startingRow = languageRow;
		createCell(languageRow, 0, "Track",SECTION.TRACK);
		if(configuration.getInterfaceLanguages()==null || configuration.getInterfaceLanguages().size()==0){
			createCell(languageRow, 1, "",SECTION.TRACK);
			languageRow=sheet.createRow(languageRow.getRowNum()+1);
		}else{
			for(PaxguiLanguage paxguiLanguage : configuration.getInterfaceLanguages()){			
				if(languageRow.getRowNum() != startingRow.getRowNum())
					createCell(languageRow, 0, "",SECTION.TRACK);			
				createCell(languageRow, 1, paxguiLanguage.getPaxguiLanguageName(),SECTION.TRACK);			
//				every time create a new row
				languageRow=sheet.createRow(languageRow.getRowNum()+1);
			}
		}		
//		PaxguiLanguage insertion done
//		###################################################################
	}
	
	private void prepareLanguageIndexing(){		
		mapLanguageIndex = new HashMap<>();
		int index=0;
		for(PaxguiLanguage paxguiLanguage : configuration.getInterfaceLanguages()){
			mapLanguageIndex.put(paxguiLanguage.getPaxguiLanguageName(), index++);
		}
	}
	
	private class LanguageSpecificCell{
		private int row,column;
		private String value;
		public LanguageSpecificCell(int row, int column, String value) {			
			this.row = row;
			this.column = column;
			this.value = value;
		}
		public int getRow() {
			return row;
		}
		public int getColumn() {
			return column;
		}
		public String getValue() {
			return value;
		}
		@Override
		public String toString() {			
			return "[ row : "+row+" , column : "+column+" , value : "+value+" ]";
		}
	}	
	
}
