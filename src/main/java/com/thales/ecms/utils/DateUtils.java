package com.thales.ecms.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import org.joda.time.DateTime;
import org.joda.time.Days;

/* Method to calculate deadline by start date and end date
 * 
 */

public class DateUtils {

	public static Date deadLineCalculator(Date startDate, Date endDate, Long orderStartDate) {
		Date shipmentDeadLine = null;
		Calendar calendarMiliSec = Calendar.getInstance();
		calendarMiliSec.setTimeInMillis(orderStartDate);
		Date orderDate= calendarMiliSec.getTime();
		if (Objects.nonNull(startDate) && Objects.nonNull(endDate)) {
			int days = Days.daysBetween(new DateTime(startDate), new DateTime(endDate)).getDays();

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(orderDate);
			calendar.add(Calendar.DATE, days);
			shipmentDeadLine = calendar.getTime();
		}
		return shipmentDeadLine;
	}

}
