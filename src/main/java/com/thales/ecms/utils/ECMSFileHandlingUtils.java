package com.thales.ecms.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import com.psddev.dari.util.MultipartRequest;

public class ECMSFileHandlingUtils {

	/**
	 * Utility method to save InputStream data to target location/file
	 * 
	 * @param inStream
	 *            - InputStream to be saved
	 * @param target
	 *            - full path to destination file
	 */
	public static boolean saveToFile(InputStream inStream, String target) {
		boolean result = true;
		BufferedInputStream in = null;
		BufferedOutputStream out = null;
		byte[] buff = new byte[32 * 1024];
		try {
			int len;
			in = new BufferedInputStream(inStream);
			out = new BufferedOutputStream(new FileOutputStream(target));
			while ((len = in.read(buff)) > 0) {
				out.write(buff, 0, len);
			}
			out.flush();
			out.close();
			in.close();
		} catch (IOException e) {
			result = false;
		}
		return result;
	}

	/**
	 * Creates a folder to desired location if it not already exists
	 * 
	 * @param dirName
	 *            - full path to the folder
	 * @return
	 * @throws SecurityException
	 *             - in case you don't have permission to create the folder
	 * @throws IOException
	 */
	public static String createFolderIfNotExists(String dirName, MultipartRequest request)
			throws SecurityException, IOException {

		String path = request.getSession().getServletContext().getRealPath("WEB-INF");

		File theDir = new File(path + File.separator + dirName);

		if (!theDir.exists()) {
			theDir.mkdir();
			System.out.println("dir created @ : " + path + File.separator + dirName);
		}
		return theDir.getAbsolutePath() + File.separator;
	}

	/**
	 * Creates a folder to desired location if it not already exists
	 * 
	 * @param dirName
	 *            - full path to the folder
	 * @return
	 * @throws SecurityException
	 *             - in case you don't have permission to create the folder
	 * @throws IOException
	 */
	public static String getUploadFile(String fileName, String directory, HttpServletRequest request) {

		File file = null;

		String path = request.getSession().getServletContext().getRealPath("WEB-INF");

		file = new File(path + File.separator + directory + File.separator + fileName);

		return file.getAbsolutePath();
	}
}
