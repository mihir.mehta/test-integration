package com.thales.ecms.utils;

import java.util.HashMap;
import java.util.Map;

import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.ContentType;
import com.thales.ecms.model.Rule;
import com.thales.ecms.model.Rule.ContentFieldValidation;
import com.thales.ecms.model.ValidatorAttribute;

/**
 * A Utility class for Rule
 * 
 * @author arjun.p
 *
 */
public class RuleUtil {

	/**
	 * Gets List of Rule associated with Configuration Prepares a map to easily
	 * find validation on fields for any ContentType It prepares a map of map of
	 * ValidatorAttribute. The internal map uses <field> name of ContentType as
	 * the key <ValidatorAttribute> as the value, assigned for a field of a
	 * ContentType The outer map uses ObjectType java name as the key Above map
	 * as the value
	 * 
	 * @return rules
	 */
	public static Map<String, Map<String, ValidatorAttribute>> getRules(Configuration configuration) {
		Map<String, Map<String, ValidatorAttribute>> rules = new HashMap<>();
		if (configuration.getRules() != null) {
			for (Rule rule : configuration.getRules()) {
				if (rule.getObjectType() != null) {
					HashMap<String, ValidatorAttribute> validators = (rules
							.get(rule.getObjectType().getInternalName()) != null
									? (HashMap<String, ValidatorAttribute>) rules
											.get(rule.getObjectType().getInternalName())
									: new HashMap<>());
					if (rule.getContentFieldValidations() != null) {
						for (ContentFieldValidation contentFieldValidation : rule.getContentFieldValidations()) {
							validators.put(contentFieldValidation.getField(),
									contentFieldValidation.getValidatorAttribute());
						}
					}
					if (rules.get(rule.getObjectType().getInternalName()) == null) {
						rules.put(rule.getObjectType().getInternalName(), validators);
					}
				}
			}
		}
		return rules;
	}

	/**
	 * Finds a Rule for a ContentType in a Configuration
	 * 
	 * @param configuration
	 *            A Configuration that has Rule for said contentType
	 * @param contentType
	 *            A ContentType for which a rule has to be found
	 * @return Rule if found, null otherwise
	 */
	public static Rule findRule(Configuration configuration, ContentType contentType) {
		Rule rule = null;
		if (contentType != null) {
			for (Rule r : configuration.getRules()) {
				if (r.getObjectType() != null) {
					if (contentType.getClass().getName().equals(r.getObjectType().getObjectClassName())) {
						rule = r;
						break;
					}
				}
			}
		}
		return rule;
	}

}
