package com.thales.ecms.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Used to get style of excel cell
 * 
 * @author arjun.p
 *
 */

public class ExcelCellStyleUtil {

	public static Map<SECTION, CellStyle> getCellStyles(Workbook wb){		
        Map<SECTION, CellStyle> styles = new HashMap<SECTION, CellStyle>();
        CellStyle style = wb.createCellStyle();
	    style.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
	    style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    style.setBorderBottom(CellStyle.BORDER_THIN);
	    style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
	    style.setBorderLeft(CellStyle.BORDER_THIN);
	    style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
	    style.setBorderRight(CellStyle.BORDER_THIN);
	    style.setRightBorderColor(IndexedColors.BLACK.getIndex());
	    style.setBorderTop(CellStyle.BORDER_THIN);
	    style.setTopBorderColor(IndexedColors.BLACK.getIndex());	    
        styles.put(SECTION.HEADER, style);
        
        style = wb.createCellStyle();
	    style.setFillForegroundColor(HSSFColor.AQUA.index);
	    style.setFillPattern(CellStyle.SOLID_FOREGROUND);	    
        styles.put(SECTION.TOP_LEVEL, style);
        
        style = wb.createCellStyle();
	    style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
	    style.setFillPattern(CellStyle.SOLID_FOREGROUND);       
        styles.put(SECTION.TRACK, style);
        
//        Create a new CellStyle for contents and write below method
//        that will wrap text for cell
//        style.setWrapText(true);
        return styles;
	}	


	/**
	 * Represents different sections in sheet
	 * 
	 * @author arjun.p
	 *
	 */
	public enum SECTION{
		HEADER,TOP_LEVEL,TRACK;
	}
	
	
	
}
