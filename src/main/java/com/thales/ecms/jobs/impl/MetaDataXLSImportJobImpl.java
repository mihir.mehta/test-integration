package com.thales.ecms.jobs.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.psddev.dari.db.ObjectType;
import com.thales.ecms.conversions.ObjectDataTypeConverter;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.jobs.MetaDataImportJob;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.Configuration.ConfigurationTitle;
import com.thales.ecms.model.ContentType;
import com.thales.ecms.model.IngestionDetail;
import com.thales.ecms.model.IngestionDetail.IngestionTitle;
import com.thales.ecms.model.PaxguiLanguage;
import com.thales.ecms.model.Rule;
import com.thales.ecms.model.util.TextClass;
import com.thales.ecms.service.ConfigurationService;
import com.thales.ecms.service.ContentTypeService;
import com.thales.ecms.service.IngestionDetailService;
import com.thales.ecms.utils.ECMSConstants;
import com.thales.ecms.model.IngestionDetail.ImportStatus;
import com.thales.ecms.model.IngestionDetail.IngestionTitle.ImportStatusTitle;
import com.thales.ecms.utils.RuleUtil;
import com.thales.ecms.utils.ValidationUtil;

/**
* <h1>Import MetaData XLS file!</h1>
* This class is used to import metadata into ECMS portal
*
* @author  Manish gour
* @version 1.0
*/

/**
 * @author Manish gour, Class for importing metadata XLS/XLSX file
 */

@Component
public class MetaDataXLSImportJobImpl implements MetaDataImportJob {

	@Autowired
	MessageSource messageSource;

	@Autowired
	ContentTypeService contentTypeService;

	@Autowired
	ConfigurationService configurationService;

	@Autowired
	IngestionDetailService ingestionDetailService;

	private String airlineId = null;

	private String ingestionDetailId = null;

	private IngestionDetail ingestionDetailFromDB = null;

	private Configuration configuration = null;

	private Set<PaxguiLanguage> languages = null;

	private String xlsFile = null;

	private FileInputStream xlsFileInputStream = null;

	private String fileNameWithoutPath = null;

	/**
	 * Map<String, Integer> fieldLocationsMap contains map of fields in XLS
	 * file.
	 */
	private Map<String, Integer> fieldLocationsMap = null;

	public static Logger logger = LoggerFactory.getLogger(MetaDataXLSImportJobImpl.class);

	private Date currentDate = null;

	@Override
	public IngestionDetail call() throws Exception {
		logger.info("#######   Now starting actual MetaDataXLSImportJobImpl  #####");

		if (this.validateFile()) {
			this.process();
		} else { // failed due to invalid file format.
			ingestionDetailFromDB.setStatus("Fail");
			ingestionDetailFromDB.save();
		}
		this.closeFile();
		logger.info("####### Completed  MetaDataXLSImportJobImpl  job #####");
		return null;
	}

	/**
	 * Instantiating objects.
	 * 
	 * @return Nothing.
	 */
	public MetaDataXLSImportJobImpl() {
		this.setCurrentDate(new Date());
	}

	/**
	 * This method is for saving required objects before staring process.
	 * 
	 * @see com.thales.ecms.jobs.MetaDataImportJob#settingRequriedObjects(String
	 *      airlineId, String configurationId, String fileName)
	 */
	@Override
	public void setRequriedObjects(String airlineId, String configurationId, String ingestionDetailId, String fileName)
			throws ObjectNotFoundException {
		Configuration configuration = null;
		Set<PaxguiLanguage> languages = null;

		this.setAirlineId(airlineId);

		this.ingestionDetailId = ingestionDetailId;
		this.ingestionDetailFromDB = ingestionDetailService.getIngestionDetailById(this.ingestionDetailId);

		// setting configuration object
		configuration = configurationService.getConfigurationById(airlineId, configurationId);
		this.setConfiguration(configuration);

		// setting language object
		languages = configuration.getInterfaceLanguages();
		this.setLanguages(languages);

		this.setXlsFile(fileName);

		this.setFileNameWithoutPath(
				FilenameUtils.getBaseName(this.getXlsFile()) + FilenameUtils.getExtension(this.getXlsFile()));
	}

	/**
	 * This method is opening file before processing.
	 * 
	 * @see com.thales.ecms.jobs.MetaDataImportJob#openfile()
	 */
	@Override
	public void openfile() {
		logger.info(messageSource.getMessage(ECMSConstants.METADATA_IMPORT_OPEN_FILE, null, Locale.US));
		try {
			this.setXlsFileInputStream(new FileInputStream(new File(this.getXlsFile())));
		} catch (FileNotFoundException e) {
			logger.info(messageSource.getMessage(ECMSConstants.METADATA_IMPORT_OPEN_FILE_ERROR, null, Locale.US));
		}
	}

	/**
	 * This method is used to return opned files's XSSFWorkbook object
	 * 
	 * @return XSSFWorkbook
	 */
	private XSSFWorkbook readSheets() {
		XSSFWorkbook wb = null;
		try {
			wb = new XSSFWorkbook(this.getXlsFileInputStream());
		} catch (IOException e) {
			logger.info(messageSource.getMessage(ECMSConstants.METADATA_IMPORT_READ_FILE_ERROR, null, Locale.US));
		}
		return wb;
	}

	/**
	 * This method will validate the file before starting process.
	 * 
	 * @see com.thales.ecms.jobs.MetaDataImportJob#validateFile()
	 */
	@Override
	public boolean validateFile() {
		// Considering file is valid by-default
		boolean status = false;

		// if file does not have sheets according to defined
		// configuration object
		status = this.sheetsAccordingToConfiguration();
		// if file does not have rows according to defined configuration object
		if (status == true) {
			status = this.rowsAccordingToConfiguration();
			if (status == true) {
				ImportStatus importStatus = new ImportStatus();
				ingestionDetailFromDB.setImportStatus(importStatus);

			}
		}
		if (status == false) {
			ingestionDetailFromDB.save();
		}

		return status;
	}

	/**
	 * This method have actual process to import the data into database.
	 * 
	 * @see com.thales.ecms.jobs.MetaDataImportJob#process()
	 */
	@Override
	public void process() {
		this.openfile();
		XSSFWorkbook wb = this.readSheets();

		int sheetCounter = 0;
		for (ObjectType objectType : this.getConfiguration().getContentTypeList()) {

			this.fieldLocationsMap = new HashMap<>();

			XSSFSheet sheet = wb.getSheetAt(sheetCounter);
			if (sheet != null) {
				this.prepareHeaderMap(sheet);
				this.readRow(objectType, sheet);
				// processFlag = true;
			}

			sheetCounter++;
		}
	}

	/**
	 * This method is used to prepare and save top columns names in a map, so
	 * that can be used while retrieving filed values without caring for
	 * position of cell.
	 * 
	 * @param sheet
	 */
	private void prepareHeaderMap(XSSFSheet sheet) {
		if (sheet != null) {
			for (int rowCounter = ECMSConstants.METADATA_IMPORT_STARTING_ROW; rowCounter < 1; rowCounter++) {
				Row row = sheet.getRow(rowCounter);
				if (row != null) {
					for (int cellCounter = ECMSConstants.METADATA_IMPORT_STARTING_COLUMN; cellCounter < row
							.getPhysicalNumberOfCells(); cellCounter++) {
						if (row.getCell(cellCounter) != null) {
							Cell cellValue = row.getCell(cellCounter);
							/*
							 * M3 Title Name ** is the first field and which is
							 * hardcoded for now.
							 */
							if (cellValue.toString()
									.equalsIgnoreCase(ECMSConstants.METADATA_IMPORT_ENGLISH_TITLE_LABEL)) {
								this.fieldLocationsMap.put(ECMSConstants.METADATA_IMPORT_ENGLISH_TITLE, cellCounter);
							} else {
								this.fieldLocationsMap.put(cellValue.toString(), cellCounter);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * This method is used to read and process is row of an excel file
	 * 
	 * @param objectType
	 * @param sheet
	 * @return Nothing
	 */
	@SuppressWarnings("unchecked")
	private void readRow(ObjectType objectType, XSSFSheet sheet) {

		// int flushTitlesCountLimit =
		// ECMSConstants.METADATA_IMPORT_THREAD_CHUNK_SIZE;

		IngestionTitle ingestionTitle = null;
		ImportStatus importStatus = new ImportStatus();
		List<IngestionTitle> flushableIngestionTitlesList = new ArrayList<>();
		List<IngestionTitle> ingestedTitlesList = new ArrayList<>();
		ImportStatusTitle importStatusTitle = null;

		if (sheet.getSheetName().equalsIgnoreCase(objectType.getDisplayName())) {

			int titleCounter = 0;
			int titleCounterWithException = 0;
			for (int rowCounter = ECMSConstants.METADATA_IMPORT_STARTING_ROW_DATA; rowCounter < sheet
					.getPhysicalNumberOfRows(); rowCounter++) {
				Row row = sheet.getRow(rowCounter);

				// Checking row has main data or language wise data?
				// (as we are just picking first row to a contentType)
				if (row.getCell(0).toString() != null && StringUtils.isNotBlank(row.getCell(0).toString())
						&& row.getCell(0).toString().equalsIgnoreCase(ECMSConstants.METADATA_IMPORT_TOP_LEVEL)) {
					titleCounter++;

					String englishTitle = null;
					if (row.getCell(2) != null) {
						boolean error = false;
						englishTitle = row.getCell(2).toString();

						ingestionTitle = new IngestionTitle();
						importStatusTitle = new ImportStatusTitle();
						Class<? extends ContentType> clazz = null;
						ContentType contentType = null;
						try {
							clazz = (Class<? extends ContentType>) Class.forName(objectType.getInternalName());

						} catch (ClassNotFoundException e) {
							error = true;
							logger.info(messageSource.getMessage(ECMSConstants.METADATA_IMPORT_CONTENTTYPE_IMPORT_ERROR,
									new Object[] { ECMSConstants.METADATA_IMPORT_CONTENT_TYPE_LABEL }, Locale.US)
									+ e.getMessage());
							titleCounterWithException++;
							continue;
						}

						/*
						 * Before creating any content Type need to check. Does
						 * this contentType with this english title already
						 * exists?
						 */
						contentType = contentTypeService.getContentTypeByEnglishTitle(englishTitle, clazz);

						if (contentType == null) {
							try {
								/*
								 * Creating instance of ContentType dynamically
								 */
								contentType = clazz.newInstance();
							} catch (InstantiationException | IllegalAccessException e) {
								error = true;
								logger.info(
										messageSource.getMessage(ECMSConstants.METADATA_IMPORT_CONTENTTYPE_IMPORT_ERROR,
												new Object[] { ECMSConstants.METADATA_IMPORT_CONTENT_TYPE_LABEL },
												Locale.US) + e.getMessage());
								titleCounterWithException++;
								continue;
							}
						}

						Field[] fieldArray = clazz.getDeclaredFields();

						for (Field field : fieldArray) {
							String fieldName = field.getName();

							DataFormatter dataFormatter = new DataFormatter();

							String fieldValue = dataFormatter
									.formatCellValue(row.getCell(this.fieldLocationsMap.get(fieldName)));

							// checking not empty cellvalue
							if (fieldValue != null && StringUtils.isNotBlank(fieldValue)) {
								try {
									// set method name
									StringBuilder setMethodName = new StringBuilder();
									setMethodName.append(ECMSConstants.METADATA_IMPORT_SET);
									setMethodName.append(WordUtils.capitalize(fieldName));

									if (field.getType().getName() == ECMSConstants.METADATA_IMPORT_JAVA_UTIL_LIST) {
										List<TextClass> textClassList = this.languageIterator(sheet, rowCounter,
												fieldName);
										MethodUtils.invokeMethod(contentType, setMethodName.toString(), textClassList);
									} else {
										Object convertedObject = ObjectDataTypeConverter.converterForClass
												.get(field.getType()).convert(fieldValue, airlineId);

										if (convertedObject != null) {
											MethodUtils.invokeMethod(contentType, setMethodName.toString(),
													convertedObject);
										}
									}

								} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException
										| ParseException | IllegalArgumentException e) {
									error = true;
									String userFrieldyMessage = null;
									if (e instanceof ParseException) {
										// datetime issue format not correct
										userFrieldyMessage = "Datetime issue format not correct in " + "ROW COUNTER :"
												+ rowCounter + " .";
									} else if (e instanceof IllegalArgumentException) {
										// fieldName does not have sopportable
										// datatype
										userFrieldyMessage = "FieldName : " + fieldName
												+ " does not have sopportable datatype in " + "ROW COUNTER :"
												+ rowCounter + ".";
									} else {
										// fieldName defined in configuration
										userFrieldyMessage = "FieldName : " + fieldName + " not defned in configuration"
												+ "ROW COUNTER :" + rowCounter + ".";

									}

									/**
									 * Inner lavel
									 */

									ingestionTitle.setTitleName(englishTitle);
									ingestionTitle.setContentType(objectType.getDisplayName());
									ingestionTitle.setStatus("Fail");
									ingestionTitle.setIngestedDate(getCurrentDate());
									importStatusTitle.setException(e.getMessage());
									importStatusTitle.setUserFriendlyMessage(userFrieldyMessage);
									ingestionTitle.setImportStatusTitle(importStatusTitle);
									logger.info(e.getMessage() + "ROW COUNTER :" + rowCounter + ", fieldName:"
											+ fieldName + ", Map Value cell number: "
											+ this.fieldLocationsMap.get(fieldName) + " ,Value:" + fieldValue
											+ e.getMessage());
									this.saveIngestionDetailsTitle(ingestedTitlesList, flushableIngestionTitlesList,
											ingestionTitle);
									titleCounterWithException++;
									break;
								}
							} // field empty check

						} // end of field loop

						/*
						 * Checking all the fields set before saving into
						 * database. Mandatory check for at least
						 * getEnglishTitle field.
						 */
						if (Objects.nonNull(contentType)) {

							Object object = null;
							try {
								object = MethodUtils.invokeMethod(contentType,
										ECMSConstants.METADATA_IMPORT_ENGLISH_TITLE_METHOD_NAME);
							} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
								error = true;
								ingestionTitle.setTitleName(englishTitle);
								ingestionTitle.setContentType(objectType.getDisplayName());
								ingestionTitle.setStatus("Fail");
								ingestionTitle.setIngestedDate(getCurrentDate());
								importStatusTitle.setException(e.getMessage());
								importStatusTitle
										.setUserFriendlyMessage("content type does not contains EnglishTitle.");
								ingestionTitle.setImportStatusTitle(importStatusTitle);
								logger.info(messageSource.getMessage(
										ECMSConstants.METADATA_IMPORT_SETTING_ENGLISHTITLE_ERROR,
										new Object[] { ECMSConstants.METADATA_IMPORT_CONTENT_TYPE_LABEL }, Locale.US)
										+ e.getMessage());
								this.saveIngestionDetailsTitle(ingestedTitlesList, flushableIngestionTitlesList,
										ingestionTitle);
								titleCounterWithException++;
								continue;
							}

							/**
							 * ensure we do not have any error then we can
							 * proceed to saving title into database
							 */
							if (error == false) {
								if (Objects.nonNull(object)) {
									/***
									 * Status field has been removed by Mihir
									 * form now, So commenting below code. //
									 * Setting default Status.Completed status
									 * while // importing try {
									 * MethodUtils.invokeMethod(contentType,
									 * ECMSConstants.METADATA_IMPORT_SET_STATUS_METHOD_NAME,
									 * Status.Completed); } catch
									 * (NoSuchMethodException |
									 * IllegalAccessException |
									 * InvocationTargetException e) {
									 * System.out.println("ISSUE FOUND"); result
									 * = false; logger.info(
									 * messageSource.getMessage(ECMSConstants.METADATA_IMPORT_SETTING_STATUS_ERROR,
									 * new Object[] {
									 * ECMSConstants.METADATA_IMPORT_CONTENT_TYPE_LABEL
									 * }, Locale.US) + e.getMessage()); }
									 */

									// Apply Rule Here
									Rule rule = RuleUtil.findRule(configuration, contentType);
									if (rule != null) {
										boolean isValidated = false;
										try {
											isValidated = ValidationUtil.validateContentType(contentType, rule, null);
										} catch (IllegalAccessException | IllegalArgumentException
												| InvocationTargetException | NoSuchMethodException
												| SecurityException e) {
											// result = false;
											ingestionTitle.setTitleName(englishTitle);
											ingestionTitle.setContentType(objectType.getDisplayName());
											ingestionTitle.setStatus("Fail");
											ingestionTitle.setIngestedDate(getCurrentDate());
											importStatusTitle.setException(e.getMessage());
											importStatusTitle.setUserFriendlyMessage(
													"Error while Rule validation on content Type.");
											ingestionTitle.setImportStatusTitle(importStatusTitle);
											logger.info(
													"Error while Rule validation content Type" + e.getMessage() + ".");
											this.saveIngestionDetailsTitle(ingestedTitlesList,
													flushableIngestionTitlesList, ingestionTitle);
											titleCounterWithException++;
											continue;
										}

										if (isValidated) {
											// Saving data using service with
											// Rule
											ImportStatusTitle importStatusTitleSucees = saveToDataBase(clazz,
													contentType, englishTitle, rowCounter);
											ingestionTitle.setTitleName(englishTitle);
											ingestionTitle.setContentType(objectType.getDisplayName());
											ingestionTitle.setStatus("Pass");
											ingestionTitle.setIngestedDate(getCurrentDate());
											ingestionTitle.setImportStatusTitle(importStatusTitleSucees);
											this.saveIngestionDetailsTitle(ingestedTitlesList,
													flushableIngestionTitlesList, ingestionTitle);
										}
									} else {
										// Saving data using service without
										// Rule
										ImportStatusTitle importStatusTitleSucees = saveToDataBase(clazz, contentType,
												englishTitle, rowCounter);
										ingestionTitle.setTitleName(englishTitle);
										ingestionTitle.setContentType(objectType.getDisplayName());
										ingestionTitle.setStatus("Pass");
										ingestionTitle.setIngestedDate(getCurrentDate());
										ingestionTitle.setImportStatusTitle(importStatusTitleSucees);
										this.saveIngestionDetailsTitle(ingestedTitlesList, flushableIngestionTitlesList,
												ingestionTitle);
									}

								}
							} // end of ensure we do not have any error then we
								// can proceed to saving title into database
						}
						// putting one row report
					}
				} // end of mail row check

			} // end of row loop

			logger.info("======================================================================");
			logger.info("One Sheet completed");

			// For handling case when only 2 titles left
			if (flushableIngestionTitlesList.size() > 0) {
				if (ingestionDetailFromDB.getTitles() != null) {
					ingestedTitlesList = ingestionDetailFromDB.getTitles();
				}
				ingestedTitlesList.addAll(flushableIngestionTitlesList);
				ingestionDetailFromDB.setTitles(ingestedTitlesList);
				ingestionDetailFromDB.setTitleCount(ingestedTitlesList.size());
				flushableIngestionTitlesList.clear();
				logger.info("######  Added remaining  titles in IngestionDetail  ===> " + ingestedTitlesList.size());
			}

			// Error occurred somewhere- means it went into any
			// catch block
			// check both the is same then Fail
			if (titleCounter == titleCounterWithException) {
				importStatus.setUserFriendlyMessage("All title have been failed to save.");
				this.saveIngestionDetails("Fail", importStatus);
			} else {
				importStatus.setUserFriendlyMessage("Title(s) have been saved successfully.");
				this.saveIngestionDetails("Pass", importStatus);
			}

		}

	}

	private void saveIngestionDetails(String status, ImportStatus importStatus) {
		this.ingestionDetailFromDB.setConfiguration(this.configuration.getName());
		this.ingestionDetailFromDB.setStatus(status);
		this.ingestionDetailFromDB.setTitleCount(ingestionDetailFromDB.getTitles().size());
		this.ingestionDetailFromDB.setImportStatus(importStatus);
		this.ingestionDetailFromDB.save();
	}

	private void saveIngestionDetailsTitle(List<IngestionTitle> ingestedTitlesList,
			List<IngestionTitle> flushableIngestionTitlesList, IngestionTitle ingestionTitle) {
		if (ingestionDetailFromDB.getTitles() != null) {
			if (ingestionDetailFromDB.getTitles().contains(ingestionTitle) == false) {
				flushableIngestionTitlesList.add(ingestionTitle);
			}
		} else {
			flushableIngestionTitlesList.add(ingestionTitle);
		}

		if (flushableIngestionTitlesList.size() == ECMSConstants.METADATA_IMPORT_THREAD_CHUNK_SIZE) {

			if (ingestionDetailFromDB.getTitles() != null) {
				ingestedTitlesList = ingestionDetailFromDB.getTitles();
			}
			ingestedTitlesList.addAll(flushableIngestionTitlesList);
			ingestionDetailFromDB.setTitles(ingestedTitlesList);
			ingestionDetailFromDB.save();
			flushableIngestionTitlesList.clear();
			logger.debug(
					"######  Added ingestion titles in IngestionDetail  ===> " + flushableIngestionTitlesList.size());
		}
	}

	/**
	 * This method is used to prepare List<TextClass> object according to
	 * defined language in configuration.
	 * 
	 * @param sheet
	 * @param rowCounter
	 * @param fieldName
	 * @return List<TextClass>
	 */
	private List<TextClass> languageIterator(XSSFSheet sheet, int rowCounter, String fieldName) {
		List<TextClass> textClassList = new ArrayList<>();

		Set<PaxguiLanguage> allLanguages = this.getLanguages();
		for (Iterator<PaxguiLanguage> iterator = allLanguages.iterator(); iterator.hasNext();) {
			Row row = sheet.getRow(rowCounter);

			String fieldValue = new DataFormatter().formatCellValue(row.getCell(this.fieldLocationsMap.get(fieldName)))
					.toString();

			PaxguiLanguage paxguiLanguage = (PaxguiLanguage) iterator.next();

			// FeatureVideo private List<TextClass> title;
			if (fieldValue != null && StringUtils.isNotBlank(fieldValue)) {
				TextClass textClass = new TextClass();
				textClass.setValue(fieldValue);
				textClass.setLanguage(paxguiLanguage);
				textClassList.add(textClass);
			}

			rowCounter++;
		}
		return textClassList;
	}

	/**
	 * Method to add new ConfigurationTitle to titleList
	 * 
	 * @param contentType
	 * @return ImportStatus
	 */
	private ImportStatusTitle saveToDataBase(Class<? extends ContentType> clazz, ContentType contentType,
			String englishTitle, int rowCounter) {
		ImportStatusTitle importStatusTitle = new ImportStatusTitle();

		try {
			// it is a new record
			if (contentType.getState().isNew()) {
				contentTypeService.addContentType(englishTitle, contentType, clazz);

				ConfigurationTitle configurationTitle = new ConfigurationTitle();
				configurationTitle.setContent(contentType);
				if (configuration.getTitleList() == null) {
					List<ConfigurationTitle> list = new ArrayList<>();
					list.add(configurationTitle);
				}
				configuration.getTitleList().add(configurationTitle);
				// Saving Configuration Object
				configurationService.saveConfiguration(configuration);
				importStatusTitle.setException(null);
				importStatusTitle.setUserFriendlyMessage("Title " + englishTitle + " has been inserted successfully.");
			} else {
				// it is an existing record
				contentTypeService.updateContentType(englishTitle, contentType);
				importStatusTitle.setException(null);
				importStatusTitle.setUserFriendlyMessage("Title " + englishTitle + " has been updated successfully.");
			}

			logger.info("------------------------------------------------------------------");
			logger.info("One row completed");
		} catch (DuplicateEntryException e) {
			logger.info("Duplicate Data: Error While saving data into database." + e.getMessage());
			importStatusTitle.setException(e.getMessage());
			importStatusTitle.setUserFriendlyMessage("Duplicate Data: Error While saving title: " + englishTitle + " into database.");
		}
		return importStatusTitle;
	}

	/**
	 * This method is used to close the open file instance.
	 * 
	 * @see com.thales.ecms.jobs.MetaDataImportJob#closeFile()
	 */
	@Override
	public void closeFile() {
		try {
			this.getXlsFileInputStream().close();
		} catch (IOException e) {
			logger.info("Error while closing file.");
		}
	}

	@SuppressWarnings("unchecked")
	private boolean rowsAccordingToConfiguration() {

		boolean isRowsAccordingToConfiguration = false;
		ImportStatus importStatus = null;
		this.openfile();

		XSSFWorkbook wb = this.readSheets();

		if (wb.getNumberOfSheets() == this.getConfiguration().getContentTypeList().size()) {
			importStatus = new ImportStatus();
			int sheetCounter = 0;
			for (ObjectType objectType : this.getConfiguration().getContentTypeList()) {
				XSSFSheet sheet = wb.getSheetAt(sheetCounter);
				int sheetCounterForXlxs = 0;
				// validate header row
				Class<? extends ContentType> clazz = null;
				try {
					clazz = (Class<? extends ContentType>) Class.forName(objectType.getInternalName());
				} catch (ClassNotFoundException e) {
					logger.info(messageSource.getMessage(ECMSConstants.METADATA_IMPORT_CONTENTTYPE_IMPORT_ERROR,
							new Object[] { ECMSConstants.METADATA_IMPORT_CONTENT_TYPE_LABEL }, Locale.US)
							+ e.getMessage());
				}
				Field[] fieldArray = clazz.getDeclaredFields();
				// validation of excel header rows
				for (int rowCounter = ECMSConstants.METADATA_IMPORT_STARTING_ROW; rowCounter < ECMSConstants.METADATA_IMPORT_STARTING_ROW_DATA; rowCounter++) {
					Row row = sheet.getRow(rowCounter);
					if ((row != null && row.getPhysicalNumberOfCells() - 2 == fieldArray.length)) {
						isRowsAccordingToConfiguration = true;
						int rowCounterForSheet = 0;
						if (rowCounter == ECMSConstants.METADATA_IMPORT_STARTING_ROW) {
							int cellCounterForSheet = 0;
							Cell contentTypeCellValue = row
									.getCell(ECMSConstants.METADATA_IMPORT_HEADER_STARTING_COLUMN);
							if (!contentTypeCellValue.toString()
									.equalsIgnoreCase(ECMSConstants.METADATA_IMPORT_CONTENT_TYPE_LABEL)) {
								rowCounterForSheet = rowCounter + 1;
								cellCounterForSheet = ECMSConstants.METADATA_IMPORT_HEADER_STARTING_COLUMN + 1;
								importStatus.setUserFriendlyMessage("Row no '" + rowCounterForSheet
										+ "' and column no '" + cellCounterForSheet + "' data is not correct");
								ingestionDetailFromDB.setImportStatus(importStatus);
								isRowsAccordingToConfiguration = false;
								return isRowsAccordingToConfiguration;
							}
							Cell languageCellValue = row.getCell(ECMSConstants.METADATA_IMPORT_LANGUAGE_COLUMN);
							if (!languageCellValue.toString()
									.equalsIgnoreCase(ECMSConstants.METADATA_IMPORT_LANGUAGE_LABEL)) {
								rowCounterForSheet = rowCounter + 1;
								cellCounterForSheet = ECMSConstants.METADATA_IMPORT_LANGUAGE_COLUMN + 1;
								importStatus.setUserFriendlyMessage("Row no '" + rowCounterForSheet
										+ "' and column no '" + cellCounterForSheet + "' data is not correct");
								ingestionDetailFromDB.setImportStatus(importStatus);
								isRowsAccordingToConfiguration = false;
								return isRowsAccordingToConfiguration;
							}
							Cell englishTitleCellValue = row.getCell(ECMSConstants.METADATA_IMPORT_STARTING_COLUMN);
							if (!englishTitleCellValue.toString()
									.equalsIgnoreCase(ECMSConstants.METADATA_IMPORT_ENGLISH_TITLE_LABEL)) {
								rowCounterForSheet = rowCounter + 1;
								cellCounterForSheet = ECMSConstants.METADATA_IMPORT_STARTING_COLUMN + 1;
								importStatus.setUserFriendlyMessage("Row no '" + rowCounterForSheet
										+ "' and column no '" + cellCounterForSheet + "' data is not correct");
								ingestionDetailFromDB.setImportStatus(importStatus);
								isRowsAccordingToConfiguration = false;
								return isRowsAccordingToConfiguration;
							}
							int cellCounter = ECMSConstants.METADATA_IMPORT_STARTING_DECLARED_FIELD_COLUMN;
							for (int i = 1; i < fieldArray.length; i++) {
								String fieldName = fieldArray[i].getName();
								if (row.getCell(cellCounter) != null
										&& (cellCounter <= row.getPhysicalNumberOfCells())) {
									Cell cellValue = row.getCell(cellCounter);
									if (fieldName.equalsIgnoreCase(cellValue.toString())) {
										cellCounter++;
									} else {
										rowCounterForSheet = rowCounter + 1;
										cellCounterForSheet = cellCounter + 1;
										importStatus.setUserFriendlyMessage("Row no '" + rowCounterForSheet
												+ "' and column no '" + cellCounterForSheet + "' data is not correct");
										ingestionDetailFromDB.setImportStatus(importStatus);
										isRowsAccordingToConfiguration = false;
										return isRowsAccordingToConfiguration;
									}
								}
							}

						} else if (rowCounter == ECMSConstants.METADATA_IMPORT_STARTING_DECLARED_FIELD_ROW) {
							if (row != null) {
								int cellCounterForSheet = 0;
								for (int cellCounter = 0; cellCounter <= ECMSConstants.METADATA_IMPORT_LANGUAGE_COLUMN; cellCounter++) {
									Cell cellValue = row.getCell(cellCounter);
									if (cellValue.toString() != null) {
										if (!cellValue.toString().isEmpty()) {
											rowCounterForSheet = rowCounter + 1;
											cellCounterForSheet = cellCounter + 1;
											importStatus.setUserFriendlyMessage(
													"Row no '" + rowCounterForSheet + "' and column no '"
															+ cellCounterForSheet + "' data is not correct");
											ingestionDetailFromDB.setImportStatus(importStatus);
											isRowsAccordingToConfiguration = false;
											return isRowsAccordingToConfiguration;
										}

									}
								}
							}
						} else if (rowCounter == ECMSConstants.METADATA_IMPORT_BLANK_ROW) {
							if (row != null) {
								int cellCounterForSheet = 0;
								for (int cellCounter = 0; cellCounter < row.getPhysicalNumberOfCells(); cellCounter++) {
									Cell cellValue = row.getCell(cellCounter);
									if (cellValue.toString() != null) {
										if (!cellValue.toString().isEmpty()) {
											rowCounterForSheet = rowCounter + 1;
											cellCounterForSheet = cellCounter + 1;
											importStatus.setUserFriendlyMessage(
													"Row no '" + rowCounterForSheet + "' and column no '"
															+ cellCounterForSheet + "' data is not correct");
											ingestionDetailFromDB.setImportStatus(importStatus);
											isRowsAccordingToConfiguration = false;
											return isRowsAccordingToConfiguration;
										}

									}
								}
							}
						}
					} else {
						sheetCounterForXlxs = sheetCounter + 1;
						importStatus.setUserFriendlyMessage(
								"Error while validating xlxs file is no of column in the sheet no '"
										+ sheetCounterForXlxs + "' is not correct");
						ingestionDetailFromDB.setImportStatus(importStatus);
						isRowsAccordingToConfiguration = false;
						return isRowsAccordingToConfiguration;
					}
				}

				int languageCount = configuration.getInterfaceLanguages().size();
				for (int rowCounter = ECMSConstants.METADATA_IMPORT_STARTING_ROW_DATA; rowCounter < sheet
						.getPhysicalNumberOfRows();) {
					Row row = sheet.getRow(rowCounter);
					isRowsAccordingToConfiguration = true;
					Cell contentTypeCellValue = row.getCell(ECMSConstants.METADATA_IMPORT_HEADER_STARTING_COLUMN);
					if (!contentTypeCellValue.toString()
							.equalsIgnoreCase(ECMSConstants.METADATA_IMPORT_TOP_LEVEL_TITLE)) {
						importStatus.setUserFriendlyMessage("Row no '" + rowCounter + "' and column no '"
								+ ECMSConstants.METADATA_IMPORT_HEADER_STARTING_COLUMN + "' data is not correct");
						ingestionDetailFromDB.setImportStatus(importStatus);
						isRowsAccordingToConfiguration = false;
						return isRowsAccordingToConfiguration;
					}
					Cell titleCellValue = row.getCell(ECMSConstants.METADATA_IMPORT_STARTING_COLUMN);
					if (titleCellValue == null) {
						importStatus.setUserFriendlyMessage("Row no '" + rowCounter + "' and column no '"
								+ ECMSConstants.METADATA_IMPORT_HEADER_STARTING_COLUMN
								+ "' English title can not be empty");
						ingestionDetailFromDB.setImportStatus(importStatus);
						isRowsAccordingToConfiguration = false;
						return isRowsAccordingToConfiguration;
					} else {
						String titleValue = titleCellValue.toString();
						if (titleValue.isEmpty()) {
							importStatus.setUserFriendlyMessage("Row no '" + rowCounter + "' and column no '"
									+ ECMSConstants.METADATA_IMPORT_HEADER_STARTING_COLUMN
									+ "' English title can not be empty");
							ingestionDetailFromDB.setImportStatus(importStatus);
							isRowsAccordingToConfiguration = false;
							return isRowsAccordingToConfiguration;
						}

					}
					rowCounter = rowCounter + languageCount * 2;
				}
				sheetCounter++;
			}

		}
		return isRowsAccordingToConfiguration;
	}

	private boolean sheetsAccordingToConfiguration() {
		ImportStatus importStatus = null;
		boolean isSheetsAccordingToConfiguration = false;
		this.openfile();
		XSSFWorkbook wb = this.readSheets();

		if (wb.getNumberOfSheets() == this.getConfiguration().getContentTypeList().size()) {
			int sheetCounter = 0;
			for (ObjectType objectType : this.getConfiguration().getContentTypeList()) {
				XSSFSheet sheet = wb.getSheetAt(sheetCounter);
				if (sheet.getSheetName().equals(objectType.getDisplayName()) == false) {
					importStatus = new ImportStatus();
					importStatus
							.setUserFriendlyMessage("Error while validating xlxs file is sheet name is not correct");
					ingestionDetailFromDB.setImportStatus(importStatus);
					isSheetsAccordingToConfiguration = false;
					return isSheetsAccordingToConfiguration;
				} else {
					isSheetsAccordingToConfiguration = true;
				}
				sheetCounter++;
			}
		} else {
			importStatus = new ImportStatus();
			importStatus.setUserFriendlyMessage("Error while validating xlxs file is Number Of Sheets is not correct");
			ingestionDetailFromDB.setImportStatus(importStatus);
			isSheetsAccordingToConfiguration = false;
			return isSheetsAccordingToConfiguration;
		}

		return isSheetsAccordingToConfiguration;
	}

	public String getXlsFile() {
		return xlsFile;
	}

	public void setXlsFile(String xlsFile) {
		this.xlsFile = xlsFile;
	}

	public Configuration getConfiguration() {
		return this.configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public Set<PaxguiLanguage> getLanguages() {
		return languages;
	}

	public void setLanguages(Set<PaxguiLanguage> languages) {
		this.languages = languages;
	}

	public String getAirlineId() {
		return airlineId;
	}

	public void setAirlineId(String airlineId) {
		this.airlineId = airlineId;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public FileInputStream getXlsFileInputStream() {
		return xlsFileInputStream;
	}

	public void setXlsFileInputStream(FileInputStream xlsFileInputStream) {
		this.xlsFileInputStream = xlsFileInputStream;
	}

	public String getFileNameWithoutPath() {
		return fileNameWithoutPath;
	}

	public void setFileNameWithoutPath(String fileNameWithoutPath) {
		this.fileNameWithoutPath = fileNameWithoutPath;
	}

}