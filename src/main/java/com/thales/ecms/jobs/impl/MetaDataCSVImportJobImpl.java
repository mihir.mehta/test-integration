package com.thales.ecms.jobs.impl;

import java.io.FileInputStream;
import java.util.Date;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.jobs.MetaDataImportJob;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.IngestionDetail;
import com.thales.ecms.model.PaxguiLanguage;
import com.thales.ecms.model.dto.IngestionDetailDTO;
import com.thales.ecms.service.ConfigurationService;
import com.thales.ecms.service.ContentTypeService;

@Component
public class MetaDataCSVImportJobImpl implements MetaDataImportJob {

	@Autowired
	MessageSource messageSource;

	@Autowired
	ContentTypeService contentTypeService;

	@Autowired
	ConfigurationService configurationService;

	public static Logger logger = LoggerFactory.getLogger(MetaDataXLSImportJobImpl.class);

	private String airlineId = null;

	private Configuration configuration = null;

	private Set<PaxguiLanguage> languages = null;

	private String csvFile = null;

	private FileInputStream csvFileInputStream = null;

	private Date currentDate = null;

	private IngestionDetailDTO ingestionDetailDTO = null;

	public MetaDataCSVImportJobImpl() {
		this.setCurrentDate(new Date());
	}

	@Override
	public IngestionDetail call() throws Exception {
		this.openfile();
		this.process();
		this.closeFile();
		return null;
	}

	@Override
	public void setRequriedObjects(String airlineId, String configurationId, String ingestionDetailId, String fileName)
			throws ObjectNotFoundException {
		// TODO Auto-generated method stub

	}

	@Override
	public void openfile() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean validateFile() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void process() {
		// TODO Auto-generated method stub

	}

	@Override
	public void closeFile() {
		// TODO Auto-generated method stub

	}

	public String getAirlineId() {
		return airlineId;
	}

	public void setAirlineId(String airlineId) {
		this.airlineId = airlineId;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public Set<PaxguiLanguage> getLanguages() {
		return languages;
	}

	public void setLanguages(Set<PaxguiLanguage> languages) {
		this.languages = languages;
	}

	public String getCsvFile() {
		return csvFile;
	}

	public void setCsvFile(String csvFile) {
		this.csvFile = csvFile;
	}

	public FileInputStream getCsvFileInputStream() {
		return csvFileInputStream;
	}

	public void setCsvFileInputStream(FileInputStream csvFileInputStream) {
		this.csvFileInputStream = csvFileInputStream;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public IngestionDetailDTO getIngestionDetailDTO() {
		return ingestionDetailDTO;
	}

	public void setIngestionDetailDTO(IngestionDetailDTO ingestionDetailDTO) {
		this.ingestionDetailDTO = ingestionDetailDTO;
	}

}
