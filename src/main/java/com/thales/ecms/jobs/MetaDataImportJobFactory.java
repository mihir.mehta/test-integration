package com.thales.ecms.jobs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.thales.ecms.jobs.impl.MetaDataCSVImportJobImpl;
import com.thales.ecms.jobs.impl.MetaDataXLSImportJobImpl;

@Component
@Configuration
@ComponentScan(basePackages = "com.thales.ecms.api.jobs")
public class MetaDataImportJobFactory {

	@Bean
	@Scope(value = "prototype")
	public MetaDataImportJob getMetaDataImportJobInstance(String metaDataImportJobType) {
		if (metaDataImportJobType.equalsIgnoreCase("XLS") || metaDataImportJobType.equalsIgnoreCase("XLSX")) {
			return new MetaDataXLSImportJobImpl();

		} else if (metaDataImportJobType.equalsIgnoreCase("CSV")) {
			return new MetaDataCSVImportJobImpl();
		}
		return null;
	}
}
