package com.thales.ecms.model;

import java.util.Set;

import com.psddev.cms.db.Content;

public class ShipmentProfile extends Content {

	@Indexed
	@Required
	private String name;
	
	private int hardDrives;
	
	private ShippingType shippingType;
	
	private Set<Location> locations;
	
	public Set<Location> getLocations() {
		return locations;
	}

	public void setLocations(Set<Location> locations) {
		this.locations = locations;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHardDrives() {
		return hardDrives;
	}

	public void setHardDrives(int hardDrives) {
		this.hardDrives = hardDrives;
	}

	public ShippingType getShippingType() {
		return shippingType;
	}

	public void setShippingType(ShippingType shippingType) {
		this.shippingType = shippingType;
	}

	public enum ShippingType {
		FEDEX, UPS, USPS, DHL
	}
}