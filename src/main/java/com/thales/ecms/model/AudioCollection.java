package com.thales.ecms.model;

import java.util.Date;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.ReferentialText;
//import com.snagfilms.model.Image;

public class AudioCollection extends ContentType{
	
	@Indexed
	private String title;
	
	@Indexed
	private String shortTitle;
	
	@Indexed
	private String longTitle;
	
	@Indexed
	private String artist;
	
	@Indexed
	private Date releaseDate;
	
	@Indexed
	private Rating rating;
	
	@Indexed
	private String distributor;
	
	private ReferentialText synopsis;
	
	private ReferentialText shortSynopsis;
	
	private ReferentialText longSynopsis;
	
	private Image originalImage;
	
	private Image standardImage;
	
	private Image smallImage;
	
	@Indexed
	private String popularityRating;
	
	@Indexed
	private Rating censorRating;
	
//	@Indexed
//	private String themeTag;
//	
//	@Indexed
//	private String styleTag;
//	
//	@Indexed
//	private String moodTag;
//	
//	@Indexed
//	private String standardTag;
//	
//	@Indexed
//	private String resourceKey;
	
	@Indexed
	private String contentPosition;
	
	@Indexed
	private String extendedDataBlob;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortTitle() {
		return shortTitle;
	}

	public void setShortTitle(String shortTitle) {
		this.shortTitle = shortTitle;
	}

	public String getLongTitle() {
		return longTitle;
	}

	public void setLongTitle(String longTitle) {
		this.longTitle = longTitle;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Rating getRating() {
		return rating;
	}

	public void setRating(Rating rating) {
		this.rating = rating;
	}

	public String getDistributor() {
		return distributor;
	}

	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}

	public ReferentialText getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(ReferentialText synopsis) {
		this.synopsis = synopsis;
	}

	public ReferentialText getShortSynopsis() {
		return shortSynopsis;
	}

	public void setShortSynopsis(ReferentialText shortSynopsis) {
		this.shortSynopsis = shortSynopsis;
	}

	public ReferentialText getLongSynopsis() {
		return longSynopsis;
	}

	public void setLongSynopsis(ReferentialText longSynopsis) {
		this.longSynopsis = longSynopsis;
	}

//	public Image getOriginalImage() {
//		return originalImage;
//	}
//
//	public void setOriginalImage(Image originalImage) {
//		this.originalImage = originalImage;
//	}
//
//	public Image getStandardImage() {
//		return standardImage;
//	}
//
//	public void setStandardImage(Image standardImage) {
//		this.standardImage = standardImage;
//	}
//
//	public Image getSmallImage() {
//		return smallImage;
//	}
//
//	public void setSmallImage(Image smallImage) {
//		this.smallImage = smallImage;
//	}

	public String getPopularityRating() {
		return popularityRating;
	}

	public void setPopularityRating(String popularityRating) {
		this.popularityRating = popularityRating;
	}

	public Rating getCensorRating() {
		return censorRating;
	}

	public void setCensorRating(Rating censorRating) {
		this.censorRating = censorRating;
	}

//	public String getThemeTag() {
//		return themeTag;
//	}
//
//	public void setThemeTag(String themeTag) {
//		this.themeTag = themeTag;
//	}
//
//	public String getStyleTag() {
//		return styleTag;
//	}
//
//	public void setStyleTag(String styleTag) {
//		this.styleTag = styleTag;
//	}
//
//	public String getMoodTag() {
//		return moodTag;
//	}
//
//	public void setMoodTag(String moodTag) {
//		this.moodTag = moodTag;
//	}
//
//	public String getStandardTag() {
//		return standardTag;
//	}
//
//	public void setStandardTag(String standardTag) {
//		this.standardTag = standardTag;
//	}
//
//	public String getResourceKey() {
//		return resourceKey;
//	}
//
//	public void setResourceKey(String resourceKey) {
//		this.resourceKey = resourceKey;
//	}

	public String getContentPosition() {
		return contentPosition;
	}

	public void setContentPosition(String contentPosition) {
		this.contentPosition = contentPosition;
	}

	public String getExtendedDataBlob() {
		return extendedDataBlob;
	}

	public void setExtendedDataBlob(String extendedDataBlob) {
		this.extendedDataBlob = extendedDataBlob;
	}
	
}
