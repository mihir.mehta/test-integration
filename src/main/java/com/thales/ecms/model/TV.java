package com.thales.ecms.model;

import com.psddev.dari.db.Recordable.Indexed;

public class TV extends FeatureVideo {

	@Indexed
	private Language subtitleLanguage;

	public Language getSubtitleLanguage() {
		return subtitleLanguage;
	}

	public void setSubtitleLanguage(Language subtitleLanguage) {
		this.subtitleLanguage = subtitleLanguage;
	}
	
}
