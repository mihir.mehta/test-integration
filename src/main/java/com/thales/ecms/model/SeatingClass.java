package com.thales.ecms.model;

import com.psddev.cms.db.Content;

/**
 * @author Mihir
 * Seating class, depending on the airline. For example: First Class, Business, Economy, Economy Plus…
 */
public class SeatingClass extends Content {
	
	@Indexed
	@Required
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
