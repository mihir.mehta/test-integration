package com.thales.ecms.model.dto;

import java.util.List;

public class SpecificContentTypeResponseDTO {

	private String contentTypeInternalId;
	
	private String contentTypeName;
	
	private String parentContentType;
	
	private List<FieldResponseDTO> fields;

	public String getContentTypeInternalId() {
		return contentTypeInternalId;
	}

	public void setContentTypeInternalId(String contentTypeInternalId) {
		this.contentTypeInternalId = contentTypeInternalId;
	}

	public String getContentTypeName() {
		return contentTypeName;
	}

	public void setContentTypeName(String contentTypeName) {
		this.contentTypeName = contentTypeName;
	}

	public String getParentContentType() {
		return parentContentType;
	}

	public void setParentContentType(String parentContentType) {
		this.parentContentType = parentContentType;
	}

	public List<FieldResponseDTO> getFields() {
		return fields;
	}

	public void setFields(List<FieldResponseDTO> fields) {
		this.fields = fields;
	}
}
