package com.thales.ecms.model.dto;

import java.util.List;

public class RuleContentTypeMappingDTO {

	private String contentTypeId;
	
	private String ContentTypeName;
	
	private List<RuleFieldDTO> contentTypeField;

	public String getContentTypeId() {
		return contentTypeId;
	}

	public void setContentTypeId(String contentTypeId) {
		this.contentTypeId = contentTypeId;
	}
	
	public String getContentTypeName() {
		return ContentTypeName;
	}

	public void setContentTypeName(String contentTypeName) {
		ContentTypeName = contentTypeName;
	}

	public List<RuleFieldDTO> getContentTypeField() {
		return contentTypeField;
	}

	public void setContentTypeField(List<RuleFieldDTO> contentTypeField) {
		this.contentTypeField = contentTypeField;
	}
	
}
