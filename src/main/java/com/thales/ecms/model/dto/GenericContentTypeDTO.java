package com.thales.ecms.model.dto;

import java.util.List;

public class GenericContentTypeDTO {

	private String displayName;
	
	private List<FieldDTO> fields;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public List<FieldDTO> getFields() {
		return fields;
	}

	public void setFields(List<FieldDTO> fields) {
		this.fields = fields;
	}
}