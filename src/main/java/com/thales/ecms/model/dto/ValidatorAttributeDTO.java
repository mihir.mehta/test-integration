package com.thales.ecms.model.dto;

public class ValidatorAttributeDTO {

	private boolean required;

	private boolean translatable;

	private String maxChar;

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public boolean isTranslatable() {
		return translatable;
	}

	public void setTranslatable(boolean translatable) {
		this.translatable = translatable;
	}

	public String getMaxChar() {
		return maxChar;
	}

	public void setMaxChar(String maxChar) {
		this.maxChar = maxChar;
	}	
	
}
