package com.thales.ecms.model.dto;

public class SeatingClassDTO {

	private String seatingClassInternalId;
	
	private String name;

	public String getSeatingClassInternalId() {
		return seatingClassInternalId;
	}

	public void setSeatingClassInternalId(String seatingClassInternalId) {
		this.seatingClassInternalId = seatingClassInternalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
