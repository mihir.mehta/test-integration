package com.thales.ecms.model.dto;


/**
 * DTO class for Route
 * @author shruti_n
 *
 */
public class RouteDTO 
{
	private String routeId;
	private String name;
	private AirportDTO origin;
	private AirportDTO destination;

	/**
	 * getter and setters for Route ID 
	 * @return routeId
	 */
	public String getRouteId() {
		return routeId;
	}
	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}
	
	/**
	 * getter and setters for Route name 
	 * @return name
	 */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * getter and setters for Route origin 
	 * @return origin
	 */
	public AirportDTO getOrigin() {
		return origin;
	}
	public void setOrigin(AirportDTO origin) {
		this.origin = origin;
	}
	
	/**
	 * getter and setters for Route destination 
	 * @return destination
	 */
	public AirportDTO getDestination() {
		return destination;
	}
	public void setDestination(AirportDTO destination) {
		this.destination = destination;
	}
	
}
