package com.thales.ecms.model.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Mihir
 * Request DTO - Used when creating a a Title
 */
public class TitleRequestDTO {

	@ApiModelProperty(required = true)
	private String configurationId;
	
	private String englishTitle;
	
	@ApiModelProperty(required = true)
	private List<TitleFieldRequestDTO> fields;

	public String getConfigurationId() {
		return configurationId;
	}

	public void setConfigurationId(String configurationId) {
		this.configurationId = configurationId;
	}

	public String getEnglishTitle() {
		return englishTitle;
	}

	public void setEnglishTitle(String englishTitle) {
		this.englishTitle = englishTitle;
	}

	public List<TitleFieldRequestDTO> getFields() {
		return fields;
	}

	public void setFields(List<TitleFieldRequestDTO> fields) {
		this.fields = fields;
	}
}
