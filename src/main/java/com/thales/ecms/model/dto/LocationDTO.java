package com.thales.ecms.model.dto;

public class LocationDTO {

	private String locationInternalId;
	
	private String locationTypeId;


	public String getLocationInternalId() {
		return locationInternalId;
	}

	public void setLocationInternalId(String locationInternalId) {
		this.locationInternalId = locationInternalId;
	}

	public String getLocationTypeId() {
		return locationTypeId;
	}

	public void setLocationTypeId(String locationTypeId) {
		this.locationTypeId = locationTypeId;
	}

}
