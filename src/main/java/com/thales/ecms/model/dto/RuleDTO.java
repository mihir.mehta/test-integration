package com.thales.ecms.model.dto;

import java.util.Date;

public class RuleDTO {

	private String ruleId;

	private String ruleName;	
	
	private RuleContentTypeMappingDTO ruleContentTypeMappingDTO;
	
	private long creationDate;
	
	private long lastModifiedDate;

	public long getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(long creationDate) {
		this.creationDate = creationDate;
	}

	public long getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(long lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	public String getRuleName() {
		return ruleName;
	}	

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public RuleContentTypeMappingDTO getRuleContentTypeMappingDTO() {
		return ruleContentTypeMappingDTO;
	}

	public void setRuleContentTypeMappingDTO(RuleContentTypeMappingDTO ruleContentTypeMappingDTO) {
		this.ruleContentTypeMappingDTO = ruleContentTypeMappingDTO;
	}

}
