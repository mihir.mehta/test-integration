package com.thales.ecms.model.dto;

public class SeatingClassLruTypeMappingDTO {

	private String seatingClassName;
	
	private String lruTypeName;

	public String getSeatingClassName() {
		return seatingClassName;
	}

	public void setSeatingClassName(String seatingClassName) {
		this.seatingClassName = seatingClassName;
	}

	public String getLruTypeName() {
		return lruTypeName;
	}

	public void setLruTypeName(String lruTypeName) {
		this.lruTypeName = lruTypeName;
	}
}
