package com.thales.ecms.model.dto;

public class FieldResponseDTO {

	private String fieldInternalId;
	
	private String fieldId;
	
	private String displayName;
	
	private String fieldType;
	
	private String selectionListName;

	public String getFieldInternalId() {
		return fieldInternalId;
	}

	public void setFieldInternalId(String fieldInternalId) {
		this.fieldInternalId = fieldInternalId;
	}

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getSelectionListName() {
		return selectionListName;
	}

	public void setSelectionListName(String selectionListName) {
		this.selectionListName = selectionListName;
	}

}
