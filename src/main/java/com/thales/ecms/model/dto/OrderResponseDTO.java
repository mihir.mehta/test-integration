package com.thales.ecms.model.dto;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.thales.ecms.model.Order.Status;

public class OrderResponseDTO {

	private String orderName;

	private String orderId;

	private String orderDescription;

	private Date orderStartDate;

	private Date orderEndDate;
	
	private Date lastModifiedDate;

	private Status orderStatus;
	
	private Date shipmentDeadline;
	
	private Date contentDeadline;
	
	private Date assetDeadline;
	
	private Set<ConfigurationDTO> configureDTOSet;

	private List<DisplayGroupDTO> displayGroupList;

	public Set<ConfigurationDTO> getConfigureDTOSet() {
		return configureDTOSet;
	}

	public void setConfigureDTOSet(Set<ConfigurationDTO> configureDTOSet) {
		this.configureDTOSet = configureDTOSet;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderDescription() {
		return orderDescription;
	}

	public void setOrderDescription(String orderDescription) {
		this.orderDescription = orderDescription;
	}

	public Date getOrderStartDate() {
		return orderStartDate;
	}

	public void setOrderStartDate(Date orderStartDate) {
		this.orderStartDate = orderStartDate;
	}

	public Date getOrderEndDate() {
		return orderEndDate;
	}

	public void setOrderEndDate(Date orderEndDate) {
		this.orderEndDate = orderEndDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Status getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Status orderStatus) {
		this.orderStatus = orderStatus;
	}


	public Date getShipmentDeadline() {
		return shipmentDeadline;
	}

	public void setShipmentDeadline(Date shipmentDeadline) {
		this.shipmentDeadline = shipmentDeadline;
	}

	public Date getContentDeadline() {
		return contentDeadline;
	}

	public void setContentDeadline(Date contentDeadline) {
		this.contentDeadline = contentDeadline;
	}

	public Date getAssetDeadline() {
		return assetDeadline;
	}

	public void setAssetDeadline(Date assetDeadline) {
		this.assetDeadline = assetDeadline;
	}

	public List<DisplayGroupDTO> getDisplayGroupList() {
		return displayGroupList;
	}

	public void setDisplayGroupList(List<DisplayGroupDTO> displayGroupList) {
		this.displayGroupList = displayGroupList;
	}


	public static class DisplayGroupDTO {

		private String month;

		private Date startDate;

		private Date endDate;

		public String getMonth() {
			return month;
		}

		public void setMonth(String month) {
			this.month = month;
		}

		public Date getStartDate() {
			return startDate;
		}

		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}

		public Date getEndDate() {
			return endDate;
		}

		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}
	}


}
