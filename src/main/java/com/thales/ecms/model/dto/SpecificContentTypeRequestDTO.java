package com.thales.ecms.model.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class SpecificContentTypeRequestDTO {

	@ApiModelProperty(required = true)
	private String contentTypeName;
	
	@ApiModelProperty(required = true)
	private String parentContentType;
	
	private List<FieldRequestDTO> fields;

	public String getContentTypeName() {
		return contentTypeName;
	}

	public void setContentTypeName(String contentTypeName) {
		this.contentTypeName = contentTypeName;
	}

	public String getParentContentType() {
		return parentContentType;
	}

	public void setParentContentType(String parentContentType) {
		this.parentContentType = parentContentType;
	}

	public List<FieldRequestDTO> getFields() {
		return fields;
	}

	public void setFields(List<FieldRequestDTO> fields) {
		this.fields = fields;
	}
}
