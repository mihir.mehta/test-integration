package com.thales.ecms.model.dto;

public class MetaDataImportDTO {
	private String airlineId;
	private String configurationId;
	private String ingestionDetailId;
	

	private String fileName;

	public String getAirlineId() {
		return airlineId;
	}

	public void setAirlineId(String airlineId) {
		this.airlineId = airlineId;
	}

	public String getConfigurationId() {
		return configurationId;
	}

	public void setConfigurationId(String configurationId) {
		this.configurationId = configurationId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getIngestionDetailId() {
		return ingestionDetailId;
	}

	public void setIngestionDetailId(String ingestionDetailId) {
		this.ingestionDetailId = ingestionDetailId;
	}

}
