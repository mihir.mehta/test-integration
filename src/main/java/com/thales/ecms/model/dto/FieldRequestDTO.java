package com.thales.ecms.model.dto;

import io.swagger.annotations.ApiModelProperty;

public class FieldRequestDTO {

	@ApiModelProperty(value = "Unique name of the Field", required = true)
	private String fieldId;
	
	@ApiModelProperty(value = "Display Name of the Field in a Content Type. This value should only be supplied when creating a Content Type, not when creating the Field itself")
	private String displayName;
	
	@ApiModelProperty(required = true, allowableValues="text, long text, integer, long, float, boolean, date, selection, image")
	private String fieldType;
	
	@ApiModelProperty(value = "The name of the list, required when the fieldType is selection")
	private String selectionListName;
	
	@ApiModelProperty(hidden = true)
	private boolean indexed;

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	
	public String getSelectionListName() {
		return selectionListName;
	}

	public void setSelectionListName(String selectionListName) {
		this.selectionListName = selectionListName;
	}

	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(boolean indexed) {
		this.indexed = indexed;
	}
	
}
