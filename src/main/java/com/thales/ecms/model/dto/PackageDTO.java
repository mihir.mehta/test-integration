package com.thales.ecms.model.dto;

import java.util.Date;
import java.util.List;

import com.thales.ecms.model.Configuration.ConfigurationTitle.Priority;
import com.thales.ecms.model.ContentType;
import com.thales.ecms.model.dto.OrderResponseDTO.DisplayGroupDTO;
import com.thales.ecms.model.util.DisplayGroup;

public class PackageDTO {
	
	private String packageId;

	private String name;

	private Date packagingDate;

	private String orderName;
	
	private String orderId;

	private List<DisplayGroupDTO> displayGroupDTO;

	private long fullSize;

	private String configurationName;
	
	private String configurationId;

	private List<PackageTitleDTO> packageTitles;

	public static class PackageTitleDTO {
		
		private ContentTypeDTO content;

		private String contentName;
		
		private String contentType;

		private String contentStatus;

		private Priority priority;

		private List<DisplayGroupDTO> displayGroupDTO;

		private List<String> destinations;
		
		public ContentTypeDTO getContent() {
			return content;
		}

		public void setContent(ContentTypeDTO content) {
			this.content = content;
		}

		public String getContentName() {
			return contentName;
		}

		public void setContentName(String contentName) {
			this.contentName = contentName;
		}

		public String getContentType() {
			return contentType;
		}

		public void setContentType(String contentType) {
			this.contentType = contentType;
		}

		public String getContentStatus() {
			return contentStatus;
		}

		public void setContentStatus(String contentStatus) {
			this.contentStatus = contentStatus;
		}

		public Priority getPriority() {
			return priority;
		}

		public void setPriority(Priority priority) {
			this.priority = priority;
		}


		public List<DisplayGroupDTO> getDisplayGroupDTO() {
			return displayGroupDTO;
		}

		public void setDisplayGroupDTO(List<DisplayGroupDTO> displayGroupDTO) {
			this.displayGroupDTO = displayGroupDTO;
		}

		public List<String> getDestinations() {
			return destinations;
		}

		public void setDestinations(List<String> destinations) {
			this.destinations = destinations;
		}
	}

	public String getPackageId() {
		return packageId;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getPackagingDate() {
		return packagingDate;
	}

	public void setPackagingDate(Date packagingDate) {
		this.packagingDate = packagingDate;
	}


	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public List<DisplayGroupDTO> getDisplayGroupDTO() {
		return displayGroupDTO;
	}

	public void setDisplayGroupDTO(List<DisplayGroupDTO> displayGroupDTO) {
		this.displayGroupDTO = displayGroupDTO;
	}

	public long getFullSize() {
		return fullSize;
	}

	public void setFullSize(long fullSize) {
		this.fullSize = fullSize;
	}

	public String getConfigurationName() {
		return configurationName;
	}

	public void setConfigurationName(String configurationName) {
		this.configurationName = configurationName;
	}

	public String getConfigurationId() {
		return configurationId;
	}

	public void setConfigurationId(String configurationId) {
		this.configurationId = configurationId;
	}

	public List<PackageTitleDTO> getPackageTitles() {
		return packageTitles;
	}

	public void setPackageTitles(List<PackageTitleDTO> packageTitles) {
		this.packageTitles = packageTitles;
	}

}
