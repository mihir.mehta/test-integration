package com.thales.ecms.model.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * DTO for Airport
 * @author shruti_n 
 */

public class AirportDTO {

	/**
	 * Airport Id
	 */
	private String airportId;

	/**
	 * Airport Name
	 */
	private String airportName;

	/**
	 * Airport ICAO code
	 * ICAO-International Civil Aviation Organization
	 */
	private String airportICAO;

	/**
	 * Airport IATA code
	 * IATA-International Air Transport Association
	 */
	private String airportIATA;

	public String getAirportId() {
		return airportId;
	}

	public void setAirportId(String airportId) {
		this.airportId = airportId;
	}

	@ApiModelProperty(required = true)
	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	@ApiModelProperty(example = "ICAO", required = true)
	public String getAirportICAO() {
		return airportICAO;
	}

	public void setAirportICAO(String airportICAO) {
		this.airportICAO = airportICAO;
	}

	@ApiModelProperty(example = "IATA", required = true)
	public String getAirportIATA() {
		return airportIATA;
	}

	public void setAirportIATA(String airportIATA) {
		this.airportIATA = airportIATA;
	}
}
