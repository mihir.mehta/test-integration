package com.thales.ecms.model.dto;

import java.util.List;

public class SpecificContentTypeDTO {

	private String displayName;
	
	private String genericContentTypeName;
	
	List<FieldDTO> fields;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getGenericContentTypeName() {
		return genericContentTypeName;
	}

	public void setGenericContentTypeName(String genericContentTypeName) {
		this.genericContentTypeName = genericContentTypeName;
	}

	public List<FieldDTO> getFields() {
		return fields;
	}

	public void setFields(List<FieldDTO> fields) {
		this.fields = fields;
	}
	
	
}
