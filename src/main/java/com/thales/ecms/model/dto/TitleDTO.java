package com.thales.ecms.model.dto;

import java.util.List;

public class TitleDTO {

	private String titleInternalId;
	
	private List<TitleFieldDTO> titleFields;

	public String getTitleInternalId() {
		return titleInternalId;
	}

	public void setTitleInternalId(String titleInternalId) {
		this.titleInternalId = titleInternalId;
	}

	public List<TitleFieldDTO> getTitleFields() {
		return titleFields;
	}

	public void setTitleFields(List<TitleFieldDTO> titleFields) {
		this.titleFields = titleFields;
	}
}