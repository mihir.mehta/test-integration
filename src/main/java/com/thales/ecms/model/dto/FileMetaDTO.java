package com.thales.ecms.model.dto;

import com.thales.ecms.utils.ECMSStringUtils;

public class FileMetaDTO {
	private String airlineId;
	private String fileName;
	private String fileSize;
	private String fileType;
	private boolean status;
	private String reason;

	public String getAirlineId() {
		return airlineId;
	}

	public void setAirlineId(String airlineId) {
		this.airlineId = airlineId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSizeInBytes) {
		this.fileSize = ECMSStringUtils.humanReadableByteCount(fileSizeInBytes, true);
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
