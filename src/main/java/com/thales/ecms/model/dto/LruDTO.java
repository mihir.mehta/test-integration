package com.thales.ecms.model.dto;

public class LruDTO {

	private String lruInternalId;
	
	private String lruName;
	
	private String lruTypeInternalId;
	
	private String lruTypeName;

	
	public String getLruInternalId() {
		return lruInternalId;
	}

	public void setLruInternalId(String lruInternalId) {
		this.lruInternalId = lruInternalId;
	}

	public String getLruName() {
		return lruName;
	}

	public void setLruName(String lruName) {
		this.lruName = lruName;
	}

	public String getLruTypeInternalId() {
		return lruTypeInternalId;
	}

	public void setLruTypeInternalId(String lruTypeInternalId) {
		this.lruTypeInternalId = lruTypeInternalId;
	}
	
	public String getLruTypeName() {
		return lruTypeName;
	}

	public void setLruTypeName(String lruTypeName) {
		this.lruTypeName = lruTypeName;
	}
}
