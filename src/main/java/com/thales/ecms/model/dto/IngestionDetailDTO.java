package com.thales.ecms.model.dto;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class IngestionDetailDTO {

	private String ingestionDetailInternalId;
	
	private String fileName;
	
	private Date ingestedDate;
	
	@ApiModelProperty(allowableValues = "Uploaded, Imported")
	private String mode;
	
	private String status;
	
	private String configuration;
	
	private String configurationId;
	
	private long titleCount;
	
	private List<IngestionTitleDTO> ingestionTitleDTOList;
	
	private ImportStatusDTO importStatus;
	
	public String getIngestionDetailInternalId() {
		return ingestionDetailInternalId;
	}

	public void setIngestionDetailInternalId(String ingestionDetailInternalId) {
		this.ingestionDetailInternalId = ingestionDetailInternalId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getIngestedDate() {
		return ingestedDate;
	}

	public void setIngestedDate(Date ingestedDate) {
		this.ingestedDate = ingestedDate;
	}
	
	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getConfiguration() {
		return configuration;
	}

	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	public long getTitleCount() {
		return titleCount;
	}

	public void setTitleCount(long titleCount) {
		this.titleCount = titleCount;
	}

	public List<IngestionTitleDTO> getIngestionTitleDTOList() {
		return ingestionTitleDTOList;
	}

	public void setIngestionTitleDTOList(List<IngestionTitleDTO> ingestionTitleDTOList) {
		this.ingestionTitleDTOList = ingestionTitleDTOList;
	}

	public String getConfigurationId() {
		return configurationId;
	}

	public void setConfigurationId(String configurationId) {
		this.configurationId = configurationId;
	}

	public ImportStatusDTO getImportStatus() {
		return importStatus;
	}

	public void setImportStatus(ImportStatusDTO importStatus) {
		this.importStatus = importStatus;
	}
	
	
}
