package com.thales.ecms.model.dto;

public class ImportStatusTitleDTO {
	
	private String exception;
	private String userFriendlyMessage;

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getUserFriendlyMessage() {
		return userFriendlyMessage;
	}

	public void setUserFriendlyMessage(String userFriendlyMessage) {
		this.userFriendlyMessage = userFriendlyMessage;
	}

}
