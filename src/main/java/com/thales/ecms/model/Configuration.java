package com.thales.ecms.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.psddev.cms.db.Content;
import com.psddev.cms.db.ToolUi;
import com.psddev.dari.db.ObjectType;
import com.psddev.dari.db.State;

/**
 * @author Mihir
 * A Configuration of an Airline represents the different available configurations (and therefore available fields) of metadata for an airline.
 * Configurations relate to the seating/class configuration for a particular type of aircraft, the different LRUs on the aircraft type for the airline, 
 * and the software running on those IFE systems.
 */
public class Configuration extends Content {
	
	@Required
	@Indexed
	private String name;
	
	private String version;
	
	@Indexed
	@ToolUi.Note("AVANT, i4000, i5000, etc.")
	private String platform;

	@Indexed
	private ConfigurationStatus configurationStatus;
		
	private PackagingType packagingType;
	
	@Indexed
	private List<Lopa> lopaList;
	
	private Set<PaxguiLanguage> interfaceLanguages;
	
	@Indexed
	@DisplayName("LRUs")
	private Set<Lru> lrus;
	
	@Indexed
	@DisplayName("LRU Types")
	@ToolUi.DropDown
	private Set<LruType> lruTypes;
	
//	@Indexed
//	@DisplayName("Server Groups")
//	private List<ServerGroup> serverGroupList;

	@Indexed
	private Set<ObjectType> contentTypeList;
	
	@Indexed
	private Set<Rule> rules;
	
	@Indexed
	@DisplayName("Titles")
	private List<ConfigurationTitle> titleList;
	
	@ToolUi.Hidden
	private ConfigurationStatus cfgPreviousStatus;	
	
	private Date creationDate;
	
	private Date lastModifiedDate;
	
//	/**
//	 * Logical grouping of LRUs 
//	 */
//	@Content.Embedded
//	public static class ServerGroup extends Content {
//		
//		@Indexed
//		@Required
//		private String name;
//		
//		@Indexed
//		@Required
//		@DisplayName("LRUs")
//		private Set<Lru> lrus;
//
//		public String getName() {
//			return name;
//		}
//
//		public void setName(String name) {
//			this.name = name;
//		}
//
//		public Set<Lru> getLrus() {
//			return lrus;
//		}
//
//		public void setLrus(Set<Lru> lrus) {
//			this.lrus = lrus;
//		}
//	}
	
	@Content.Embedded
	@Content.LabelFields("content")
	@DisplayName("Title")
	public static class ConfigurationTitle extends Content {
		
		@Indexed
		private ContentType content;
		
		@Indexed
		private Status contentStatus;
		
		private Date startDate;
		
		private Date endDate;
		
		private Priority priority;
		
		@ToolUi.Note("The List of destinations for this Title")
		private List<LruType> destinations;

		public ContentType getContent() {
			return content;
		}

		public void setContent(ContentType content) {
			this.content = content;
		}

		public Status getContentStatus() {
			return contentStatus;
		}

		public void setContentStatus(Status contentStatus) {
			this.contentStatus = contentStatus;
		}

		public Date getStartDate() {
			return startDate;
		}

		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}

		public Date getEndDate() {
			return endDate;
		}

		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}
		
		public Priority getPriority() {
			return priority;
		}

		public void setPriority(Priority priority) {
			this.priority = priority;
		}
		
		public List<LruType> getDestinations() {
			return destinations;
		}

		public void setDestinations(List<LruType> destinations) {
			this.destinations = destinations;
		}
		
		public enum Status {
			Incomplete, Validated, Completed
		}
		
		public enum Priority {
			Default(1), High(5);
			
			int value;
			
			Priority(int value){
				this.value = value;
			}
			
			public int getValue(){
				return value;
			}
		}
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
	public ConfigurationStatus getConfigurationStatus() {
		return configurationStatus;
	}

	public void setConfigurationStatus(ConfigurationStatus configurationStatus) {
		this.configurationStatus = configurationStatus;
	}

	public PackagingType getPackagingType() {
		return packagingType;
	}

	public void setPackagingType(PackagingType packagingType) {
		this.packagingType = packagingType;
	}

	public List<Lopa> getLopaList() {
		return lopaList;
	}

	public void setLopaList(List<Lopa> lopaList) {
		this.lopaList = lopaList;
	}

	public Set<PaxguiLanguage> getInterfaceLanguages() {
		return interfaceLanguages;
	}

	public void setInterfaceLanguages(Set<PaxguiLanguage> interfaceLanguages) {
		this.interfaceLanguages = interfaceLanguages;
	}
	
	public Set<Lru> getLrus() {
		return lrus;
	}

	public void setLrus(Set<Lru> lrus) {
		this.lrus = lrus;
	}

	public Set<LruType> getLruTypes() {
		return lruTypes;
	}

	public void setLruTypes(Set<LruType> lruTypes) {
		this.lruTypes = lruTypes;
	}

//	public List<ServerGroup> getServerGroupList() {
//		return serverGroupList;
//	}
//
//	public void setServerGroupList(List<ServerGroup> serverGroupList) {
//		this.serverGroupList = serverGroupList;
//	}
	
	public Set<ObjectType> getContentTypeList() {
		return contentTypeList;
	}

	public void setContentTypeList(Set<ObjectType> contentTypeList) {
		this.contentTypeList = contentTypeList;
	}

	public Set<Rule> getRules() {
		return rules;
	}

	public void setRules(Set<Rule> rules) {
		this.rules = rules;
	}
	
	public List<ConfigurationTitle> getTitleList() {
		return titleList;
	}

	public void setTitleList(List<ConfigurationTitle> titleList) {
		this.titleList = titleList;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public enum PackagingType {
		CIL, XCIL
	}
	
	public enum ConfigurationStatus{
		ACTIVE, INACTIVE, DEPRECATED
	}
	
	/**
	 * Method triggered by BrightSpot Framework before saving an object to database
	 * 
	 * @author arjun.p
	 */
	@Override
    public void beforeSave() {		
		State state = this.getState();			
		if(state.isNew()){
			cfgPreviousStatus=getConfigurationStatus();
		}else{
			checkState(state);
		}
    }
	
	/**
	 * @param state A State is an object from DARI API
	 * 
	 * @author arjun.p  
	 */
	private void checkState(State state){		
		if(cfgPreviousStatus!=null) {
			if(!state.isNew() && cfgPreviousStatus == ConfigurationStatus.INACTIVE){
				if(getConfigurationStatus()==ConfigurationStatus.INACTIVE){
					state.addError(state.getField("configurationStatus"), "Can Not Update! Configuration Status is "+ConfigurationStatus.INACTIVE);					
				}else{
					state.getErrors(state.getField("configurationStatus")).clear();// clearing the list for ObjectField
				}
			}else{
				state.getErrors(state.getField("configurationStatus")).clear();// clearing the list for ObjectField
			}
		}
	}	

	/**
	 * set cfgPreviousStatus before commit so that it can be used on next modification
	 * 
	 * @author arjun.p  
	 */
	@Override
	protected void beforeCommit() {
		cfgPreviousStatus=getConfigurationStatus();		
		super.beforeCommit();
	}
}
