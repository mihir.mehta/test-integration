package com.thales.ecms.model;

import java.util.List;

import com.psddev.cms.db.Content;

public class PlayList extends Content{

	private String title;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<Ad> getPlayList() {
		return playList;
	}
	public void setPlayList(List<Ad> playList) {
		this.playList = playList;
	}
	private List<Ad> playList;
	
}
