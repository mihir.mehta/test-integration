package com.thales.ecms.model;

import com.psddev.cms.db.Content;

public class Ad extends Content {

	private String name;
	
	private String asset;
	
	private int position;
	
	private Type type;
	
	public enum Type {
		PREROLL, POSTROLL
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAsset() {
		return asset;
	}

	public void setAsset(String asset) {
		this.asset = asset;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
	
	
	
}
