package com.thales.ecms.model;

import java.util.Set;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.Required;

/**
 * @author Mihir
 * A logical group of routes specific to the airline, e.g., North American, Short Haul, etc.
 */
public class RouteGroup extends Content {

	@Indexed
	@Required
	private String name;
	
	@Indexed
	@Required
	private Set<Route> routes;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(Set<Route> routes) {
		this.routes = routes;
	}
}
