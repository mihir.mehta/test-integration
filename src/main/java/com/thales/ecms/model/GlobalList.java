package com.thales.ecms.model;

import java.util.Set;

import com.psddev.cms.db.Content;

public class GlobalList extends Content {

	@Indexed
	@Required
	private String name; 
	
	@Indexed
	private Set<String> values;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getValues() {
		return values;
	}

	public void setValues(Set<String> values) {
		this.values = values;
	}
}
