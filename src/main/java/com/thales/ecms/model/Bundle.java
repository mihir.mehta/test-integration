package com.thales.ecms.model;

import java.util.Date;
import java.util.Set;

import com.psddev.cms.db.Content;

public class Bundle extends Content {

	@Indexed
	private String bundleName;
	
	@Indexed
	private Set<SeatingClassPriceMapping> seatingClassPriceMappings;
	
	private Date creationDate;
	
	private Date lastModifiedDate;
	
	@Content.Embedded
	@DisplayName("Seating Class")
	@Content.LabelFields("seatingClass")
	public static class SeatingClassPriceMapping extends Content {
		
		@Indexed
		private SeatingClass seatingClass;
		
		@DisplayName("Pay Per Access")
		private double ppa;
		
		@DisplayName("Pay Per View")
		private double ppv;

		public SeatingClass getSeatingClass() {
			return seatingClass;
		}

		public void setSeatingClass(SeatingClass seatingClass) {
			this.seatingClass = seatingClass;
		}

		public double getPpa() {
			return ppa;
		}

		public void setPpa(double ppa) {
			this.ppa = ppa;
		}

		public double getPpv() {
			return ppv;
		}

		public void setPpv(double ppv) {
			this.ppv = ppv;
		}

	}
	
	public String getBundleName() {
		return bundleName;
	}

	public void setBundleName(String bundleName) {
		this.bundleName = bundleName;
	}

	public Set<SeatingClassPriceMapping> getSeatingClassPriceMappings() {
		return seatingClassPriceMappings;
	}

	public void setSeatingClassPriceMappings(Set<SeatingClassPriceMapping> seatingClassPriceMappings) {
		this.seatingClassPriceMappings = seatingClassPriceMappings;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}
