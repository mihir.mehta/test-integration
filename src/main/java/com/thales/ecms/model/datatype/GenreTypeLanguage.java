package com.thales.ecms.model.datatype;

import java.util.Set;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;
import com.thales.ecms.model.Genre;
import com.thales.ecms.model.PaxguiLanguage;

@Content.Embedded
@DisplayName("Language-Specific Genre Values")
public class GenreTypeLanguage extends Content {

	@Indexed
	private PaxguiLanguage language;
	
	@Indexed
	private Set<Genre> values;

	public PaxguiLanguage getLanguage() {
		return language;
	}

	public void setLanguage(PaxguiLanguage language) {
		this.language = language;
	}

	public Set<Genre> getValues() {
		return values;
	}

	public void setValues(Set<Genre> values) {
		this.values = values;
	}
}
