package com.thales.ecms.model.datatype;

import java.util.Set;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;

@Content.Embedded
@DisplayName("Configuration-Specific Double Values")
public class DoubleType extends BaseType {

	@Indexed
	private Set<Double> values;

	public Set<Double> getValues() {
		return values;
	}

	public void setValues(Set<Double> values) {
		this.values = values;
	}
}
