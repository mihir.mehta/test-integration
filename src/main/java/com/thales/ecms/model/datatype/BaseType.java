package com.thales.ecms.model.datatype;

import java.util.Set;

import com.psddev.cms.db.Content;
import com.thales.ecms.model.Configuration;

@Content.LabelFields("configuration")
public abstract class BaseType extends Content {

	@Indexed
	protected Configuration configuration;

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
	
	//private String language;
	
}
