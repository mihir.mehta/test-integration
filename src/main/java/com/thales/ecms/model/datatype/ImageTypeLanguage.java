package com.thales.ecms.model.datatype;

import java.util.List;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;
import com.thales.ecms.model.Image;
import com.thales.ecms.model.PaxguiLanguage;

@Content.Embedded
@DisplayName("Language-Specific Image Values")
public class ImageTypeLanguage extends Content {

	@Indexed
	private PaxguiLanguage language;
	
	@Indexed
	private List<Image> values;

	public PaxguiLanguage getLanguage() {
		return language;
	}

	public void setLanguage(PaxguiLanguage language) {
		this.language = language;
	}

	public List<Image> getValues() {
		return values;
	}

	public void setValues(List<Image> values) {
		this.values = values;
	}
}
