package com.thales.ecms.model.datatype;

import java.util.List;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;
import com.thales.ecms.model.PaxguiLanguage;

@Content.Embedded
@DisplayName("Language-Specific Text Values")
public class TextTypeLanguage extends Content{

	@Indexed
	private PaxguiLanguage language;
	
	@Indexed
	private List<String> values;

	public PaxguiLanguage getLanguage() {
		return language;
	}

	public void setLanguage(PaxguiLanguage language) {
		this.language = language;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}
}
