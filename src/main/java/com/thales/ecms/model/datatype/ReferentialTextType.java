package com.thales.ecms.model.datatype;

import java.util.List;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;

@Content.Embedded
@DisplayName("Configuration-Specific Long Text Values")
public class ReferentialTextType extends BaseType {

//	@Indexed
//	private List<ReferentialText> values;
//
//	public List<ReferentialText> getValues() {
//		return values;
//	}
//
//	public void setValues(List<ReferentialText> values) {
//		this.values = values;
//	}
	
	@Indexed
	private List<ReferentialTextTypeLanguage> values;

	public List<ReferentialTextTypeLanguage> getValues() {
		return values;
	}

	public void setValues(List<ReferentialTextTypeLanguage> values) {
		this.values = values;
	}
}
