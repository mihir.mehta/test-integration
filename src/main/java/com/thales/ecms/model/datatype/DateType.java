package com.thales.ecms.model.datatype;

import java.util.Set;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;

@Content.Embedded
@DisplayName("Configuration-Specific Date Values")
public class DateType extends BaseType {

	@Indexed
	private Set<DateTypeUtil> values;

	public Set<DateTypeUtil> getValues() {
		return values;
	}

	public void setValues(Set<DateTypeUtil> values) {
		this.values = values;
	}
}
