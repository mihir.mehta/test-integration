package com.thales.ecms.model.datatype;

import java.util.Set;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;

@Content.Embedded
@DisplayName("Configuration-Specific Boolean Values")
public class BooleanType extends BaseType {

	@Indexed
	private Set<Boolean> values;

	public Set<Boolean> getValues() {
		return values;
	}

	public void setValues(Set<Boolean> values) {
		this.values = values;
	}
}
