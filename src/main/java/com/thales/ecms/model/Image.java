package com.thales.ecms.model;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable;
import com.psddev.dari.db.Recordable.Indexed;
import com.psddev.dari.util.StorageItem;

@Recordable.PreviewField("file")
public class Image extends Content {
	
	@Indexed
    private String name;
	
	private StorageItem file;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public StorageItem getFile() {
		return file;
	}

	public void setFile(StorageItem file) {
		this.file = file;
	}

}
