package com.thales.ecms.model;

import java.util.ArrayList;
import java.util.List;

import com.psddev.cms.tool.Plugin;
import com.psddev.cms.tool.Tool;

public class RuleEngine extends Tool{

	@Override
    public List<Plugin> getPlugins() {
        List<Plugin> plugins = new ArrayList();

        /*plugins.add(createArea2("Create An Order", "orderManagement",
                "orderManagement", "/cms/content/createAnOrder.jsp"));*/
        
        plugins.add(createArea2("Rule Engine", "ruleEngine",
                "dashboard/ruleEngine", "/cms/content/ruleEngine.jsp"));

        return plugins;
    }
}
