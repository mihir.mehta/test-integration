package com.thales.ecms.model;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Record;
import com.psddev.dari.db.Recordable.Embedded;
import com.psddev.dari.db.Recordable.Indexed;

@Embedded
public class ValidatorAttribute extends Content{

	@Indexed
	private boolean required;

	@Indexed
	private boolean translatable;

	@Indexed
	private String maxChar;	

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public boolean isTranslatable() {
		return translatable;
	}

	public void setTranslatable(boolean translatable) {
		this.translatable = translatable;
	}

	public String getMaxChar() {
		return maxChar;
	}

	public void setMaxChar(String maxChar) {
		this.maxChar = maxChar;
	}			
}