package com.thales.ecms.model.airline;

import java.util.Date;
import java.util.List;

import com.psddev.cms.db.ToolUi;
import com.psddev.dari.db.Modification;

/**
 * @author Mihir
 *
 */
public class AirlinePortal extends Modification<com.psddev.cms.db.Site>{

	@ToolUi.Note("The status of the Airline")
	@DisplayName("Status")
	private AirlineStatus airlineStatus = AirlineStatus.ACTIVE;

	private String companyName;
	
	private String email;
	
	private String phone;
	
	private String fax;
	
	@Indexed
	@ToolUi.Note("Code Share Details, e.g., Star Alliance, One World, etc.")
	private String codeShare;
	
	@Indexed
	private List<AirlineContact> airlineContactList;
	
	@Required
	@DisplayName("ICAO Code")
	@Indexed
//	@Maximum(4)
	private String icaoCode;
	
	@DisplayName("IATA Code")
	@Indexed
//	@Maximum(3)
	private String iataCode;
	
	@DisplayName("Shipment Start Date")
	private Date shipmentDeadlineStartDate;
	
	@DisplayName("Shipment End Date")
	@ToolUi.Note("Cannot be later than 30 days from the start date")
	private Date shipmentDeadlineEndDate;
	
	@DisplayName("Content Start Date")
	private Date contentDeadlineStartDate;
	
	@DisplayName("Content End Date")
	@ToolUi.Note("Cannot be later than 90 days from the start date")
	private Date contentDeadlineEndDate;
	
	@DisplayName("Asset Start Date")
	private Date assetDeadlineStartDate;
	
	@DisplayName("Asset End Date")
	@ToolUi.Note("Cannot be later than 90 days from the start date")
	private Date assetDeadlineEndDate;

	public enum AirlineStatus {
		ACTIVE, INACTIVE
	}

	public AirlineStatus getAirlineStatus() {
		return airlineStatus;
	}

	public void setAirlineStatus(AirlineStatus airlineStatus) {
		this.airlineStatus = airlineStatus;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCodeShare() {
		return codeShare;
	}

	public void setCodeShare(String codeShare) {
		this.codeShare = codeShare;
	}

	public List<AirlineContact> getAirlineContactList() {
		return airlineContactList;
	}

	public void setAirlineContactList(List<AirlineContact> airlineContactList) {
		this.airlineContactList = airlineContactList;
	}

	public String getIcaoCode() {
		return icaoCode;
	}

	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

	public String getIataCode() {
		return iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public Date getShipmentDeadlineStartDate() {
		return shipmentDeadlineStartDate;
	}

	public void setShipmentDeadlineStartDate(Date shipmentDeadlineStartDate) {
		this.shipmentDeadlineStartDate = shipmentDeadlineStartDate;
	}

	public Date getShipmentDeadlineEndDate() {
		return shipmentDeadlineEndDate;
	}

	public void setShipmentDeadlineEndDate(Date shipmentDeadlineEndDate) {
		this.shipmentDeadlineEndDate = shipmentDeadlineEndDate;
	}

	public Date getContentDeadlineStartDate() {
		return contentDeadlineStartDate;
	}

	public void setContentDeadlineStartDate(Date contentDeadlineStartDate) {
		this.contentDeadlineStartDate = contentDeadlineStartDate;
	}

	public Date getContentDeadlineEndDate() {
		return contentDeadlineEndDate;
	}

	public void setContentDeadlineEndDate(Date contentDeadlineEndDate) {
		this.contentDeadlineEndDate = contentDeadlineEndDate;
	}

	public Date getAssetDeadlineStartDate() {
		return assetDeadlineStartDate;
	}

	public void setAssetDeadlineStartDate(Date assetDeadlineStartDate) {
		this.assetDeadlineStartDate = assetDeadlineStartDate;
	}

	public Date getAssetDeadlineEndDate() {
		return assetDeadlineEndDate;
	}

	public void setAssetDeadlineEndDate(Date assetDeadlineEndDate) {
		this.assetDeadlineEndDate = assetDeadlineEndDate;
	}
}
