package com.thales.ecms.model.airline;

import com.psddev.cms.db.Content;

public class AirlineContact extends Content {
	
	@Indexed
	@Required
	private String firstName;
	
	@Indexed
	@Required
	private String lastName;
	
	@Indexed
	@Required
	private String phone;
	
	@Indexed
	@Required
	private String email;
	
	@Indexed
	@Required
	private AirlineContactType airlineContactType;

	public enum AirlineContactType {
		MAINTENANCE, HOTLINE
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
		
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public AirlineContactType getAirlineContactType() {
		return airlineContactType;
	}

	public void setAirlineContactType(AirlineContactType airlineContactType) {
		this.airlineContactType = airlineContactType;
	}
	
}
