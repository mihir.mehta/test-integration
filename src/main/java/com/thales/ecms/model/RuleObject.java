//package com.thales.ecms.model;
//
//import java.util.List;
//
//import com.psddev.cms.db.Content;
//
//public class RuleObject extends Content {
//	
//	@Indexed
//	private String contentTypeId;
//	
//	@Indexed
//	private ContentType contentTypeObj;
//	
//	private List<ContentFieldValidation> contentFieldValidations;
//
//	public List<ContentFieldValidation> getContentFieldValidations() {
//		return contentFieldValidations;
//	}
//
//	public void setContentFieldValidations(List<ContentFieldValidation> contentFieldValidations) {
//		this.contentFieldValidations = contentFieldValidations;
//	}
//
//	public String getContentTypeId() {
//		return contentTypeId;
//	}
//
//	public void setContentTypeId(String contentTypeId) {
//		this.contentTypeId = contentTypeId;
//	}
//
//	public ContentType getContentTypeObj() {
//		return contentTypeObj;
//	}
//
//	public void setContentTypeObj(ContentType contentTypeObj) {
//		this.contentTypeObj = contentTypeObj;
//	}
//
//	@Embedded
//	public static class ContentFieldValidation extends Content {
//		private String field;
//
//		private ValidatorAttribute validatorAttribute;
//
//		public String getField() {
//			return field;
//		}
//
//		public void setField(String field) {
//			this.field = field;
//		}
//
//		public ValidatorAttribute getValidatorAttribute() {
//			return validatorAttribute;
//		}
//
//		public void setValidatorAttribute(ValidatorAttribute validatorAttribute) {
//			this.validatorAttribute = validatorAttribute;
//		}
//
//	}
//
//}
