package com.thales.ecms.model;

import java.util.Date;
import java.util.List;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;
import com.psddev.dari.db.Recordable.Indexed;
import com.thales.ecms.model.Configuration.ConfigurationTitle.Priority;
import com.thales.ecms.model.Configuration.ConfigurationTitle.Status;
import com.thales.ecms.model.util.DisplayGroup;

@DisplayName("Package")
public class Package extends Content {

	@Indexed
	private String name;
	
	@Indexed
	private Date packagingDate;
	
	@Indexed
	private Order order;
	
	@Indexed
	private List<DisplayGroup> displayGroups;

	@Indexed
	private long fullSize;
	
	@Indexed
	private Configuration configuration;
	
	//This list will hold the list of all Validated titles present in the Configuration
	@Indexed
	@DisplayName("Titles")
	private List<PackageTitle> packageTitles;
	
	@Content.Embedded
	@Content.LabelFields("content")
	@DisplayName("Title")
	public static class PackageTitle extends Content {
		
		@Indexed
		private ContentType content;
		
		@Indexed
		private Status contentStatus;
		
		private Priority priority;
		
		@Embedded
		private List<DisplayGroup> displayGroups;
		
		private List<LruType> destinations;

		public ContentType getContent() {
			return content;
		}

		public void setContent(ContentType content) {
			this.content = content;
		}

		public Status getContentStatus() {
			return contentStatus;
		}

		public void setContentStatus(Status contentStatus) {
			this.contentStatus = contentStatus;
		}

		public Priority getPriority() {
			return priority;
		}

		public void setPriority(Priority priority) {
			this.priority = priority;
		}

		public List<DisplayGroup> getDisplayGroups() {
			return displayGroups;
		}

		public void setDisplayGroups(List<DisplayGroup> displayGroups) {
			this.displayGroups = displayGroups;
		}
		
		public List<LruType> getDestinations() {
			return destinations;
		}

		public void setDestinations(List<LruType> destinations) {
			this.destinations = destinations;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getPackagingDate() {
		return packagingDate;
	}

	public void setPackagingDate(Date packagingDate) {
		this.packagingDate = packagingDate;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
	
	public List<DisplayGroup> getDisplayGroups() {
		return displayGroups;
	}

	public void setDisplayGroups(List<DisplayGroup> displayGroups) {
		this.displayGroups = displayGroups;
	}

	public long getFullSize() {
		return fullSize;
	}

	public void setFullSize(long fullSize) {
		this.fullSize = fullSize;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public List<PackageTitle> getPackageTitles() {
		return packageTitles;
	}

	public void setPackageTitles(List<PackageTitle> packageTitles) {
		this.packageTitles = packageTitles;
	}
	
	public enum FileType {
		MEDIA, METADATA, SIF
	}
	
}
