package com.thales.ecms.model;

import java.util.Date;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.ReferentialText;
//import com.snagfilms.model.Image;

public class ShortSubjectVideo extends ContentType{

	@Indexed
	private String title;
	
	@Indexed
	private String shortTitle;
	
	@Indexed
	private String longTitle;
	
//	@Indexed
//	private Genre genre;
	
	@Indexed
	private String actor;
	
	@Indexed
	private String director;
	
	@Indexed
	private Date releaseDate;
	
	@Indexed
	private String releaseYear;
	
	@Indexed
	private Rating rating;
	
	@Indexed
	private long duration;
	
	@Indexed
	private String distributor;
	
	@Indexed
	private Language audioLanguage;
	
	@Indexed
	private Language subtitleLanguage;
	
	@Indexed
	private Language closedCaptionsLanguage;
	
	private ReferentialText synopsis;
	
	private ReferentialText shortSynopsis;
	
	private ReferentialText longSynopsis;
	
//	private Image originalImage;
//	
//	private Image standardImage;
//	
//	private Image smallImage;
	
	@Indexed
	private int episodeNumber;
	
	@Indexed
	private String episodeName;
	
	@Indexed
	private String seriesName;
	
	@Indexed
	private Rating censorRating;
	
	//theme tag, style tag, mood tag, standard tag
	
	//preview aspect ratio - lookup
	
	@Indexed
	private int contentPosition;
	
	@Indexed
	private String tagLine;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortTitle() {
		return shortTitle;
	}

	public void setShortTitle(String shortTitle) {
		this.shortTitle = shortTitle;
	}

	public String getLongTitle() {
		return longTitle;
	}

	public void setLongTitle(String longTitle) {
		this.longTitle = longTitle;
	}

//	public Genre getGenre() {
//		return genre;
//	}
//
//	public void setGenre(Genre genre) {
//		this.genre = genre;
//	}

	public String getActor() {
		return actor;
	}

	public void setActor(String actor) {
		this.actor = actor;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}

	public Rating getRating() {
		return rating;
	}

	public void setRating(Rating rating) {
		this.rating = rating;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getDistributor() {
		return distributor;
	}

	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}

	public Language getAudioLanguage() {
		return audioLanguage;
	}

	public void setAudioLanguage(Language audioLanguage) {
		this.audioLanguage = audioLanguage;
	}

	public Language getSubtitleLanguage() {
		return subtitleLanguage;
	}

	public void setSubtitleLanguage(Language subtitleLanguage) {
		this.subtitleLanguage = subtitleLanguage;
	}

	public Language getClosedCaptionsLanguage() {
		return closedCaptionsLanguage;
	}

	public void setClosedCaptionsLanguage(Language closedCaptionsLanguage) {
		this.closedCaptionsLanguage = closedCaptionsLanguage;
	}

	public ReferentialText getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(ReferentialText synopsis) {
		this.synopsis = synopsis;
	}

	public ReferentialText getShortSynopsis() {
		return shortSynopsis;
	}

	public void setShortSynopsis(ReferentialText shortSynopsis) {
		this.shortSynopsis = shortSynopsis;
	}

	public ReferentialText getLongSynopsis() {
		return longSynopsis;
	}

	public void setLongSynopsis(ReferentialText longSynopsis) {
		this.longSynopsis = longSynopsis;
	}

//	public Image getOriginalImage() {
//		return originalImage;
//	}
//
//	public void setOriginalImage(Image originalImage) {
//		this.originalImage = originalImage;
//	}
//
//	public Image getStandardImage() {
//		return standardImage;
//	}
//
//	public void setStandardImage(Image standardImage) {
//		this.standardImage = standardImage;
//	}
//
//	public Image getSmallImage() {
//		return smallImage;
//	}
//
//	public void setSmallImage(Image smallImage) {
//		this.smallImage = smallImage;
//	}

	public int getEpisodeNumber() {
		return episodeNumber;
	}

	public void setEpisodeNumber(int episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

	public String getEpisodeName() {
		return episodeName;
	}

	public void setEpisodeName(String episodeName) {
		this.episodeName = episodeName;
	}

	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}

	public Rating getCensorRating() {
		return censorRating;
	}

	public void setCensorRating(Rating censorRating) {
		this.censorRating = censorRating;
	}

	public int getContentPosition() {
		return contentPosition;
	}

	public void setContentPosition(int contentPosition) {
		this.contentPosition = contentPosition;
	}

	public String getTagLine() {
		return tagLine;
	}

	public void setTagLine(String tagLine) {
		this.tagLine = tagLine;
	}
	
	
}
