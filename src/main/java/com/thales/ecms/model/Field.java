package com.thales.ecms.model;

import com.psddev.cms.db.Content;

public class Field extends Content {

	@Indexed
	private String fieldId;
	
//	@Indexed
//	private String displayName;
	
	@Indexed
	private String fieldType;
	
	@Indexed
	private String selectionListName;
	
	//might not use this
	private String detailId;

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

//	public String getDisplayName() {
//		return displayName;
//	}
//
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getDetailId() {
		return detailId;
	}

	public void setDetailId(String detailId) {
		this.detailId = detailId;
	}

	public String getSelectionListName() {
		return selectionListName;
	}

	public void setSelectionListName(String selectionListName) {
		this.selectionListName = selectionListName;
	}

}
