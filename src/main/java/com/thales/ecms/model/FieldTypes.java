package com.thales.ecms.model;

public enum FieldTypes {
	Text, LongText, Boolean, Integer, Asset, Long, Double, Image, Select
}
