package com.thales.ecms.model;

import java.util.Date;
import java.util.List;
import com.psddev.cms.db.Content;
import com.psddev.dari.db.ObjectType;
import com.psddev.dari.db.Record;
import com.psddev.dari.db.Recordable.Embedded;
import com.psddev.dari.db.Recordable.Indexed;

public class Rule extends Content {

	@Indexed
	@Required
	private String name;

	@Indexed
	private ObjectType objectType;

	@Indexed
	private List<ContentFieldValidation> contentFieldValidations;
	
	private Date creationDate;
	
	private Date lastModifiedDate;

	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public ObjectType getObjectType() {
		return objectType;
	}
	public void setObjectType(ObjectType objectType) {
		this.objectType = objectType;
	}

	public List<ContentFieldValidation> getContentFieldValidations() {
		return contentFieldValidations;
	}
	public void setContentFieldValidations(List<ContentFieldValidation> contentFieldValidations) {
		this.contentFieldValidations = contentFieldValidations;
	}

	@Embedded
	public static class ContentFieldValidation extends Content{

		@Indexed
		private String field;

		@Indexed
		private ValidatorAttribute validatorAttribute;

		public String getField() {
			return field;
		}
		public void setField(String field) {
			this.field = field;
		}

		public ValidatorAttribute getValidatorAttribute() {
			return validatorAttribute;
		}
		public void setValidatorAttribute(ValidatorAttribute validatorAttribute) {
			this.validatorAttribute = validatorAttribute;
		}
	}	
}

