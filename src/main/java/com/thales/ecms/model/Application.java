package com.thales.ecms.model;

import java.util.Date;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.ReferentialText;
import com.psddev.dari.db.Recordable.Indexed;

public class Application extends ContentType{

	@Indexed
	private String title;
	
	@Indexed
	private String shortTitle;
	
	@Indexed
	private String longTitle;
		
//	@Indexed
//	private Genre genre;
	
	@Indexed
	private Date releaseDate;
	
	@Indexed
	private Rating rating;
	
	@Indexed
	private String publisher;
	
	@Indexed
	private String platform;
	
	private ReferentialText synopsis;
	
	private ReferentialText shortSynopsis;
	
	private ReferentialText longSynopsis;
	
	private Image originalImage;
	
	private Image standardImage;
	
	private Image smallImage;
	
	private Image screenShotImage;
	
	@Indexed
	private String popularityRating;
	
	@Indexed
	private Rating censorRating;
	
	@Indexed
	private String numberOfPlayers;
	
	//fld_filePackage - binary, asset
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortTitle() {
		return shortTitle;
	}

	public void setShortTitle(String shortTitle) {
		this.shortTitle = shortTitle;
	}

	public String getLongTitle() {
		return longTitle;
	}

	public void setLongTitle(String longTitle) {
		this.longTitle = longTitle;
	}

//	public Genre getGenre() {
//		return genre;
//	}
//
//	public void setGenre(Genre genre) {
//		this.genre = genre;
//	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Rating getRating() {
		return rating;
	}

	public void setRating(Rating rating) {
		this.rating = rating;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public ReferentialText getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(ReferentialText synopsis) {
		this.synopsis = synopsis;
	}

	public ReferentialText getShortSynopsis() {
		return shortSynopsis;
	}

	public void setShortSynopsis(ReferentialText shortSynopsis) {
		this.shortSynopsis = shortSynopsis;
	}

	public ReferentialText getLongSynopsis() {
		return longSynopsis;
	}

	public void setLongSynopsis(ReferentialText longSynopsis) {
		this.longSynopsis = longSynopsis;
	}

	public Image getOriginalImage() {
		return originalImage;
	}

	public void setOriginalImage(Image originalImage) {
		this.originalImage = originalImage;
	}

	public Image getStandardImage() {
		return standardImage;
	}

	public void setStandardImage(Image standardImage) {
		this.standardImage = standardImage;
	}

	public Image getSmallImage() {
		return smallImage;
	}

	public void setSmallImage(Image smallImage) {
		this.smallImage = smallImage;
	}

	public Image getScreenShotImage() {
		return screenShotImage;
	}

	public void setScreenShotImage(Image screenShotImage) {
		this.screenShotImage = screenShotImage;
	}

	public String getPopularityRating() {
		return popularityRating;
	}

	public void setPopularityRating(String popularityRating) {
		this.popularityRating = popularityRating;
	}

	public Rating getCensorRating() {
		return censorRating;
	}

	public void setCensorRating(Rating censorRating) {
		this.censorRating = censorRating;
	}

	public String getNumberOfPlayers() {
		return numberOfPlayers;
	}

	public void setNumberOfPlayers(String numberOfPlayers) {
		this.numberOfPlayers = numberOfPlayers;
	}
	
}
