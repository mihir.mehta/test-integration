package com.thales.ecms.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.State;
import com.thales.ecms.model.Lopa.SeatingClassLRUTypeMapping;

public class ServerGroup extends Content {

	@Indexed
	@Required
	private String name;
	
	@Indexed
	@Required
	@DisplayName("LOPA")
	private Lopa lopa;
	
	@Indexed
	@Required
	@DisplayName("LRU Types")
	private Set<LRUTypeForLOPA> lruTypes;
	
	@Content.Embedded
	@DisplayName("LRU Type")
	@Content.LabelFields("lruType")
	public static class LRUTypeForLOPA extends Content {
		
		@DisplayName("LRU Type")
		private LruType lruType;
		
		private double maxSpaceAllowance;

		public LruType getLruType() {
			return lruType;
		}

		public void setLruType(LruType lruType) {
			this.lruType = lruType;
		}

		public double getMaxSpaceAllowance() {
			return maxSpaceAllowance;
		}

		public void setMaxSpaceAllowance(double maxSpaceAllowance) {
			this.maxSpaceAllowance = maxSpaceAllowance;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Lopa getLopa() {
		return lopa;
	}

	public void setLopa(Lopa lopa) {
		this.lopa = lopa;
	}

	public Set<LRUTypeForLOPA> getLruTypes() {
		return lruTypes;
	}

	public void setLruTypes(Set<LRUTypeForLOPA> lruTypes) {
		this.lruTypes = lruTypes;
	}
	
	@Override
	protected void beforeSave(){
		State state = this.getState();
		
		System.out.println(lopa.getName());
		
		List<SeatingClassLRUTypeMapping> seatingClassLRUTypeMappings = lopa.getSeatingClassLruTypeMappingList();
		Set<LruType> lruTypes = new HashSet<>();
		for(SeatingClassLRUTypeMapping seatingClassLRUTypeMapping : seatingClassLRUTypeMappings){
			lruTypes.add(seatingClassLRUTypeMapping.getLruType());
		}
		//lruTypes now contains all the LRU Types present in the LOPA
		
		//Check if the LRU types in the Server Group are valid, i.e., these LRU types should be only those whcih are already there in the LOPA and not any other
		for(LRUTypeForLOPA lruType : this.lruTypes){
			if(!lruTypes.contains(lruType.getLruType())){
				System.out.println(lruType.getLruType().getName());
				state.addError(state.getField("lruTypes"), "Invalid LRU Type " + lruType.getLruType().getName()+ "! Please add an LRU Type which exists in the LOPA");
			}
		}
	}
}
