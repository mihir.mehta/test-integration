package com.thales.ecms.model;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.ReferentialText;

public class Document extends ContentType {

	@Indexed
	private String title;
	
	//category
	
	private ReferentialText synopsis;
	
	private Image standardImage;

	private Image smallImage;
	
	//file asset, video asset
	
	@Indexed
	private Language audioLanguage;
	
	@Indexed
	private Rating censorRating;
	
	//standard tag
	
	@Indexed
	private int contentPosition;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ReferentialText getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(ReferentialText synopsis) {
		this.synopsis = synopsis;
	}

	public Image getStandardImage() {
		return standardImage;
	}

	public void setStandardImage(Image standardImage) {
		this.standardImage = standardImage;
	}

	public Image getSmallImage() {
		return smallImage;
	}

	public void setSmallImage(Image smallImage) {
		this.smallImage = smallImage;
	}

	public Language getAudioLanguage() {
		return audioLanguage;
	}

	public void setAudioLanguage(Language audioLanguage) {
		this.audioLanguage = audioLanguage;
	}

	public Rating getCensorRating() {
		return censorRating;
	}

	public void setCensorRating(Rating censorRating) {
		this.censorRating = censorRating;
	}

	public int getContentPosition() {
		return contentPosition;
	}

	public void setContentPosition(int contentPosition) {
		this.contentPosition = contentPosition;
	}
}
