package com.thales.ecms.model;

import com.psddev.cms.db.Content;
/**
 * An Airport POJO to be used by DARI framework
 */
public class Airport extends Content {
	
	/**
	 * Airport Name
	 */
	@Indexed
	@Required
	private String airportName;

	/**
	 * Airport ICAO code
	 * ICAO-International Civil Aviation Organization
	 */
	@Indexed(unique = true)
	@Required
	private String airportICAO;

	/**
	 * Airport IATA code
	 * IATA-International Air Transport Association
	 */
	@Indexed(unique = true)
	@Required
	private String airportIATA;

	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	
	public String getAirportICAO() {
		return airportICAO;
	}

	public void setAirportICAO(String airportICAO) {
		this.airportICAO = airportICAO;
	}

	public String getAirportIATA() {
		return airportIATA;
	}

	public void setAirportIATA(String airportIATA) {
		this.airportIATA = airportIATA;
	}

	@Override
	public String toString() {
		return "Airport instance:- Name: "+this.getAirportName()+" IATA code: "+this.getAirportIATA()+" ICAO code: "+this.getAirportICAO();
	}

}
