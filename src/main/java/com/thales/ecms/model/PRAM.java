package com.thales.ecms.model;

import com.psddev.cms.db.Content;

public class PRAM extends ContentType{
	
	@Indexed
	private String title;
	
	@Indexed
	private int contentPosition;
	
	@Indexed
	private Long duration;
	
	@Indexed
	private Language language;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getContentPosition() {
		return contentPosition;
	}

	public void setContentPosition(int contentPosition) {
		this.contentPosition = contentPosition;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}
	
	
}
