package com.thales.ecms.model;

import java.util.List;

import com.psddev.cms.db.Content;
import com.psddev.dari.db.Recordable.DisplayName;

/**
 * @author Mihir
 * Location of Passenger Accommodations. The LOPA is an engineering diagram outlining the aircraft's cabin interior that includes, but is not limited to, 
 * locations of passenger and crew seats, emergency equipment, lavatories, galleys, and exits.
 */
@DisplayName("LOPA")
public class Lopa extends Content {

	@Indexed
	@Required
	private String name;
	
	@Indexed
	@Required
	private Aircraft aircraft;
	
	@Indexed
	@Embedded
	@DisplayName("Seating Class LRU Type Mapping List")
	private List<SeatingClassLRUTypeMapping> seatingClassLruTypeMappingList;

	@Content.Embedded
	@DisplayName("Seating Class LRU Type Mapping")
	@Content.LabelFields("seatingClass")
	public static class SeatingClassLRUTypeMapping extends Content {
		
		@Indexed
		@Required
		private SeatingClass seatingClass;
		
		@Indexed
		@Required
		@DisplayName("LRU Type")
		private LruType lruType;
		
		private double maxSpaceAllowance;

		public SeatingClass getSeatingClass() {
			return seatingClass;
		}

		public void setSeatingClass(SeatingClass seatingClass) {
			this.seatingClass = seatingClass;
		}

		public LruType getLruType() {
			return lruType;
		}

		public void setLruType(LruType lruType) {
			this.lruType = lruType;
		}		
		
		public double getMaxSpaceAllowance() {
			return maxSpaceAllowance;
		}

		public void setMaxSpaceAllowance(double maxSpaceAllowance) {
			this.maxSpaceAllowance = maxSpaceAllowance;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Aircraft getAircraft() {
		return aircraft;
	}

	public void setAircraft(Aircraft aircraft) {
		this.aircraft = aircraft;
	}
	
	public List<SeatingClassLRUTypeMapping> getSeatingClassLruTypeMappingList() {
		return seatingClassLruTypeMappingList;
	}

	public void setSeatingClassLruTypeMappingList(List<SeatingClassLRUTypeMapping> seatingClassLruTypeMappingList) {
		this.seatingClassLruTypeMappingList = seatingClassLruTypeMappingList;
	}
}
