package com.thales.ecms.model;

import com.psddev.cms.db.Content;

public class Genre extends Content {
	
	@Indexed
	@Required
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
//public enum Genre {
//	
//	Action, Comedy, Crime, Drama, Fantasy, Historical, Horror, Kids, Mystery, Romance, Thriller;
//
//}
