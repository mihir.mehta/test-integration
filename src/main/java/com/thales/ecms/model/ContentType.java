package com.thales.ecms.model;

import com.psddev.cms.db.Content;

/**
 * @author Mihir
 * Marker Interface for all Content Types, e.g., Feature Video, Audio Collection, etc.
 */
public class ContentType extends Content {
	
	@Indexed
	private String englishTitle;
	
	public String getEnglishTitle() {
		return englishTitle;
	}

	public void setEnglishTitle(String englishTitle) {
		this.englishTitle = englishTitle;
	}
	
	public enum ContentTypeReferences {
		Application("CT0"), Audio_Collection("CT1"), Broadcast_Audio("CT2"), Document("CT3"), Feature_Video("CT4"), PRAM("CT5"), Short_Subject_Video("CT6");
		
		private String id;
		
		private ContentTypeReferences(String id) {
			this.id = id;
		}
		
		public String getId(){
			return id;
		}
		
		@Override
		public String toString() {
			String s = super.toString();
			return s.replaceAll("_", " ");
		}
	}
	
}
