package com.thales.ecms.views;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import com.psddev.dari.db.ObjectType;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.ValidatorAttribute;
import com.thales.ecms.utils.TemplateBuilderImpl;
import com.thales.ecms.utils.ExcelCellStyleUtil;
import com.thales.ecms.utils.ExcelTemplateManager;
import com.thales.ecms.utils.RuleUtil;
import com.thales.ecms.utils.TemplateDataIntegraterImpl;
import com.thales.ecms.utils.ExcelCellStyleUtil.SECTION;

/**
 * Class used by ViewResolver to render excel file as response
 * 
 * @author arjun.p
 *
 */
public class ContentTypeListExcelView extends AbstractXlsxView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {		
		response.setHeader("Content-Disposition", "attachment; filename=\"config.xlsx\"");
		Configuration configuration = (Configuration)model.get("configuration");		
		if(configuration!=null) {			
			Map<String, Map<String,ValidatorAttribute>> rules = RuleUtil.getRules(configuration);
			Map<SECTION, CellStyle> cellStyles = ExcelCellStyleUtil.getCellStyles(workbook);
			ExcelTemplateManager excelTemplateManager = ExcelTemplateManager.newInstance(configuration, workbook, rules , cellStyles);
			if(model.containsKey("objectType") && model.get("objectType")!=null){				
				excelTemplateManager.getTemplateBuilder().buildSheet((ObjectType)model.get("objectType"));
			}else{
				excelTemplateManager.getTemplateBuilder().buildTemplate();
			}
			if(model.get("DATA_EXPORT")!=null && ((Boolean)model.get("DATA_EXPORT")))
				excelTemplateManager.getTemplateDataIntegrater().fillWorkbook();
		}			
	}
}
