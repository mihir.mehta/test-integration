package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.ObjectType;
import com.psddev.dari.db.Query;
import com.thales.ecms.conversions.ContentTypeFactory;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.model.ContentType;
import com.thales.ecms.model.dto.ContentTypeDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir Service Class to return a list of Content Types
 */
@Service
public class ContentTypeService {
	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(ContentTypeService.class);

	public List<ContentTypeDTO> getContentTypes() {

		List<ContentTypeDTO> contentTypeDTOList = new ArrayList<>();
		ContentTypeDTO contentTypeDTO = null;

		Query<ObjectType> objectTypeQuery = Query.from(ObjectType.class);

		for (ObjectType objectType : objectTypeQuery.iterable(1000)) {
			// logger.info("ObjectType Name:" + objectType.getDisplayName());
			List<String> superClassNames = objectType.getSuperClassNames();
			if (superClassNames.contains("com.thales.ecms.model.ContentType")) {
				// This ObjectType is a ContentType
				// Do not include ContentType itself in the list
				if (!objectType.getDisplayName().equalsIgnoreCase("ContentType")) {
					contentTypeDTO = new ContentTypeDTO();
					contentTypeDTO.setContentTypeInternalId(objectType.getId().toString());
					contentTypeDTO.setContentTypeName(objectType.getDisplayName());
					contentTypeDTOList.add(contentTypeDTO);
				}

			}
		}
		return contentTypeDTOList;
	}

	/**
	 * Method for adding content type
	 * 
	 * @param englishTitle
	 * @return Nothing
	 * @throws DuplicateEntryException
	 */
	public void addContentType(String englishTitle, ContentType contentType, Class<? extends ContentType> clazz)
			throws DuplicateEntryException {

		ContentType contentTypeObject = this.getContentTypeByEnglishTitle(englishTitle, clazz);

		if (contentTypeObject == null) {
			ContentType targetContentType = null;
			if (!Objects.isNull(contentType) && contentType instanceof ContentType) {

				targetContentType = ContentTypeFactory.getContentType(contentType);

				// Applying rule validation before saving content
				System.out.println("Saving object in ContentTypeService" + targetContentType.toString());
				targetContentType.save();

				System.out.println("Saved object in ContentTypeService");
			} // end of if
		} else {
			logger.error("Cannot create Content Type - ContentType Type with same englisTitle " + englishTitle
					+ " already exists");
			throw new DuplicateEntryException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
							new Object[] { ECMSConstants.TITLE, ECMSConstants.NAME }, Locale.US),
					HttpStatus.FORBIDDEN));
		}

	}

	/**
	 * Method for updating content type
	 * 
	 * @param englishTitle
	 * @param contentType
	 * @param Class<?
	 *            extends ContentType> clazz
	 * @return Nothing
	 * @throws DuplicateEntryException
	 */
	public void updateContentType(String englishTitle, ContentType contentType) throws DuplicateEntryException {

		// Applying rule validation before saving content
		System.out.println("Updateting object in ContentTypeService" + contentType.toString());
		contentType.save();

		System.out.println("Updateted object in ContentTypeService");

	}

	/**
	 * Method for checking duplicate content
	 * 
	 * @param englishTitle
	 * @return ContentType
	 */
	public ContentType getContentTypeByEnglishTitle(String englishTitle, Class<? extends ContentType> clazz) {
		ContentType contentType = null;

		contentType = Query.from(clazz).where("englishTitle = ?", englishTitle).first();

		if (Objects.nonNull(contentType)) {
			return contentType;
		}
		return contentType;
	}

}
