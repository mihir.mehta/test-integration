package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.psddev.dari.db.ValidationException;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Lopa;
import com.thales.ecms.model.airline.AirlineContact;
import com.thales.ecms.model.airline.AirlinePortal;
import com.thales.ecms.model.airline.AirlinePortal.AirlineStatus;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir Service Class to perform CRUD operations on Airline Contacts
 */
@Service
public class AirlineService {

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(AirlineService.class);

	/**
	 * Service Method to add an Airline
	 * 
	 * @param airlineDetails
	 * @return
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 * @throws DuplicateEntryException
	 */
	public AirlineDTO addAirline(AirlineDTO airlineDetails)
			throws BadRequestException, ObjectExistsException, DuplicateEntryException {

		AirlineDTO airlineDTO = null;

		Site airline = new Site();

		String airlineName = airlineDetails.getAirlineName();
		if (airlineName != null && StringUtils.isNotBlank(airlineName)) {
			if (Query.from(Site.class).where("name = ?", airlineName).first() != null) {
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.AIRLINE, ECMSConstants.NAME }, Locale.US),
						HttpStatus.FORBIDDEN));
			} else {
				airline.setName(airlineName);
			}
		} else {
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}

		String website = airlineDetails.getWebsite();
		if (website != null && StringUtils.isNotBlank(website)) {
			UrlValidator urlValidator = new UrlValidator();
			if (urlValidator.isValid(website)) {
				List<String> urls = new ArrayList<>();
				urls.add(website);
				airline.setUrls(urls);
			} else {
				throw new BadRequestException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.WEBSITE, null, Locale.US), HttpStatus.BAD_REQUEST));
			}
		} else {
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}

		String companyName = airlineDetails.getCompanyName();
		if (companyName != null && StringUtils.isNotBlank(companyName)) {
			airline.as(AirlinePortal.class).setCompanyName(companyName);
		}

		if (airlineDetails.getActivate()) {
			airline.as(AirlinePortal.class).setAirlineStatus(AirlineStatus.ACTIVE);
		} else {
			airline.as(AirlinePortal.class).setAirlineStatus(AirlineStatus.INACTIVE);
		}

		String icaoCode = airlineDetails.getIcaoCode();
		if (icaoCode != null && StringUtils.isNotBlank(icaoCode)) {
			if (Query.from(Site.class).where("icaoCode = ?", icaoCode).first() != null) {
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.ICAO, ECMSConstants.ICAO }, Locale.US),
						HttpStatus.FORBIDDEN));
			} else {
				airline.as(AirlinePortal.class).setIcaoCode(icaoCode);
			}
		} else {
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}

		String iataCode = airlineDetails.getIataCode();
		if (iataCode != null && StringUtils.isNotBlank(iataCode)) {
			if (Query.from(Site.class).where("iataCode = ?", iataCode).first() != null) {
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.IATA, ECMSConstants.IATA }, Locale.US),
						HttpStatus.FORBIDDEN));
			} else {
				airline.as(AirlinePortal.class).setIataCode(iataCode);
			}
		} else {
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}
		String codeShare = airlineDetails.getCodeShare();
		if (codeShare != null && StringUtils.isNotBlank(codeShare)) {
			airline.as(AirlinePortal.class).setCodeShare(codeShare);
		}

		Date shipmentStartDate = airlineDetails.getShipmentStartdate();
		if (Objects.nonNull(shipmentStartDate) && StringUtils.isNotBlank(shipmentStartDate.toString())) {
			airline.as(AirlinePortal.class).setShipmentDeadlineStartDate(shipmentStartDate);
		} else {
			throw new ObjectExistsException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
							new Object[] { ECMSConstants.SHIPMENTDATE, ECMSConstants.AIRLINE }, Locale.US),
					HttpStatus.FORBIDDEN));
		}

		Date shipmentEndDate = airlineDetails.getShipmentEndDate();
		if (Objects.nonNull(shipmentEndDate) && StringUtils.isNotBlank(shipmentEndDate.toString())) {
			airline.as(AirlinePortal.class).setShipmentDeadlineEndDate(shipmentEndDate);
		} else {
			throw new ObjectExistsException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
							new Object[] { ECMSConstants.SHIPMENTDATE, ECMSConstants.AIRLINE }, Locale.US),
					HttpStatus.FORBIDDEN));
		}
		Date contentDeadLineStartDate = airlineDetails.getContentStartDate();
		if (Objects.nonNull(contentDeadLineStartDate) && StringUtils.isNotBlank(contentDeadLineStartDate.toString())) {
			airline.as(AirlinePortal.class).setContentDeadlineStartDate(contentDeadLineStartDate);
		} else {
			throw new ObjectExistsException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
							new Object[] { ECMSConstants.CONTENTDATE, ECMSConstants.AIRLINE }, Locale.US),
					HttpStatus.FORBIDDEN));
		}
		Date contentDeadLineEndDate = airlineDetails.getContentEndDate();
		if (Objects.nonNull(contentDeadLineEndDate) && StringUtils.isNotBlank(contentDeadLineEndDate.toString())) {
			airline.as(AirlinePortal.class).setContentDeadlineEndDate(contentDeadLineEndDate);
		} else {
			throw new ObjectExistsException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
							new Object[] { ECMSConstants.CONTENTDATE, ECMSConstants.AIRLINE }, Locale.US),
					HttpStatus.FORBIDDEN));
		}
		Date assetStartDate = airlineDetails.getAssetStartDate();
		if (Objects.nonNull(assetStartDate) && StringUtils.isNotBlank(assetStartDate.toString())) {
			airline.as(AirlinePortal.class).setAssetDeadlineStartDate(assetStartDate);
		} else {
			throw new ObjectExistsException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
							new Object[] { ECMSConstants.ASSETDATE, ECMSConstants.AIRLINE }, Locale.US),
					HttpStatus.FORBIDDEN));
		}
		Date assetEndDate = airlineDetails.getAssetEndDate();
		if (Objects.nonNull(assetEndDate) && StringUtils.isNotBlank(assetEndDate.toString())) {
			airline.as(AirlinePortal.class).setAssetDeadlineEndDate(assetEndDate);
		} else {
			throw new ObjectExistsException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
							new Object[] { ECMSConstants.ASSETDATE, ECMSConstants.AIRLINE }, Locale.US),
					HttpStatus.FORBIDDEN));
		}
		try {
			airline.save();
		} catch (ValidationException e) {
			logger.error("Cannot save airline!", e);
			throw new BadRequestException(new ErrorResponse(e.getMessage(), HttpStatus.BAD_REQUEST));
		}

		airlineDTO = new AirlineDTO();
		airlineDTO.setAirlineInternalId(airline.getId().toString());
		airlineDTO.setAirlineName(airline.getName());
		airlineDTO.setWebsite(airline.getUrls().get(0));
		if (airline.as(AirlinePortal.class).getCompanyName() != null
				&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getCompanyName())) {
			airlineDTO.setCompanyName(airline.as(AirlinePortal.class).getCompanyName());
		}
		if (airline.as(AirlinePortal.class).getAirlineStatus() != null) {

			if (airline.as(AirlinePortal.class).getAirlineStatus().equals(AirlineStatus.ACTIVE)) {
				airlineDTO.setActivate(true);
			} else {
				airlineDTO.setActivate(false);
			}

		}
		if (airline.as(AirlinePortal.class).getIcaoCode() != null
				&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getIcaoCode())) {
			airlineDTO.setIcaoCode(airline.as(AirlinePortal.class).getIcaoCode());
		}
		if (airline.as(AirlinePortal.class).getIataCode() != null
				&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getIataCode())) {
			airlineDTO.setIataCode(airline.as(AirlinePortal.class).getIataCode());
		}

		if (airline.as(AirlinePortal.class).getCodeShare() != null
				&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getCodeShare())) {
			airlineDTO.setCodeShare(airline.as(AirlinePortal.class).getCodeShare());
		}
		if (airline.as(AirlinePortal.class).getShipmentDeadlineStartDate() != null
				&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getShipmentDeadlineStartDate().toString())) {
			airlineDTO.setShipmentStartdate(airline.as(AirlinePortal.class).getShipmentDeadlineStartDate());
		}
		if (airline.as(AirlinePortal.class).getShipmentDeadlineEndDate() != null
				&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getShipmentDeadlineEndDate().toString())) {
			airlineDTO.setShipmentEndDate(airline.as(AirlinePortal.class).getShipmentDeadlineEndDate());
		}
		if (airline.as(AirlinePortal.class).getContentDeadlineStartDate() != null
				&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getContentDeadlineStartDate().toString())) {
			airlineDTO.setContentStartDate(airline.as(AirlinePortal.class).getContentDeadlineStartDate());
		}
		if (airline.as(AirlinePortal.class).getContentDeadlineEndDate() != null
				&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getContentDeadlineEndDate().toString())) {
			airlineDTO.setContentEndDate(airline.as(AirlinePortal.class).getContentDeadlineEndDate());
		}
		if (airline.as(AirlinePortal.class).getAssetDeadlineStartDate() != null
				&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getAssetDeadlineStartDate().toString())) {
			airlineDTO.setAssetStartDate(airline.as(AirlinePortal.class).getAssetDeadlineStartDate());
		}
		if (airline.as(AirlinePortal.class).getAssetDeadlineEndDate() != null
				&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getAssetDeadlineEndDate().toString())) {
			airlineDTO.setAssetEndDate(airline.as(AirlinePortal.class).getAssetDeadlineEndDate());
		}
		return airlineDTO;

	}

	/**
	 * Service Method to return a list of all Airlines
	 * 
	 * @return
	 */
	public List<AirlineDTO> getAirlines() {

		List<AirlineDTO> airlineDTOList = null;

		List<Site> airlineList = Query.from(Site.class).selectAll();

		if (airlineList != null && !airlineList.isEmpty()) {
			airlineDTOList = new ArrayList<>();
			for (Site airline : airlineList) {
				AirlineDTO airlineDTO = new AirlineDTO();

				airlineDTO.setAirlineInternalId(airline.getId().toString());
				airlineDTO.setAirlineName(airline.getName());
				if (!CollectionUtils.isEmpty(airline.getUrls())) {
					airlineDTO.setWebsite(airline.getUrls().get(0));
				}

				if (airline.as(AirlinePortal.class).getCompanyName() != null
						&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getCompanyName())) {
					airlineDTO.setCompanyName(airline.as(AirlinePortal.class).getCompanyName());
				}

				if (airline.as(AirlinePortal.class).getAirlineStatus() != null) {

					if (airline.as(AirlinePortal.class).getAirlineStatus().equals(AirlineStatus.ACTIVE)) {
						airlineDTO.setActivate(true);
					} else {
						airlineDTO.setActivate(false);
					}

				}
				if (airline.as(AirlinePortal.class).getIcaoCode() != null
						&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getIcaoCode())) {
					airlineDTO.setIcaoCode(airline.as(AirlinePortal.class).getIcaoCode());
				}

				if (airline.as(AirlinePortal.class).getIataCode() != null
						&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getIataCode())) {
					airlineDTO.setIataCode(airline.as(AirlinePortal.class).getIataCode());
				}

				if (airline.as(AirlinePortal.class).getCodeShare() != null
						&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getCodeShare())) {
					airlineDTO.setCodeShare(airline.as(AirlinePortal.class).getCodeShare());
				}
				if (airline.as(AirlinePortal.class).getShipmentDeadlineStartDate() != null && StringUtils
						.isNotBlank(airline.as(AirlinePortal.class).getShipmentDeadlineStartDate().toString())) {
					airlineDTO.setShipmentStartdate(airline.as(AirlinePortal.class).getShipmentDeadlineStartDate());
				}
				if (airline.as(AirlinePortal.class).getShipmentDeadlineEndDate() != null && StringUtils
						.isNotBlank(airline.as(AirlinePortal.class).getShipmentDeadlineEndDate().toString())) {
					airlineDTO.setShipmentEndDate(airline.as(AirlinePortal.class).getShipmentDeadlineEndDate());
				}
				if (airline.as(AirlinePortal.class).getContentDeadlineStartDate() != null && StringUtils
						.isNotBlank(airline.as(AirlinePortal.class).getContentDeadlineStartDate().toString())) {
					airlineDTO.setContentStartDate(airline.as(AirlinePortal.class).getContentDeadlineStartDate());
				}
				if (airline.as(AirlinePortal.class).getContentDeadlineEndDate() != null && StringUtils
						.isNotBlank(airline.as(AirlinePortal.class).getContentDeadlineEndDate().toString())) {
					airlineDTO.setContentEndDate(airline.as(AirlinePortal.class).getContentDeadlineEndDate());
				}
				if (airline.as(AirlinePortal.class).getAssetDeadlineStartDate() != null && StringUtils
						.isNotBlank(airline.as(AirlinePortal.class).getAssetDeadlineStartDate().toString())) {
					airlineDTO.setAssetStartDate(airline.as(AirlinePortal.class).getAssetDeadlineStartDate());
				}
				if (airline.as(AirlinePortal.class).getAssetDeadlineEndDate() != null && StringUtils
						.isNotBlank(airline.as(AirlinePortal.class).getAssetDeadlineEndDate().toString())) {
					airlineDTO.setAssetEndDate(airline.as(AirlinePortal.class).getAssetDeadlineEndDate());
				}

				airlineDTOList.add(airlineDTO);
			}
		}
		return airlineDTOList;
	}

	/**
	 * Service method to return an Airline
	 * 
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public AirlineDTO getAirlineById(String airlineId) throws ObjectNotFoundException {

		AirlineDTO airlineDTO = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {
			airlineDTO = new AirlineDTO();

			airlineDTO.setAirlineInternalId(airline.getId().toString());
			airlineDTO.setAirlineName(airline.getName());
			if (!CollectionUtils.isEmpty(airline.getUrls())) {
				airlineDTO.setWebsite(airline.getUrls().get(0));
			}

			if (airline.as(AirlinePortal.class).getCompanyName() != null
					&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getCompanyName())) {
				airlineDTO.setCompanyName(airline.as(AirlinePortal.class).getCompanyName());
			}
			if (airline.as(AirlinePortal.class).getAirlineStatus() != null) {

				if (airline.as(AirlinePortal.class).getAirlineStatus().equals(AirlineStatus.ACTIVE)) {
					airlineDTO.setActivate(true);
				} else {
					airlineDTO.setActivate(false);
				}

			}
			if (airline.as(AirlinePortal.class).getIcaoCode() != null
					&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getIcaoCode())) {
				airlineDTO.setIcaoCode(airline.as(AirlinePortal.class).getIcaoCode());
			}

			if (airline.as(AirlinePortal.class).getIataCode() != null
					&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getIataCode())) {
				airlineDTO.setIataCode(airline.as(AirlinePortal.class).getIataCode());
			}

			if (airline.as(AirlinePortal.class).getCodeShare() != null
					&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getCodeShare())) {
				airlineDTO.setCodeShare(airline.as(AirlinePortal.class).getCodeShare());
			}
			if (airline.as(AirlinePortal.class).getShipmentDeadlineStartDate() != null && StringUtils
					.isNotBlank(airline.as(AirlinePortal.class).getShipmentDeadlineStartDate().toString())) {
				airlineDTO.setShipmentStartdate(airline.as(AirlinePortal.class).getShipmentDeadlineStartDate());
			}
			if (airline.as(AirlinePortal.class).getShipmentDeadlineEndDate() != null && StringUtils
					.isNotBlank(airline.as(AirlinePortal.class).getShipmentDeadlineEndDate().toString())) {
				airlineDTO.setShipmentEndDate(airline.as(AirlinePortal.class).getShipmentDeadlineEndDate());
			}
			if (airline.as(AirlinePortal.class).getContentDeadlineStartDate() != null && StringUtils
					.isNotBlank(airline.as(AirlinePortal.class).getContentDeadlineStartDate().toString())) {
				airlineDTO.setContentStartDate(airline.as(AirlinePortal.class).getContentDeadlineStartDate());
			}
			if (airline.as(AirlinePortal.class).getContentDeadlineEndDate() != null
					&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getContentDeadlineEndDate().toString())) {
				airlineDTO.setContentEndDate(airline.as(AirlinePortal.class).getContentDeadlineEndDate());
			}
			if (airline.as(AirlinePortal.class).getAssetDeadlineStartDate() != null
					&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getAssetDeadlineStartDate().toString())) {
				airlineDTO.setAssetStartDate(airline.as(AirlinePortal.class).getAssetDeadlineStartDate());
			}
			if (airline.as(AirlinePortal.class).getAssetDeadlineEndDate() != null
					&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getAssetDeadlineEndDate().toString())) {
				airlineDTO.setAssetEndDate(airline.as(AirlinePortal.class).getAssetDeadlineEndDate());
			}
		} else {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return airlineDTO;
	}

	/**
	 * Service Method to update an Airline
	 * 
	 * @param airlineId
	 * @param airlineDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws ObjectExistsException
	 * @throws BadRequestException
	 * @throws DuplicateEntryException
	 */
	public AirlineDTO updateAirline(String airlineId, AirlineDTO airlineDetails)
			throws ObjectNotFoundException, ObjectExistsException, BadRequestException, DuplicateEntryException {

		AirlineDTO airlineDTO = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {

			String airlineName = airlineDetails.getAirlineName();
			if (airlineName != null && StringUtils.isNotBlank(airlineName)) {
				if (Query.from(Site.class).where("name = ?", airlineName).first() != null) {
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.AIRLINE, ECMSConstants.NAME }, Locale.US),
							HttpStatus.FORBIDDEN));
				} else {
					airline.setName(airlineName);
				}
			}

			String website = airlineDetails.getWebsite();
			if (website != null && StringUtils.isNotBlank(website)) {
				UrlValidator urlValidator = new UrlValidator();
				if (urlValidator.isValid(website)) {
					List<String> urls = new ArrayList<>();
					urls.add(website);
					airline.setUrls(urls);
				} else {
					throw new BadRequestException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.WEBSITE, null, Locale.US), HttpStatus.BAD_REQUEST));
				}
			}

			String companyName = airlineDetails.getCompanyName();
			if (companyName != null && StringUtils.isNotBlank(companyName)) {
				airline.as(AirlinePortal.class).setCompanyName(companyName);
			}

			if (airlineDetails.getActivate()) {
				airline.as(AirlinePortal.class).setAirlineStatus(AirlineStatus.ACTIVE);
			} else {
				airline.as(AirlinePortal.class).setAirlineStatus(AirlineStatus.INACTIVE);
			}

			String icaoCode = airlineDetails.getIcaoCode();
			if (icaoCode != null && StringUtils.isNotBlank(icaoCode)) {
				if (Query.from(Site.class).where("icaoCode = ?", icaoCode).and("id != ?", airlineId).first() != null) {
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.ICAO, ECMSConstants.ICAO }, Locale.US),
							HttpStatus.FORBIDDEN));
				} else {
					airline.as(AirlinePortal.class).setIcaoCode(icaoCode);
				}
			}

			String iataCode = airlineDetails.getIataCode();
			if (iataCode != null && StringUtils.isNotBlank(iataCode)) {
				if (Query.from(Site.class).where("iataCode = ?", iataCode).and("id != ?", airlineId).first() != null) {
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.IATA, ECMSConstants.IATA }, Locale.US),
							HttpStatus.FORBIDDEN));
				} else {
					airline.as(AirlinePortal.class).setIataCode(iataCode);
				}
			}

			String codeShare = airlineDetails.getCodeShare();
			if (codeShare != null && StringUtils.isNotBlank(codeShare)) {
				airline.as(AirlinePortal.class).setCodeShare(codeShare);
			}

			Date shipmentStartDate = airlineDetails.getShipmentStartdate();
			if (Objects.nonNull(shipmentStartDate) && StringUtils.isNotBlank(shipmentStartDate.toString())) {
				airline.as(AirlinePortal.class).setShipmentDeadlineStartDate(shipmentStartDate);
			} else {
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
								new Object[] { ECMSConstants.SHIPMENTDATE, ECMSConstants.AIRLINE }, Locale.US),
						HttpStatus.FORBIDDEN));
			}

			Date shipmentEndDate = airlineDetails.getShipmentEndDate();
			if (Objects.nonNull(shipmentEndDate) && StringUtils.isNotBlank(shipmentEndDate.toString())) {
				airline.as(AirlinePortal.class).setShipmentDeadlineEndDate(shipmentEndDate);
			} else {
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
								new Object[] { ECMSConstants.SHIPMENTDATE, ECMSConstants.AIRLINE }, Locale.US),
						HttpStatus.FORBIDDEN));
			}
			Date contentDeadLineStartDate = airlineDetails.getContentStartDate();
			if (Objects.nonNull(contentDeadLineStartDate)
					&& StringUtils.isNotBlank(contentDeadLineStartDate.toString())) {
				airline.as(AirlinePortal.class).setContentDeadlineStartDate(contentDeadLineStartDate);
			} else {
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
								new Object[] { ECMSConstants.CONTENTDATE, ECMSConstants.AIRLINE }, Locale.US),
						HttpStatus.FORBIDDEN));
			}
			Date contentDeadLineEndDate = airlineDetails.getContentEndDate();
			if (Objects.nonNull(contentDeadLineEndDate) && StringUtils.isNotBlank(contentDeadLineEndDate.toString())) {
				airline.as(AirlinePortal.class).setContentDeadlineEndDate(contentDeadLineEndDate);
			} else {
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
								new Object[] { ECMSConstants.CONTENTDATE, ECMSConstants.AIRLINE }, Locale.US),
						HttpStatus.FORBIDDEN));
			}
			Date assetStartDate = airlineDetails.getAssetStartDate();
			if (Objects.nonNull(assetStartDate) && StringUtils.isNotBlank(assetStartDate.toString())) {
				airline.as(AirlinePortal.class).setAssetDeadlineStartDate(assetStartDate);
			} else {
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
								new Object[] { ECMSConstants.ASSETDATE, ECMSConstants.AIRLINE }, Locale.US),
						HttpStatus.FORBIDDEN));
			}
			Date assetEndDate = airlineDetails.getAssetEndDate();
			if (Objects.nonNull(assetEndDate) && StringUtils.isNotBlank(assetEndDate.toString())) {
				airline.as(AirlinePortal.class).setAssetDeadlineEndDate(assetEndDate);
			} else {
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
								new Object[] { ECMSConstants.ASSETDATE, ECMSConstants.AIRLINE }, Locale.US),
						HttpStatus.FORBIDDEN));
			}

			try {
				airline.save();
			} catch (ValidationException e) {
				logger.error("Duplicate value of website - Site having the same value for website already exists");
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.DUPLICATE_URL, ECMSConstants.DUPLICATE_URL }, Locale.US),
						HttpStatus.FORBIDDEN));
			}

			airlineDTO = new AirlineDTO();

			airlineDTO.setAirlineInternalId(airline.getId().toString());
			airlineDTO.setAirlineName(airline.getName());
			airlineDTO.setWebsite(airline.getUrls().get(0));

			if (airline.as(AirlinePortal.class).getCompanyName() != null
					&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getCompanyName())) {
				airlineDTO.setCompanyName(airline.as(AirlinePortal.class).getCompanyName());
			}
			if (airline.as(AirlinePortal.class).getAirlineStatus() != null) {

				if (airline.as(AirlinePortal.class).getAirlineStatus().equals(AirlineStatus.ACTIVE)) {
					airlineDTO.setActivate(true);
				} else {
					airlineDTO.setActivate(false);
				}

			}
			if (airline.as(AirlinePortal.class).getIcaoCode() != null
					&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getIcaoCode())) {
				airlineDTO.setIcaoCode(airline.as(AirlinePortal.class).getIcaoCode());
			}

			if (airline.as(AirlinePortal.class).getIataCode() != null
					&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getIataCode())) {
				airlineDTO.setIataCode(airline.as(AirlinePortal.class).getIataCode());
			}

			if (airline.as(AirlinePortal.class).getCodeShare() != null
					&& StringUtils.isNotBlank(airline.as(AirlinePortal.class).getCodeShare())) {
				airlineDTO.setCodeShare(airline.as(AirlinePortal.class).getCodeShare());
			}
		} else {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}

		return airlineDTO;
	}

	/**
	 * Service Method to delete an Airline. Deleting an Airline also deletes its
	 * AirlineContacts
	 * 
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws NoSuchMessageException
	 * @throws ObjectExistsException
	 */
	public String deleteAirline(String airlineId)
			throws ObjectNotFoundException, NoSuchMessageException, BadRequestException, ObjectExistsException {
		if (StringUtils.isNotBlank(airlineId)) {
			Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
			if (airline != null) {
				List<AirlineContact> airlineContactList = airline.as(AirlinePortal.class).getAirlineContactList();
				Lopa lopa = Query.from(Lopa.class).where("cms.site.owner = ?", airline).first();
				if (lopa != null) {
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.LOPA, ECMSConstants.LOPA }, Locale.US),
							HttpStatus.FORBIDDEN));
				} else {
					if (airlineContactList != null && !airlineContactList.isEmpty()) {
						for (AirlineContact airlineContact : airlineContactList) {
							airlineContact.delete();
							logger.info("Deleted Airline Contact");
						}
					}

					airline.delete();
					return "Airline deleted successfully";
				}
			} else {
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Bad Request - Airline Id Cannot be Blank");
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}

	}

	/**
	 * 
	 * @param airlineId
	 * @param airlineDetails
	 * @return
	 * @throws ObjectNotFoundException
	 */

	public Boolean changeAirlineStatus(String airlineId, AirlineStatus status) throws ObjectNotFoundException {

		/*
		 * AirlinePortal airlinePortal = null; AirlineDTO airlineDTO = null;
		 */
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();

		if (airline != null) {
			airline.as(AirlinePortal.class).setAirlineStatus(status);
			airline.save();
			logger.debug("");
			// It will return false when we have the condition of running
			// processes with respect to an airline in place
			return true;
		} else {
			logger.error("Airline " + airlineId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}
}
