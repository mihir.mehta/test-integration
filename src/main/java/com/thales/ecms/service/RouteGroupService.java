package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;
import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Airport;
import com.thales.ecms.model.Route;
import com.thales.ecms.model.RouteGroup;
import com.thales.ecms.model.dto.AirportDTO;
import com.thales.ecms.model.dto.RouteDTO;
import com.thales.ecms.model.dto.RouteGroupDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * Service class to perform CRUD on RouteGroup
 * 
 * @author arjun.p
 *
 */
@Service
public class RouteGroupService {

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(RouteGroupService.class);

	/**
	 * A Service method to create a new RouteGroup
	 * 
	 * @param routeGroupDetails
	 *            RouteGroupDTO
	 * @return RouteGroupDTO
	 * @throws ObjectExistsException
	 * @throws BadRequestException
	 * @throws ObjectNotFoundException
	 */
	public RouteGroupDTO addRouteGroup(String airlineId, RouteGroupDTO routeGroupDetails)
			throws ObjectExistsException, BadRequestException, ObjectNotFoundException {
		RouteGroupDTO routeGroupDTO = new RouteGroupDTO();
		RouteGroup routeGroup = null;
		Set<Site> consumers = null;
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {
			consumers = Sets.newHashSet();
			consumers.add(airline);
		} else {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		String routeGroupName = routeGroupDetails.getRouteGroupName();
		if (routeGroupName != null && StringUtils.isNotBlank(routeGroupName)) {
			if (Query.from(RouteGroup.class).where("name = ?", routeGroupName).and("cms.site.owner = ?", airline)
					.first() != null) {
				// This RouteGroup already exists - throw an Exception
				logger.error("RouteGroup already exists");
				throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
						new Object[] { ECMSConstants.ROUTE_GROUP, "" }, Locale.US), HttpStatus.FORBIDDEN));
			}
			routeGroup = new RouteGroup();
			routeGroup.setName(routeGroupName);

			Set<RouteDTO> routesDTO = routeGroupDetails.getRoutes();
			logger.debug("Route Set : " + routesDTO);
			Set<Route> routes = new HashSet<Route>();

			if (routesDTO != null && !routesDTO.isEmpty()) {
				logger.debug("Route Set size : " + routesDTO);
				for (RouteDTO routeDTO : routesDTO) {

					Route route = Query.from(Route.class).where("id = ?", routeDTO.getRouteId()).first();
					if (route == null)
						throw new BadRequestException(
								new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US),
										HttpStatus.BAD_REQUEST));
					routes.add(route);
				}
			}
			routeGroup.setRoutes(routes);
			routeGroup.as(Site.ObjectModification.class).setOwner(airline);
			routeGroup.as(Site.ObjectModification.class).setConsumers(consumers);
			routeGroup.save();
			logger.debug("RouteGroup saved successfully");
			routeGroupDTO.setRouteGroupId(routeGroup.getId().toString());
			routeGroupDTO.setRouteGroupName(routeGroup.getName());
			routeGroupDTO.setRoutes(routesDTO);
		} else {
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}
		return routeGroupDTO;
	}

	/**
	 * Service Method to return a list of all RouteGroup. It returns an empty
	 * list in case no record found
	 * 
	 * @param airlineId
	 *            Airline ID
	 * @return List<RouteGroupDTO>
	 * @throws ObjectNotFoundException
	 */
	public List<RouteGroupDTO> getRouteGroups(String airlineId) throws ObjectNotFoundException {
		List<RouteGroupDTO> routeGroupDTOList = new ArrayList<RouteGroupDTO>();
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline == null)
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		List<RouteGroup> routeGroupList = Query.from(RouteGroup.class).and("cms.site.owner = ?", airline).selectAll();
		if (routeGroupList != null && !routeGroupList.isEmpty()) {
			for (RouteGroup routeGroup : routeGroupList) {
				RouteGroupDTO routeGroupDTO = new RouteGroupDTO();
				routeGroupDTO.setRouteGroupId(routeGroup.getId().toString());
				routeGroupDTO.setRouteGroupName(routeGroup.getName());
				Set<RouteDTO> routesDTO = new HashSet<>();
				Set<Route> routes = routeGroup.getRoutes();
				logger.debug("Route set : " + routes);
				if (routes != null && !routes.isEmpty()) {
					logger.debug("Route set size : " + routes.size());
					for (Route route : routes) {
						RouteDTO routeDTO = new RouteDTO();
						routeDTO.setRouteId(route.getId().toString());
						routeDTO.setName(route.getName());

						AirportDTO originAirportDTO = new AirportDTO();
						originAirportDTO.setAirportId(route.getOrigin().getId().toString());
						originAirportDTO.setAirportName(route.getOrigin().getAirportName());
						originAirportDTO.setAirportICAO(route.getOrigin().getAirportICAO());
						originAirportDTO.setAirportIATA(route.getOrigin().getAirportIATA());
						routeDTO.setOrigin(originAirportDTO);

						AirportDTO destinationAirportDTO = new AirportDTO();
						destinationAirportDTO.setAirportId(route.getDestination().getId().toString());
						destinationAirportDTO.setAirportName(route.getDestination().getAirportName());
						destinationAirportDTO.setAirportICAO(route.getDestination().getAirportICAO());
						destinationAirportDTO.setAirportIATA(route.getDestination().getAirportIATA());
						routeDTO.setDestination(destinationAirportDTO);

						routesDTO.add(routeDTO);
					}
				}
				routeGroupDTO.setRoutes(routesDTO);
				routeGroupDTOList.add(routeGroupDTO);
			}
		}
		return routeGroupDTOList;
	}

	/**
	 * Service Method to return a RouteGroup.
	 * 
	 * @param airlineId
	 *            Airline ID
	 * @param routeGroupId
	 *            RouteGroup ID
	 * @return RouteGroupDTO
	 * @throws ObjectNotFoundException
	 */
	public RouteGroupDTO getRouteGroupById(String airlineId, String routeGroupId) throws ObjectNotFoundException {
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline == null)
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		RouteGroup routeGroup = Query.from(RouteGroup.class).where("id = ?", routeGroupId)
				.and("cms.site.owner = ?", airline).first();
		RouteGroupDTO routeGroupDTO = null;
		if (routeGroup != null) {
			routeGroupDTO = new RouteGroupDTO();
			routeGroupDTO.setRouteGroupId(routeGroup.getId().toString());
			routeGroupDTO.setRouteGroupName(routeGroup.getName());
			Set<RouteDTO> routesDTO = new HashSet<>();
			Set<Route> routes = routeGroup.getRoutes();
			logger.debug("Route set : " + routes);
			if (routes != null && !routes.isEmpty()) {
				logger.debug("Route set size : " + routes.size());
				for (Route route : routes) {
					RouteDTO routeDTO = new RouteDTO();
					routeDTO.setRouteId(route.getId().toString());
					routeDTO.setName(route.getName());

					AirportDTO originAirportDTO = new AirportDTO();
					originAirportDTO.setAirportId(route.getOrigin().getId().toString());
					originAirportDTO.setAirportName(route.getOrigin().getAirportName());
					originAirportDTO.setAirportICAO(route.getOrigin().getAirportICAO());
					originAirportDTO.setAirportIATA(route.getOrigin().getAirportIATA());
					routeDTO.setOrigin(originAirportDTO);

					AirportDTO destinationAirportDTO = new AirportDTO();
					destinationAirportDTO.setAirportId(route.getDestination().getId().toString());
					destinationAirportDTO.setAirportName(route.getDestination().getAirportName());
					destinationAirportDTO.setAirportICAO(route.getDestination().getAirportICAO());
					destinationAirportDTO.setAirportIATA(route.getDestination().getAirportIATA());
					routeDTO.setDestination(destinationAirportDTO);

					routesDTO.add(routeDTO);
				}
			}
			routeGroupDTO.setRoutes(routesDTO);
		} else {
			throw new ObjectNotFoundException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] { ECMSConstants.ROUTE_GROUP}, Locale.US), HttpStatus.NOT_FOUND));
		}
		return routeGroupDTO;
	}

	/**
	 * Service Method to update a RouteGroup
	 * 
	 * @param airlineId
	 *            Airline ID
	 * @param routeGroupId
	 * @param routeGroupDetails
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public RouteGroupDTO updateRouteGroup(String airlineId, String routeGroupId, RouteGroupDTO routeGroupDetails)
			throws ObjectNotFoundException, BadRequestException {
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline == null)
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		RouteGroupDTO routeGroupDTO = null;
		RouteGroup routeGroup = Query.from(RouteGroup.class).where("id = ?", routeGroupId)
				.and("cms.site.owner = ?", airline).first();
		if (routeGroup == null) {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.ROUTE_GROUP }, Locale.US), HttpStatus.NOT_FOUND));
		}
		String routeGroupName = routeGroupDetails.getRouteGroupName();
		if (routeGroupName != null && StringUtils.isNotBlank(routeGroupName)) {
			routeGroup.setName(routeGroupName);
		} else {
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}
		Set<RouteDTO> routesDTO = routeGroupDetails.getRoutes();
		logger.debug("Route Set : " + routesDTO);
		Set<Route> routes = new HashSet<>();

		if (routesDTO != null && !routesDTO.isEmpty()) {
			logger.debug("Route Set size : " + routesDTO.size());
			for (RouteDTO routeDTO : routesDTO) {

				Route route = Query.from(Route.class).where("id = ?", routeDTO.getRouteId()).first();
				if (route == null)
					throw new BadRequestException(
							new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US),
									HttpStatus.BAD_REQUEST));

				route.setName(routeDTO.getName());

				Airport origin = new Airport();
				origin.setAirportName(routeDTO.getOrigin().getAirportName());
				origin.setAirportICAO(routeDTO.getOrigin().getAirportICAO());
				origin.setAirportIATA(routeDTO.getOrigin().getAirportIATA());
				route.setOrigin(origin);

				Airport destination = new Airport();
				destination.setAirportName(routeDTO.getDestination().getAirportName());
				destination.setAirportICAO(routeDTO.getDestination().getAirportICAO());
				destination.setAirportIATA(routeDTO.getDestination().getAirportIATA());
				route.setDestination(destination);

				routes.add(route);
			}
		}
		routeGroupDTO = new RouteGroupDTO();
		routeGroup.setRoutes(routes);
		routeGroup.save();
		routeGroupDTO.setRouteGroupId(routeGroup.getId().toString());
		routeGroupDTO.setRouteGroupName(routeGroup.getName());
		routeGroupDTO.setRoutes(routesDTO);
		routeGroupDTO.setRouteGroupId(routeGroup.getId().toString());
		return routeGroupDTO;
	}
	
	public RouteGroupDTO updateRouteGroupFromXlsx(String airlineId,String routeGroupId, RouteGroupDTO routeGroupDetails) throws ObjectNotFoundException,BadRequestException{
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline == null)
			throw new ObjectNotFoundException(new ErrorResponse("Airline does not exist", HttpStatus.NOT_FOUND));	
		RouteGroupDTO routeGroupDTO = null;
		RouteGroup routeGroup = Query.from(RouteGroup.class).where("id = ?",routeGroupId).first();
		if(routeGroup == null){
			throw new ObjectNotFoundException(new ErrorResponse("RouteGroup Not Found", HttpStatus.NOT_FOUND));
		}
		String routeGroupName = routeGroupDetails.getRouteGroupName();
		if(routeGroupName != null && StringUtils.isNotBlank(routeGroupName)){
			routeGroup.setName(routeGroupName);
		}else{
			throw new BadRequestException(new ErrorResponse("Bad Request - Empty RouteGroup Name", HttpStatus.BAD_REQUEST));
		}
		Set<RouteDTO> routesDTO = routeGroupDetails.getRoutes();
		logger.debug("Route Set : "+routesDTO);
		Set<Route> routes = new HashSet<>();
		
		Map<String, RouteDTO>mapOfAlreadyExistingRoute = new HashMap<>();
//		Iterating over all existing routes of routegroup
		for(Route route : routeGroup.getRoutes()){
//			checking whether the set of routes already in routegroup
			for(RouteDTO routeDTO : routesDTO){
//				checking whether the route sent for update already in set of routes in respective routegroup
				if(route.getName().equals(routeDTO.getName())){
					logger.info("same name route already exists for this routegroup");
					logger.info("Route Name already exists : "+routeDTO.getName());
					mapOfAlreadyExistingRoute.put(routeDTO.getName(), routeDTO);
				}
			}
		}
		if(routesDTO!=null && !routesDTO.isEmpty()){
			logger.debug("Route Set size : "+routesDTO.size());
			for(RouteDTO routeDTO : routesDTO){					
//				Add route to routegroup only if not already present in Routes set of routegroup
				if(!mapOfAlreadyExistingRoute.containsKey(routeDTO.getName())){
					Route route = Query.from(Route.class).where("id = ?", routeDTO.getRouteId()).first();
					if(route==null)
						throw new BadRequestException(new ErrorResponse("Bad Request - Route does not exists Id : "+routeDTO.getRouteId(), HttpStatus.BAD_REQUEST));
					
					route.setName(routeDTO.getName());
										
					Airport origin = new Airport();					
					origin.setAirportName(routeDTO.getOrigin().getAirportName());
					origin.setAirportICAO(routeDTO.getOrigin().getAirportICAO());
					origin.setAirportIATA(routeDTO.getOrigin().getAirportIATA());
					route.setOrigin(origin);
					
					Airport destination=new Airport();										
					destination.setAirportName(routeDTO.getDestination().getAirportName());
					destination.setAirportICAO(routeDTO.getDestination().getAirportICAO());
					destination.setAirportIATA(routeDTO.getDestination().getAirportIATA());
					route.setDestination(destination);					
					
					routes.add(route);
				}				
			}
		}
		routeGroupDTO = new RouteGroupDTO();
		routeGroup.setRoutes(routes);
		routeGroup.save();			
		routeGroupDTO.setRouteGroupId(routeGroup.getId().toString());
		routeGroupDTO.setRouteGroupName(routeGroup.getName());
		routeGroupDTO.setRoutes(routesDTO);		
		routeGroupDTO.setRouteGroupId(routeGroup.getId().toString());		
		return routeGroupDTO;
	}


	/**
	 * Service Method to delete a RouteGroup
	 * 
	 * @param routeGroupId
	 * @return boolean status
	 * @throws ObjectNotFoundException
	 */
	public Boolean deleteRouteGroup(String airlineId, String routeGroupId) throws ObjectNotFoundException {
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline == null)
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));

		RouteGroup routeGroup = Query.from(RouteGroup.class).where("id = ?", routeGroupId)
				.and("cms.site.owner = ?", airline).first();
		if (routeGroup != null) {
			routeGroup.delete();
			return true;
		} else {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.ROUTE_GROUP }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}

}
