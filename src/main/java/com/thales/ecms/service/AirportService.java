package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Airport;
import com.thales.ecms.model.RouteGroup;
import com.thales.ecms.model.dto.AirportDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * Service Class to perform CRUD operations on Airport.
 * 
 * @author shruti_n
 * 
 */

@Service
public class AirportService {

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(AirportService.class);

	/**
	 * Service Method to add an Airport
	 * 
	 * @param airportDetails
	 * @throws ObjectExistsException
	 * @return airportDTO
	 */

	public AirportDTO addAirport(AirportDTO airportDetails) throws ObjectExistsException,BadRequestException {

		AirportDTO airportDTO = null;
		Airport airport = new Airport();
		airportDTO = new AirportDTO();

		if (StringUtils.isNotBlank(airportDetails.getAirportName())) {
			airport.setAirportName(airportDetails.getAirportName().trim());
		} else {
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}
		if (StringUtils.isNotBlank(airportDetails.getAirportIATA())) {
			if (Query.from(Airport.class).where("airportIATA = ?", airportDetails.getAirportIATA().trim()).first() != null) {
				// This Airport IATA already exists - throw an Exception

				logger.error("Airport IATA is Already Present");
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.AIRPORT, ECMSConstants.IATA }, Locale.US),
						HttpStatus.FORBIDDEN));
			} else {
				airport.setAirportIATA(airportDetails.getAirportIATA().trim());

			}
		}
		if (StringUtils.isNotBlank(airportDetails.getAirportICAO())) {
			if (Query.from(Airport.class).where("airportICAO = ?", airportDetails.getAirportICAO().trim()).first() != null) {
				// This Airport ICAO already exists - throw an Exception
				logger.error("Airport ICAO is Already Present");
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.AIRPORT, ECMSConstants.ICAO }, Locale.US),
						HttpStatus.FORBIDDEN));
			} else {
				airport.setAirportICAO(airportDetails.getAirportICAO().trim());
			}
		}

		logger.debug("Airport instance saved successfully! " + airport.toString());
		airport.save();

		airportDTO.setAirportId(airport.getId().toString());
		airportDTO.setAirportName(airportDetails.getAirportName().trim());
		airportDTO.setAirportIATA(airportDetails.getAirportIATA().trim());
		airportDTO.setAirportICAO(airportDetails.getAirportICAO().trim());
		return airportDTO;
	}

	/**
	 * Service Method to return list of airports
	 * 
	 * @param airportDetails
	 * @return airportDTOList
	 */

	public List<AirportDTO> getAirport() {

		List<AirportDTO> airportDTOList = null;
		List<Airport> airportList = Query.from(Airport.class).selectAll();

		if (!CollectionUtils.isEmpty(airportList)) {
			airportDTOList = new ArrayList<>();
			for (Airport airport : airportList) {
				AirportDTO airportDTO = new AirportDTO();
				airportDTO.setAirportId(airport.getId().toString());
				airportDTO.setAirportName(airport.getAirportName());
				airportDTO.setAirportICAO(airport.getAirportICAO());
				airportDTO.setAirportIATA(airport.getAirportIATA());

				airportDTOList.add(airportDTO);
			}
		}
		return airportDTOList;
	}

	/**
	 * Service Method to update an Airport
	 * 
	 * @param airportId
	 * @param airportDetails
	 * @throws ObjectNotFoundException
	 * @return airportDTO
	 */

	public AirportDTO updateAirport(String airportId, AirportDTO airportDetails)
			throws ObjectNotFoundException, ObjectExistsException, BadRequestException {

		AirportDTO airportDTO = null;
		Airport airport = Query.from(Airport.class).where("id = ?", airportId).first();
		if (airport == null) {
			logger.error("Airport not found!");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRPORT }, Locale.US), HttpStatus.NOT_FOUND));
		}

		String newAirportName = airportDetails.getAirportName();
		if (StringUtils.isNotBlank(newAirportName)) {
			airport.setAirportName(newAirportName.trim());
		} else {
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}

		String newAirportIATA = airportDetails.getAirportIATA();
		if (StringUtils.isNotBlank(newAirportIATA)) {
			Airport airport2 = Query.from(Airport.class).where("airportIATA = ?", newAirportIATA.trim()).first();
			if (null != airport2) {
				logger.error("Airport IATA is Already Exists");
				throw new ObjectNotFoundException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.AIRPORT, ECMSConstants.IATA }, Locale.US),
						HttpStatus.FORBIDDEN));
			} else {
				airport.setAirportIATA(newAirportIATA.trim());

			}
		}
		String newAirportICAO = airportDetails.getAirportICAO();
		if (StringUtils.isNotBlank(newAirportICAO)) {
			Airport airport3 = Query.from(Airport.class).where("airportICAO = ?", newAirportICAO.trim()).first();
			if (null != airport3) {
				logger.error("Airport ICAO is Already Exists");
				throw new ObjectNotFoundException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.AIRPORT, ECMSConstants.ICAO }, Locale.US),
						HttpStatus.FORBIDDEN));
			} else {
				airport.setAirportICAO(newAirportICAO.trim());
			}
		}
		logger.debug("Airport instance updated successfully! " + airport.toString());
		airport.save();
		airportDTO = new AirportDTO();
		airportDTO.setAirportId(airport.getId().toString());
		airportDTO.setAirportName(airport.getAirportName().trim());
		airportDTO.setAirportICAO(airport.getAirportICAO().trim());
		airportDTO.setAirportIATA(airport.getAirportIATA().trim());

		return airportDTO;
	}

	/**
	 * Service Method to delete an Airport
	 * 
	 * @param airportId
	 * @throws ObjectNotFoundException
	 * @return airportDTO
	 */

	public Boolean deleteAirport(String airportId) throws ObjectNotFoundException {

		Airport airport = Query.from(Airport.class).where("id = ?", airportId).first();
		if (null != airport) {
			airport.delete();
			return true;
		} else {
			logger.error("Airport not found!");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRPORT }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}

	public AirportDTO getAirportById(String airportId) {

		AirportDTO airportDTO = null;

		Airport airline = Query.from(Airport.class).where("id = ?", airportId).first();
		if (null != airline) {
			airportDTO = new AirportDTO();
			airportDTO.setAirportId(airline.getId().toString());
			airportDTO.setAirportName(airline.getAirportName());
			airportDTO.setAirportICAO(airline.getAirportICAO());
			airportDTO.setAirportIATA(airline.getAirportIATA());

		}
		return airportDTO;
	}
}