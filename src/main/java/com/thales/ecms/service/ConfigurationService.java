package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Sets;
import com.psddev.cms.db.Site;
import com.psddev.dari.db.ObjectType;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.Configuration.ConfigurationStatus;
import com.thales.ecms.model.Configuration.ConfigurationTitle;
import com.thales.ecms.model.Configuration.ConfigurationTitle.Priority;
//import com.thales.ecms.model.Configuration.ServerGroup;
import com.thales.ecms.model.ContentType;
import com.thales.ecms.model.Lopa;
import com.thales.ecms.model.Lru;
import com.thales.ecms.model.LruType;
import com.thales.ecms.model.PaxguiLanguage;
import com.thales.ecms.model.Rule;
import com.thales.ecms.model.dto.ConfigurationDTO;
import com.thales.ecms.model.dto.ConfigurationRequestDTO;
import com.thales.ecms.model.dto.ConfigurationRequestDTO.ConfigurationTitleDTO;
import com.thales.ecms.model.dto.ConfigurationRequestDTO.ServerGroupDTO;
import com.thales.ecms.model.dto.OrderResponseDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * Service Class to perform CRUD operations on RuleService
 */
@Service
public class ConfigurationService {

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(ConfigurationService.class);

	/**
	 * Service Method to return a list of all ConfigurationService
	 * 
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public List<ConfigurationRequestDTO> getconfigurationList(String airlineId) throws ObjectNotFoundException{
		List<ConfigurationRequestDTO> configurationDTOList = new ArrayList<>();
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (Objects.nonNull(airline)) {
			List<Configuration> configuration = Query.from(Configuration.class)
					.where("configurationStatus = ?", ConfigurationStatus.ACTIVE).and("cms.site.owner = ?", airline).selectAll();
			if (Objects.nonNull(configuration)) {
				configurationDTOList = new ArrayList<ConfigurationRequestDTO>();
				for (Configuration config : configuration) {										
					configurationDTOList.add(populateConfigurationDTO(config));
				}
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}
		return configurationDTOList;
	}

	/**
	 * Method for delete Configuration Of an Airline.
	 * 
	 * @param airlineId
	 * @param configurationId
	 * @param airlineConfigDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */

	public Boolean deleteconfigurationById(String airlineId, String configurationId) throws ObjectNotFoundException {		
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (Objects.nonNull(airline)) {
			Configuration configuration = Query.from(Configuration.class).where("id = ?", configurationId).and("cms.site.owner = ?", airline).first();
			if (Objects.nonNull(configuration)){
				configuration.delete();				
				return Boolean.TRUE;
			}else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));
			}
		} else {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}		
	}

	public ConfigurationRequestDTO deactivateconfigurationById(String airlineId, String configurationId)
			throws ObjectNotFoundException {
		ConfigurationRequestDTO configurationDTO = null;
		Configuration configuration = null;
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (Objects.nonNull(airline)) {
			configuration = Query.from(Configuration.class).where("id = ?", configurationId).and("cms.site.owner = ?", airline).first();
			if (Objects.nonNull(configuration)){
				configuration.setConfigurationStatus(ConfigurationStatus.INACTIVE);
				configuration.save();
				configurationDTO = populateConfigurationDTO(configuration);
			}else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));
			}
		} else {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}

		return configurationDTO;
	}

	/**
	 * Method to get a Configuration Of an Airline.
	 * 
	 * @param airlineId
	 * @param configurationId
	 * @return Configuration
	 * @throws ObjectNotFoundException
	 * 
	 * @author arjun.p
	 */
	public Configuration getConfigurationById(String airlineId, String configurationId) throws ObjectNotFoundException {
		Configuration configuration = null;
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (Objects.nonNull(airline)) {
			configuration = Query.from(Configuration.class).where("id = ?", configurationId).and("cms.site.owner = ?", airline).first();
		} else {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}
		return configuration;
	}
	
	/**
	 * Method to Get Configuration By configurationId
	 * 
	 * @param airlineId
	 * @param configurationId
	 * @return ConfigurationRequestDTO
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	public ConfigurationRequestDTO getConfigurationByCfgId(String airlineId, String configurationId) throws ObjectNotFoundException, BadRequestException {
		ConfigurationRequestDTO configurationRequestDTO = null;
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (Objects.nonNull(airline)) {
			Configuration configuration = Query.from(Configuration.class).where("id = ?", configurationId).and("cms.site.owner = ?", airline).first();
			if(configuration==null)
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));
			if(configuration.getConfigurationStatus()!=null && configuration.getConfigurationStatus()==ConfigurationStatus.INACTIVE)
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_ACCESSIBLE,
						new Object[] { ECMSConstants.CONFIGURATION, ECMSConstants.CONFIGURATION_INACTIVE }, Locale.US), HttpStatus.FORBIDDEN));
			configurationRequestDTO = populateConfigurationDTO(configuration);
		} else {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}
		return configurationRequestDTO;
	}
	

	/**
	 * Method to get an ObjectType of a Configuration
	 * 
	 * @param airlineId
	 * @param configurationId
	 * @param contentTypeId
	 * @return ObjectType
	 * @throws ObjectNotFoundException
	 * 
	 * @author arjun.p
	 */
	public ObjectType getContentTypeById(String contentTypeId)
			throws ObjectNotFoundException {
		ObjectType objectType = Query.from(ObjectType.class).where("id = ?", contentTypeId).first();
		if(objectType==null)
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
					ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.CONTENT_TYPE }, Locale.US), HttpStatus.FORBIDDEN));		
		return objectType;
	}

	/**
	 * Method to get a Configuration for an Order by configurationId
	 * 
	 * @param airlineId
	 * @param configurationId
	 * @param contentTypeId
	 * @return ObjectType
	 * @throws ObjectNotFoundException
	 * 
	 * @author arjun.p
	 */
	public Configuration getConfigurationById(OrderResponseDTO orderDTO, String airlineId, String configurationId)
			throws ObjectNotFoundException {
		Configuration configuration = null;
		Set<ConfigurationDTO> configureDTOs = orderDTO.getConfigureDTOSet();
		if (Objects.nonNull(configureDTOs) && !configureDTOs.isEmpty()) {
			for (ConfigurationDTO configureDTO : configureDTOs) {
				if (configurationId.equals(configureDTO.getConfigurationId())) {
					configuration = getConfigurationById(airlineId, configurationId);
					break;
				}
			}
		} else {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));
		}
		return configuration;
	}
	
	
	public ConfigurationRequestDTO addConfiguration(String airlineId, ConfigurationRequestDTO configurationDTO)
			throws BadRequestException, ObjectExistsException, NoSuchMessageException, ObjectNotFoundException {
		List<Lopa> lopaList = null;
		
		Set<PaxguiLanguage> paxguilanguage = null;
		Set<Lru> lrus = null;
		Set<LruType> lruTypes = null;
		Set<ObjectType> contentTypeList = null;
		Set<Rule> rules = null;
//		List<ServerGroup> serverGroupList = null;
		List<ConfigurationTitle> configurationTitles = null;
		Configuration configuration = new Configuration();

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (Objects.nonNull(airline)) {
			Set<Site> consumers = null;						
			consumers = Sets.newHashSet();
			consumers.add(airline);			
			if (StringUtils.isNotBlank(configurationDTO.getConfigurationName())) {
				Configuration existingConfiguration = Query.from(Configuration.class).where("name = ?", configurationDTO.getConfigurationName()).and("cms.site.owner = ?", airline).first();
				if (existingConfiguration == null) {					
					configuration.setName(configurationDTO.getConfigurationName());
					if(StringUtils.isNotBlank(configurationDTO.getVersion()))
						configuration.setVersion(configurationDTO.getVersion());
					if(StringUtils.isNotBlank(configurationDTO.getPlatform()))
						configuration.setPlatform(configurationDTO.getPlatform());
					if (StringUtils.isNotBlank(configurationDTO.getConfigurationStatus())) {
						if (configurationDTO.getConfigurationStatus()
								.equalsIgnoreCase(Configuration.ConfigurationStatus.ACTIVE.toString())) {
							configuration.setConfigurationStatus(Configuration.ConfigurationStatus.ACTIVE);
						} else if (configurationDTO.getConfigurationStatus()
								.equalsIgnoreCase(Configuration.ConfigurationStatus.INACTIVE.toString())) {
							configuration.setConfigurationStatus(Configuration.ConfigurationStatus.INACTIVE);
						} else if (configurationDTO.getConfigurationStatus()
								.equalsIgnoreCase(Configuration.ConfigurationStatus.DEPRECATED.toString())) {
							configuration.setConfigurationStatus(Configuration.ConfigurationStatus.DEPRECATED);
						} else {
							throw new BadRequestException(
									new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER, 
											new Object[]{ECMSConstants.CONFIGURATION_STATUS}, Locale.US),
											HttpStatus.FORBIDDEN));
						}
					}

					if (StringUtils.isNotBlank(configurationDTO.getPackagingType())) {
						if (configurationDTO.getPackagingType()
								.equalsIgnoreCase(Configuration.PackagingType.CIL.toString())) {
							configuration.setPackagingType(Configuration.PackagingType.CIL);
						} else if (configurationDTO.getPackagingType()
								.equalsIgnoreCase(Configuration.PackagingType.XCIL.toString())) {
							configuration.setPackagingType(Configuration.PackagingType.XCIL);
						} else {
							throw new BadRequestException(
									new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER, 
											new Object[]{ECMSConstants.PACKAGING_TYPE}, Locale.US),
											HttpStatus.FORBIDDEN));
						}
					}

					if (!CollectionUtils.isEmpty(configurationDTO.getLopaIdList())) {
						lopaList = new ArrayList<>();
						for (String lopaId : configurationDTO.getLopaIdList()) {
							Lopa lopa = Query.from(Lopa.class).where("id = ?", lopaId).and("cms.site.owner = ?", airline).first();
							if(lopa==null){
								throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
										ECMSConstants.OBJECT_NOT_FOUND,
										new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.FORBIDDEN));
							}
							lopaList.add(lopa);
						}
					}

					if (!CollectionUtils.isEmpty(configurationDTO.getInterfaceLanguagesId())) {
						paxguilanguage = new HashSet<>();
						for (String languageId : configurationDTO.getInterfaceLanguagesId()) {
							PaxguiLanguage paxgui = Query.from(PaxguiLanguage.class).where("id = ?", languageId).and("cms.site.owner = ?", airline).first();
							if(paxgui==null){
								throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
										ECMSConstants.OBJECT_NOT_FOUND,
										new Object[] { ECMSConstants.PAXGUILANGUAGE }, Locale.US), HttpStatus.FORBIDDEN));
							}
							paxguilanguage.add(paxgui);
						}
					}

					if (!CollectionUtils.isEmpty(configurationDTO.getLrusId())) {
						lrus = new HashSet<>();
						for (String lruId : configurationDTO.getLrusId()) {
							Lru lru = Query.from(Lru.class).where("id = ?", lruId).and("cms.site.owner = ?", airline).first();
							if(lru==null){
								throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
										ECMSConstants.OBJECT_NOT_FOUND,
										new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.FORBIDDEN));
							}
							lrus.add(lru);
						}
					}

					if (!CollectionUtils.isEmpty(configurationDTO.getLruTypesId())) {
						lruTypes = new HashSet<>();
						for (String lruTypeId : configurationDTO.getLruTypesId()) {
							LruType lruType = Query.from(LruType.class).where("id = ?", lruTypeId).first();
							if(lruType==null){
								throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
										ECMSConstants.OBJECT_NOT_FOUND,
										new Object[] { ECMSConstants.LRU_TYPE }, Locale.US), HttpStatus.FORBIDDEN));
							}
							lruTypes.add(lruType);
						}
					}

					if (!CollectionUtils.isEmpty(configurationDTO.getServerGroupList())) {
//						serverGroupList = new ArrayList<>();
//						for (ServerGroupDTO serverGroupDTO : configurationDTO.getServerGroupList()) {
//							ServerGroup serverGroup = new ServerGroup();
//							serverGroup.setName(serverGroupDTO.getName());
//							Set<Lru> lrus1=null;
//							if (!CollectionUtils.isEmpty(serverGroupDTO.getLrus())) {
//								lrus1 = new HashSet<>();
//								for (String lruId : serverGroupDTO.getLrus()) {
//									Lru lru = Query.from(Lru.class).where("id = ?", lruId).and("cms.site.owner = ?", airline).first();
//									if(lru==null){
//										throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
//												ECMSConstants.OBJECT_NOT_FOUND,
//												new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.FORBIDDEN));
//									}
//									lrus1.add(lru);
//								}
//							}
//							serverGroup.setLrus(lrus1);
//							serverGroupList.add(serverGroup);
//						}
					}

					if (!CollectionUtils.isEmpty(configurationDTO.getContentTypeIdList())) {
						contentTypeList = new HashSet<>();
						for (String contentTypeId : configurationDTO.getContentTypeIdList()) {
							ObjectType objectType = Query.from(ObjectType.class).where("id = ?", contentTypeId).first();
							if(objectType==null){
								throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
										ECMSConstants.OBJECT_NOT_FOUND,
										new Object[] { ECMSConstants.CONTENT_TYPE }, Locale.US), HttpStatus.FORBIDDEN));
							}
							contentTypeList.add(objectType);
						}
					}

					if (!CollectionUtils.isEmpty(configurationDTO.getRulesId())) {
						rules = new HashSet<>();
						for (String ruleId : configurationDTO.getRulesId()) { 
							Rule rule = Query.from(Rule.class).where("id = ?", ruleId).and("cms.site.owner = ?", airline).first();
							if(rule==null){
								throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
										ECMSConstants.OBJECT_NOT_FOUND,
										new Object[] { ECMSConstants.RULE }, Locale.US), HttpStatus.FORBIDDEN));
							}
							rules.add(rule);
						}
					}

					if (!CollectionUtils.isEmpty(configurationDTO.getTitleList())) {
						configurationTitles = new ArrayList<>();
						for (ConfigurationTitleDTO configurationTitleDTO : configurationDTO.getTitleList()) {
							ConfigurationTitle configurationTitle = new ConfigurationTitle();
							if(StringUtils.isNotEmpty(configurationTitleDTO.getContentId())){
								ContentType contentType = Query.from(ContentType.class).where("id = ?", configurationTitleDTO.getContentId()).and("cms.site.owner = ?", airline).first();
								if(contentType==null)
										throw new BadRequestException(
												new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER, 
														new Object[]{ECMSConstants.CONTENT_TYPE}, Locale.US),
														HttpStatus.FORBIDDEN));
								configurationTitle.setContent(contentType);
							}
							if(StringUtils.isNotBlank(configurationTitleDTO.getContentStatus())){
								if (configurationTitleDTO.getContentStatus()
										.equalsIgnoreCase(Configuration.ConfigurationTitle.Status.Incomplete.toString())) {
									configurationTitle.setContentStatus(Configuration.ConfigurationTitle.Status.Incomplete);
								} else if (configurationDTO.getConfigurationStatus()
										.equalsIgnoreCase(Configuration.ConfigurationTitle.Status.Validated.toString())) {
									configurationTitle.setContentStatus(Configuration.ConfigurationTitle.Status.Validated);
								} else if (configurationDTO.getConfigurationStatus()
										.equalsIgnoreCase(Configuration.ConfigurationTitle.Status.Completed.toString())) {
									configurationTitle.setContentStatus(Configuration.ConfigurationTitle.Status.Completed);
								} else {
									throw new BadRequestException(
											new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER, 
													new Object[]{ECMSConstants.CONFIGURATION_TITLE_STATUS}, Locale.US),
													HttpStatus.FORBIDDEN));
								}								
							}														
							
							if (!CollectionUtils.isEmpty(configurationTitleDTO.getDestinations())) {
								List<LruType> lruTypes1 = new ArrayList<>();
								for (String lruTypeId : configurationTitleDTO.getDestinations()) {
									LruType lruType = Query.from(LruType.class).where("id = ?", lruTypeId).first();
									if(lruType==null){
										throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
												ECMSConstants.OBJECT_NOT_FOUND,
												new Object[] { ECMSConstants.LRU_TYPE }, Locale.US), HttpStatus.FORBIDDEN));
									}
									lruTypes1.add(lruType);
								}
								configurationTitle.setDestinations(lruTypes1);
							}
							if(configurationTitleDTO.getEndDate()!=null && configurationTitleDTO.getEndDate()>0)
								configurationTitle.setEndDate(new Date(configurationTitleDTO.getEndDate()));
							if(configurationTitleDTO.getStartDate()!=null && configurationTitleDTO.getStartDate()>0)
								configurationTitle.setStartDate(new Date(configurationTitleDTO.getStartDate()));
							if(configurationTitleDTO.getPriority()!=null){
								if(configurationTitleDTO.getPriority().equalsIgnoreCase(Priority.Default.toString()))
									configurationTitle.setPriority(Priority.Default);
								else if(configurationTitleDTO.getPriority().equalsIgnoreCase(Priority.High.toString()))
									configurationTitle.setPriority(Priority.High);
							}														
							configurationTitles.add(configurationTitle);
						}
					} 

					configuration.setLopaList(lopaList);
					configuration.setInterfaceLanguages(paxguilanguage);
					configuration.setLrus(lrus);
					configuration.setLruTypes(lruTypes);
//					configuration.setServerGroupList(serverGroupList);
					configuration.setContentTypeList(contentTypeList);
					configuration.setRules(rules);
					configuration.setTitleList(configurationTitles);
					Date date = new Date();
					configuration.setCreationDate(date);
					configuration.setLastModifiedDate(date);
					configuration.as(Site.ObjectModification.class).setOwner(airline);
					configuration.as(Site.ObjectModification.class).setConsumers(consumers);
					configuration.save();
					configurationDTO.setConfigurationId(configuration.getId().toString());
					configurationDTO.setCreationDate(configuration.getCreationDate().getTime());
					configurationDTO.setLastModifiedDate(configuration.getLastModifiedDate().getTime());
				} else {
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS, 
									new Object[]{ECMSConstants.CONFIGURATION,ECMSConstants.CONFIGURATION_NAME}, Locale.US), 
							HttpStatus.FORBIDDEN));
				}
			} else {
				throw new BadRequestException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
								new Object[] { ECMSConstants.CONFIGURATION_NAME}, Locale.US),
						HttpStatus.FORBIDDEN));
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}		
		return configurationDTO;
	}
	
	
	
	
	public ConfigurationRequestDTO updateConfiguration(String airlineId, String configurationId, ConfigurationRequestDTO configurationDTO)
			throws BadRequestException, ObjectExistsException, NoSuchMessageException, ObjectNotFoundException {
		List<Lopa> lopaList = null;		
		Set<PaxguiLanguage> paxguilanguage = null;
		Set<Lru> lrus = null;
		Set<LruType> lruTypes = null;
		Set<ObjectType> contentTypeList = null;
		Set<Rule> rules = null;
//		List<ServerGroup> serverGroupList = null;
		List<ConfigurationTitle> configurationTitles = null;
		Configuration configuration = null;
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (Objects.nonNull(airline)) {
			Set<Site> consumers = null;						
			consumers = Sets.newHashSet();
			consumers.add(airline);
			Configuration existingConfiguration=Query.from(Configuration.class).where("id = ?", configurationId).and("cms.site.owner = ?", airline).first();			
			if(Objects.nonNull(existingConfiguration)){
				configuration = existingConfiguration;
				if(StringUtils.isNotBlank(configurationDTO.getConfigurationName()))
					configuration.setName(configurationDTO.getConfigurationName());
				if(StringUtils.isNotBlank(configurationDTO.getVersion()))
					configuration.setVersion(configurationDTO.getVersion());
				if(StringUtils.isNotBlank(configurationDTO.getPlatform()))
					configuration.setPlatform(configurationDTO.getPlatform());
				if (StringUtils.isNotBlank(configurationDTO.getConfigurationStatus())) {
					if (configurationDTO.getConfigurationStatus()
							.equalsIgnoreCase(Configuration.ConfigurationStatus.ACTIVE.toString())) {
						configuration.setConfigurationStatus(Configuration.ConfigurationStatus.ACTIVE);
					} else if (configurationDTO.getConfigurationStatus()
							.equalsIgnoreCase(Configuration.ConfigurationStatus.INACTIVE.toString())) {
						configuration.setConfigurationStatus(Configuration.ConfigurationStatus.INACTIVE);
					} else if (configurationDTO.getConfigurationStatus()
							.equalsIgnoreCase(Configuration.ConfigurationStatus.DEPRECATED.toString())) {
						configuration.setConfigurationStatus(Configuration.ConfigurationStatus.DEPRECATED);
					} else {
						throw new BadRequestException(
								new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER, 
										new Object[]{ECMSConstants.CONFIGURATION_STATUS}, Locale.US),
										HttpStatus.FORBIDDEN));
					}
				}

				if (StringUtils.isNotBlank(configurationDTO.getPackagingType())) {
					if (configurationDTO.getPackagingType()
							.equalsIgnoreCase(Configuration.PackagingType.CIL.toString())) {
						configuration.setPackagingType(Configuration.PackagingType.CIL);
					} else if (configurationDTO.getPackagingType()
							.equalsIgnoreCase(Configuration.PackagingType.XCIL.toString())) {
						configuration.setPackagingType(Configuration.PackagingType.XCIL);
					} else {
						throw new BadRequestException(
								new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER, 
										new Object[]{ECMSConstants.PACKAGING_TYPE}, Locale.US),
										HttpStatus.FORBIDDEN));
					}
				}

				if (!CollectionUtils.isEmpty(configurationDTO.getLopaIdList())) {
					lopaList = new ArrayList<>();
					for (String lopaId : configurationDTO.getLopaIdList()) {
						Lopa lopa = Query.from(Lopa.class).where("id = ?", lopaId).and("cms.site.owner = ?", airline).first();
						if(lopa==null){
							throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
									ECMSConstants.OBJECT_NOT_FOUND,
									new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.FORBIDDEN));
						}
						lopaList.add(lopa);
					}
				}

				if (!CollectionUtils.isEmpty(configurationDTO.getInterfaceLanguagesId())) {
					paxguilanguage = new HashSet<>();
					for (String languageId : configurationDTO.getInterfaceLanguagesId()) {
						PaxguiLanguage paxgui = Query.from(PaxguiLanguage.class).where("id = ?", languageId).and("cms.site.owner = ?", airline).first();
						if(paxgui==null){
							throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
									ECMSConstants.OBJECT_NOT_FOUND,
									new Object[] { ECMSConstants.PAXGUILANGUAGE }, Locale.US), HttpStatus.FORBIDDEN));
						}
						paxguilanguage.add(paxgui);
					}
				}

				if (!CollectionUtils.isEmpty(configurationDTO.getLrusId())) {
					lrus = new HashSet<>();
					for (String lruId : configurationDTO.getLrusId()) {
						Lru lru = Query.from(Lru.class).where("id = ?", lruId).and("cms.site.owner = ?", airline).first();
						if(lru==null){
							throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
									ECMSConstants.OBJECT_NOT_FOUND,
									new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.FORBIDDEN));
						}
						lrus.add(lru);
					}
				}

				if (!CollectionUtils.isEmpty(configurationDTO.getLruTypesId())) {
					lruTypes = new HashSet<>();
					for (String lruTypeId : configurationDTO.getLruTypesId()) {
						LruType lruType = Query.from(LruType.class).where("id = ?", lruTypeId).first();
						if(lruType==null){
							throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
									ECMSConstants.OBJECT_NOT_FOUND,
									new Object[] { ECMSConstants.LRU_TYPE }, Locale.US), HttpStatus.FORBIDDEN));
						}
						lruTypes.add(lruType);
					}
				}

				if (!CollectionUtils.isEmpty(configurationDTO.getServerGroupList())) {
//					serverGroupList = new ArrayList<>();
//					for (ServerGroupDTO serverGroupDTO : configurationDTO.getServerGroupList()) {												
//						ServerGroup serverGroup = new ServerGroup();
//						serverGroup.setName(serverGroupDTO.getName());
//						Set<Lru> lrus1=null;
//						if (!CollectionUtils.isEmpty(serverGroupDTO.getLrus())) {
//							lrus1 = new HashSet<>();
//							for (String lruId : serverGroupDTO.getLrus()) {
//								Lru lru = Query.from(Lru.class).where("id = ?", lruId).and("cms.site.owner = ?", airline).first();
//								if(lru==null){
//									throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
//											ECMSConstants.OBJECT_NOT_FOUND,
//											new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.FORBIDDEN));
//								}
//								lrus1.add(lru);
//							}
//						}
//						serverGroup.setLrus(lrus1);
//						serverGroupList.add(serverGroup);
//					}
				}

				if (!CollectionUtils.isEmpty(configurationDTO.getContentTypeIdList())) {
					contentTypeList = new HashSet<>();
					for (String contentTypeId : configurationDTO.getContentTypeIdList()) {
						ObjectType objectType = Query.from(ObjectType.class).where("id = ?", contentTypeId).first();
						if(objectType==null){
							throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
									ECMSConstants.OBJECT_NOT_FOUND,
									new Object[] { ECMSConstants.CONTENT_TYPE }, Locale.US), HttpStatus.FORBIDDEN));
						}
						contentTypeList.add(objectType);
					}
				}

				if (!CollectionUtils.isEmpty(configurationDTO.getRulesId())) {
					rules = new HashSet<>();
					for (String ruleId : configurationDTO.getRulesId()) { 
						Rule rule = Query.from(Rule.class).where("id = ?", ruleId).and("cms.site.owner = ?", airline).first();
						if(rule==null){
							throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
									ECMSConstants.OBJECT_NOT_FOUND,
									new Object[] { ECMSConstants.RULE }, Locale.US), HttpStatus.FORBIDDEN));
						}
						rules.add(rule);
					}
				}

				if (!CollectionUtils.isEmpty(configurationDTO.getTitleList())) {
					configurationTitles = new ArrayList<>();
					for (ConfigurationTitleDTO configurationTitleDTO : configurationDTO.getTitleList()) {
						ConfigurationTitle configurationTitle = new ConfigurationTitle();
						if(StringUtils.isNotEmpty(configurationTitleDTO.getContentId())){
							ContentType contentType = Query.from(ContentType.class).where("id = ?", configurationTitleDTO.getContentId()).and("cms.site.owner = ?", airline).first();
							if(contentType==null)
									throw new BadRequestException(
											new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER, 
													new Object[]{ECMSConstants.CONTENT_TYPE}, Locale.US),
													HttpStatus.FORBIDDEN));
							configurationTitle.setContent(contentType);
						}
						if(StringUtils.isNotBlank(configurationTitleDTO.getContentStatus())){
							if (configurationTitleDTO.getContentStatus()
									.equalsIgnoreCase(Configuration.ConfigurationTitle.Status.Incomplete.toString())) {
								configurationTitle.setContentStatus(Configuration.ConfigurationTitle.Status.Incomplete);
							} else if (configurationDTO.getConfigurationStatus()
									.equalsIgnoreCase(Configuration.ConfigurationTitle.Status.Validated.toString())) {
								configurationTitle.setContentStatus(Configuration.ConfigurationTitle.Status.Validated);
							} else if (configurationDTO.getConfigurationStatus()
									.equalsIgnoreCase(Configuration.ConfigurationTitle.Status.Completed.toString())) {
								configurationTitle.setContentStatus(Configuration.ConfigurationTitle.Status.Completed);
							} else {
								throw new BadRequestException(
										new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER, 
												new Object[]{ECMSConstants.CONFIGURATION_TITLE_STATUS}, Locale.US),
												HttpStatus.FORBIDDEN));
							}								
						}														
						
						if (!CollectionUtils.isEmpty(configurationTitleDTO.getDestinations())) {
							List<LruType> lruTypes1 = new ArrayList<>();
							for (String lruTypeId : configurationTitleDTO.getDestinations()) {
								LruType lruType = Query.from(LruType.class).where("id = ?", lruTypeId).first();
								if(lruType==null){
									throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(
											ECMSConstants.OBJECT_NOT_FOUND,
											new Object[] { ECMSConstants.LRU_TYPE }, Locale.US), HttpStatus.FORBIDDEN));
								}
								lruTypes1.add(lruType);
							}
							configurationTitle.setDestinations(lruTypes1);
						}
						if(configurationTitleDTO.getEndDate()!=null && configurationTitleDTO.getEndDate()>0)
							configurationTitle.setEndDate(new Date(configurationTitleDTO.getEndDate()));
						if(configurationTitleDTO.getStartDate()!=null && configurationTitleDTO.getStartDate()>0)
							configurationTitle.setStartDate(new Date(configurationTitleDTO.getStartDate()));
						if(configurationTitleDTO.getPriority()!=null){
							if(configurationTitleDTO.getPriority().equalsIgnoreCase(Priority.Default.toString()))
								configurationTitle.setPriority(Priority.Default);
							else if(configurationTitleDTO.getPriority().equalsIgnoreCase(Priority.High.toString()))
								configurationTitle.setPriority(Priority.High);
						}														
						configurationTitles.add(configurationTitle);
					}
				} 

				configuration.setLopaList(lopaList);
				configuration.setInterfaceLanguages(paxguilanguage);
				configuration.setLrus(lrus);
				configuration.setLruTypes(lruTypes);
//				configuration.setServerGroupList(serverGroupList);
				configuration.setContentTypeList(contentTypeList);
				configuration.setRules(rules);
				configuration.setTitleList(configurationTitles);
				Date date = new Date();
				if(configuration.getCreationDate()==null)
					configuration.setCreationDate(date);
				configuration.setLastModifiedDate(date);
				configuration.as(Site.ObjectModification.class).setOwner(airline);
				configuration.as(Site.ObjectModification.class).setConsumers(consumers);
				configuration.save();
				configurationDTO.setConfigurationId(configuration.getId().toString());
				configurationDTO.setCreationDate(configuration.getCreationDate().getTime());
				configurationDTO.setLastModifiedDate(configuration.getLastModifiedDate().getTime());
			}else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.CONFIGURATION }, Locale.US), HttpStatus.FORBIDDEN));
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}		
		return configurationDTO;
	}

	
	

	// populate ConfigurationDTO

	public ConfigurationRequestDTO populateConfigurationDTO(Configuration configuration) {
		ConfigurationRequestDTO configurationRequestDTO = new ConfigurationRequestDTO();
		if(configuration.getId()!=null)
			configurationRequestDTO.setConfigurationId(configuration.getId().toString());
		if(configuration.getName()!=null)
			configurationRequestDTO.setConfigurationName(configuration.getName());
		if(configuration.getVersion()!=null)
			configurationRequestDTO.setVersion(configuration.getVersion());
		if(configuration.getPlatform()!=null)
			configurationRequestDTO.setPlatform(configuration.getPlatform());
		if(configuration.getConfigurationStatus()!=null)
			configurationRequestDTO.setConfigurationStatus(configuration.getConfigurationStatus().toString());
		if(configuration.getPackagingType()!=null)
			configurationRequestDTO.setPackagingType(configuration.getPackagingType().toString());
		if(configuration.getLopaList()!=null)
			configurationRequestDTO.setLopaIdList(configuration.getLopaList().stream().map(lopa->lopa.getId().toString()).collect(Collectors.toList()));		
		
		if(configuration.getInterfaceLanguages()!=null)
			configurationRequestDTO.setInterfaceLanguagesId(configuration.getInterfaceLanguages().stream().map(paxguiLang->paxguiLang.getId().toString()).collect(Collectors.toSet()));		
		
		if(configuration.getLrus()!=null)
			configurationRequestDTO.setLrusId(configuration.getLrus().stream().map(lru->lru.getId().toString()).collect(Collectors.toSet()));		
		
		if(configuration.getLruTypes()!=null)
			configurationRequestDTO.setLruTypesId(configuration.getLruTypes().stream().map(lruType->lruType.getId().toString()).collect(Collectors.toSet()));								

		List<ServerGroupDTO> serverGroupList=new ArrayList<>();
//		if(configuration.getServerGroupList()!=null)
//			for(ServerGroup serverGroup: configuration.getServerGroupList()){
//				ServerGroupDTO serverGroupDTO = new ServerGroupDTO(); 
//				serverGroupDTO.setName(serverGroup.getName());
//				if(serverGroup.getLrus()!=null)
//					serverGroupDTO.setLrus(serverGroup.getLrus().stream().map(lru->lru.getId().toString()).collect(Collectors.toSet()));						
//				serverGroupList.add(serverGroupDTO);
//			}
		configurationRequestDTO.setServerGroupList(serverGroupList);
				
		if(configuration.getContentTypeList()!=null)
			configurationRequestDTO.setContentTypeIdList(configuration.getContentTypeList().stream().map(objectType->objectType.getId().toString()).collect(Collectors.toSet()));		

		if(configuration.getRules()!=null)
			configurationRequestDTO.setRulesId(configuration.getRules().stream().map(rule->rule.getId().toString()).collect(Collectors.toSet()));		

		List<ConfigurationTitleDTO> titleList=new ArrayList<>();
		if(configuration.getTitleList()!=null)
			for(ConfigurationTitle configurationTitle : configuration.getTitleList()){
				ConfigurationTitleDTO configurationTitleDTO = new ConfigurationTitleDTO();
				if(configurationTitle.getId()!=null)
					configurationTitleDTO.setContentId(configurationTitle.getId().toString());
				if(configurationTitle.getContentStatus()!=null)
					configurationTitleDTO.setContentStatus(configurationTitle.getContentStatus().toString());
				List<String>list = null;
				if(configurationTitle.getDestinations()!=null){
					list = configurationTitle.getDestinations().stream().map(lruType->lruType.getId().toString()).collect(Collectors.toList());
				}			
				configurationTitleDTO.setDestinations(list);
				if(configurationTitle.getEndDate()!=null)
					configurationTitleDTO.setEndDate(configurationTitle.getEndDate().getTime());
				if(configurationTitle.getStartDate()!=null)
					configurationTitleDTO.setStartDate(configurationTitle.getStartDate().getTime());
				if(configurationTitle.getPriority()!=null)
					configurationTitleDTO.setPriority(configurationTitle.getPriority().toString());
				titleList.add(configurationTitleDTO);
			}
		configurationRequestDTO.setTitleList(titleList);
		if(configuration.getCreationDate()!=null)
			configurationRequestDTO.setCreationDate(configuration.getCreationDate().getTime());
		if(configuration.getLastModifiedDate()!=null)
			configurationRequestDTO.setLastModifiedDate(configuration.getLastModifiedDate().getTime());
		return configurationRequestDTO;
	}

	public void saveConfiguration(Configuration configuration) {
		if (configuration != null) {
			configuration.save();
		}
	}

}
