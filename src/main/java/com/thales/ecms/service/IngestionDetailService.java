package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.IngestionDetail;
import com.thales.ecms.model.IngestionDetail.ImportStatus;
import com.thales.ecms.model.IngestionDetail.IngestionTitle;
import com.thales.ecms.model.IngestionDetail.IngestionTitle.ImportStatusTitle;
import com.thales.ecms.model.IngestionDetail.Mode;
import com.thales.ecms.model.dto.ImportStatusDTO;
import com.thales.ecms.model.dto.ImportStatusTitleDTO;
import com.thales.ecms.model.dto.IngestionDetailDTO;
import com.thales.ecms.model.dto.IngestionTitleDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir Service Class for Ingestion Details
 */
@Service
public class IngestionDetailService {

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(IngestionDetailService.class);

	/**
	 * Service Method to get a list of all Ingestion jobs between a start date
	 * and end date
	 * 
	 * @param airlineId
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public List<IngestionDetailDTO> getIngestionDetails(String airlineId, String modeValue) throws ObjectNotFoundException {

		List<IngestionDetailDTO> ingestionDetailDTOList = null;
		IngestionDetailDTO ingestionDetailDTO = null;

		Mode mode = Mode.valueOf(modeValue);
		List<IngestionDetail> ingestionDetailList = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {
			// fetch ingestion details
			ingestionDetailList = Query.from(IngestionDetail.class).where("cms.site.owner = ?", airline)
						.and("mode = ?", mode).sortDescending("ingestedDate").selectAll();

			if (!CollectionUtils.isEmpty(ingestionDetailList)) {
				ingestionDetailDTOList = new ArrayList<>();
				for (IngestionDetail ingestionDetail : ingestionDetailList) {

					ingestionDetailDTO = new IngestionDetailDTO();
					ingestionDetailDTO.setIngestionDetailInternalId(ingestionDetail.getId().toString());
					ingestionDetailDTO.setFileName(ingestionDetail.getFileName());
					ingestionDetailDTO.setIngestedDate(ingestionDetail.getIngestedDate());
					ingestionDetailDTO.setMode(ingestionDetail.getMode().toString());
					ingestionDetailDTO.setStatus(ingestionDetail.getStatus());
					ingestionDetailDTO.setConfiguration(ingestionDetail.getConfiguration());
					ingestionDetailDTO.setConfigurationId(ingestionDetail.getConfigurationId());
					ImportStatus importStatus = ingestionDetail.getImportStatus();

					if(importStatus != null){
						ImportStatusDTO importStatusDTO = new ImportStatusDTO();
						importStatusDTO.setException(importStatus.getException());
						importStatusDTO.setUserFriendlyMessage(importStatus.getUserFriendlyMessage());

						ingestionDetailDTO.setImportStatus(importStatusDTO);
					}

					if (null != ingestionDetail.getTitles() && ingestionDetail.getTitles().size() > 0) {
						List<IngestionTitleDTO> ingestionTitleDTOList = null;
						IngestionTitleDTO ingestionTitleDTO = null;
						
						ingestionDetailDTO.setTitleCount(ingestionDetail.getTitles().size());
						
						List<IngestionTitle> ingestionTitleList = ingestionDetail.getTitles();
						if (!CollectionUtils.isEmpty(ingestionTitleList)) {

							ingestionTitleDTOList = new ArrayList<>();
							for (IngestionTitle ingestionTitle : ingestionTitleList) {
								ingestionTitleDTO = new IngestionTitleDTO();
								ingestionTitleDTO.setTitleName(ingestionTitle.getTitleName());
								ingestionTitleDTO.setStatus(ingestionTitle.getStatus());
								ingestionTitleDTO.setContentType(ingestionTitle.getContentType());
								ingestionTitleDTO.setIngestedDate(ingestionTitle.getIngestedDate());
								//
								ImportStatusTitle importStatusTitle = ingestionTitle.getImportStatusTitle();

								if(importStatusTitle != null){
									ImportStatusTitleDTO importStatusTitleDTO = new ImportStatusTitleDTO();
									importStatusTitleDTO.setException(importStatusTitle.getException());
									importStatusTitleDTO.setUserFriendlyMessage(importStatusTitle.getUserFriendlyMessage());

									ingestionTitleDTO.setImportStatusTitleDTO(importStatusTitleDTO);
								}

								//
								
								ingestionTitleDTOList.add(ingestionTitleDTO);
							}
							ingestionDetailDTO.setIngestionTitleDTOList(ingestionTitleDTOList);
						}
						
					} else {
						ingestionDetailDTO.setTitleCount(0);
					}
					ingestionDetailDTO.setTitleCount(ingestionDetail.getTitleCount());
					// ingestionDetailDTO.setTitleDetails(ingestionDetail.getTitleDetails());
					ingestionDetailDTOList.add(ingestionDetailDTO);
				}
			} else {
				logger.error("No Ingestion Detail objects present");
				throw new ObjectNotFoundException(
						new ErrorResponse(
								messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
										new Object[] { ECMSConstants.INGESTION_DETAILS }, Locale.US),
								HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}

		return ingestionDetailDTOList;
	}

	/**
	 * Service Method to get details about an Ingestion Job
	 * 
	 * @param ingestionDetailId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public IngestionDetailDTO getIngestionDetailsById(String ingestionDetailId) throws ObjectNotFoundException {

		IngestionDetailDTO ingestionDetailDTO = null;
		List<IngestionTitleDTO> ingestionTitleDTOList = null;
		IngestionTitleDTO ingestionTitleDTO = null;

		IngestionDetail ingestionDetail = Query.from(IngestionDetail.class).where("id = ?", ingestionDetailId).first();
		if (ingestionDetail != null) {

			ingestionDetailDTO = new IngestionDetailDTO();
			ingestionDetailDTO.setIngestionDetailInternalId(ingestionDetail.getId().toString());
			ingestionDetailDTO.setFileName(ingestionDetail.getFileName());
			ingestionDetailDTO.setIngestedDate(ingestionDetail.getIngestedDate());
			ingestionDetailDTO.setMode(ingestionDetail.getMode().toString());
			ingestionDetailDTO.setStatus(ingestionDetail.getStatus());
			ingestionDetailDTO.setConfiguration(ingestionDetail.getConfiguration());
			ingestionDetailDTO.setConfigurationId(ingestionDetail.getConfigurationId());
			ingestionDetailDTO.setTitleCount(ingestionDetail.getTitleCount());
			
			ImportStatus importStatus = ingestionDetail.getImportStatus();

			ImportStatusDTO importStatusDTO = new ImportStatusDTO();
			if(null != importStatus){
				importStatusDTO.setException(importStatus.getException());
				importStatusDTO.setUserFriendlyMessage(importStatus.getUserFriendlyMessage());
			}
			ingestionDetailDTO.setImportStatus(importStatusDTO);

			List<IngestionTitle> ingestionTitleList = ingestionDetail.getTitles();
			if (!CollectionUtils.isEmpty(ingestionTitleList)) {

				ingestionTitleDTOList = new ArrayList<>();
				for (IngestionTitle ingestionTitle : ingestionTitleList) {
					ingestionTitleDTO = new IngestionTitleDTO();
					ingestionTitleDTO.setTitleName(ingestionTitle.getTitleName());
					ingestionTitleDTO.setStatus(ingestionTitle.getStatus());
					ingestionTitleDTO.setContentType(ingestionTitle.getContentType());
					ingestionTitleDTO.setIngestedDate(ingestionTitle.getIngestedDate());
					//
					ImportStatusTitle importStatusTitle = ingestionTitle.getImportStatusTitle();

					ImportStatusTitleDTO importStatusTitleDTO = new ImportStatusTitleDTO();
					importStatusTitleDTO.setException(importStatusTitle.getException());
					importStatusTitleDTO.setUserFriendlyMessage(importStatusTitle.getUserFriendlyMessage());

					ingestionTitleDTO.setImportStatusTitleDTO(importStatusTitleDTO);
					//
					ingestionTitleDTOList.add(ingestionTitleDTO);
				}
				ingestionDetailDTO.setIngestionTitleDTOList(ingestionTitleDTOList);
			}
		} else {
			logger.error("The Ingestion Detail does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.INGESTION_DETAIL }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return ingestionDetailDTO;
	}

	/**
	 * Service Method to update IngestionDetail Object
	 * 
	 * @param airlineId
	 * @return
	 * @throws ObjectExistsException
	 * @throws NoSuchMessageException
	 * @throws BadRequestException
	 * @throws ObjectNotFoundException
	 */
	public IngestionDetailDTO updateIngestionDetailsByAirlineId(String airlineId, String ingestionDetailId,
			IngestionDetailDTO ingestionDetailsDTO)
			throws NoSuchMessageException, ObjectExistsException, BadRequestException, ObjectNotFoundException {

		IngestionDetailDTO ingestionDetailDTO = null;
		IngestionDetail ingestionDetail = null;
		String fileName = ingestionDetailsDTO.getFileName();
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();

		if (null != airline) {
			if (fileName != null && StringUtils.isNotBlank(fileName)) {

				ingestionDetail = Query.from(IngestionDetail.class).where("fileName = ?", fileName)
						.and("id = ?", ingestionDetailId).first();

				if (ingestionDetail == null) {
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.INGESTION_DETAIL, ECMSConstants.NAME }, Locale.US),
							HttpStatus.FORBIDDEN));
				} else {
					ingestionDetail.setFileName(ingestionDetailsDTO.getFileName());
					ingestionDetail.setIngestedDate(ingestionDetailsDTO.getIngestedDate());
					ingestionDetail.setMode(Mode.valueOf(ingestionDetailsDTO.getMode()));
					ingestionDetail.setStatus(ingestionDetailsDTO.getStatus());
					ingestionDetail.setConfiguration(ingestionDetailsDTO.getConfiguration());
					ingestionDetail.setConfigurationId(ingestionDetailsDTO.getConfigurationId());

					List<IngestionTitleDTO> ingestionTitleDTOList = ingestionDetailsDTO.getIngestionTitleDTOList();
					if (!CollectionUtils.isEmpty(ingestionDetailsDTO.getIngestionTitleDTOList())) {
						List<IngestionTitle> ingestionTitleList = new ArrayList<>();

						for (IngestionTitleDTO ingestionTitleDTO : ingestionTitleDTOList) {
							IngestionTitle ingestionTitle = new IngestionTitle();
							ingestionTitle.setTitleName(ingestionTitleDTO.getTitleName());
							ingestionTitle.setStatus(ingestionTitleDTO.getStatus());
							ingestionTitle.setContentType(ingestionTitleDTO.getContentType());
							ingestionTitle.setIngestedDate(ingestionTitleDTO.getIngestedDate());
							ingestionTitleList.add(ingestionTitle);
						}
						ingestionDetail.setTitles(ingestionTitleList);
						ingestionDetail.setTitleCount(ingestionTitleList.size());
					}
					ingestionDetail.save();

					ingestionDetailDTO = new IngestionDetailDTO();
					ingestionDetailDTO.setIngestionDetailInternalId(ingestionDetail.getId().toString());
					ingestionDetailDTO.setFileName(ingestionDetail.getFileName());
					ingestionDetailDTO.setIngestedDate(ingestionDetail.getIngestedDate());
					ingestionDetailDTO.setMode(ingestionDetail.getMode().toString());
					ingestionDetailDTO.setStatus(ingestionDetail.getStatus());
					ingestionDetailDTO.setConfiguration(ingestionDetail.getConfiguration());
					ingestionDetailDTO.setConfigurationId(ingestionDetail.getConfigurationId());

					if (!CollectionUtils.isEmpty(ingestionDetail.getTitles())) {
						ingestionDetailDTO.setIngestionTitleDTOList(ingestionTitleDTOList);
						ingestionDetailDTO.setTitleCount(ingestionTitleDTOList.size());
					}
				}
			} else {
				throw new BadRequestException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}
		} else {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.INGESTION_DETAILS }, Locale.US), HttpStatus.NOT_FOUND));
		}

		return ingestionDetailDTO;
	}

	/**
	 * Service Method to save/update IngestionDetail Object
	 * 
	 * @param airlineId
	 * @return
	 * @throws ObjectExistsException
	 * @throws NoSuchMessageException
	 * @throws BadRequestException
	 * @throws ObjectNotFoundException
	 */
	public IngestionDetailDTO saveIngestionDetailsByAirlineId(String airlineId, IngestionDetailDTO ingestionDetailsDTO)
			throws NoSuchMessageException, ObjectExistsException, BadRequestException, ObjectNotFoundException {

		IngestionDetailDTO ingestionDetailDTO = null;
		IngestionDetail ingestionDetail = null;
		String fileName = ingestionDetailsDTO.getFileName();
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();

		if (null != airline) {
			if (fileName != null && StringUtils.isNotBlank(fileName)) {
				if (Query.from(IngestionDetail.class).where("fileName = ?", fileName).first() != null) {
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.INGESTION_DETAIL, ECMSConstants.NAME }, Locale.US),
							HttpStatus.FORBIDDEN));
				} else {
					ingestionDetail = new IngestionDetail();
					ingestionDetail.setFileName(ingestionDetailsDTO.getFileName());
					ingestionDetail.setIngestedDate(ingestionDetailsDTO.getIngestedDate());
					ingestionDetail.setMode(Mode.valueOf(ingestionDetailsDTO.getMode()));
					ingestionDetail.setStatus(ingestionDetailsDTO.getStatus());
					ingestionDetail.setConfiguration(ingestionDetailsDTO.getConfiguration());
					ingestionDetail.setConfigurationId(ingestionDetailsDTO.getConfigurationId());
					ingestionDetail.setTitleCount(ingestionDetailsDTO.getTitleCount());

					List<IngestionTitleDTO> ingestionTitleDTOList = ingestionDetailsDTO.getIngestionTitleDTOList();
					if (ingestionDetailsDTO.getMode().equals(ECMSConstants.IMPORT)) {
						if (!CollectionUtils.isEmpty(ingestionTitleDTOList)) {
							List<IngestionTitle> ingestionTitleList = new ArrayList<>();
							for (IngestionTitleDTO ingestionTitleDTO : ingestionTitleDTOList) {
								IngestionTitle ingestionTitle = new IngestionTitle();
								ingestionTitle.setTitleName(ingestionTitleDTO.getTitleName());
								ingestionTitle.setStatus(ingestionTitleDTO.getStatus());
								ingestionTitle.setContentType(ingestionTitleDTO.getContentType());
								ingestionTitle.setIngestedDate(ingestionTitleDTO.getIngestedDate());
								ingestionTitleList.add(ingestionTitle);
							}
							ingestionDetail.setTitles(ingestionTitleList);
							ingestionDetail.setTitleCount(ingestionTitleList.size());
						}

					} else {
						ingestionDetail.setTitles(null);
						ingestionDetail.setTitleCount(0);
					}
					ingestionDetail.as(Site.ObjectModification.class).setOwner(airline);
					ingestionDetail.save();

					ingestionDetailDTO = new IngestionDetailDTO();
					ingestionDetailDTO.setIngestionDetailInternalId(ingestionDetail.getId().toString());
					ingestionDetailDTO.setFileName(ingestionDetail.getFileName());
					ingestionDetailDTO.setIngestedDate(ingestionDetail.getIngestedDate());
					ingestionDetailDTO.setMode(ingestionDetail.getMode().toString());
					ingestionDetailDTO.setStatus(ingestionDetail.getStatus());
					ingestionDetailDTO.setConfiguration(ingestionDetail.getConfiguration());
					ingestionDetailDTO.setConfigurationId(ingestionDetail.getConfigurationId());
					if (!CollectionUtils.isEmpty(ingestionDetail.getTitles())) {
						ingestionDetailDTO.setIngestionTitleDTOList(ingestionTitleDTOList);
						ingestionDetailDTO.setTitleCount(ingestionTitleDTOList.size());
					} else {
						ingestionDetailDTO.setIngestionTitleDTOList(null);
						ingestionDetailDTO.setTitleCount(0);
					}
				}
			} else {
				throw new BadRequestException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}
		} else {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.INGESTION_DETAILS }, Locale.US), HttpStatus.NOT_FOUND));
		}

		return ingestionDetailDTO;
	}

	/**
	 * Service Method to get IngestionDetail
	 * 
	 * @param fileName
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public IngestionDetail getIngestionDetailsByFileName(String fileName) {

		IngestionDetail ingestionDetail = Query.from(IngestionDetail.class).where("fileName = ?", fileName).first();

		return ingestionDetail;
	}

	/**
	 * Service Method to get IngestionDetail object
	 * 
	 * @param fileName
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public IngestionDetail getIngestionDetailById(String id) {

		IngestionDetail ingestionDetail = Query.from(IngestionDetail.class).where("id = ?", id).first();

		return ingestionDetail;
	}

}
