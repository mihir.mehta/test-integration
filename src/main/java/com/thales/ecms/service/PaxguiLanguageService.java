package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.PaxguiLanguage;
import com.thales.ecms.model.dto.PaxguiLanguageDTO;
import com.thales.ecms.utils.ECMSConstants;

@Service
public class PaxguiLanguageService {

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(PaxguiLanguageService.class);

	private Locale locale = null;
	public static final Set<String> isoLanguageCodes = new HashSet<>(Arrays.asList(Locale.getISOLanguages()));
	public static final Set<String> isoLanguageNames = new HashSet<>();


	@PostConstruct
	public void initializeLanguages(){

		//initialize the set of language Names
		for(String isoLanguageCode : isoLanguageCodes){
			locale = new Locale(isoLanguageCode, "US");
			isoLanguageNames.add(locale.getDisplayLanguage());
			//logger.info(isoLanguageCode);
			//logger.info(locale.getDisplayLanguage());
		}
	}

	/**
	 * Service method for adding new language
	 * 
	 * @param airlineId
	 * @param paxguiDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	public PaxguiLanguageDTO addPaxguiLanguage(String airlineId, PaxguiLanguageDTO paxguiDetails)
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException {
		
		PaxguiLanguageDTO paxguiLanguageDTO = null;
		PaxguiLanguage paxguiLanguage = null;
		paxguiLanguage = new PaxguiLanguage();

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();

		if (airline != null) {

			//Language Name
			String languageName = paxguiDetails.getPaxguiLanguageName();
			if (StringUtils.isNotBlank(languageName)) {
				//Checking if the language name is valid
				if(isoLanguageNames.contains(languageName)){
					//Checking if a language with the same name already exists
					if(Query.from(PaxguiLanguage.class).where("paxguiLanguageName = ?",languageName).and("cms.site.owner = ?",airline).first() == null){
						paxguiLanguage.setPaxguiLanguageName(languageName);
					}
					else{
						logger.error("Paxgui Language already exists in this Airline");
						throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS, new Object[]{ECMSConstants.LANGUAGE_NAME, languageName}, Locale.US), HttpStatus.FORBIDDEN));
					}
				}
				else{
					logger.error("Invalid Language Name");
					throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER, new Object[]{ECMSConstants.LANGUAGE_NAME}, Locale.US), HttpStatus.BAD_REQUEST));
				}
			} else {
				logger.error("Bad Request - Empty Language Name");
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null,Locale.US), HttpStatus.BAD_REQUEST));
			}
			
			//Language Code
			String languageCode = paxguiDetails.getPaxguiLanguageCode();
			//Checking if the language code is valid
			if (StringUtils.isNotBlank(languageCode)) {
				//Checking if a language with the same code already exists
				if(isoLanguageCodes.contains(languageCode.toLowerCase())){
					if(Query.from(PaxguiLanguage.class).where("paxguiLanguageCode = ?",languageCode.toUpperCase()).and("cms.site.owner = ?",airline).first() == null){
						if(validateLanguageCodeMapping(languageName, languageCode.toLowerCase())){
							paxguiLanguage.setPaxguiLanguageCode(languageCode.toUpperCase());
						}
						else{
							throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_MAPPING, new Object[]{ECMSConstants.LANGUAGE_NAME, ECMSConstants.LANGUAGE_CODE}, Locale.US), HttpStatus.BAD_REQUEST));
						}
					}
					else{
						logger.error("Paxgui Language already exists in this Airline");
						throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS, new Object[]{ECMSConstants.LANGUAGE_CODE, languageCode}, Locale.US), HttpStatus.FORBIDDEN));
					}
				}
				else{
					logger.error("Invalid Language Code");
					throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER, new Object[]{ECMSConstants.LANGUAGE_CODE}, Locale.US), HttpStatus.BAD_REQUEST));
				}
				
			} else {
				logger.error("Bad Request - Empty Language Code");
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null,Locale.US), HttpStatus.BAD_REQUEST));
			}

			paxguiLanguage.as(Site.ObjectModification.class).setOwner(airline);

			paxguiLanguage.save();
			logger.debug("PAXGUILanguage instance saved successfully");

			paxguiLanguageDTO = new PaxguiLanguageDTO();
			paxguiLanguageDTO.setPaxguiLanguageName(paxguiLanguage.getPaxguiLanguageName());
			paxguiLanguageDTO.setPaxguiLanguageCode(paxguiLanguage.getPaxguiLanguageCode());
			paxguiLanguageDTO.setPaxguiLanguageId(paxguiLanguage.getId().toString());

		} else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[] {ECMSConstants.AIRLINE},Locale.US), HttpStatus.NOT_FOUND));
		}

		return paxguiLanguageDTO;
	}

	/**
	 * Service method for getting all languages
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException
	 */

	public List<PaxguiLanguageDTO> getPaxguiLanguages(String airlineId) throws ObjectNotFoundException {
		List<PaxguiLanguageDTO> paxguiLanguageDTOList = null;
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();

		if (airline != null) {
			List<PaxguiLanguage> paxguiLanguagesList = Query.from(PaxguiLanguage.class)
					.and("cms.site.owner = ?", airline).selectAll();

			if (!CollectionUtils.isEmpty(paxguiLanguagesList)) {
				paxguiLanguageDTOList = new ArrayList<>();
				for (PaxguiLanguage paxguiLangObj : paxguiLanguagesList) {
					PaxguiLanguageDTO pld = new PaxguiLanguageDTO();
					pld.setPaxguiLanguageName(paxguiLangObj.getPaxguiLanguageName());
					pld.setPaxguiLanguageCode(paxguiLangObj.getPaxguiLanguageCode());
					pld.setPaxguiLanguageId(paxguiLangObj.getId().toString());
					paxguiLanguageDTOList.add(pld);
				}
			}
		}
		else{
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[] {ECMSConstants.AIRLINE},Locale.US), HttpStatus.NOT_FOUND));
		}
		return paxguiLanguageDTOList;

	}

	/**
	 * Service method for deleting a language
	 * @param airlineId
	 * @param id
	 * @return
	 * @throws ObjectNotFoundException
	 */

	public Boolean deletePaxguiLanguage(String airlineId, String id) throws ObjectNotFoundException {
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();

		if (airline != null) {
			PaxguiLanguage paxguiLanguage = Query.from(PaxguiLanguage.class).where("id = ?", id)
					.and("cms.site.owner = ?", airline).first();
			if (paxguiLanguage != null) {
				paxguiLanguage.delete();
				return true;
			} else {
				logger.error("PaxguiLanguage does not exist");
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[] {ECMSConstants.PAXGUILANGUAGE},Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[] {ECMSConstants.AIRLINE},Locale.US), HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Service method for updating a language
	 * 
	 * @param id
	 * @param paxguiLanguageDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	public PaxguiLanguageDTO updatePaxguiLanguage(String airlineId, String id, PaxguiLanguageDTO paxguiLanguageDetails)
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException {

		PaxguiLanguageDTO languageDTO = null;
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();

		if (airline != null) {
			PaxguiLanguage paxguiLanguage = Query.from(PaxguiLanguage.class).where("id = ?", id)
					.and("cms.site.owner = ?", airline).first();

			if (paxguiLanguage != null) {
				// String newId = (String)
				// paxguiLanguageDetails.getPaxguiLanguageId();

				if (Query.from(PaxguiLanguage.class)
						.where("paxguiLanguageName = ?", paxguiLanguageDetails.getPaxguiLanguageName()).and("cms.site.owner = ?", airline)
						.first() != null) {
					// This Language already exists - throw an Exception
					logger.error("PaxguiLanguage Already Present");
					throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS, new Object[] {ECMSConstants.PAXGUILANGUAGE,ECMSConstants.NAME},Locale.US), HttpStatus.FORBIDDEN));
				}

				// if (newId != null && StringUtils.isNotBlank(newId)) {
				if(paxguiLanguageDetails.getPaxguiLanguageName() != null && StringUtils.isNotBlank(paxguiLanguageDetails.getPaxguiLanguageName())){
					paxguiLanguage.setPaxguiLanguageName(paxguiLanguageDetails.getPaxguiLanguageName());
				}
				if(paxguiLanguageDetails.getPaxguiLanguageCode() != null && StringUtils.isNotBlank(paxguiLanguageDetails.getPaxguiLanguageCode())){
					paxguiLanguage.setPaxguiLanguageCode(paxguiLanguageDetails.getPaxguiLanguageCode());
				}

				// }
				logger.debug("PAXGUILanguage instance saved successfully");
				paxguiLanguage.save();

				languageDTO = new PaxguiLanguageDTO();
				languageDTO.setPaxguiLanguageCode(paxguiLanguage.getPaxguiLanguageCode());
				languageDTO.setPaxguiLanguageName(paxguiLanguage.getPaxguiLanguageName());
				languageDTO.setPaxguiLanguageId(paxguiLanguage.getId().toString());
			} else {
				logger.error("PaxguiLanguage does not exist");
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[] {ECMSConstants.PAXGUILANGUAGE},Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[] {ECMSConstants.AIRLINE},Locale.US), HttpStatus.NOT_FOUND));
		}

		return languageDTO;
	}

	
	/**
	 * Helper method to validate a mapping between the language name and language code
	 * @param languageName
	 * @param languageCode
	 * @return
	 */
	public boolean validateLanguageCodeMapping(String languageName, String languageCode){
		
		locale = new Locale(languageCode, "US");
		if(locale.getDisplayLanguage().equalsIgnoreCase(languageName)){
			return true;
		}
		return false;
	}
}
