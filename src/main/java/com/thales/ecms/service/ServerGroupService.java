package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;
import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.psddev.dari.util.StringUtils;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Lopa;
import com.thales.ecms.model.Lopa.SeatingClassLRUTypeMapping;
import com.thales.ecms.model.LruType;
import com.thales.ecms.model.ServerGroup;
import com.thales.ecms.model.ServerGroup.LRUTypeForLOPA;
import com.thales.ecms.model.dto.ServerGroupDTO;
import com.thales.ecms.model.dto.ServerGroupDTO.LRUTypeDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * 
 * @author kumargupta.v
 *
 */
@Service
public class ServerGroupService {
	
	public Logger logger = LoggerFactory.getLogger(ServerGroupService.class);

	@Autowired
	MessageSource messageSource;	
	
	Lopa lopa;
	
	public ServerGroupDTO addServerGroup(String airlineId,ServerGroupDTO serverGroupDTO)
			throws BadRequestException, ObjectExistsException, NoSuchMessageException, ObjectNotFoundException
	{
		Site airline = Query.from(Site.class).where(" id = ?", airlineId).first();
		ServerGroup serverGroup=null;
		if (airline == null) {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));			
		}
		Set<Site> consumers = null;
		consumers = Sets.newHashSet();
		consumers.add(airline);
		
		serverGroup=new ServerGroup();
		
		if(StringUtils.isBlank(serverGroupDTO.getName()))
		{
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
					new Object[] { ECMSConstants.SERVERGROUP_NAME }, Locale.US), HttpStatus.NOT_FOUND));	
		}
		serverGroup.setName(serverGroupDTO.getName());
		
		if(StringUtils.isBlank(serverGroupDTO.getLopaId()))
		{
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
					new Object[] {ECMSConstants.LOPA_ID }, Locale.US), HttpStatus.NOT_FOUND));
		}
		lopa=Query.from(Lopa.class).where("id = ?",serverGroupDTO.getLopaId()).and("cms.site.owner = ?",airline).first();
		
		if(lopa==null)
		{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.NOT_FOUND));	
		}
		serverGroup.setLopa(lopa);
		
		
		Set<LRUTypeDTO> lruTypeDtos=serverGroupDTO.getLruTypeDTOs();
		
		if(lruTypeDtos==null||lruTypeDtos.isEmpty())
		{
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,new Object[] {},Locale.US),
					HttpStatus.BAD_REQUEST));
		}
		
		Set<LRUTypeForLOPA> lruTypes=new HashSet<>();
		LRUTypeForLOPA lruTypeForLopa=new LRUTypeForLOPA();
		for(LRUTypeDTO lruTypeDto:lruTypeDtos)
		{
			if(lruTypeDto==null)
			{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { }, Locale.US), HttpStatus.NOT_FOUND));
			}
			LruType lruType=Query.from(LruType.class).where("id = ?",lruTypeDto.getLruTypeId()).and("cms.site.owner = ?",airline).first();
			if(lruType==null)
			{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] {ECMSConstants.LRU_TYPE }, Locale.US), HttpStatus.NOT_FOUND));
			}
			lruTypeForLopa.setLruType(lruType);
			lruTypeForLopa.setMaxSpaceAllowance(lruTypeDto.getMaxSpaceAllowance());
			lruTypes.add(lruTypeForLopa);
		}
		
		checkLruTypesExistInLopa(lruTypes);
		
		serverGroup.setLruTypes(lruTypes);
		serverGroup.as(Site.ObjectModification.class).setOwner(airline);
		serverGroup.as(Site.ObjectModification.class).setConsumers(consumers);
		serverGroup.save();
		serverGroupDTO.setServerGroupId(serverGroup.getId().toString());
		
		return serverGroupDTO;
	}
	
	public ServerGroupDTO updateServerGroup(String airlineId,String serverGroupId,ServerGroupDTO serverGroupDTO)
			throws BadRequestException, ObjectExistsException, NoSuchMessageException, ObjectNotFoundException
	{
		Site airline = Query.from(Site.class).where(" id = ?", airlineId).first();
		ServerGroup serverGroup=null;
		if (airline == null) {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));			
		}
		Set<Site> consumers = null;
		consumers = Sets.newHashSet();
		consumers.add(airline);
		
		serverGroup=Query.from(ServerGroup.class).where("id = ?",serverGroupId).and("cms.site.owner = ?",airline).first();
		if(serverGroup==null)
		{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.SERVERGROUP }, Locale.US), HttpStatus.NOT_FOUND));
		}
		
		if(StringUtils.isBlank(serverGroupDTO.getName()))
		{
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
					new Object[] { ECMSConstants.SERVERGROUP_NAME }, Locale.US), HttpStatus.NOT_FOUND));	
		}
		serverGroup.setName(serverGroupDTO.getName());
		
		if(StringUtils.isBlank(serverGroupDTO.getLopaId()))
		{
			throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
					new Object[] {ECMSConstants.LOPA_ID }, Locale.US), HttpStatus.NOT_FOUND));
		}
		lopa=Query.from(Lopa.class).where("id = ?",serverGroupDTO.getLopaId()).and("cms.site.owner = ?",airline).first();
		
		if(lopa==null)
		{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.NOT_FOUND));	
		}
		serverGroup.setLopa(lopa);
		
		
		Set<LRUTypeDTO> lruTypeDtos=serverGroupDTO.getLruTypeDTOs();
		
		if(lruTypeDtos==null||lruTypeDtos.isEmpty())
		{
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,new Object[] {},Locale.US),
					HttpStatus.BAD_REQUEST));
		}
		
		Set<LRUTypeForLOPA> lruTypes=new HashSet<>();
		LRUTypeForLOPA lruTypeForLopa=new LRUTypeForLOPA();
		for(LRUTypeDTO lruTypeDto:lruTypeDtos)
		{
			if(lruTypeDto==null)
			{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { }, Locale.US), HttpStatus.NOT_FOUND));
			}
			LruType lruType=Query.from(LruType.class).where("id = ?",lruTypeDto.getLruTypeId()).and("cms.site.owner = ?",airline).first();
			if(lruType==null)
			{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] {ECMSConstants.LRU_TYPE }, Locale.US), HttpStatus.NOT_FOUND));
			}
			lruTypeForLopa.setLruType(lruType);
			lruTypeForLopa.setMaxSpaceAllowance(lruTypeDto.getMaxSpaceAllowance());
			lruTypes.add(lruTypeForLopa);
		}
		
		checkLruTypesExistInLopa(lruTypes);
		
		serverGroup.setLruTypes(lruTypes);
		serverGroup.as(Site.ObjectModification.class).setOwner(airline);
		serverGroup.as(Site.ObjectModification.class).setConsumers(consumers);
		serverGroup.save();
		serverGroupDTO.setServerGroupId(serverGroup.getId().toString());
		
		return serverGroupDTO;
	}
	
	public void checkLruTypesExistInLopa(Set<LRUTypeForLOPA> lruTypesForLopa) throws BadRequestException
	{		
		List<SeatingClassLRUTypeMapping> seatingClassLRUTypeMappings = lopa.getSeatingClassLruTypeMappingList();
		Set<LruType> lruTypes = new HashSet<>();
		for(SeatingClassLRUTypeMapping seatingClassLRUTypeMapping : seatingClassLRUTypeMappings){
			lruTypes.add(seatingClassLRUTypeMapping.getLruType());
		}
		//lruTypes now contains all the LRU Types present in the LOPA
		
		//Check if the LRU types in the Server Group are valid, i.e., these LRU types should be only those whcih are already there in the LOPA and not any other
		for(LRUTypeForLOPA lruType :lruTypesForLopa){
			if(!lruTypes.contains(lruType.getLruType())){
				System.out.println(lruType.getLruType().getName());
				throw new BadRequestException("Invalid LRU Type " + lruType.getLruType().getName()+ "! Please add an LRU Type which exists in the LOPA");
			
			}
		}
		
	}
	
	public Boolean deleteServerGroup(String airlineId,String serverGroupId) throws ObjectNotFoundException {
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline != null){
			ServerGroup serverGroup = Query.from(ServerGroup.class).where("id = ?",serverGroupId).and("cms.site.owner = ?",airline).first();
			if(Objects.nonNull(serverGroup)){
				serverGroup.delete();
				return Boolean.TRUE;
			}else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] {ECMSConstants.SERVERGROUP  }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}
	
	public List<ServerGroupDTO> getServerGroupList(String airlineId) 
			throws NoSuchMessageException, ObjectNotFoundException {
		List<ServerGroupDTO> list = null;
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline == null)
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
				
			list = new ArrayList<>();
			List<ServerGroup> serverGroups = Query.from(ServerGroup.class).where("cms.site.owner = ?",airline).selectAll();
			if(Objects.nonNull(serverGroups)){
				for(ServerGroup serverGroup : serverGroups){
					list.add(populateShipmentProfileDTO(serverGroup));
					
				}
			}		
		return list;
	}
	
	public ServerGroupDTO getServerGroupById(String airlineId,String serverGroupId) 
			throws NoSuchMessageException, ObjectNotFoundException {
		ServerGroupDTO serverGroupDTO = null;
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline == null)
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
			ServerGroup serverGroup = Query.from(ServerGroup.class).where("id = ?",serverGroupId).and("cms.site.owner = ?",airline).first();
			
			if(Objects.nonNull(serverGroup)){
				serverGroupDTO=populateShipmentProfileDTO(serverGroup);
			}else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.SHIPMENTPROFILE }, Locale.US), HttpStatus.NOT_FOUND));
			}
		
		return serverGroupDTO;
	}
	
	public ServerGroupDTO populateShipmentProfileDTO(ServerGroup serverGroup)
	{
		ServerGroupDTO serverGroupDTO=new ServerGroupDTO();
		serverGroupDTO.setServerGroupId(serverGroup.getId().toString());
		serverGroupDTO.setLopaId(serverGroup.getLopa().getId().toString());
		serverGroupDTO.setName(serverGroup.getName());
		Set<LRUTypeDTO> lruTypeDTOs=new HashSet<>();
		
		Set<LRUTypeForLOPA>lruTypeForLOPAs=serverGroup.getLruTypes();
		for(LRUTypeForLOPA lruTypeForLOPA:lruTypeForLOPAs)
		{
			LRUTypeDTO lruTypeDTO=new LRUTypeDTO();
			lruTypeDTO.setLruTypeId(lruTypeForLOPA.getLruType().getId().toString());
			lruTypeDTO.setMaxSpaceAllowance(lruTypeForLOPA.getMaxSpaceAllowance());
			lruTypeDTOs.add(lruTypeDTO);
		}
		serverGroupDTO.setLruTypeDTOs(lruTypeDTOs);		
		return serverGroupDTO;
	}
}
