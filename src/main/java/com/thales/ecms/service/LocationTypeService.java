package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
//import com.thales.ecms.model.GenericResponseDTO;
import com.thales.ecms.model.Location;
import com.thales.ecms.model.LocationType;
import com.thales.ecms.model.dto.LocationTypeDTO;

/**
 * Service Class to perform CRUD operations on Airline Location
 */
@Service
public class LocationTypeService {

	public static Logger logger = LoggerFactory.getLogger(LocationTypeService.class);

	public LocationTypeDTO addLocationType(LocationTypeDTO locationDetails)throws ObjectNotFoundException, BadRequestException, ObjectExistsException{

		LocationTypeDTO locationTypeDTO = null;
			LocationType locationType = null;
			if(locationDetails != null && StringUtils.isNotBlank(locationDetails.getLocationType())){
				if(Query.from(LocationType.class).where("name = ?",locationDetails.getLocationType()).first() != null){
					//This Language  already exists - throw an Exception
					throw new ObjectExistsException(new ErrorResponse("LocationType Already Present", HttpStatus.OK));
				}
				locationType = new LocationType();
				locationType.setName(locationDetails.getLocationType());
				
				locationType.save();
				locationTypeDTO = new LocationTypeDTO();
				locationTypeDTO.setLocationTypeInternalId(locationType.getId().toString());
				locationTypeDTO.setLocationType(locationType.getName());
				//return new GenericResponseDTO("Location has been created successfully..", HttpStatus.OK);
			}else {
				throw new BadRequestException(new ErrorResponse("Bad Request - Empty Location Type", HttpStatus.OK));
			}
			
			
		return locationTypeDTO;
	}

	/**
	 * Service Method to return a list of all Airline Contacts of an Airline
	 * 
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public List<LocationTypeDTO> getlocationTypeList() {
		List<LocationTypeDTO> locationTypeDTO = new ArrayList<>();;
		List<LocationType> locationTypeList = Query.from(LocationType.class).selectAll();

		if(locationTypeList != null && !locationTypeList.isEmpty()){
			for(LocationType obj : locationTypeList){
				LocationTypeDTO cod= new LocationTypeDTO();
				cod.setLocationTypeInternalId(obj.getId().toString());
				cod.setLocationType(obj.getName());
				locationTypeDTO.add(cod);
				
			}
		}
		return locationTypeDTO;
	}

	/**
	 * Delete location type if not referred by any location
	 * @param locationId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public String deleteLocationTypeById(String locationId) throws ObjectNotFoundException{
			String responseString;
			LocationType locationType = Query.from(LocationType.class).where("id = ?", locationId).first();
			if(locationType != null)
			{
				Location location = Query.from(Location.class).where("locationType = ?", locationType).first();
				if(location == null)
				{
					locationType.delete();
					responseString = "LocationType deleted successfully";
				}
				else {
					throw new ObjectNotFoundException(new ErrorResponse("Cannot delete LocationType - referred in Location", HttpStatus.OK));
				}
				locationType.delete();
				responseString = "LocationType deleted successfully";
			}
			else {
				throw new ObjectNotFoundException(new ErrorResponse("LocationType does not exist", HttpStatus.OK));
			}
			
			return responseString;
		
		
	}
	/**
	 * Update location type
	 * @param locationTypeId
	 * @param locationTypeDetails
	 * @return
	 */
	public LocationTypeDTO updateLocationTypeById(String locationTypeId, LocationTypeDTO locationTypeDetails)
			throws ObjectNotFoundException, ObjectExistsException {
		LocationType locationType = null;
		LocationTypeDTO locationTypeDTO = null;

		if (locationTypeId != null && locationTypeDetails != null &&  StringUtils.isNotBlank(locationTypeDetails.getLocationType())) {
			if (Query.from(LocationType.class).where("name = ?", locationTypeDetails.getLocationType()).first() != null) {
				throw new ObjectExistsException(new ErrorResponse("LocationType Already Present", HttpStatus.OK));
			}

			locationType = Query.from(LocationType.class).where("id = ?", locationTypeId).first();

			if (null != locationType) {
					locationType.setName(locationTypeDetails.getLocationType());
					
				locationType.save();

				locationTypeDTO = new LocationTypeDTO();
				locationTypeDTO.setLocationTypeInternalId(locationType.getId().toString());
				locationTypeDTO.setLocationType(locationType.getName());
			} else {
				throw new ObjectNotFoundException(new ErrorResponse("LocationType does not exist", HttpStatus.OK));
			}
		} else {
			throw new ObjectNotFoundException(
					new ErrorResponse("LocationType cannot be blank", HttpStatus.OK));
		}
		return locationTypeDTO;

	}
}
