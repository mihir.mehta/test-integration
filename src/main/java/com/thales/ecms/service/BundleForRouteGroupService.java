package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;
import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Bundle;
import com.thales.ecms.model.BundleForRouteGroup;
import com.thales.ecms.model.RouteGroup;
import com.thales.ecms.model.SeatingClass;
import com.thales.ecms.model.dto.BundleForRouteGroupDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * Service class to perform CRUD on BundleForRouteGroup
 * 
 * @author arjun.p
 *
 */
@Service
public class BundleForRouteGroupService {

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(BundleForRouteGroupService.class);
	
	public BundleForRouteGroupDTO addBundleForRouteGroup(String airlineId, BundleForRouteGroupDTO bundleForRouteGroupDTO)
			throws BadRequestException, ObjectExistsException, NoSuchMessageException, ObjectNotFoundException {

		BundleForRouteGroup bundleForRouteGroup = null;
		Site airline = Query.from(Site.class).where(" id = ?", airlineId).first();
		if (airline != null) {
			Set<Site> consumers = null;
			consumers = Sets.newHashSet();
			consumers.add(airline);
			bundleForRouteGroup = new BundleForRouteGroup();
			Bundle bundle = Query.from(Bundle.class).where(" id = ?", bundleForRouteGroupDTO.getBundleId())
					.and("cms.site.owner = ?", airline).first();
			if (Objects.nonNull(bundle)) {
				bundleForRouteGroup.setBundle(bundle);
			} else {
				throw new ObjectNotFoundException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[] {ECMSConstants.BUNDLE}, Locale.US),
						HttpStatus.NOT_FOUND));
			}
			
			/*RouteGroup routeGroup = Query.from(RouteGroup.class)
					.where(" id = ?", bundleForRouteGroupDTO.getRouteGroupId()).and("cms.site.owner = ?", airline)
					.first();
			if (Objects.nonNull(routeGroup)) {
				bundleForRouteGroup.setRouteGroup(routeGroup);
			} else {
				throw new ObjectNotFoundException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[] {ECMSConstants.ROUTE_GROUP}, Locale.US),
						HttpStatus.NOT_FOUND));
			}*/

			SeatingClass seatingClass = Query.from(SeatingClass.class)
					.where(" id = ?", bundleForRouteGroupDTO.getSeatingClassId()).and("cms.site.owner = ?", airline)
					.first();
			if (Objects.nonNull(seatingClass)) {
				bundleForRouteGroup.setSeatingClass(seatingClass);
			} else {
				throw new ObjectNotFoundException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[] {ECMSConstants.SEATING_CLASS}, Locale.US),
						HttpStatus.NOT_FOUND));
			}

			if (bundleForRouteGroupDTO.getPpaFee() >= 0.0) {
				bundleForRouteGroup.setPpaFee(bundleForRouteGroupDTO.getPpaFee());
			}
			if (bundleForRouteGroupDTO.getPpvFee() >= 0.0) {
				bundleForRouteGroup.setPpvFee(bundleForRouteGroupDTO.getPpvFee());
			}
			bundleForRouteGroup.as(Site.ObjectModification.class).setOwner(airline);
			bundleForRouteGroup.as(Site.ObjectModification.class).setConsumers(consumers);
			bundleForRouteGroup.save();
			bundleForRouteGroupDTO.setBundleForRouteGroupId(bundleForRouteGroup.getId().toString());
		} else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}

		return bundleForRouteGroupDTO;
	}

	
	public List<BundleForRouteGroupDTO> getAll(String airlineId) throws NoSuchMessageException, ObjectNotFoundException {
		List<BundleForRouteGroupDTO> list = new ArrayList<>();
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline != null){
			List<BundleForRouteGroup> bundleForRouteGroups = Query.from(BundleForRouteGroup.class).where("cms.site.owner = ?",airline).selectAll();
			if(Objects.nonNull(bundleForRouteGroups)){
				for(BundleForRouteGroup bundleForRouteGroup : bundleForRouteGroups){
					list.add(populateBundleForRouteGroupDTO(bundleForRouteGroup));
				}
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return list;
	}
	
	public BundleForRouteGroupDTO getBundleForRouteGroupById(String airlineId,String bundleForRouteGroupId) throws NoSuchMessageException, ObjectNotFoundException {
		BundleForRouteGroupDTO bundleForRouteGroupDTO = null;
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline != null){
			BundleForRouteGroup bundleForRouteGroup = Query.from(BundleForRouteGroup.class).where("id = ?",bundleForRouteGroupId).and("cms.site.owner = ?",airline).first();
			if(Objects.nonNull(bundleForRouteGroup)){
				bundleForRouteGroupDTO=populateBundleForRouteGroupDTO(bundleForRouteGroup);
			}else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.BUNDLE_FOR_ROUTE_GROUP }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return bundleForRouteGroupDTO;
	}
	
	
	public BundleForRouteGroupDTO updateBundleForRouteGroup(String airlineId, String bundleForRouteGroupId,
			BundleForRouteGroupDTO bundleForRouteGroupDTO)
			throws BadRequestException, ObjectExistsException, NoSuchMessageException, ObjectNotFoundException {

		BundleForRouteGroup bundleForRouteGroup = null;
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		Set<Site> consumers = null;
		if (airline == null)
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));

		consumers = Sets.newHashSet();
		consumers.add(airline);
		bundleForRouteGroup = Query.from(BundleForRouteGroup.class).where("id = ?", bundleForRouteGroupId)
				.and("cms.site.owner = ?", airline).first();

		if(bundleForRouteGroup==null)
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.BUNDLE_FOR_ROUTE_GROUP }, Locale.US), HttpStatus.NOT_FOUND));
		
		Bundle bundle = Query.from(Bundle.class).where(" id = ?", bundleForRouteGroupDTO.getBundleId())
				.and("cms.site.owner = ?", airline).first();
		if (bundle == null) {
			throw new ObjectNotFoundException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[] {ECMSConstants.BUNDLE}, Locale.US),
					HttpStatus.NOT_FOUND));
		}

		bundleForRouteGroup.setBundle(bundle);

		/*RouteGroup routeGroup = Query.from(RouteGroup.class).where("id = ?", bundleForRouteGroupDTO.getRouteGroupId())
				.and("cms.site.owner = ?", airline).first();
		if (routeGroup == null) {
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.ROUTE_GROUP }, Locale.US), HttpStatus.NOT_FOUND));
		}

		bundleForRouteGroup.setRouteGroup(routeGroup);*/

		SeatingClass seatingClass = Query.from(SeatingClass.class)
				.where(" id = ?", bundleForRouteGroupDTO.getSeatingClassId()).and("cms.site.owner = ?", airline).first();
		if (seatingClass == null) {
			throw new ObjectNotFoundException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[] {ECMSConstants.SEATING_CLASS}, Locale.US),
					HttpStatus.NOT_FOUND));
		}

		bundleForRouteGroup.setSeatingClass(seatingClass);

		if (bundleForRouteGroupDTO.getPpaFee() >= 0.0) {
			bundleForRouteGroup.setPpaFee(bundleForRouteGroupDTO.getPpaFee());
		}
		if (bundleForRouteGroupDTO.getPpvFee() >= 0.0) {
			bundleForRouteGroup.setPpvFee(bundleForRouteGroupDTO.getPpvFee());
		}
		bundleForRouteGroup.as(Site.ObjectModification.class).setOwner(airline);
		bundleForRouteGroup.as(Site.ObjectModification.class).setConsumers(consumers);
		bundleForRouteGroup.save();
		bundleForRouteGroupDTO.setBundleForRouteGroupId(bundleForRouteGroup.getId().toString());

		return bundleForRouteGroupDTO;
	}

	
	public Boolean deleteBundleForRouteGroup(String airlineId,String bundleForRouteGroupId) throws ObjectNotFoundException {
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline != null){
			BundleForRouteGroup bundleForRouteGroup = Query.from(BundleForRouteGroup.class).where("id = ?",bundleForRouteGroupId).and("cms.site.owner = ?",airline).first();
			if(Objects.nonNull(bundleForRouteGroup)){
				bundleForRouteGroup.delete();
				return Boolean.TRUE;
			}else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.BUNDLE_FOR_ROUTE_GROUP }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}
	
	private BundleForRouteGroupDTO populateBundleForRouteGroupDTO(BundleForRouteGroup bundleForRouteGroup){
		BundleForRouteGroupDTO bundleForRouteGroupDTO = new BundleForRouteGroupDTO();
		if(bundleForRouteGroup.getId()!=null)
			bundleForRouteGroupDTO.setBundleForRouteGroupId(bundleForRouteGroup.getId().toString());
		if(bundleForRouteGroup.getBundle()!=null)
			bundleForRouteGroupDTO.setBundleId(bundleForRouteGroup.getBundle().getId().toString());
		if(bundleForRouteGroup.getRouteGroup()!=null)
			bundleForRouteGroupDTO.setRouteGroupId(bundleForRouteGroup.getRouteGroup().getId().toString());
		if(bundleForRouteGroup.getSeatingClass()!=null)
			bundleForRouteGroupDTO.setSeatingClassId(bundleForRouteGroup.getSeatingClass().getId().toString());		
		bundleForRouteGroupDTO.setPpaFee(bundleForRouteGroup.getPpaFee());
		bundleForRouteGroupDTO.setPpvFee(bundleForRouteGroup.getPpvFee());
		return bundleForRouteGroupDTO;
	}
	
}
