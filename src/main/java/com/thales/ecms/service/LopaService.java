package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Aircraft;
import com.thales.ecms.model.Lopa;
import com.thales.ecms.model.Lopa.SeatingClassLRUTypeMapping;
import com.thales.ecms.model.LruType;
import com.thales.ecms.model.SeatingClass;
import com.thales.ecms.model.dto.LopaDTO;
import com.thales.ecms.model.dto.SeatingClassLruTypeMappingDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir Service Class to perform CRUD operations on LOPA
 */
@Service
public class LopaService {

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(LopaService.class);

	/**
	 * Service Method to add a LOPA
	 * 
	 * @param airlineId
	 * @param lopaDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	public LopaDTO addLopa(String airlineId, LopaDTO lopaDetails)
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException {

		Lopa lopa = null;
		LopaDTO lopaDTO = null;
		List<SeatingClassLRUTypeMapping> seatingClassLRUTypeMappingList = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();

		if (airline != null) {

			// name of the LOPA
			String lopaName = lopaDetails.getLopaName();
			if (lopaName != null && StringUtils.isNotBlank(lopaName)) {
				if (Query.from(Lopa.class).where("name = ?", lopaName).and("cms.site.owner = ?", airline)
						.first() == null) {
					lopa = new Lopa();
					lopa.setName(lopaName);
				} else {
					logger.error("Lopa name" + lopaName + "already exists.");
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.LOPA, ECMSConstants.NAME }, Locale.US),
							HttpStatus.FORBIDDEN));
				}
			} else {
				logger.error("Bad Request - Empty Parameter: LOPA Name.");
				throw new BadRequestException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}

			// The Aircraft for the LOPA
			String aircraftTypeId = lopaDetails.getAircraftTypeId();
			if (aircraftTypeId != null && StringUtils.isNotBlank(aircraftTypeId)) {
				Aircraft aircraft = Query.from(Aircraft.class).where("id = ?", aircraftTypeId).first();
				if (aircraft != null) {
					lopa.setAircraft(aircraft);
				} else {
					logger.error("Aircraft " + aircraftTypeId + " does not exist.");
					throw new ObjectNotFoundException(
							new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
									new Object[] { ECMSConstants.AIRCRAFT }, Locale.US), HttpStatus.NOT_FOUND));
				}
			} else {
				logger.error("Bad Request - Empty Parameter: Aircraft Id.");
				throw new BadRequestException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}

			// Seating Class and LRU Type Mapping
			List<SeatingClassLruTypeMappingDTO> seatingClassLruTypeMappingDTOList = lopaDetails
					.getSeatingClassLruTypeMappingDTOList();
			if (seatingClassLruTypeMappingDTOList != null && !seatingClassLruTypeMappingDTOList.isEmpty()) {

				seatingClassLRUTypeMappingList = new ArrayList<>();

				for (SeatingClassLruTypeMappingDTO seatingClassLruTypeMappingDTO : seatingClassLruTypeMappingDTOList) {

					SeatingClassLRUTypeMapping seatingClassLRUTypeMapping = new SeatingClassLRUTypeMapping();

					// Seating Class
					String seatingClassName = seatingClassLruTypeMappingDTO.getSeatingClassName();
					if (seatingClassName != null && StringUtils.isNotBlank(seatingClassName)) {
						SeatingClass seatingClass = Query.from(SeatingClass.class)
								.where("name = ?", seatingClassName).and("cms.site.owner = ?", airline).first();
						if (seatingClass != null) {
							if (!seatingClassLRUTypeMappingList.isEmpty()) {
								for (SeatingClassLRUTypeMapping seatingClassLRUTypeMapping2 : seatingClassLRUTypeMappingList) {
									if (seatingClassLRUTypeMapping2.getSeatingClass().equals(seatingClass)) {
										logger.error("Seating class in LOPA already exists");
										throw new ObjectExistsException(new ErrorResponse(
												messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
														new Object[] { ECMSConstants.LOPA,
																ECMSConstants.SEATING_CLASS },
														Locale.US),
												HttpStatus.FORBIDDEN));
									}
								}
								seatingClassLRUTypeMapping.setSeatingClass(seatingClass);
							} else {
								seatingClassLRUTypeMapping.setSeatingClass(seatingClass);
							}
							// seatingClassLRUTypeMapping.setSeatingClass(seatingClass);
						} else {
							logger.error("Seating Class " + seatingClassName + " does not exist");
							throw new ObjectNotFoundException(
									new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
											new Object[] { ECMSConstants.SEATING_CLASS },
											Locale.US), HttpStatus.NOT_FOUND));
						}
					} else {
						logger.error("Bad Request - Empty Parameter: Seating Class Id");
						throw new BadRequestException(
								new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US),
										HttpStatus.BAD_REQUEST));
					}

					// LRU Type
					String lruTypeName = seatingClassLruTypeMappingDTO.getLruTypeName();
					if (lruTypeName != null && StringUtils.isNotBlank(lruTypeName)) {
						LruType lruType = Query.from(LruType.class).where("name = ?", lruTypeName).first();
						if (lruType != null) {
							seatingClassLRUTypeMapping.setLruType(lruType);
						} else {
							logger.error("LRU Type " + lruTypeName + " does not exist");
							throw new ObjectNotFoundException(
									new ErrorResponse(
											messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
													new Object[] { ECMSConstants.LRU_TYPE }, Locale.US),
											HttpStatus.NOT_FOUND));
						}
					}

					seatingClassLRUTypeMappingList.add(seatingClassLRUTypeMapping);
				}
				lopa.setSeatingClassLruTypeMappingList(seatingClassLRUTypeMappingList);
			}

			lopa.as(Site.ObjectModification.class).setOwner(airline);
			logger.debug("LOPA saved successfully");
			lopa.save();

			lopaDTO = new LopaDTO();
			lopaDTO.setLopaInternalId(lopa.getId().toString());
			lopaDTO.setLopaName(lopa.getName());
			lopaDTO.setAircraftTypeId(lopa.getAircraft().getId().toString());
			lopaDTO.setAircraftType(lopa.getAircraft().getName());

			seatingClassLRUTypeMappingList = lopa.getSeatingClassLruTypeMappingList();
			if (seatingClassLRUTypeMappingList != null && !seatingClassLRUTypeMappingList.isEmpty()) {

				seatingClassLruTypeMappingDTOList = new ArrayList<>();

				for (SeatingClassLRUTypeMapping seatingClassLRUTypeMapping : seatingClassLRUTypeMappingList) {

					SeatingClassLruTypeMappingDTO seatingClassLruTypeMappingDTO = new SeatingClassLruTypeMappingDTO();
					
					seatingClassLruTypeMappingDTO
					.setSeatingClassName(seatingClassLRUTypeMapping.getSeatingClass().getName());

					LruType lruType = seatingClassLRUTypeMapping.getLruType();
					if (lruType != null) {
						seatingClassLruTypeMappingDTO.setLruTypeName(lruType.getName());
					}

					seatingClassLruTypeMappingDTOList.add(seatingClassLruTypeMappingDTO);
				}
				lopaDTO.setSeatingClassLruTypeMappingDTOList(seatingClassLruTypeMappingDTOList);
			}
		} else {
			logger.error("Airline " + airlineId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return lopaDTO;
	}

	/**
	 * Service Method to return a List of LOPAs
	 * 
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException,EmptyResponseException
	 */
	public List<LopaDTO> getLopas(String airlineId) throws ObjectNotFoundException, EmptyResponseException {

		List<LopaDTO> lopaDTOList = null;
		LopaDTO lopaDTO = null;
		List<SeatingClassLRUTypeMapping> seatingClassLRUTypeMappingList = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();

		if (airline != null) {
			List<Lopa> lopaList = Query.from(Lopa.class).where("cms.site.owner = ?", airline).selectAll();
			
			if (lopaList != null && !lopaList.isEmpty()) {

				lopaDTOList = new ArrayList<>();
				for (Lopa lopa : lopaList) {

					lopaDTO = new LopaDTO();
					lopaDTO.setLopaInternalId(lopa.getId().toString());
					lopaDTO.setLopaName(lopa.getName());

					lopaDTO.setAircraftTypeId(lopa.getAircraft().getId().toString());
					lopaDTO.setAircraftType(lopa.getAircraft().getName());

					seatingClassLRUTypeMappingList = lopa.getSeatingClassLruTypeMappingList();
					if (seatingClassLRUTypeMappingList != null && !seatingClassLRUTypeMappingList.isEmpty()) {

						List<SeatingClassLruTypeMappingDTO> seatingClassLruTypeMappingDTOList = new ArrayList<>();

						for (SeatingClassLRUTypeMapping seatingClassLRUTypeMapping : seatingClassLRUTypeMappingList) {

							SeatingClassLruTypeMappingDTO seatingClassLruTypeMappingDTO = new SeatingClassLruTypeMappingDTO();
							
							seatingClassLruTypeMappingDTO
							.setSeatingClassName(seatingClassLRUTypeMapping.getSeatingClass().getName());

							LruType lruType = seatingClassLRUTypeMapping.getLruType();
							if (lruType != null) {
							
								seatingClassLruTypeMappingDTO.setLruTypeName(lruType.getName());
							}

							seatingClassLruTypeMappingDTOList.add(seatingClassLruTypeMappingDTO);
						}
						lopaDTO.setSeatingClassLruTypeMappingDTOList(seatingClassLruTypeMappingDTOList);
					}
					lopaDTOList.add(lopaDTO);
				}
			} else {
				logger.error("No LOPAs exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline " + airlineId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		
		
		return lopaDTOList;
	}

	/**
	 * Service method to return a LOPA
	 * 
	 * @param airlineId
	 * @param lopaId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public LopaDTO getLopaById(String airlineId, String lopaId) throws ObjectNotFoundException {

		LopaDTO lopaDTO = null;
		List<SeatingClassLRUTypeMapping> seatingClassLRUTypeMappingList = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {

			Lopa lopa = Query.from(Lopa.class).where("id = ?", lopaId).and("cms.site.owner = ?", airline).first();
			if (lopa != null) {

				lopaDTO = new LopaDTO();
				lopaDTO.setLopaInternalId(lopa.getId().toString());
				lopaDTO.setLopaName(lopa.getName());
				lopaDTO.setAircraftTypeId(lopa.getAircraft().getId().toString());
				lopaDTO.setAircraftType(lopa.getAircraft().getName());

				seatingClassLRUTypeMappingList = lopa.getSeatingClassLruTypeMappingList();
				if (seatingClassLRUTypeMappingList != null && !seatingClassLRUTypeMappingList.isEmpty()) {

					List<SeatingClassLruTypeMappingDTO> seatingClassLruTypeMappingDTOList = new ArrayList<>();

					for (SeatingClassLRUTypeMapping seatingClassLRUTypeMapping : seatingClassLRUTypeMappingList) {

						SeatingClassLruTypeMappingDTO seatingClassLruTypeMappingDTO = new SeatingClassLruTypeMappingDTO();
						
						seatingClassLruTypeMappingDTO
						.setSeatingClassName(seatingClassLRUTypeMapping.getSeatingClass().getName());

						LruType lruType = seatingClassLRUTypeMapping.getLruType();
						if (lruType != null) {
						
							seatingClassLruTypeMappingDTO.setLruTypeName(lruType.getName());
						}

						seatingClassLruTypeMappingDTOList.add(seatingClassLruTypeMappingDTO);
					}
					lopaDTO.setSeatingClassLruTypeMappingDTOList(seatingClassLruTypeMappingDTOList);
				}
			} else {
				logger.error("LOPA " + lopaId + " does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline " + airlineId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return lopaDTO;
	}

	/**
	 * Service Method to update a LOPA - can only update the name of the LOPA
	 * 
	 * @param airlineId
	 * @param lopaId
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws ObjectExistsException
	 */
	public LopaDTO updateLopa(String lopaId, LopaDTO lopaDetails)
			throws ObjectNotFoundException, ObjectExistsException {

		LopaDTO lopaDTO = null;
		List<SeatingClassLRUTypeMapping> seatingClassLRUTypeMappingList = null;

		Lopa lopa = Query.from(Lopa.class).where("id = ?", lopaId).first();
		if (lopa != null) {

			String newLopaName = lopaDetails.getLopaName();
			if (newLopaName != null && StringUtils.isNotBlank(newLopaName)) {
				if (Query.from(Lopa.class).where("name = ?", newLopaName).first() == null) {
					lopa.setName(newLopaName);
				} else {
					logger.error("Cannot update LOPA - A LOPA with the same name already exists");
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.LOPA, ECMSConstants.NAME }, Locale.US),
							HttpStatus.FORBIDDEN));
				}
			}

			lopaDTO = new LopaDTO();
			lopaDTO.setLopaInternalId(lopa.getId().toString());
			lopaDTO.setLopaName(lopa.getName());
			lopaDTO.setAircraftTypeId(lopa.getAircraft().getId().toString());
			lopaDTO.setAircraftType(lopa.getAircraft().getName());

			seatingClassLRUTypeMappingList = lopa.getSeatingClassLruTypeMappingList();
			if (seatingClassLRUTypeMappingList != null && !seatingClassLRUTypeMappingList.isEmpty()) {

				List<SeatingClassLruTypeMappingDTO> seatingClassLruTypeMappingDTOList = new ArrayList<>();

				for (SeatingClassLRUTypeMapping seatingClassLRUTypeMapping : seatingClassLRUTypeMappingList) {

					SeatingClassLruTypeMappingDTO seatingClassLruTypeMappingDTO = new SeatingClassLruTypeMappingDTO();
					
					seatingClassLruTypeMappingDTO
					.setSeatingClassName(seatingClassLRUTypeMapping.getSeatingClass().getName());

					LruType lruType = seatingClassLRUTypeMapping.getLruType();
					if (lruType != null) {
					
						seatingClassLruTypeMappingDTO.setLruTypeName(lruType.getName());
					}

					seatingClassLruTypeMappingDTOList.add(seatingClassLruTypeMappingDTO);
				}
				lopaDTO.setSeatingClassLruTypeMappingDTOList(seatingClassLruTypeMappingDTOList);
			}
		} else {
			logger.error("LOPA " + lopaId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return lopaDTO;
	}

	/**
	 * Service method to delete a LOPA
	 * 
	 * @param airlineId
	 * @param lopaId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public Boolean deleteLopa(String airlineId, String lopaId) throws ObjectNotFoundException {

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {
			Lopa lopa = Query.from(Lopa.class).where("id = ?", lopaId).and("cms.site.owner = ?", airline).first();
			if (lopa != null) {
				lopa.delete();
				return true;
			} else {
				logger.error("LOPA " + lopaId + " does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline " + airlineId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Service Method to add Seating Class LRU Type Mapping to a LOPA
	 * To add a Mapping, you need to specify both the Seating Class and LRU Type
	 * @param airlineId
	 * @param lopaId
	 * @param seatingClassLruTypeMappingDTO
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	public LopaDTO addSeatingClassLruType(String airlineId, String lopaId,
			SeatingClassLruTypeMappingDTO seatingClassLruTypeMappingDTO)
					throws ObjectNotFoundException, BadRequestException, ObjectExistsException {

		LopaDTO lopaDTO = null;
		List<SeatingClassLRUTypeMapping> seatingClassLRUTypeMappingList = null;
		SeatingClassLRUTypeMapping seatingClassLRUTypeMapping = null;
		LruType lruType = null;
		
		
		int flag = 0;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {
			Lopa lopa = Query.from(Lopa.class).where("id = ?", lopaId).and("cms.site.owner = ?", airline).first();
			if (lopa != null) {

				seatingClassLRUTypeMappingList = lopa.getSeatingClassLruTypeMappingList();
				if (seatingClassLRUTypeMappingList == null || seatingClassLRUTypeMappingList.isEmpty()) {
					seatingClassLRUTypeMappingList = new ArrayList<>();
				}

				//LRU Type
				String lruTypeName = seatingClassLruTypeMappingDTO.getLruTypeName();
				if (StringUtils.isNotBlank(lruTypeName)) {
					lruType = Query.from(LruType.class).where("name = ?", lruTypeName).first();
					if (lruType == null) {
						logger.error("LRU Type " + lruTypeName + " does not exist");
						throw new ObjectNotFoundException(
								new ErrorResponse(
										messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
												new Object[] { ECMSConstants.LRU_TYPE }, Locale.US),
										HttpStatus.NOT_FOUND));
					}
				}
				else{
					throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
				}

				String seatingClassName = seatingClassLruTypeMappingDTO.getSeatingClassName();
				// Seating Class
				if (StringUtils.isNotBlank(seatingClassName)) {
					SeatingClass seatingClass = Query.from(SeatingClass.class).where("name = ?", seatingClassName)
							.and("cms.site.owner = ?", airline).first();
					if (seatingClass != null) {
						if (!seatingClassLRUTypeMappingList.isEmpty()) {
							for (SeatingClassLRUTypeMapping seatingClassLRUTypeMapping2 : seatingClassLRUTypeMappingList) {
								if (seatingClassLRUTypeMapping2.getSeatingClass().equals(seatingClass)) {
									if(seatingClassLRUTypeMapping2.getLruType() == null){
										seatingClassLRUTypeMapping2.setLruType(lruType);
										seatingClassLRUTypeMapping = seatingClassLRUTypeMapping2;
										seatingClassLRUTypeMappingList.remove(seatingClassLRUTypeMapping2);
										flag = 1;
										break;
									}
									else if(seatingClassLRUTypeMapping2.getLruType().equals(lruType)){
										logger.info("Seating class and LRU Type mapping already exists in this LOPA");
										throw new ObjectExistsException(new ErrorResponse("Seating Class and LRU Type Mapping already exists in this LOPA", HttpStatus.FORBIDDEN));
									}
								}
							}
							if(flag == 0){
								seatingClassLRUTypeMapping = new SeatingClassLRUTypeMapping();
								seatingClassLRUTypeMapping.setSeatingClass(seatingClass);
								if(lruType != null){
									seatingClassLRUTypeMapping.setLruType(lruType);
								}
							}
							
						} else {
							seatingClassLRUTypeMapping = new SeatingClassLRUTypeMapping();
							seatingClassLRUTypeMapping.setSeatingClass(seatingClass);
							if(lruType != null){
								seatingClassLRUTypeMapping.setLruType(lruType);
							}
						}
					} else {
						logger.error("Seating Class " + seatingClassName + " does not exist");
						throw new ObjectNotFoundException(new ErrorResponse(
								messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
										new Object[] { ECMSConstants.SEATING_CLASS }, Locale.US),
								HttpStatus.NOT_FOUND));
					}
				} else {
					logger.error("Bad Request - Empty Parameter: Seating Class Id");
					throw new BadRequestException(
							new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US),
									HttpStatus.BAD_REQUEST));
				}

				seatingClassLRUTypeMappingList.add(seatingClassLRUTypeMapping);
				lopa.setSeatingClassLruTypeMappingList(seatingClassLRUTypeMappingList);
				logger.debug("LOPA saved successfully");
				lopa.save();
					
				lopaDTO = new LopaDTO();
				lopaDTO.setLopaInternalId(lopa.getId().toString());
				lopaDTO.setLopaName(lopa.getName());
				lopaDTO.setAircraftTypeId(lopa.getAircraft().getId().toString());
				lopaDTO.setAircraftType(lopa.getAircraft().getName());

				seatingClassLRUTypeMappingList = lopa.getSeatingClassLruTypeMappingList();
				if (seatingClassLRUTypeMappingList != null && !seatingClassLRUTypeMappingList.isEmpty()) {

					List<SeatingClassLruTypeMappingDTO> seatingClassLruTypeMappingDTOList = new ArrayList<>();

					for (SeatingClassLRUTypeMapping seatingClassLRUTypeMapping2 : seatingClassLRUTypeMappingList) {

						seatingClassLruTypeMappingDTO = new SeatingClassLruTypeMappingDTO();
						
						seatingClassLruTypeMappingDTO
						.setSeatingClassName(seatingClassLRUTypeMapping2.getSeatingClass().getName());

						LruType lruType2 = seatingClassLRUTypeMapping2.getLruType();
						if (lruType2 != null) {
						
							seatingClassLruTypeMappingDTO.setLruTypeName(lruType2.getName());
						}

						seatingClassLruTypeMappingDTOList.add(seatingClassLruTypeMappingDTO);
					}
					lopaDTO.setSeatingClassLruTypeMappingDTOList(seatingClassLruTypeMappingDTOList);
				}
			} else {
				logger.error("LOPA " + lopaId + " does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline " + airlineId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return lopaDTO;
	}

	/**
	 * Service Method to delete a Seating Class LRU Type Mapping from a LOPA You
	 * only need to pass in the Seating Class Id
	 * 
	 * @param airlineId
	 * @param lopaId
	 * @param seatingClassId
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */
	public Boolean deleteSeatingClassLruType(String airlineId, String lopaId, String seatingClassName)
			throws ObjectNotFoundException, BadRequestException {

		Boolean responseString = false;
		int flag = 0;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {
			Lopa lopa = Query.from(Lopa.class).where("id = ?", lopaId).and("cms.site.owner = ?", airline).first();
			if (lopa != null) {

				if (seatingClassName != null && StringUtils.isNotBlank(seatingClassName)) {
					SeatingClass seatingClass = Query.from(SeatingClass.class).where("name = ?", seatingClassName)
							.and("cms.site.owner = ?", airline).first();

					if (seatingClass != null) {

						List<SeatingClassLRUTypeMapping> seatingClassLRUTypeMappingList = lopa
								.getSeatingClassLruTypeMappingList();
						if (seatingClassLRUTypeMappingList != null && !seatingClassLRUTypeMappingList.isEmpty()) {
							for (SeatingClassLRUTypeMapping seatingClassLRUTypeMapping : seatingClassLRUTypeMappingList) {
								if (seatingClassLRUTypeMapping.getSeatingClass().equals(seatingClass)) {
									// we found the Seating Class
									seatingClassLRUTypeMappingList.remove(seatingClassLRUTypeMapping);
									lopa.setSeatingClassLruTypeMappingList(seatingClassLRUTypeMappingList);
									lopa.save();
									logger.debug("LOPA saved successfully");
									flag = 1;
									return true;
								}
							}
							if (flag == 0) {
								// we did not find the seating class
								logger.error(
										"Seating Class " + seatingClass.getName() + " does not exist in this LOPA");
								throw new ObjectNotFoundException(
										new ErrorResponse(
												messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
														new Object[] { ECMSConstants.SEATING_CLASS,
																ECMSConstants.LOPA },
														Locale.US),
												HttpStatus.NOT_FOUND));
							}
						} else {
							logger.error("LOPA does not contain any Seating Classes");
							throw new ObjectNotFoundException(new ErrorResponse(
									messageSource.getMessage(ECMSConstants.MAPPING_NOT_EXISTS,
											new Object[] { ECMSConstants.SEATING_CLASS }, Locale.US),
									HttpStatus.NOT_FOUND));
						}
					} else {
						logger.error("Seating Class " + seatingClassName + "does not exist");
						throw new ObjectNotFoundException(new ErrorResponse(
								messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
										new Object[] { ECMSConstants.SEATING_CLASS }, Locale.US),
								HttpStatus.NOT_FOUND));
					}
				} else {
					logger.error("Bad Request - Empty Parameter: Seating Class Id");
					throw new BadRequestException(
							new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US),
									HttpStatus.BAD_REQUEST));
				}
			} else {
				logger.error("LOPA " + lopaId + " does not exists");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline " + airlineId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return responseString;
	}

	/**
	 * 
	 * @param aircraftInternalId
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 */
	public List<LopaDTO> getLopasByAircraftId(String aircraftInternalId)
			throws ObjectNotFoundException, EmptyResponseException {

		List<LopaDTO> lopaDTOList = null;
		LopaDTO lopaDTO = null;
		List<SeatingClassLRUTypeMapping> seatingClassLRUTypeMappingList = null;

		Aircraft aircraft = Query.from(Aircraft.class).where("id = ?", aircraftInternalId).first();

		if (aircraft != null) {
			List<Lopa> lopaList = Query.from(Lopa.class).where("aircraft = ?", aircraft).selectAll();
			if (lopaList != null && !lopaList.isEmpty()) {

				lopaDTOList = new ArrayList<>();
				for (Lopa lopa : lopaList) {

					lopaDTO = new LopaDTO();
					lopaDTO.setLopaInternalId(lopa.getId().toString());
					lopaDTO.setLopaName(lopa.getName());

					lopaDTO.setAircraftTypeId(lopa.getAircraft().getId().toString());
					lopaDTO.setAircraftType(lopa.getAircraft().getName());

					seatingClassLRUTypeMappingList = lopa.getSeatingClassLruTypeMappingList();
					if (seatingClassLRUTypeMappingList != null && !seatingClassLRUTypeMappingList.isEmpty()) {

						List<SeatingClassLruTypeMappingDTO> seatingClassLruTypeMappingDTOList = new ArrayList<>();

						for (SeatingClassLRUTypeMapping seatingClassLRUTypeMapping : seatingClassLRUTypeMappingList) {

							SeatingClassLruTypeMappingDTO seatingClassLruTypeMappingDTO = new SeatingClassLruTypeMappingDTO();
							
							seatingClassLruTypeMappingDTO
							.setSeatingClassName(seatingClassLRUTypeMapping.getSeatingClass().getName());

							LruType lruType = seatingClassLRUTypeMapping.getLruType();
							if (lruType != null) {
							
								seatingClassLruTypeMappingDTO.setLruTypeName(lruType.getName());
							}

							seatingClassLruTypeMappingDTOList.add(seatingClassLruTypeMappingDTO);
						}
						lopaDTO.setSeatingClassLruTypeMappingDTOList(seatingClassLruTypeMappingDTOList);
					}
					lopaDTOList.add(lopaDTO);
				}
			} else {
				logger.error("No LOPAs exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.LOPA }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}

		else {
			logger.error("Aircraft " + aircraftInternalId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRCRAFT }, Locale.US), HttpStatus.NOT_FOUND));
		}

		return lopaDTOList;
	}

}