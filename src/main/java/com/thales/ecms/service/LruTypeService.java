package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Lopa;
import com.thales.ecms.model.Lru;
import com.thales.ecms.model.LruType;
import com.thales.ecms.model.SeatingClass;
import com.thales.ecms.model.dto.LruTypeDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir Service Class for CRUD operations on LRU Type
 */
@Service
public class LruTypeService {

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(LruTypeService.class);

	/**
	 * Service Method to add an LRU Type
	 * 
	 * @param airlineId
	 * @param lruTypeDetails
	 * @return
	 * @throws ObjectExistsException
	 * @throws BadRequestException
	 * @throws ObjectNotFoundException
	 */
	public LruTypeDTO addLRUType(LruTypeDTO lruTypeDetails)
			throws ObjectExistsException, BadRequestException, ObjectNotFoundException {

		LruTypeDTO lruTypeDTO = null;
		LruType lruType = null;

		String lruTypeName = lruTypeDetails.getLruTypeName();
		if (lruTypeName != null && StringUtils.isNotBlank(lruTypeName)) {
			lruType = Query.from(LruType.class).where("name = ?", lruTypeName).first();
			if (lruType == null) {
				lruType = new LruType();
				lruType.setName(lruTypeName);
//				double maxSpace = lruTypeDetails.getMaxSpaceAllowance();
//				lruType.setMaxSpaceAllowance(maxSpace);

				lruType.as(Site.ObjectModification.class).setGlobal(true);
				try {
					lruType.save();
					logger.debug("LruType instance saved successfully");
				} catch (Exception e) {
					logger.error("Bad Request - LRU Type Name too long");
					throw new BadRequestException(
							new ErrorResponse(messageSource.getMessage(ECMSConstants.LRU_TYPE_NAME, null, Locale.US),
									HttpStatus.BAD_REQUEST));
				}
				

				lruTypeDTO = new LruTypeDTO();
				lruTypeDTO.setLruTypeInternalId(lruType.getId().toString());
				lruTypeDTO.setLruTypeName(lruType.getName());
//				lruTypeDTO.setMaxSpaceAllowance(lruType.getMaxSpaceAllowance());
			} else {
				logger.error("Cannot create LRU Type - LRU Type with name " + lruTypeName + " already exists");
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.LRU_TYPE, ECMSConstants.NAME }, Locale.US),
						HttpStatus.FORBIDDEN));
			}

		} else {
			logger.error("Bad Request - Empty Parameter: LRU Type Name");
			throw new BadRequestException(new ErrorResponse(
					messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
		}
		return lruTypeDTO;
	}

	/**
	 * Service Method to get a list of all LRU Types
	 * 
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public List<LruTypeDTO> getLRUTypes() throws ObjectNotFoundException {

		List<LruTypeDTO> lruTypeDTOList = null;

		List<LruType> lruTypeList = Query.from(LruType.class).selectAll();
		if (lruTypeList != null && !lruTypeList.isEmpty()) {
			lruTypeDTOList = new ArrayList<>();
			for (LruType lruType : lruTypeList) {
				if (Objects.nonNull(lruType.getName())) {
					LruTypeDTO lruTypeDTO = new LruTypeDTO();
					lruTypeDTO.setLruTypeInternalId(lruType.getId().toString());
					lruTypeDTO.setLruTypeName(lruType.getName());
//					lruTypeDTO.setMaxSpaceAllowance(lruType.getMaxSpaceAllowance());
					lruTypeDTOList.add(lruTypeDTO);
				}
			}
		} else {
			logger.error("No LRU Types exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.LRU_TYPE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return lruTypeDTOList;

	}

	/**
	 * Service Method to return an LRU Type
	 * 
	 * @param airlineId
	 * @param lruTypeId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public LruTypeDTO getLRUTypeById(String lruTypeId) throws ObjectNotFoundException {

		LruTypeDTO lruTypeDTO = null;

		LruType lruType = Query.from(LruType.class).where("id = ?", lruTypeId).first();
		if (lruType != null) {
			lruTypeDTO = new LruTypeDTO();
			lruTypeDTO.setLruTypeInternalId(lruType.getId().toString());
			lruTypeDTO.setLruTypeName(lruType.getName());
//			lruTypeDTO.setMaxSpaceAllowance(lruType.getMaxSpaceAllowance());
		} else {
			logger.error("LRU Type " + lruTypeId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.LRU_TYPE }, Locale.US), HttpStatus.NOT_FOUND));
		}

		return lruTypeDTO;
	}

	/**
	 * Service Method to update an LRU Type
	 * 
	 * @param airlineId
	 * @param lruTypeId
	 * @param lruTypeDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	public LruTypeDTO updateLRUType(String lruTypeId, LruTypeDTO lruTypeDetails)
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException {

		LruTypeDTO lruTypeDTO = null;

		LruType lruType = Query.from(LruType.class).where("id = ?", lruTypeId).first();

		if (lruType != null) {

			String newLruTypeName = lruTypeDetails.getLruTypeName();
			if (newLruTypeName != null && StringUtils.isNotBlank(newLruTypeName)) {
				
					if (Query.from(LruType.class).where("name = ?", newLruTypeName).first() == null) {
						lruType.setName(newLruTypeName);
					} else {
						logger.error("Bad Request - LRU Type Name too long");
						throw new BadRequestException(
								new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US),
										HttpStatus.BAD_REQUEST));
					}
				} else {
					logger.error("Cannot update LRU Type - LRU Type with name " + newLruTypeName + " already exists");
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.LRU_TYPE, ECMSConstants.NAME }, Locale.US),
							HttpStatus.FORBIDDEN));
				}
			

//			double maxSpace = lruTypeDetails.getMaxSpaceAllowance();
//			lruType.setMaxSpaceAllowance(maxSpace);

			try {
				lruType.save();
				logger.debug("LruType instance saved successfully");
			} catch (Exception e) {
				logger.error("Bad Request - LRU Type Name too long");
				throw new BadRequestException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.LRU_TYPE_NAME, null, Locale.US),
								HttpStatus.BAD_REQUEST));
			}
			

			lruTypeDTO = new LruTypeDTO();
			lruTypeDTO.setLruTypeInternalId(lruType.getId().toString());
			lruTypeDTO.setLruTypeName(lruType.getName());
//			lruTypeDTO.setMaxSpaceAllowance(lruType.getMaxSpaceAllowance());
		} else {
			logger.error("LRU Type " + lruTypeId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.LRU_TYPE }, Locale.US), HttpStatus.NOT_FOUND));
		}

		return lruTypeDTO;
	}

	/**
	 * Service Method to delete an LRU Type
	 * 
	 * @param lruTypeId
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws ObjectExistsException
	 * @throws BadRequestException 
	 * @throws NoSuchMessageException 
	 */
	public Boolean deleteLRUType(String lruTypeId) throws ObjectNotFoundException, ObjectExistsException, NoSuchMessageException, BadRequestException {
		if (StringUtils.isNotBlank(lruTypeId)) {
			LruType lruType = Query.from(LruType.class).where("id = ?", lruTypeId.trim()).first();
			if (lruType != null) {
				Lru lru = Query.from(Lru.class).where("lruType = ?", lruTypeId).first();
				if (lru == null) {
					Lopa lopa = Query.from(Lopa.class).where("seatingClassLruTypeMappingList/lruType = ?", lruTypeId.trim())
							.first();
					if (lopa == null) {
						lruType.delete();
						return true;
					} else {
						logger.error("Cannot delete LRU Type - 1 or more LOPA exist for this LRUType");
						throw new ObjectExistsException(new ErrorResponse(
								messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
										new Object[] { ECMSConstants.LOPA, ECMSConstants.LRU }, Locale.US),
								HttpStatus.FORBIDDEN));
					}
				} else {
					logger.error("Cannot delete LRU Type - 1 or more LRUs exist for this LRUType");
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.LRU_TYPE, ECMSConstants.LRU }, Locale.US),
							HttpStatus.FORBIDDEN));
				}
			} else {
				logger.error("LRU Type " + lruTypeId + " does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.LRU_TYPE }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}else{
			logger.error("Bad Request - LRU Type Id Cannot be Blank");
			throw new BadRequestException(
					new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US),
							HttpStatus.BAD_REQUEST));
		}
	}
}
