package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;
import com.psddev.cms.db.Site;
import com.psddev.dari.db.AsyncDatabaseWriter;
import com.psddev.dari.db.Database;
import com.psddev.dari.db.ObjectField;
import com.psddev.dari.db.ObjectType;
import com.psddev.dari.db.Query;
import com.psddev.dari.db.WriteOperation;
import com.psddev.dari.util.AsyncQueue;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.Configuration.ConfigurationTitle;
import com.thales.ecms.model.Configuration.ConfigurationTitle.Status;
import com.thales.ecms.model.ContentType;
import com.thales.ecms.model.Rule;
import com.thales.ecms.model.Rule.ContentFieldValidation;
import com.thales.ecms.model.ValidatorAttribute;
import com.thales.ecms.model.dto.RuleContentFieldDTO;
import com.thales.ecms.model.dto.RuleContentTypeMappingDTO;
import com.thales.ecms.model.dto.RuleDTO;
import com.thales.ecms.model.dto.RuleFieldDTO;
import com.thales.ecms.model.dto.ValidatorAttributeDTO;
import com.thales.ecms.utils.ECMSConstants;
import com.thales.ecms.utils.RuleApplicationUtil;

/**
 * Service Class to perform CRUD operations on RuleService
 */
@Service
public class RuleService {

	public static Logger logger = LoggerFactory.getLogger(RuleService.class);
	
	@Autowired
	private MessageSource messageSource;

	/**
	 * Service Method to return a list of all contentType
	 * 
	 */
	public List<RuleContentFieldDTO> contentTypeList() {
		List<RuleContentFieldDTO> ruleContentFieldDTOList = null;
		RuleContentFieldDTO ruleContentFieldDTO = null;
		List<ObjectType> types = new ArrayList<ObjectType>(Database.Static.getDefault().getEnvironment().getTypes());
		Reflections reflections = new Reflections("com.thales.ecms.model");
		Set<Class<? extends ContentType>> contentModels = reflections.getSubTypesOf(ContentType.class);
		if (Objects.nonNull(types)) {
			ruleContentFieldDTOList = new ArrayList<RuleContentFieldDTO>();
			for (ObjectType objectType : types) {
				if (Objects.nonNull(objectType)) {
					String dbClassName = objectType.getInternalName().toString();
					for (Class<? extends ContentType> contentClass : contentModels) {
						String modelClassName = contentClass.getName().toString();
						if (modelClassName.equals(dbClassName)) {
							ruleContentFieldDTO = new RuleContentFieldDTO();
							ruleContentFieldDTO.setContentTypeId(objectType.getId().toString());
							ruleContentFieldDTO.setContentTypeName(objectType.getDisplayName());
							ruleContentFieldDTOList.add(ruleContentFieldDTO);
						}
					}
				}
			}
		}
		return ruleContentFieldDTOList;
	}

	/**
	 * Service Method to get a list of all Fields of a Content Type by contentTypeId
	 * 
	 * @param contentTypeId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public List<RuleContentFieldDTO> getFieldByContentTypeId(String contentTypeId) throws ObjectNotFoundException{
		List<RuleContentFieldDTO> ruleContentFieldDTOList = null;
		RuleContentFieldDTO ruleContentFieldDTO = null;

		//List<ObjectType> types = new ArrayList<ObjectType>(Database.Static.getDefault().getEnvironment().getTypes());
		ObjectType objectType = Query.from(ObjectType.class).where("id = ?",contentTypeId).first();
		ruleContentFieldDTOList = new ArrayList<RuleContentFieldDTO>();
		if (Objects.nonNull(objectType)) {
			List<ObjectField> objectFieldList = objectType.getFields();
			for (ObjectField objectField : objectFieldList) {
				if (Objects.nonNull(objectField)) {
					ruleContentFieldDTO = new RuleContentFieldDTO();
					ruleContentFieldDTO.setFieldId(objectField.getId().toString());
					ruleContentFieldDTO.setFieldName(objectField.getInternalName());
					//boolean dataType = checkDataTypeOfField(objectField);
					/*if(dataType){
						//The dataType of ObjectField is a String
						//set translatable and maxChar is true. This means that on the UI, we'll need to show the checkbox/textbox for translatable and maxChar
						ruleContentFieldDTO.setTranslatable(true);
						ruleContentFieldDTO.setMaxChar(true);
					}*/
					ruleContentFieldDTOList.add(ruleContentFieldDTO);
				}
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.CONTENT_TYPE }, Locale.US), HttpStatus.FORBIDDEN));
		}

		return ruleContentFieldDTOList;
	}


	/**
	 * Service Method to add a Rule
	 * @param airlineId
	 * @param ruleDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	public String addRule(String airlineId, RuleDTO ruleDetails)
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException {

		logger.info(ruleDetails.toString());
		
		RuleContentTypeMappingDTO ruleContentTypeMappingDTO = null;
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(Objects.nonNull(airline)){
			Set<Site> consumers = Sets.newHashSet();
			consumers.add(airline);
			String ruleName = ruleDetails.getRuleName();
			if (Objects.nonNull(ruleName) && StringUtils.isNotBlank(ruleName)) {
				if (Objects.nonNull(ruleDetails.getRuleContentTypeMappingDTO())) {

					ruleContentTypeMappingDTO = ruleDetails.getRuleContentTypeMappingDTO();
					String contentTypeInternalId = ruleContentTypeMappingDTO.getContentTypeId();
					logger.info("Content Type Id: " + contentTypeInternalId);

					ObjectType objectType = Query.from(ObjectType.class).where("id = ?", contentTypeInternalId).first();
					if(Objects.isNull(objectType))
						throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.CONTENT_TYPE }, Locale.US), HttpStatus.FORBIDDEN));

					logger.info("Content Type Name:" + objectType.getInternalName());

					List<String> fieldList = objectType.getFields().stream().map(fieldName -> fieldName.getInternalName())
							.collect(Collectors.toList());

					List<RuleFieldDTO> ruleFieldDTOList = ruleContentTypeMappingDTO.getContentTypeField();
					List<ContentFieldValidation> contentFieldValidations = null;
					ValidatorAttribute validatorAttribute = null;
					if (Objects.nonNull(ruleFieldDTOList)) {
						contentFieldValidations = new ArrayList<>();
						for (RuleFieldDTO ruleFieldDTO : ruleFieldDTOList) {
							ContentFieldValidation contentFieldValidation = null;
							if (Objects.nonNull(ruleFieldDTO) && fieldList.contains(ruleFieldDTO.getFieldName())) {
								contentFieldValidation = new ContentFieldValidation();
								validatorAttribute = new ValidatorAttribute();
								validatorAttribute.setRequired(ruleFieldDTO.getValidatorDTO().isRequired());
								validatorAttribute.setTranslatable(ruleFieldDTO.getValidatorDTO().isTranslatable());
								validatorAttribute.setMaxChar(ruleFieldDTO.getValidatorDTO().getMaxChar());
								contentFieldValidation.setField(ruleFieldDTO.getFieldName());
								contentFieldValidation.setValidatorAttribute(validatorAttribute);
								contentFieldValidations.add(contentFieldValidation);
							}
						}
					}
					Rule rule = new Rule();
					rule.setName(ruleName);
					rule.setObjectType(objectType);
					rule.setContentFieldValidations(contentFieldValidations);
					Date date = new Date();
					rule.setCreationDate(date);
					rule.setLastModifiedDate(date);
					rule.as(Site.ObjectModification.class).setOwner(airline);
					rule.as(Site.ObjectModification.class).setConsumers(consumers);
					rule.save();
					logger.info("Rule saved successfully:" + rule.getId().toString());					

				} else {
					throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
							new Object[] { ECMSConstants.CONTENT_TYPE }, Locale.US), HttpStatus.FORBIDDEN));
				}
			} else {
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
						new Object[] { ECMSConstants.RULE_NAME }, Locale.US), HttpStatus.FORBIDDEN));
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}
		return "Rule saved successfully";
	}

	/**
	 * Service Method to get List of All Rules created in eCMS
	 * 
	 */
	public List<RuleDTO> getRules(String airlineId) throws ObjectNotFoundException{
		
		List<RuleDTO> ruleDTOList = null;		
		List<Rule> ruleList = null;
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		
		if(Objects.nonNull(airline)){
			ruleList = Query.from(Rule.class).where("cms.site.owner = ?",airline).selectAll();
			if (Objects.nonNull(ruleList)) {				
				ruleDTOList = new ArrayList<RuleDTO>();
				for (Rule rule : ruleList) {
					if (Objects.nonNull(rule)) {												
						ruleDTOList.add(populateRuleDTO(rule));
					}
				}
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}
		
		return ruleDTOList;
	}

	/**
	 * Service Method to delete a Rule By its Id
	 * 
	 * @param ruleId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public String deleteRule(String airlineId, String ruleId) throws ObjectNotFoundException {
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();		
		if(Objects.nonNull(airline)){
			Rule rule = Query.from(Rule.class).where("id = ?", ruleId).and("cms.site.owner = ?",airline).first();
			if (rule != null) {
				rule.delete();
				return "Rule deleted successfully";
			} else {
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.RULE }, Locale.US), HttpStatus.FORBIDDEN));
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}		
	}

	/**
	 * Service Method to get a Rule By Its Id
	 * 
	 * @param ruleId
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 */

	public RuleDTO getRuleById(String airlineId,String ruleId) throws ObjectNotFoundException, BadRequestException {
		RuleDTO ruleDTO = null;
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(Objects.nonNull(airline)){			
			if (Objects.nonNull(ruleId)) {
				Rule rule = Query.from(Rule.class).where("id = ?", ruleId).and("cms.site.owner = ?",airline).first();
				if (Objects.nonNull(rule)) {
					ruleDTO = new RuleDTO();
					List<RuleFieldDTO> ruleFieldDTOList = new ArrayList<>();
					List<Rule.ContentFieldValidation> contentFieldValidation = new ArrayList<>();
					RuleContentTypeMappingDTO ruleContentTypeMappingDTO = null;
					ValidatorAttributeDTO validatorAttributeDTO = null;
					RuleFieldDTO ruleFieldDTO = null;
					ObjectType objectType = rule.getObjectType();
					if (Objects.nonNull(objectType)) {
						ruleContentTypeMappingDTO = new RuleContentTypeMappingDTO();
						contentFieldValidation = rule.getContentFieldValidations();
						for (ContentFieldValidation contentFieldVldn : contentFieldValidation) {
							ruleFieldDTO = new RuleFieldDTO();
							if (Objects.nonNull(contentFieldVldn.getValidatorAttribute())) {
								validatorAttributeDTO = new ValidatorAttributeDTO();
								validatorAttributeDTO.setRequired(contentFieldVldn.getValidatorAttribute().isRequired());
								validatorAttributeDTO.setTranslatable(contentFieldVldn.getValidatorAttribute().isTranslatable());
								validatorAttributeDTO.setMaxChar(contentFieldVldn.getValidatorAttribute().getMaxChar());
							}
							ruleFieldDTO.setFieldName(contentFieldVldn.getField());
							ruleFieldDTO.setValidatorDTO(validatorAttributeDTO);
							ruleFieldDTOList.add(ruleFieldDTO);
						} // end for
						ruleContentTypeMappingDTO.setContentTypeId(objectType.getId().toString());
						ruleContentTypeMappingDTO.setContentTypeName(objectType.getDisplayName());
						ruleContentTypeMappingDTO.setContentTypeField(ruleFieldDTOList);								

						ruleDTO.setRuleId(rule.getId().toString());
						ruleDTO.setRuleName(rule.getName());
						if(rule.getCreationDate()!=null)
							ruleDTO.setCreationDate(rule.getCreationDate().getTime());
						if(rule.getLastModifiedDate()!=null)
							ruleDTO.setLastModifiedDate(rule.getLastModifiedDate().getTime());
						ruleDTO.setRuleContentTypeMappingDTO(ruleContentTypeMappingDTO);

					} 
				} else {
					throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] { ECMSConstants.RULE }, Locale.US), HttpStatus.FORBIDDEN));
				}

			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}		
		return ruleDTO;
	}

	/**
	 * Helper Method to get the data type of a Field
	 * Based on the data type, we'll determine what validation attributes will be applied on the field 
	 * @param objectField
	 * @return
	 */
	public boolean checkDataTypeOfField(ObjectField objectField){
		
		String dataType = objectField.getInternalType();
		logger.info(dataType);
		logger.info("Internal Item Type:"+objectField.getInternalItemType());
		
//		if((dataType.equalsIgnoreCase("text") && objectField.getJavaEnumClassName() == null) || dataType.equalsIgnoreCase("referentialText")){
//			//The data type of this field is a String or Referential Text
//			return true;
//		}
		return false;
	}
	
	/**
	 * Service Method to update a Rule
	 * @param airlineId
	 * @param ruleDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	public String updateRule(String airlineId, String ruleId ,RuleDTO ruleDetails)
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException {				
		RuleContentTypeMappingDTO ruleContentTypeMappingDTO = null;
		
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(Objects.nonNull(airline)){
			Set<Site> consumers = Sets.newHashSet();
			consumers.add(airline);
			String ruleName = ruleDetails.getRuleName();			
			if (Objects.nonNull(ruleName) && StringUtils.isNotBlank(ruleName)) {
				Rule existingRule = Query.from(Rule.class).where("id = ?",ruleId).and("cms.site.owner = ?",airline).first();
				if(Objects.nonNull(existingRule)){
					Rule rule = existingRule;
					Rule temp = Query.from(Rule.class).where("name = ?",ruleName).and("cms.site.owner = ?",airline).first();
//					Rule not exists with this name
					if(temp!=null && !temp.getId().toString().equals(existingRule.getId().toString()))
						throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
								new Object[] { ECMSConstants.RULE,ECMSConstants.RULE_NAME }, Locale.US), HttpStatus.FORBIDDEN));
					
					if (Objects.nonNull(ruleDetails.getRuleContentTypeMappingDTO())) {
						ruleContentTypeMappingDTO = ruleDetails.getRuleContentTypeMappingDTO();
						String contentTypeInternalId = ruleContentTypeMappingDTO.getContentTypeId();
						
						logger.info("Content Type Id: " + contentTypeInternalId);

						ObjectType objectType = Query.from(ObjectType.class).where("id = ?", contentTypeInternalId).first();						
						if(Objects.isNull(objectType))
							throw new ObjectNotFoundException(
									new ErrorResponse("Given Content Type does not exist", HttpStatus.OK));

						logger.info("ObjectType Name: " + objectType.getInternalName()+", id: "+objectType.getId().toString());
						List<Configuration> listOfConfigUsingThisRule = Query.from(Configuration.class).where("rules/name = ?",rule.getName()).and("cms.site.owner = ?",airline).selectAll();						
						if(existingRule.getObjectType()!=null && !existingRule.getObjectType().getId().toString().equals(objectType.getId().toString())){
							boolean proceedToUpdate = true;
//							If this rule has been used in any Configuration then do not change its objectType							
							logger.info("listOfConfiguration for this airline : " + listOfConfigUsingThisRule);
							for(Configuration configuration : listOfConfigUsingThisRule){
								Set<Rule> rules = configuration.getRules();
								if(rules!=null && !rules.isEmpty() && rules.contains(rule)){
									proceedToUpdate=false;
									break;
								}
							}
							logger.info("proceedToUpdate : " + proceedToUpdate);
							if(!proceedToUpdate)
								throw new BadRequestException(
										new ErrorResponse("Bad Request - Can not update Rule with new ContentType", HttpStatus.FORBIDDEN));
						}
						
						List<String> fieldList = objectType.getFields().stream().map(fieldName -> fieldName.getInternalName())
								.collect(Collectors.toList());

						List<RuleFieldDTO> ruleFieldDTOList = ruleContentTypeMappingDTO.getContentTypeField();
						List<ContentFieldValidation> contentFieldValidations = null;
						ValidatorAttribute validatorAttribute = null;
						if (Objects.nonNull(ruleFieldDTOList)) {
							contentFieldValidations = new ArrayList<>();
							for (RuleFieldDTO ruleFieldDTO : ruleFieldDTOList) {
								ContentFieldValidation contentFieldValidation = null;
								if (Objects.nonNull(ruleFieldDTO) && fieldList.contains(ruleFieldDTO.getFieldName())) {
									contentFieldValidation = new ContentFieldValidation();
									validatorAttribute = new ValidatorAttribute();
									validatorAttribute.setRequired(ruleFieldDTO.getValidatorDTO().isRequired());
									validatorAttribute.setTranslatable(ruleFieldDTO.getValidatorDTO().isTranslatable());
									validatorAttribute.setMaxChar(ruleFieldDTO.getValidatorDTO().getMaxChar());
									contentFieldValidation.setField(ruleFieldDTO.getFieldName());
									contentFieldValidation.setValidatorAttribute(validatorAttribute);
									contentFieldValidations.add(contentFieldValidation);
								}
							}
						}
						
						rule.setName(ruleName);
						rule.setObjectType(objectType);
						rule.setContentFieldValidations(contentFieldValidations);
						Date date = new Date();
						if(rule.getCreationDate()==null)
							rule.setCreationDate(date);
						rule.setLastModifiedDate(date);
						rule.as(Site.ObjectModification.class).setOwner(airline);
						rule.as(Site.ObjectModification.class).setConsumers(consumers);
						AsyncQueue<Configuration> asyncQueue = new AsyncQueue<>();
						for(Configuration configuration : listOfConfigUsingThisRule){
							if(configuration.getTitleList()!=null){
								for(ConfigurationTitle configurationTitle : configuration.getTitleList()){
									if(configurationTitle.getContent()!=null){
										if(configurationTitle.getContent().getClass().getName().equals(objectType.getObjectClassName())){
											try {
												RuleApplicationUtil.applyRuleOnConfigurationTitle(existingRule, configurationTitle);
												logger.info("configurationTitle.getContentStatus : "+configurationTitle.getContentStatus());
//												I don't know what to do if Status is Complete								
											} catch (Exception e) {
												e.printStackTrace();
												logger.info("Something Wrong. Exception raised : "+e.getMessage());
												configurationTitle.setContentStatus(Status.Incomplete);								
											}
											asyncQueue.add(configuration);
//											configuration.save();
										}
									}								
								}
							}							
						}
						AsyncDatabaseWriter<Configuration> writer = new AsyncDatabaseWriter<Configuration>("Configuration Batch Writer",
								asyncQueue,
								Database.Static.getDefault(),
								WriteOperation.SAVE, listOfConfigUsingThisRule.size(), true);
					    writer.submit();
						rule.save();
						logger.info("Rule updated successfully:" + rule.getId().toString());						

					} else {
						throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
								new Object[] { ECMSConstants.CONTENT_TYPE }, Locale.US), HttpStatus.FORBIDDEN));
					}
				}else{
					throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
							new Object[] { ECMSConstants.RULE }, Locale.US), HttpStatus.FORBIDDEN));
				}				
			} else {
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
						new Object[] { ECMSConstants.RULE_NAME }, Locale.US), HttpStatus.FORBIDDEN));
			}
		}
		else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.FORBIDDEN));
		}
		return "Rule updated successfully";
	}	
	
	private RuleDTO populateRuleDTO(Rule rule){
		RuleDTO ruleDTO = new RuleDTO();		
		ruleDTO.setRuleId(rule.getId().toString());
		ruleDTO.setRuleName(rule.getName());
		RuleContentTypeMappingDTO ruleContentTypeMappingDTO = new RuleContentTypeMappingDTO();
		ObjectType objectType = rule.getObjectType();
		if(objectType!=null){
			ruleContentTypeMappingDTO.setContentTypeId(objectType.getId().toString());
			ruleContentTypeMappingDTO.setContentTypeName(objectType.getDisplayName());
		}
		List<RuleFieldDTO> ruleFieldDTOList = new ArrayList<>();
		if(rule.getContentFieldValidations()!=null){
			for(ContentFieldValidation contentFieldValidation : rule.getContentFieldValidations()){
				RuleFieldDTO ruleFieldDTO = new RuleFieldDTO();				
				ruleFieldDTO.setFieldName(contentFieldValidation.getField());
				ValidatorAttribute validatorAttribute = contentFieldValidation.getValidatorAttribute();
				if(validatorAttribute!=null){
					ValidatorAttributeDTO validatorAttributeDTO = new ValidatorAttributeDTO();
					validatorAttributeDTO.setRequired(validatorAttribute.isRequired());
					validatorAttributeDTO.setTranslatable(validatorAttribute.isTranslatable());
					validatorAttributeDTO.setMaxChar(validatorAttribute.getMaxChar());
					ruleFieldDTO.setValidatorDTO(validatorAttributeDTO);
				}				
				ruleFieldDTOList.add(ruleFieldDTO);
			}							
		}
		ruleContentTypeMappingDTO.setContentTypeField(ruleFieldDTOList);
		ruleDTO.setRuleContentTypeMappingDTO(ruleContentTypeMappingDTO);
		if(rule.getCreationDate()!=null)
			ruleDTO.setCreationDate(rule.getCreationDate().getTime());
		if(rule.getLastModifiedDate()!=null)
			ruleDTO.setLastModifiedDate(rule.getLastModifiedDate().getTime());
		return ruleDTO;
	}
	
}
