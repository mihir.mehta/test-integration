/*package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;
import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.LocationType;
import com.thales.ecms.model.PaxguiLanguage;
//import com.thales.ecms.model.GenericResponseDTO;
import com.thales.ecms.model.Location;
import com.thales.ecms.model.dto.LocationDTO;
import com.thales.ecms.model.dto.LocationTypeDTO;

*//**
 * @author Sachin Service Class to perform CRUD operations on Airline Location
 *//*
@Service
public class LocationService {

	public static Logger logger = LoggerFactory.getLogger(LocationService.class);

	*//**
	 * Method for adding location in an airline.
	 * 
	 * @param airlineId
	 * @param locationTypeId
	 * @param locationDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 *//*
	public LocationDTO addLocation(String airlineId, LocationDTO locationDetails)
			throws ObjectNotFoundException, BadRequestException {

		Location location = null;
		LocationDTO locationDTO = null;
		Set<Site> consumers = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		
		if (airline != null) {

			consumers = Sets.newHashSet();
			consumers.add(airline);

		} else {
			throw new ObjectNotFoundException(new ErrorResponse("Airline does not exist", HttpStatus.OK));
		}

		if (locationDetails != null && StringUtils.isNotBlank(locationDetails.getLocationTypeId())) {
			
			LocationType locType = Query.from(LocationType.class).where("id = ?", locationDetails.getLocationTypeId()).first();
			if(locType != null){
				location = new Location();
				location.setLocationType(locType);

				location.as(Site.ObjectModification.class).setOwner(airline);
				location.as(Site.ObjectModification.class).setConsumers(consumers);
				location.save();

				locationDTO = populateLocationDTO(location, locType);
			}else{
				throw new BadRequestException(new ErrorResponse("Bad Request - LocationTypeId does not exist", HttpStatus.OK));
			}
		} else {
			throw new BadRequestException(new ErrorResponse("Bad Request - LocationType cannot be blank", HttpStatus.OK));
		}

		return locationDTO;
	}

	*//**
	 * 
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException
	 *//*
	public List<LocationDTO> getlocationList(String airlineId) throws ObjectNotFoundException {
		List<LocationDTO> locationDTO = new ArrayList<>();

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();

		if (airline != null) {

			List<Location> locationList = Query.from(Location.class).and("cms.site.owner = ?", airline).selectAll();

			if (locationList != null && !locationList.isEmpty()) {
				for (Location location : locationList) {
					LocationType locationType = Query.from(LocationType.class).where("id = ?",location.getLocationType().getId().toString()).first();
					locationDTO.add(populateLocationDTO(location,locationType));
				}
			}
		} else {
			throw new ObjectNotFoundException(new ErrorResponse("Airline does not exist", HttpStatus.OK));
		}
		return locationDTO;
	}

	*//**
	 * 
	 * @param airlineId
	 * @param locationId
	 * @return
	 * @throws ObjectNotFoundException
	 *//*
	public String deleteLocationById(String airlineId, String locationId) throws ObjectNotFoundException {
		String responseString;
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();

		if (airline != null) {
			Location location = Query.from(Location.class).where("id = ?", locationId).first();
			if (location != null)
				location.delete();
			responseString = "Location deleted successfully";

		} else {
			throw new ObjectNotFoundException(new ErrorResponse("Airline does not exist", HttpStatus.OK));
		}

		return responseString;

	}

	*//**
	 * Method for updating location
	 * 
	 * @param airlineId
	 * @param locationId
	 * @param locationDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 *//*
	public LocationDTO updateLocationById(String airlineId, String locationId, LocationDTO locationDetails)
			throws ObjectNotFoundException, BadRequestException {
		Location location = null;
		LocationDTO locationDTO = null;
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();

		if (airline != null) {

			location = Query.from(Location.class).where("id = ?", locationId).and("cms.site.owner = ?", airline)
					.first();
			if (location != null) {
				if (null != locationDetails ) {
					// if(locationDetails.getLocationName() != null)
					// location.setLocationName(locationDetails.getLocationName());

					String locTypeId = locationDetails.getLocationTypeId();
					LocationType locationTypeObj = Query.from(LocationType.class).where("id = ?", locTypeId).first();
					// locationTypeObj.setName(locationDetails.getLocationType());
					// locationTypeObj.save();
					if (locationTypeObj != null) {

						location.setLocationType(locationTypeObj);
						location.save();
					} else {
						throw new ObjectNotFoundException(
								new ErrorResponse("LocationType does not exist", HttpStatus.OK));

					}
					locationDTO = populateLocationDTO(location, locationTypeObj);
				}
			} else {
				throw new ObjectNotFoundException(new ErrorResponse("Location does not exist", HttpStatus.OK));

			}
		} else {
			throw new ObjectNotFoundException(new ErrorResponse("Airline does not exist", HttpStatus.OK));
		}
		return locationDTO;
	}
	
	
	 * Created a function to populate LocationDTO by location object to get in Location CRUD Operation
	 
	public LocationDTO populateLocationDTO(Location location, LocationType locationType){
		LocationDTO locationDTO = null;
		if(location != null && locationType!=null){
			locationDTO = new LocationDTO();
			locationDTO.setLocationInternalId(location.getId().toString());
			locationDTO.setLocationTypeId(locationType.getId().toString());	
		}
		return locationDTO;
	} 
	
}
*/