package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;
import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Location;
import com.thales.ecms.model.ShipmentProfile;
import com.thales.ecms.model.dto.ShipmentProfileDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author kumargupta.v
 *
 */

@Service
public class ShipmentProfileService {

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(ShipmentProfileService.class);
	
	public ShipmentProfileDTO addShipmentProfile(String airlineId, ShipmentProfileDTO shipmentProfileDTO)
			throws BadRequestException, ObjectExistsException, NoSuchMessageException, ObjectNotFoundException
	{
		ShipmentProfile shipmentProfile=null;
		Site airline = Query.from(Site.class).where(" id = ?", airlineId).first();
		if (airline != null) {
			Set<Site> consumers = null;
			consumers = Sets.newHashSet();
			consumers.add(airline);
			shipmentProfile=new ShipmentProfile();
			if(shipmentProfileDTO.getHardDrives()>=0){
				shipmentProfile.setHardDrives(shipmentProfileDTO.getHardDrives());
			}
			else
			{
				throw new BadRequestException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
								new Object[] {ECMSConstants.SHIPMENT_HARD_DRIVE  }, Locale.US), HttpStatus.BAD_REQUEST));
			}
			
			ShipmentProfile alreadyExistshipmentProfile=Query.from(ShipmentProfile.class).where(" name = ?", shipmentProfileDTO.getName())
			.and("cms.site.owner = ?", airline).first();
			
			if (alreadyExistshipmentProfile==null)
			{
				if(StringUtils.isNotBlank(shipmentProfileDTO.getName()))
				{
					shipmentProfile.setName(shipmentProfileDTO.getName());
				}
				else
				{
					throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
							new Object[] {ECMSConstants.SHIPMENT_PROFILE_NAME}, Locale.US), HttpStatus.BAD_REQUEST));
				}
			}
			else
			{
				throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
						new Object[] {ECMSConstants.SHIPMENTPROFILE,shipmentProfileDTO.getName()}, Locale.US), HttpStatus.BAD_REQUEST));
			}
			
			if (StringUtils.isNotBlank(shipmentProfileDTO.getShippingType())) {
				if (shipmentProfileDTO.getShippingType()
						.equalsIgnoreCase(ShipmentProfile.ShippingType.DHL.toString())) {
					shipmentProfile.setShippingType(ShipmentProfile.ShippingType.DHL);
				}
				else if(shipmentProfileDTO.getShippingType()
						.equalsIgnoreCase(ShipmentProfile.ShippingType.FEDEX.toString())) {
					shipmentProfile.setShippingType(ShipmentProfile.ShippingType.FEDEX);
				}
				else if(shipmentProfileDTO.getShippingType()
						.equalsIgnoreCase(ShipmentProfile.ShippingType.UPS.toString())) {
					shipmentProfile.setShippingType(ShipmentProfile.ShippingType.UPS);
				}
				else if(shipmentProfileDTO.getShippingType()
						.equalsIgnoreCase(ShipmentProfile.ShippingType.USPS.toString())) {
					shipmentProfile.setShippingType(ShipmentProfile.ShippingType.USPS);
				}
				else
				{
					throw new BadRequestException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,new Object[] {ECMSConstants.SHIPMENT_TYPE},Locale.US),
						HttpStatus.BAD_REQUEST));
				}
			}
			
			Set<String>locations=shipmentProfileDTO.getLocationIds();
			if(locations==null||locations.isEmpty())
			{
				throw new BadRequestException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,new Object[] {ECMSConstants.LOCATION},Locale.US),
						HttpStatus.BAD_REQUEST));
			}
			
			Set<Location>locationSet=new HashSet<>();
			
			for(String loc:locations)
			{
				Location location=Query.from(Location.class).where(" id = ?", loc)
					.and("cms.site.owner = ?", airline).first();
				if(location==null){
					throw new ObjectNotFoundException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[] {ECMSConstants.LOCATION}, Locale.US),
							HttpStatus.NOT_FOUND));
				}
				locationSet.add(location);
			
			}
			
				shipmentProfile.setLocations(locationSet);
			
			
			shipmentProfile.as(Site.ObjectModification.class).setOwner(airline);
			shipmentProfile.as(Site.ObjectModification.class).setConsumers(consumers);		
			shipmentProfile.save();
			shipmentProfileDTO.setShipmentProfileId(shipmentProfile.getId().toString());
		}
		else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		
		
		return shipmentProfileDTO;		
	}
	
	
	public ShipmentProfileDTO updateShipmentProfile(String airlineId,String shipmentProfileId, ShipmentProfileDTO shipmentProfileDTO)
			throws BadRequestException, ObjectExistsException, NoSuchMessageException, ObjectNotFoundException
	{
		ShipmentProfile shipmentProfile=null;
		Site airline = Query.from(Site.class).where(" id = ?", airlineId).first();
		if (airline != null) {
			Set<Site> consumers = null;
			consumers = Sets.newHashSet();
			consumers.add(airline);
			shipmentProfile=Query.from(ShipmentProfile.class).where(" id = ?", shipmentProfileId)
					.and("cms.site.owner = ?", airline).first();
			
			if(shipmentProfile==null)
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] {ECMSConstants.SHIPMENTPROFILE  }, Locale.US), HttpStatus.NOT_FOUND));
			
			if(shipmentProfileDTO.getHardDrives()>=0){
				shipmentProfile.setHardDrives(shipmentProfileDTO.getHardDrives());
			}
			else
			{
				throw new BadRequestException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,
								new Object[] { ECMSConstants.SHIPMENT_HARD_DRIVE }, Locale.US), HttpStatus.BAD_REQUEST));
			}
							
			if(StringUtils.isNotBlank(shipmentProfileDTO.getName()))
			{
				shipmentProfile.setName(shipmentProfileDTO.getName());
			}
					
			else
			{
				throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.FIELD_REQUIRED,
					new Object[] {ECMSConstants.SHIPMENT_PROFILE_NAME}, Locale.US), HttpStatus.BAD_REQUEST));
			}
			
			if (StringUtils.isNotBlank(shipmentProfileDTO.getShippingType())) {
				if (shipmentProfileDTO.getShippingType()
						.equalsIgnoreCase(ShipmentProfile.ShippingType.DHL.toString())) {
					shipmentProfile.setShippingType(ShipmentProfile.ShippingType.DHL);
				}
				else if(shipmentProfileDTO.getShippingType()
						.equalsIgnoreCase(ShipmentProfile.ShippingType.FEDEX.toString())) {
					shipmentProfile.setShippingType(ShipmentProfile.ShippingType.FEDEX);
				}
				else if(shipmentProfileDTO.getShippingType()
						.equalsIgnoreCase(ShipmentProfile.ShippingType.UPS.toString())) {
					shipmentProfile.setShippingType(ShipmentProfile.ShippingType.UPS);
				}
				else if(shipmentProfileDTO.getShippingType()
						.equalsIgnoreCase(ShipmentProfile.ShippingType.USPS.toString())) {
					shipmentProfile.setShippingType(ShipmentProfile.ShippingType.USPS);
				}
				else
				{
					throw new BadRequestException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,new Object[] {ECMSConstants.SHIPMENT_TYPE},Locale.US),
							HttpStatus.BAD_REQUEST));
				}
			}

			Set<String>locations=shipmentProfileDTO.getLocationIds();
			if(locations==null||locations.isEmpty())
			{
				throw new BadRequestException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.INVALID_PARAMETER,new Object[] {},Locale.US),
						HttpStatus.BAD_REQUEST));
			}
			
			Set<Location>locationSet=new HashSet<>();
			
			for(String loc:locations)
			{
				Location location=Query.from(Location.class).where(" id = ?", loc)
					.and("cms.site.owner = ?", airline).first();
				if(location==null){
					throw new ObjectNotFoundException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[] {ECMSConstants.LOCATION}, Locale.US),
							HttpStatus.BAD_REQUEST));
				}
				locationSet.add(location);
			
			}
			
				shipmentProfile.setLocations(locationSet);
			
			shipmentProfile.as(Site.ObjectModification.class).setOwner(airline);
			shipmentProfile.as(Site.ObjectModification.class).setConsumers(consumers);		
			shipmentProfile.save();
			shipmentProfileDTO.setShipmentProfileId(shipmentProfile.getId().toString());
		}
		else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		
		
		return shipmentProfileDTO;		
	}
	
	public List<ShipmentProfileDTO> getShipmentProfileList(String airlineId) 
			throws NoSuchMessageException, ObjectNotFoundException {
		List<ShipmentProfileDTO> list = new ArrayList<>();
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline != null){
			List<ShipmentProfile> shipmentProfiles = Query.from(ShipmentProfile.class).where("cms.site.owner = ?",airline).selectAll();
			if(Objects.nonNull(shipmentProfiles)){
				for(ShipmentProfile shipmentProfile : shipmentProfiles){
					list.add(populateShipmentProfileDTO(shipmentProfile));
					
				}
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return list;
	}
	
	public Boolean deleteShipmentProfile(String airlineId,String shipmentProfileId) throws ObjectNotFoundException {
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline != null){
			ShipmentProfile shipmentProfile = Query.from(ShipmentProfile.class).where("id = ?",shipmentProfileId).and("cms.site.owner = ?",airline).first();
			if(Objects.nonNull(shipmentProfile)){
				shipmentProfile.delete();
				return Boolean.TRUE;
			}else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] {ECMSConstants.SHIPMENTPROFILE  }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}
	
	public ShipmentProfileDTO getShipmentProfileById(String airlineId,String shipmentProfileId) 
			throws NoSuchMessageException, ObjectNotFoundException {
		ShipmentProfileDTO shipmentProfileDTO = null;
		Site airline = Query.from(Site.class).where("id = ?",airlineId).first();
		if(airline != null){
			ShipmentProfile shipmentProfile = Query.from(ShipmentProfile.class).where("id = ?",shipmentProfileId).and("cms.site.owner = ?",airline).first();
			if(Objects.nonNull(shipmentProfile)){
				shipmentProfileDTO=populateShipmentProfileDTO(shipmentProfile);
			}else{
				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
						new Object[] { ECMSConstants.SHIPMENTPROFILE }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return shipmentProfileDTO;
	}
	
	private ShipmentProfileDTO populateShipmentProfileDTO(ShipmentProfile shipmentProfile){
		ShipmentProfileDTO shipmentProfileDTO = new ShipmentProfileDTO();
		Set<String> locationIds=new HashSet<>();
		if(shipmentProfile.getId()!=null)
		{
			shipmentProfileDTO.setShipmentProfileId(shipmentProfile.getId().toString());
		}
		shipmentProfileDTO.setHardDrives(shipmentProfile.getHardDrives());
		for(Location location:shipmentProfile.getLocations())
		{
			locationIds.add(location.getId().toString());
		}
		shipmentProfileDTO.setLocationIds(locationIds);
		shipmentProfileDTO.setName(shipmentProfile.getName());
		shipmentProfileDTO.setShippingType(shipmentProfile.getShippingType().toString());
				
		return shipmentProfileDTO;
	}
	
}
