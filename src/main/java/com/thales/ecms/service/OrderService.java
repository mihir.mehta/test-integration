package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.conversions.ContentTypeFactory;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Configuration;
import com.thales.ecms.model.Configuration.ConfigurationTitle;
import com.thales.ecms.model.LruType;
import com.thales.ecms.model.Order;
import com.thales.ecms.model.airline.AirlinePortal;
import com.thales.ecms.model.dto.ConfigurationDTO;
import com.thales.ecms.model.dto.ConfigurationDTO.TitleDTO;
import com.thales.ecms.model.dto.OrderRequestDTO;
import com.thales.ecms.model.dto.OrderRequestDTO.DisplayGroupRequestDTO;
import com.thales.ecms.model.dto.OrderResponseDTO;
import com.thales.ecms.model.dto.OrderResponseDTO.DisplayGroupDTO;
import com.thales.ecms.model.util.DisplayGroup;
import com.thales.ecms.utils.DateUtils;
import com.thales.ecms.utils.ECMSConstants;

@Service
public class OrderService {

	public Logger logger = LoggerFactory.getLogger(OrderService.class);

	@Autowired
	MessageSource messageSource;

	/**
	 * Service method for getting orderList
	 * 
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public List<OrderResponseDTO> getOrerList(String airlineId) throws ObjectNotFoundException {

		List<OrderResponseDTO> orderResponseDTOList = null;
		List<DisplayGroupDTO> displayGroupDTOList = null;
		OrderResponseDTO orderResponseDTO = null;
		Set<Configuration> configurationSet = null;
		Set<ConfigurationDTO> configureDTOSet = null;
		ConfigurationDTO configurationDTO = null;
		List<Order> orders = null;

		List<ConfigurationTitle> configurationTitleList = null;
		List<TitleDTO> titleDTOList = null;

		Site airline = Query.from(Site.class).where(" id = ? ", airlineId).first();
		if (Objects.nonNull(airline)) {
			orders = Query.from(Order.class).where("cms.site.owner = ?", airline).selectAll();
			if (Objects.nonNull(orders) && !orders.isEmpty()) {
				orderResponseDTOList = new ArrayList<OrderResponseDTO>();
				for (Order order : orders) {
					orderResponseDTO = new OrderResponseDTO();

					orderResponseDTO.setOrderId(order.getId().toString());
					if (Objects.nonNull(order.getName()) && StringUtils.isNotBlank(order.getName())) {
						orderResponseDTO.setOrderName(order.getName());
					}
					if (Objects.nonNull(order.getDescription()) && StringUtils.isNotBlank(order.getDescription())) {
						orderResponseDTO.setOrderDescription(order.getDescription());
					}
					if (Objects.nonNull(order.getOrderStatus())) {
						orderResponseDTO.setOrderStatus(order.getOrderStatus());
					}
					if (Objects.nonNull(order.getStartDate())
							&& StringUtils.isNotBlank(order.getStartDate().toString())) {
						orderResponseDTO.setOrderStartDate(order.getStartDate());
					}
					if (Objects.nonNull(order.getEndDate()) && StringUtils.isNotBlank(order.getEndDate().toString())) {
						orderResponseDTO.setOrderEndDate(order.getEndDate());
					}
					if (Objects.nonNull(order.getLastModifiedDate()) && StringUtils.isNotBlank(order.getLastModifiedDate().toString())) {
						orderResponseDTO.setLastModifiedDate(order.getLastModifiedDate());
					}
					if (Objects.nonNull(order.getShipmentDeadLine())
							&& StringUtils.isNotBlank(order.getShipmentDeadLine().toString())) {
						orderResponseDTO.setShipmentDeadline(order.getShipmentDeadLine());
					}
					if (Objects.nonNull(order.getContentDeadLine())
							&& StringUtils.isNotBlank(order.getContentDeadLine().toString())) {
						orderResponseDTO.setContentDeadline(order.getContentDeadLine());
					}
					if (Objects.nonNull(order.getAssetDeadLine())
							&& StringUtils.isNotBlank(order.getAssetDeadLine().toString())) {
						orderResponseDTO.setAssetDeadline(order.getAssetDeadLine());
					}

					displayGroupDTOList = getDisplayGroup(order.getDisplayGroups());
					orderResponseDTO.setDisplayGroupList(displayGroupDTOList);

					configurationSet = order.getConfigurationSet();
					configureDTOSet = new HashSet<>();

					if (Objects.nonNull(configurationSet) && !configurationSet.isEmpty()) {
						for (Configuration configuration : configurationSet) {
							configurationDTO = new ConfigurationDTO();
							if (Objects.nonNull(configuration)) {
								if (Objects.nonNull(configuration.getId())
										&& StringUtils.isNotBlank(configuration.getId().toString())) {
									configurationDTO.setConfigurationId(configuration.getId().toString());
								}
								if (Objects.nonNull(configuration.getName())
										&& StringUtils.isNotBlank(configuration.getName())) {
									configurationDTO.setConfigurationName(configuration.getName());
								}
								if (Objects.nonNull(configuration.getConfigurationStatus())
										&& Objects.nonNull(configuration.getConfigurationStatus().name())
										&& StringUtils.isNotBlank(configuration.getConfigurationStatus().name())) {
									configurationDTO
											.setConfigurationStatus(configuration.getConfigurationStatus().name());
								}
								if (Objects.nonNull(configuration.getPackagingType())
										&& Objects.nonNull(configuration.getPackagingType().name())
										&& StringUtils.isNotBlank(configuration.getPackagingType().name())) {
									configurationDTO.setPackagingType(configuration.getPackagingType().name());
								}

								configurationTitleList = configuration.getTitleList();

								if (Objects.nonNull(configurationTitleList) && !configurationTitleList.isEmpty()) {

									titleDTOList = setTitle(configurationTitleList);
									configurationDTO.setTitleList(titleDTOList);

								} else {
									logger.info("No Title exists for Configuration");
								}

								configureDTOSet.add(configurationDTO);
							}
						}
					} else {
						logger.info("No Configuration exists for Order");
					}

					orderResponseDTO.setConfigureDTOSet(configureDTOSet);

					orderResponseDTOList.add(orderResponseDTO);
				}

			} else {
				logger.error("order does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.ORDER }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}

		return orderResponseDTOList;

	}

	/**
	 * Service method for search order by id
	 * 
	 * @param orderId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public OrderResponseDTO getOrderById(String airlineId, String orderId) throws ObjectNotFoundException {

		OrderResponseDTO orderResponseDTO = null;
		Set<Configuration> configurationSet = null;
		Set<ConfigurationDTO> configurationDTOSet = null;
		ConfigurationDTO configurationDTO = null;
		List<DisplayGroupDTO> displayGroupDTOList = null;
		List<ConfigurationTitle> configurationTitleList = null;
		List<TitleDTO> titleDTOList = null;

		Site airline = Query.from(Site.class).where(" id = ?", airlineId).first();
		if (Objects.nonNull(airline)) {
			Order order = Query.from(Order.class).where("id = ?", orderId).and("cms.site.owner = ?", airline).first();
			if (order != null) {

				orderResponseDTO = new OrderResponseDTO();

				orderResponseDTO.setOrderId(order.getId().toString());
				if (Objects.nonNull(order.getName()) && StringUtils.isNotBlank(order.getName())) {
					orderResponseDTO.setOrderName(order.getName());
				}
				if (Objects.nonNull(order.getDescription()) && StringUtils.isNotBlank(order.getDescription())) {
					orderResponseDTO.setOrderDescription(order.getDescription());
				}
				if (Objects.nonNull(order.getOrderStatus())) {
					orderResponseDTO.setOrderStatus(order.getOrderStatus());
				}
				if (Objects.nonNull(order.getStartDate()) && StringUtils.isNotBlank(order.getStartDate().toString())) {
					orderResponseDTO.setOrderStartDate(order.getStartDate());
				}
				if (Objects.nonNull(order.getEndDate()) && StringUtils.isNotBlank(order.getEndDate().toString())) {
					orderResponseDTO.setOrderEndDate(order.getEndDate());
				}
				if (Objects.nonNull(order.getLastModifiedDate()) && StringUtils.isNotBlank(order.getLastModifiedDate().toString())) {
					orderResponseDTO.setLastModifiedDate(order.getLastModifiedDate());
				}
				if (Objects.nonNull(order.getShipmentDeadLine())
						&& StringUtils.isNotBlank(order.getShipmentDeadLine().toString())) {
					orderResponseDTO.setShipmentDeadline(order.getShipmentDeadLine());
				}
				if (Objects.nonNull(order.getContentDeadLine())
						&& StringUtils.isNotBlank(order.getContentDeadLine().toString())) {
					orderResponseDTO.setContentDeadline(order.getContentDeadLine());
				}
				if (Objects.nonNull(order.getAssetDeadLine())
						&& StringUtils.isNotBlank(order.getAssetDeadLine().toString())) {
					orderResponseDTO.setAssetDeadline(order.getAssetDeadLine());
				}

				displayGroupDTOList = getDisplayGroup(order.getDisplayGroups());
				orderResponseDTO.setDisplayGroupList(displayGroupDTOList);

				configurationSet = order.getConfigurationSet();
				configurationDTOSet = new HashSet<>();

				if (Objects.nonNull(configurationSet) && !configurationSet.isEmpty()) {
					for (Configuration configuration : configurationSet) {
						configurationDTO = new ConfigurationDTO();
						if (Objects.nonNull(configuration)) {

							if (Objects.nonNull(configuration.getId())
									&& StringUtils.isNotBlank(configuration.getId().toString())) {
								configurationDTO.setConfigurationId(configuration.getId().toString());
							}
							if (Objects.nonNull(configuration.getName())
									&& StringUtils.isNotBlank(configuration.getName())) {
								configurationDTO.setConfigurationName(configuration.getName());
							}
							if (Objects.nonNull(configuration.getConfigurationStatus())
									&& Objects.nonNull(configuration.getConfigurationStatus().name())
									&& StringUtils.isNotBlank(configuration.getConfigurationStatus().name())) {
								configurationDTO.setConfigurationStatus(configuration.getConfigurationStatus().name());
							}
							if (Objects.nonNull(configuration.getPackagingType())
									&& Objects.nonNull(configuration.getPackagingType().name())
									&& StringUtils.isNotBlank(configuration.getPackagingType().name())) {
								configurationDTO.setPackagingType(configuration.getPackagingType().name());
							}

							configurationTitleList = configuration.getTitleList();

							if (Objects.nonNull(configurationTitleList)) {
								titleDTOList = setTitle(configurationTitleList);
								configurationDTO.setTitleList(titleDTOList);
							}

							configurationDTOSet.add(configurationDTO);
						}
					}

				}

				orderResponseDTO.setConfigureDTOSet(configurationDTOSet);

			} else {
				logger.error("Order does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.ORDER }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return orderResponseDTO;
	}

	/**
	 * Service method for create new order
	 * 
	 * @param airlineId
	 * @param orderDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 * @throws ObjectExistsException
	 * @throws NoSuchMessageException
	 */
	public OrderResponseDTO createOrder(String airlineId, OrderRequestDTO orderDetails)
			throws ObjectNotFoundException, EmptyResponseException, NoSuchMessageException, ObjectExistsException {
		OrderResponseDTO orderDTO = null;
		Order order = null;

		Site airline = Query.from(Site.class).where(" id = ?", airlineId).first();

		if (airline != null) {
			order = new Order();
			if (!Objects.isNull(orderDetails.getOrderName()) && StringUtils.isNotBlank(orderDetails.getOrderName())) {
				Order orderName = Query.from(Order.class).where("name = ?", orderDetails.getOrderName())
						.and("cms.site.owner = ?", airline).first();
				if (Objects.isNull(orderName)) {
					order.setName(orderDetails.getOrderName());
				} else {
					logger.error("order name Already Present");
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.ORDER, ECMSConstants.NAME }, Locale.US),
							HttpStatus.FORBIDDEN));
				}
			} else {
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.ORDER_FIELD_REQUIRED,
								new Object[] { ECMSConstants.ORDER, ECMSConstants.NAME }, Locale.US),
						HttpStatus.FORBIDDEN));
			}
			if (!Objects.isNull(orderDetails.getOrderDescription())
					&& StringUtils.isNoneBlank(orderDetails.getOrderDescription())) {
				order.setDescription(orderDetails.getOrderDescription());
			}
			if (!Objects.isNull(orderDetails.getOrderStartDate())
					&& StringUtils.isNotBlank(orderDetails.getOrderStartDate().toString())) {
				order.setStartDate(new Date(orderDetails.getOrderStartDate()));
				order.setLastModifiedDate(new Date(orderDetails.getOrderStartDate()));
			} else {
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.ORDER_FIELD_REQUIRED,
								new Object[] { ECMSConstants.ORDER, ECMSConstants.DATE }, Locale.US),
						HttpStatus.FORBIDDEN));
			}
			if (!Objects.isNull(orderDetails.getOrderEndDate())
					&& StringUtils.isNotBlank(orderDetails.getOrderEndDate().toString())) {
				order.setEndDate(new Date(orderDetails.getOrderEndDate()));
			} else {
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.ORDER_FIELD_REQUIRED,
								new Object[] { ECMSConstants.ORDER, ECMSConstants.DATE }, Locale.US),
						HttpStatus.FORBIDDEN));
			}

			if (!Objects.isNull(orderDetails.getDisplayGroupList()) && !orderDetails.getDisplayGroupList().isEmpty()) {
				List<DisplayGroup> displayGroupsList = setDisplayGroup(orderDetails.getDisplayGroupList());
				order.setDisplayGroups(displayGroupsList);
			}
			if (!Objects.isNull(orderDetails.getConfigurationIdList())
					&& !orderDetails.getConfigurationIdList().isEmpty()) {
				Set<Configuration> configurationSet = new HashSet<>();
				for (String configurationId : orderDetails.getConfigurationIdList()) {

					Configuration configuration = Query.from(Configuration.class).where(" id = ?", configurationId)
							.and("cms.site.owner = ?", airline).first();

					if (Objects.nonNull(configuration)) {
						configurationSet.add(configuration);
						order.setConfigurationSet(configurationSet);
						order.setOrderStatus(Order.Status.COMPLETE);
					} else {
						order.setOrderStatus(Order.Status.INCOMPLETE);
					}
				}

			} else {
				order.setOrderStatus(Order.Status.INCOMPLETE);
			}

			Date shipmentStartDate = airline.as(AirlinePortal.class).getShipmentDeadlineStartDate();
			Date shipmentEndDate = airline.as(AirlinePortal.class).getShipmentDeadlineEndDate();
			if (Objects.nonNull(shipmentStartDate) && Objects.nonNull(shipmentEndDate)
					&& StringUtils.isNotBlank(shipmentStartDate.toString())
					&& StringUtils.isNotBlank(shipmentEndDate.toString())) {
				Date shipmentDeadLine = DateUtils.deadLineCalculator(shipmentStartDate, shipmentEndDate,
						orderDetails.getOrderStartDate());
				if (Objects.nonNull(shipmentDeadLine)) {
					order.setShipmentDeadLine(shipmentDeadLine);
				}
			} else {
				throw new ObjectExistsException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.OBJECT_NOT_CONTAINS_OBJECT,
								new Object[] { ECMSConstants.SHIPMENTDATE, ECMSConstants.AIRLINE }, Locale.US),
						HttpStatus.FORBIDDEN));
			}

			Date contentStartDate = airline.as(AirlinePortal.class).getContentDeadlineStartDate();
			Date contentEndDate = airline.as(AirlinePortal.class).getContentDeadlineEndDate();
			if (Objects.nonNull(contentStartDate) && Objects.nonNull(contentEndDate)
					&& StringUtils.isNotBlank(contentStartDate.toString())
					&& StringUtils.isNotBlank(contentEndDate.toString())) {
				Date contentdeadline = DateUtils.deadLineCalculator(contentStartDate, contentEndDate,
						orderDetails.getOrderStartDate());
				if (Objects.nonNull(contentdeadline)) {
					order.setContentDeadLine(contentdeadline);
				}
			}

			Date assetStartDate = airline.as(AirlinePortal.class).getAssetDeadlineStartDate();
			Date assetEndDate = airline.as(AirlinePortal.class).getAssetDeadlineEndDate();
			if (Objects.nonNull(assetStartDate) && Objects.nonNull(assetEndDate)
					&& StringUtils.isNotBlank(assetStartDate.toString())
					&& StringUtils.isNotBlank(assetEndDate.toString())) {
				Date assetdeadline = DateUtils.deadLineCalculator(assetStartDate, assetEndDate,
						orderDetails.getOrderStartDate());

				if (Objects.nonNull(assetdeadline)) {
					order.setAssetDeadLine(assetdeadline);
				}
			}

			if (!Objects.isNull(orderDetails.getOrderName()) && StringUtils.isNotBlank(orderDetails.getOrderName())
					&& !Objects.isNull(orderDetails.getOrderStartDate())
					&& StringUtils.isNotBlank(orderDetails.getOrderStartDate().toString())
					&& !Objects.isNull(orderDetails.getOrderEndDate())
					&& StringUtils.isNotBlank(orderDetails.getOrderEndDate().toString())) {
				order.as(Site.ObjectModification.class).setOwner(airline);
				order.save();
			}
			// calling the service method to get details of newly created
			// order.
			orderDTO = getOrderById(airlineId, order.getId().toString());
			return orderDTO;
		} else
			logger.error("Airline does not exist");
		throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
				new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
	}

	/**
	 * Service method to update order
	 * 
	 * @param airlineId
	 * @param orderDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws EmptyResponseException
	 * @throws ObjectExistsException
	 * @throws NoSuchMessageException
	 */
	public OrderResponseDTO updateOrder(String airlineId, String orderId, OrderRequestDTO orderDetails)
			throws ObjectNotFoundException, EmptyResponseException, NoSuchMessageException, ObjectExistsException {
		Order order = null;
		OrderResponseDTO orderResponseDTO = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (Objects.nonNull(airline)) {
			order = Query.from(Order.class).where("id = ?", orderId).and("cms.site.owner = ?", airline).first();

			if (Objects.nonNull(order)) {
				if (Objects.nonNull(orderDetails.getOrderName())
						&& StringUtils.isNotBlank(orderDetails.getOrderName())) {
					Order orderName = Query.from(Order.class).where("name = ?", orderDetails.getOrderName())
							.and("id != ?", orderId).and("cms.site.owner = ?", airline).first();
					if (Objects.isNull(orderName)) {
						order.setName(orderDetails.getOrderName());
					} else {
						throw new EmptyResponseException(
								new ErrorResponse("Order name already exist ...", HttpStatus.OK));
					}
				}
				if (Objects.nonNull(orderDetails.getOrderDescription())
						&& StringUtils.isNotBlank(orderDetails.getOrderDescription())) {
					order.setDescription(orderDetails.getOrderDescription());
				}
				if (Objects.nonNull(orderDetails.getOrderStartDate())
						&& StringUtils.isNotBlank(orderDetails.getOrderStartDate().toString())) {
					order.setStartDate(new Date(orderDetails.getOrderStartDate()));
				}
				if (Objects.nonNull(orderDetails.getOrderEndDate())
						&& StringUtils.isNotBlank(orderDetails.getOrderEndDate().toString())) {
					order.setEndDate(new Date(orderDetails.getOrderEndDate()));
				}
				if (Objects.nonNull(orderDetails.getLastModifiedDate())
						&& StringUtils.isNotBlank(orderDetails.getLastModifiedDate().toString())) {
					order.setLastModifiedDate(orderDetails.getLastModifiedDate());
				}
				if (Objects.nonNull(orderDetails.getDisplayGroupList())
						&& !orderDetails.getDisplayGroupList().isEmpty()) {
					List<DisplayGroup> displayGroupList = setDisplayGroup(orderDetails.getDisplayGroupList());
					order.setDisplayGroups(displayGroupList);
				}

				if (!Objects.isNull(orderDetails.getConfigurationIdList())
						&& !orderDetails.getConfigurationIdList().isEmpty()) {
					Set<Configuration> configurationSet = new HashSet<>();
					for (String configurationId : orderDetails.getConfigurationIdList()) {

						if (!Objects.isNull(configurationId) && !Objects.isNull(configurationId)
								&& StringUtils.isNotBlank(configurationId)) {

							Configuration configuration = Query.from(Configuration.class)
									.where(" id = ?", configurationId).and("cms.site.owner = ?", airline).first();
							if (Objects.nonNull(configuration)) {
								configurationSet.add(configuration);
								order.setConfigurationSet(configurationSet);
								order.setOrderStatus(Order.Status.COMPLETE);
							} else {
								order.setOrderStatus(Order.Status.INCOMPLETE);
							}
						}
					}
				} else {
					order.setConfigurationSet(null);
					order.setOrderStatus(Order.Status.INCOMPLETE);
				}
				Date shipmentStartDate = airline.as(AirlinePortal.class).getShipmentDeadlineStartDate();
				Date shipmentEndDate = airline.as(AirlinePortal.class).getShipmentDeadlineEndDate();
				if (Objects.nonNull(shipmentStartDate) && Objects.nonNull(shipmentEndDate)
						&& Objects.nonNull(orderDetails.getOrderStartDate())
						&& StringUtils.isNotBlank(shipmentStartDate.toString())
						&& StringUtils.isNotBlank(shipmentEndDate.toString())
						&& StringUtils.isNotBlank(orderDetails.getOrderStartDate().toString())) {
					Date shipmentDeadLine = DateUtils.deadLineCalculator(shipmentStartDate, shipmentEndDate,
							orderDetails.getOrderStartDate());
					if (Objects.nonNull(shipmentDeadLine)) {
						order.setShipmentDeadLine(shipmentDeadLine);
					}
				}

				Date contentStartDate = airline.as(AirlinePortal.class).getContentDeadlineStartDate();
				Date contentEndDate = airline.as(AirlinePortal.class).getContentDeadlineEndDate();
				if (Objects.nonNull(contentStartDate) && Objects.nonNull(contentEndDate)
						&& Objects.nonNull(orderDetails.getOrderStartDate())
						&& StringUtils.isNotBlank(contentStartDate.toString())
						&& StringUtils.isNotBlank(contentEndDate.toString())
						&& StringUtils.isNotBlank(orderDetails.getOrderStartDate().toString())) {
					Date contentdeadline = DateUtils.deadLineCalculator(contentStartDate, contentEndDate,
							orderDetails.getOrderStartDate());
					if (Objects.nonNull(contentdeadline)) {
						order.setContentDeadLine(contentdeadline);
					}
				}

				Date assetStartDate = airline.as(AirlinePortal.class).getAssetDeadlineStartDate();
				Date assetEndDate = airline.as(AirlinePortal.class).getAssetDeadlineEndDate();
				if (Objects.nonNull(assetStartDate) && Objects.nonNull(assetEndDate)
						&& Objects.nonNull(orderDetails.getOrderStartDate())
						&& StringUtils.isNotBlank(assetStartDate.toString())
						&& StringUtils.isNotBlank(assetEndDate.toString())
						&& StringUtils.isNotBlank(orderDetails.getOrderStartDate().toString())) {
					Date assetdeadline = DateUtils.deadLineCalculator(assetStartDate, assetEndDate,
							orderDetails.getOrderStartDate());

					if (Objects.nonNull(assetdeadline)) {
						order.setAssetDeadLine(assetdeadline);
					}
				}
				order.as(Site.ObjectModification.class).setOwner(airline);
				order.save();
				// calling the service method to get details of newly created
				// order.
				orderResponseDTO = getOrderById(airlineId, order.getId().toString());
			} else {
				logger.error("order does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.ORDER }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return orderResponseDTO;
	}

	/**
	 * This method takes search parameter like order name, description etc and
	 * make dynamic String based on parameter and execute the query and return
	 * result.
	 * 
	 * @param orderRequestDTO
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public List<OrderResponseDTO> searchOrder(String airlineId, OrderRequestDTO orderRequestDTO)
			throws ObjectNotFoundException {

		StringBuilder queryPredicate = null;
		List<OrderResponseDTO> orderDTOList = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (Objects.nonNull(airline)) {
			int counter = 0;
			if (!Objects.isNull(orderRequestDTO)) {

				queryPredicate = new StringBuilder();

				if (!Objects.isNull(orderRequestDTO.getOrderName())
						&& StringUtils.isNotBlank(orderRequestDTO.getOrderName())) {

					queryPredicate.append("name contains ");
					// queryPredicate.append("'");
					queryPredicate.append(orderRequestDTO.getOrderName());
					// queryPredicate.append("'");
					counter = counter + 1;
				}

				if (!Objects.isNull(orderRequestDTO.getOrderDescription())
						&& StringUtils.isNotBlank(orderRequestDTO.getOrderDescription())) {

					if (counter > 0) {
						queryPredicate.append(" ");
						queryPredicate.append("and");
						queryPredicate.append(" ");
						queryPredicate.append("description contains ");
						// queryPredicate.append("'");
						queryPredicate.append(orderRequestDTO.getOrderDescription());
						// queryPredicate.append("'");

					} else {
						queryPredicate.append("description contains ");
						// queryPredicate.append("'");
						queryPredicate.append(orderRequestDTO.getOrderDescription());
						// queryPredicate.append("'");
					}
					counter = counter + 1;
				}

				if (!Objects.isNull(orderRequestDTO.getOrderStartDate())
						&& StringUtils.isNotBlank(orderRequestDTO.getOrderStartDate().toString())) {
					if (counter > 0) {
						queryPredicate.append(" ");
						queryPredicate.append("and");
						queryPredicate.append(" ");
						queryPredicate.append("startDate >= ");
						queryPredicate.append("'");
						queryPredicate.append(orderRequestDTO.getOrderStartDate().toString());
						queryPredicate.append("'");

					} else {
						queryPredicate.append("startDate >= ");
						queryPredicate.append("'");
						queryPredicate.append(orderRequestDTO.getOrderStartDate().toString());
						queryPredicate.append("'");
					}
					counter = counter + 1;
				}

				if (!Objects.isNull(orderRequestDTO.getOrderEndDate())
						&& StringUtils.isNotBlank(orderRequestDTO.getOrderEndDate().toString())) {

					if (counter > 0) {
						queryPredicate.append(" ");
						queryPredicate.append("and");
						queryPredicate.append(" ");
						queryPredicate.append("endDate >= ");
						queryPredicate.append("'");
						queryPredicate.append(orderRequestDTO.getOrderEndDate().toString());
						queryPredicate.append("'");
						counter = counter + 1;
					} else {
						queryPredicate.append("endDate >= ");
						queryPredicate.append("'");
						queryPredicate.append(orderRequestDTO.getOrderEndDate().toString());
						queryPredicate.append("'");
					}
					counter = counter + 1;
				}
				
				if (!Objects.isNull(orderRequestDTO.getLastModifiedDate())
						&& StringUtils.isNotBlank(orderRequestDTO.getLastModifiedDate().toString())) {
					Long lastModified = orderRequestDTO.getLastModifiedDate().getTime();
					if (counter > 0) {
						queryPredicate.append(" ");
						queryPredicate.append("and");
						queryPredicate.append(" ");
						queryPredicate.append("lastModifiedDate >= ");
						queryPredicate.append("'");
						queryPredicate.append(lastModified);
						queryPredicate.append("'");

					} else {
						queryPredicate.append("lastModifiedDate >= ");
						queryPredicate.append("'");
						queryPredicate.append(lastModified);
						queryPredicate.append("'");
					}
					counter = counter + 1;
				}
				if (!Objects.isNull(orderRequestDTO.getOrderStatus())
						&& StringUtils.isNotBlank(orderRequestDTO.getOrderStatus())) {

					if (counter > 0) {
						queryPredicate.append(" ");
						queryPredicate.append("and").append(" ");
						queryPredicate.append("orderStatus = ");
						queryPredicate.append("'");
						queryPredicate.append(orderRequestDTO.getOrderStatus().toUpperCase());
						queryPredicate.append("'");

					} else {
						queryPredicate.append("orderStatus = ");
						queryPredicate.append("'");
						queryPredicate.append(orderRequestDTO.getOrderStatus().toUpperCase());
						queryPredicate.append("'");
					}

				}

				List<Order> orderList = Query.from(Order.class).where(queryPredicate.toString())
						.and("cms.site.owner = ?", airline).selectAll();

				if (!Objects.isNull(orderList) && !orderList.isEmpty()) {
					OrderResponseDTO orderDTO = null;
					orderDTOList = new ArrayList<>();
					for (Order order : orderList) {
						orderDTO = getOrderById(airlineId, order.getId().toString());
						orderDTOList.add(orderDTO);
					}
				} else {
					logger.info("we didn't find Order for given search criteria");
					throw new ObjectNotFoundException(
							new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
									new Object[] { ECMSConstants.ORDER }, Locale.US), HttpStatus.NOT_FOUND));

				}

			}
		} else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}

		return orderDTOList;

	}

	/**
	 * Service method to set list of display group
	 * 
	 * @param displayGroups
	 * @return
	 */
	private List<DisplayGroup> setDisplayGroup(List<DisplayGroupRequestDTO> displayGroups) {
		List<DisplayGroup> displayGroupsList = null;

		if (Objects.nonNull(displayGroups) && !displayGroups.isEmpty()) {
			displayGroupsList = new ArrayList<>();
			for (DisplayGroupRequestDTO displayGroupDTO : displayGroups) {
				DisplayGroup displayGroup = null;
				displayGroup = new DisplayGroup();
				if (Objects.nonNull(displayGroupDTO.getMonth()) && StringUtils.isNotBlank(displayGroupDTO.getMonth())) {
					displayGroup.setMonth(displayGroupDTO.getMonth());
				}
				if (Objects.nonNull(displayGroupDTO.getStartDate())
						&& StringUtils.isNotBlank(displayGroupDTO.getStartDate().toString())) {
					displayGroup.setStartDate(displayGroupDTO.getStartDate());
				}
				if (Objects.nonNull(displayGroupDTO.getEndDate())
						&& StringUtils.isNotBlank(displayGroupDTO.getEndDate().toString())) {
					displayGroup.setEndDate(displayGroupDTO.getEndDate());
				}
				displayGroupsList.add(displayGroup);
			}
		} else {
			logger.info("No display groups are availble for this order");
		}
		return displayGroupsList;
	}

	/**
	 * Service method to get list of display group
	 * 
	 * @param displayGroups
	 * @return
	 */
	private List<DisplayGroupDTO> getDisplayGroup(List<DisplayGroup> displayGroups) {
		List<DisplayGroupDTO> displayGroupDTOList = null;
		DisplayGroupDTO displayGroupDTO = null;

		if (Objects.nonNull(displayGroups) && !displayGroups.isEmpty()) {
			displayGroupDTOList = new ArrayList<>();
			for (DisplayGroup displayGroup : displayGroups) {
				displayGroupDTO = new DisplayGroupDTO();
				displayGroupDTO.setMonth(displayGroup.getMonth());
				displayGroupDTO.setStartDate(displayGroup.getStartDate());
				displayGroupDTO.setEndDate(displayGroup.getEndDate());
				displayGroupDTOList.add(displayGroupDTO);
			}
		} else {
			logger.info("No display groups are availble for this order");
		}
		return displayGroupDTOList;
	}

	/**
	 * Service method to set list of title
	 * 
	 * @param configurationTitleList
	 * @return
	 */
	private List<TitleDTO> setTitle(List<ConfigurationTitle> configurationTitleList) {

		TitleDTO titleDTO = null;
		List<TitleDTO> titleDTOList = new ArrayList<>();
		String configurationName = null;
		String configurationId = null;
		if (configurationTitleList != null && !configurationTitleList.isEmpty()) {
			for (ConfigurationTitle configurationTitle : configurationTitleList) {

				titleDTO = new TitleDTO();
				if (Objects.nonNull(configurationTitle.getId())
						&& StringUtils.isNotEmpty(configurationTitle.getId().toString())) {
					titleDTO.setContentInternalId(configurationTitle.getId().toString());
				}
				if (Objects.nonNull(configurationTitle.getContent())
						&& Objects.nonNull(configurationTitle.getContent().getLabel())
						&& StringUtils.isNotEmpty(configurationTitle.getContent().getLabel())) {
					titleDTO.setContentName(configurationTitle.getContent().getLabel());
				}
				if (Objects.nonNull(configurationTitle.getContent())) {
					titleDTO.setContentType(ContentTypeFactory
							.convertClassNameWithUnderScore(configurationTitle.getContent().getClass().toString()));
				}
				if (Objects.nonNull(configurationTitle.getContentStatus())
						&& Objects.nonNull(configurationTitle.getContentStatus().name())
						&& StringUtils.isNotEmpty(configurationTitle.getContentStatus().name())) {
					titleDTO.setContentStatus(configurationTitle.getContentStatus().name());
				}
				if (Objects.nonNull(configurationTitle.getStartDate())) {
					titleDTO.setContentStartDate(configurationTitle.getStartDate());
				}
				if (Objects.nonNull(configurationTitle.getEndDate())) {
					titleDTO.setContentendDate(configurationTitle.getEndDate());
				}
				if (Objects.nonNull(configurationTitle.getPriority())) {
					titleDTO.setPriority(configurationTitle.getPriority());
				}
				List<LruType> destinations = configurationTitle.getDestinations();
				if (Objects.nonNull(destinations) && !destinations.isEmpty()) {
					List<String> lruTypeList = new ArrayList<>();
					for (LruType lruType : destinations) {
						lruTypeList.add(lruType.getName());
					}
					titleDTO.setDestinationList(lruTypeList);
				}

				titleDTOList.add(titleDTO);
			}

		} else {
			logger.info("configurationId: " + configurationId + " configurationName: " + configurationName
					+ "has not have title(s)");
		}

		return titleDTOList;
	}

	/**
	 * Service method for List of configuration under airline
	 * 
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws NoSuchMessageException
	 */
	public List<ConfigurationDTO> getCongifurationByAirline(String airlinId)
			throws NoSuchMessageException, ObjectNotFoundException {
		ConfigurationDTO configurationDTO = null;
		List<ConfigurationDTO> configurationDTOList = null;

		Site airline = Query.from(Site.class).where("id = ?", airlinId).first();
		if (Objects.nonNull(airline)) {
			List<Configuration> configurationList = Query.from(Configuration.class).where("cms.site.owner = ?", airline)
					.selectAll();
			if (Objects.nonNull(configurationList) && !configurationList.isEmpty()) {
				configurationDTOList = new ArrayList<>();
				for (Configuration configuration : configurationList) {
					configurationDTO = new ConfigurationDTO();
					if (Objects.nonNull(configuration.getName()) && StringUtils.isNotBlank(configuration.getName())) {
						configurationDTO.setConfigurationName(configuration.getName());
					}
					if (Objects.nonNull(configuration.getId())
							&& StringUtils.isNotBlank(configuration.getId().toString())) {
						configurationDTO.setConfigurationId(configuration.getId().toString());
					}
					if (Objects.nonNull(configuration.getConfigurationStatus())
							&& StringUtils.isNotBlank(configuration.getConfigurationStatus().toString())) {
						configurationDTO.setConfigurationStatus(configuration.getConfigurationStatus().toString());
					}
					if (Objects.nonNull(configuration.getPackagingType())
							&& StringUtils.isNotBlank(configuration.getPackagingType().toString())) {
						configurationDTO.setPackagingType(configuration.getPackagingType().toString());
					}
					if (Objects.nonNull(configuration.getTitleList()) && !configuration.getTitleList().isEmpty()) {
						List<TitleDTO> titleDTOList = setTitle(configuration.getTitleList());
						configurationDTO.setTitleList(titleDTOList);
					}
					configurationDTOList.add(configurationDTO);
				}
			}
		} else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return configurationDTOList;
	}

	/**
	 * Service method for delete an order
	 * 
	 * @param airlineId
	 * @param orderId
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws NoSuchMessageException
	 */
	public String deleteOrder(String airlineId, String orderId) throws NoSuchMessageException, ObjectNotFoundException {
		Order order = null;
		String response;
		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (Objects.nonNull(airline)) {
			order = Query.from(Order.class).where("id = ?", orderId).and("cms.site.owner = ?", airline).first();
			if (Objects.nonNull(order)) {
				order.delete();
				response = "order deleted succesfully";
			} else {
				logger.error("order does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.ORDER }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return response;
	}

}
