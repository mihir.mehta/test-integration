package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Lru;
import com.thales.ecms.model.LruType;
import com.thales.ecms.model.dto.LruDTO;
import com.thales.ecms.model.dto.LruTypeDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * @author Mihir Service Class for CRUD operations on LRUs
 */
@Service
public class LruService {

	@Autowired
	private MessageSource messageSource;

	public static Logger logger = LoggerFactory.getLogger(LruService.class);

	/**
	 * Service Method to add an LRU
	 * 
	 * @param airlineId
	 * @param lruDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	public LruDTO addLRU(String airlineId, LruDTO lruDetails)
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException {

		Lru lru = null;
		LruDTO lruDTO = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {

			String lruName = lruDetails.getLruName();

			if (lruName != null && StringUtils.isNotBlank(lruName)) {
				if (Query.from(Lru.class).where("lruName = ?", lruName).and("cms.site.owner = ?", airline)
						.first() == null) {
					lru = new Lru();
					lru.setLruName(lruName);
				} else {
					logger.error("LRU with the name " + lruName + " already exists");
					throw new ObjectExistsException(new ErrorResponse(
							messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
									new Object[] { ECMSConstants.LRU, ECMSConstants.NAME }, Locale.US),
							HttpStatus.FORBIDDEN));
				}
			} else {
				logger.error("Bad request - Empty Parameter: LRU Name");
				throw new BadRequestException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}

			String lruTypeId = lruDetails.getLruTypeInternalId();
			if (lruTypeId != null && StringUtils.isNotBlank(lruTypeId)) {
				LruType lruType = Query.from(LruType.class).where("id = ?", lruTypeId).first();
				if (lruType != null) {
					lru.setLruType(lruType);
				} else {
					logger.error("LRU Type" + lruTypeId + " does not exist");
					throw new ObjectNotFoundException(
							new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
									new Object[] { ECMSConstants.LRU_TYPE }, Locale.US), HttpStatus.NOT_FOUND));
				}
			} else {
				logger.error("Bad request - Empty Parameter: LRU Type Id");
				throw new BadRequestException(new ErrorResponse(
						messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}

			lru.as(Site.ObjectModification.class).setOwner(airline);
			lru.save();
			logger.debug("LRU insatnce saved successfully");

			lruDTO = new LruDTO();
			lruDTO.setLruInternalId(lru.getId().toString());
			lruDTO.setLruName(lru.getLruName());
			lruDTO.setLruTypeInternalId(lru.getLruType().getId().toString());
			lruDTO.setLruTypeName(lru.getLruType().getName());
		} else {
			logger.error("Airline " + airlineId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return lruDTO;
	}

	/**
	 * Service Method to return a list of all LRUs
	 * 
	 * @param airlineId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public List<LruDTO> getLRUs(String airlineId) throws ObjectNotFoundException {

		List<LruDTO> lruDTOList = null;
		LruDTO lruDTO = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {
			List<Lru> lruList = Query.from(Lru.class).where("cms.site.owner = ?", airline).selectAll();
			if (lruList != null && !lruList.isEmpty()) {
				lruDTOList = new ArrayList<>();
				for (Lru lru : lruList) {
					lruDTO = new LruDTO();
					lruDTO.setLruInternalId(lru.getId().toString());
					lruDTO.setLruName(lru.getLruName());
					lruDTO.setLruTypeInternalId(lru.getLruType().getId().toString());
					lruDTO.setLruTypeName(lru.getLruType().getName());
					lruDTOList.add(lruDTO);
				}
			} else {
				logger.error("No LRUs present");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline " + airlineId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return lruDTOList;
	}

	/**
	 * Service Method to return an LRU
	 * 
	 * @param airlineId
	 * @param lruId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public LruDTO getLRU(String airlineId, String lruId) throws ObjectNotFoundException {

		LruDTO lruDTO = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {
			Lru lru = Query.from(Lru.class).where("id = ?", lruId).and("cms.site.owner = ?", airlineId).first();
			if (lru != null) {
				lruDTO = new LruDTO();
				lruDTO.setLruInternalId(lru.getId().toString());
				lruDTO.setLruName(lru.getLruName());
				lruDTO.setLruTypeInternalId(lru.getLruType().getId().toString());
				lruDTO.setLruTypeName(lru.getLruType().getName());
			} else {
				logger.error("LRU " + lruId + " does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline " + airlineId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return lruDTO;
	}



	/**
	 * Service Method to return an LRU by LRU ID
	 *
	 * @param lruTypeId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public LruDTO getLRUByLRUTypeId(String lruTypeId) throws ObjectNotFoundException {
		LruDTO lruDTO = null;

		if (StringUtils.isNotBlank(lruTypeId)) {
			LruType lruType = Query.from(LruType.class).where("id = ?", lruTypeId.trim()).first();
			if (lruType != null) {
				Lru lru = Query.from(Lru.class).where("lruType = ?", lruType).first();
				if (lru != null) {
					lruDTO = new LruDTO();
					lruDTO.setLruInternalId(lru.getId().toString());
					lruDTO.setLruName(lru.getLruName());
					lruDTO.setLruTypeInternalId(lru.getLruType().getId().toString());
					lruDTO.setLruTypeName(lru.getLruType().getName());
				}
			} else {
				logger.error("LRU Type ID " + lruTypeId + " does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.NOT_FOUND));
			}
		}
		return lruDTO;
	}



	/**
	 * Service Method to update an LRU
	 * 
	 * @param airlineId
	 * @param lruId
	 * @param lruDetails
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	public LruDTO updateLRU(String airlineId, String lruId, LruDTO lruDetails)
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException {

		LruDTO lruDTO = null;

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {
			Lru lru = Query.from(Lru.class).where("id = ?", lruId).and("cms.site.owner = ?", airlineId).first();
			if (lru != null) {
				String lruName = lruDetails.getLruName();

				if (lruName != null && StringUtils.isNotBlank(lruName)) {
					if (Query.from(Lru.class).where("lruName = ?", lruName).and("cms.site.owner = ?", airline)
							.first() == null) {
						lru.setLruName(lruName);
					} else {
						logger.error("Cannot update LRU - LRU with the name " + lruName + " already exists");
						throw new ObjectExistsException(new ErrorResponse(
								messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,
										new Object[] { ECMSConstants.LRU, ECMSConstants.NAME }, Locale.US),
								HttpStatus.FORBIDDEN));
					}
				}

				String lruTypeId = lruDetails.getLruTypeInternalId();
				if (lruTypeId != null && StringUtils.isNotBlank(lruTypeId)) {
					LruType lruType = Query.from(LruType.class).where("id = ?", lruTypeId).first();
					if (lruType != null) {
						lru.setLruType(lruType);
					} else {
						logger.error("LRU Type " + lruTypeId + " does not exist");
						throw new ObjectNotFoundException(
								new ErrorResponse(
										messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
												new Object[] { ECMSConstants.LRU_TYPE }, Locale.US),
										HttpStatus.NOT_FOUND));
					}
				}

				lru.save();
				logger.debug("LRU insatnce saved successfully");

				lruDTO = new LruDTO();
				lruDTO.setLruInternalId(lru.getId().toString());
				lruDTO.setLruName(lru.getLruName());
				lruDTO.setLruTypeInternalId(lru.getLruType().getId().toString());
				lruDTO.setLruTypeName(lru.getLruType().getName());
			} else {
				logger.error("LRU " + lruId + " does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline " + airlineId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
		return lruDTO;
	}

	/**
	 * Service Method to delete an LRU
	 * 
	 * @param airlineId
	 * @param lruId
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public Boolean deleteLRU(String airlineId, String lruId) throws ObjectNotFoundException {

		Site airline = Query.from(Site.class).where("id = ?", airlineId).first();
		if (airline != null) {
			Lru lru = Query.from(Lru.class).where("id = ?", lruId).and("cms.site.owner = ?", airline).first();
			if (lru != null) {
				lru.delete();
				return true;
			} else {
				logger.error("LRU " + lruId + " does not exist");
				throw new ObjectNotFoundException(
						new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
								new Object[] { ECMSConstants.LRU }, Locale.US), HttpStatus.NOT_FOUND));
			}
		} else {
			logger.error("Airline " + airlineId + " does not exist");
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND,
					new Object[] { ECMSConstants.AIRLINE }, Locale.US), HttpStatus.NOT_FOUND));
		}
	}
}