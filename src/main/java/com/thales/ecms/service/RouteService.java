package com.thales.ecms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ErrorResponse;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.Airport;
import com.thales.ecms.model.Route;
import com.thales.ecms.model.dto.AirportDTO;
import com.thales.ecms.model.dto.RouteDTO;
import com.thales.ecms.utils.ECMSConstants;

/**
 * Service Class to perform CRUD operations on Routes.
 * 
 * @author shruti_n
 */
@Service
public class RouteService {
	
	
	public static Logger logger = LoggerFactory.getLogger(RouteService.class);
	@Autowired
	private MessageSource messageSource;

	/**
	 * Service Method to add new Route
	 * 
	 * @param routeDetails
	 * @throws ObjectNotFoundException,
	 *             BadRequestException, ObjectExistsException
	 * @return routeDTO
	 */

	public RouteDTO addRoute(RouteDTO routeDetails)
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException {
		RouteDTO routeDTO = null;
		Route route = new Route();
		Airport airportOrigin = null;
		Airport airportDestination = null;

		if (StringUtils.isNotBlank(routeDetails.getName())) {
			Route dupRoute = Query.from(Route.class).where("name = ?", routeDetails.getName()).first();
			if (null == dupRoute) {
				route.setName(routeDetails.getName().trim());
			} else {
				// Route with this routeName is already present- throws an
				// Exception
				logger.error("Route with this routeName is already present.");
				throw new BadRequestException(new ErrorResponse(
						"Bad Request -Route with this routeName is already present", HttpStatus.BAD_REQUEST));
			}
		}

		if (routeDetails.getDestination() != null && routeDetails.getOrigin() != null
				&& !routeDetails.getDestination().getAirportId().equals(routeDetails.getOrigin().getAirportId())) {
			airportOrigin = Query.from(Airport.class).where("id = ?", routeDetails.getOrigin().getAirportId()).first();

			airportDestination = Query.from(Airport.class).where("id = ?", routeDetails.getDestination().getAirportId())
					.first();
		} else {
			if (null == routeDetails.getDestination()) {
				// This destination Airport not present- throws an Exception
				logger.error("Destination airport not present");
				throw new BadRequestException(
						new ErrorResponse("Bad Request -destination airport not present", HttpStatus.BAD_REQUEST));
			} else if (null == routeDetails.getOrigin()) {
				// This origin Airport not present- throws an Exception
				logger.error("Origin airport not present");
				throw new BadRequestException(
						new ErrorResponse("Bad Request - origin airport not present", HttpStatus.FORBIDDEN));
			} else if (routeDetails.getDestination().getAirportId().equals(routeDetails.getOrigin().getAirportId())) {
				// Origin and destination can not be same- throws an Exception
				logger.error("Origin and destination can not be same");
				throw new BadRequestException(
						new ErrorResponse("Wrong input- origin and destination can not be same", HttpStatus.FORBIDDEN));
			}
		}

		Route routeQuery = Query.from(Route.class).where("origin = ? ", airportOrigin)
				.and("destination = ?", airportDestination).first();

		if (null != routeQuery) {
			// This Route already exists - throw an Exception
			logger.error("Route is already present");
			throw new ObjectExistsException(new ErrorResponse("Route is already present", HttpStatus.FORBIDDEN));
		}

		route.setOrigin(airportOrigin);
		route.setDestination(airportDestination);
		route.as(Site.ObjectModification.class).setGlobal(true);

		logger.debug("Route instance saved successfully! " + route.toString());
		route.save();
		routeDTO = new RouteDTO();
		routeDTO.setRouteId(route.getId().toString());
		routeDTO.setName(route.getName().toString());

		AirportDTO airportDTOOrig = new AirportDTO();
		Airport origin = route.getOrigin();
		airportDTOOrig.setAirportId(origin.getId().toString());
		airportDTOOrig.setAirportIATA(origin.getAirportIATA());
		airportDTOOrig.setAirportICAO(origin.getAirportICAO());
		airportDTOOrig.setAirportName(origin.getAirportName());

		Airport airportDest = route.getDestination();
		AirportDTO destinationDTO = new AirportDTO();
		destinationDTO.setAirportId(airportDest.getId().toString());
		destinationDTO.setAirportIATA(airportDest.getAirportIATA());
		destinationDTO.setAirportICAO(airportDest.getAirportICAO());
		destinationDTO.setAirportName(airportDest.getAirportName());

		routeDTO.setOrigin(airportDTOOrig);
		routeDTO.setDestination(destinationDTO);

		return routeDTO;
	}

	/**
	 * Service Method to return a list of Routes
	 * 
	 * @return routeDTOList
	 */

	public List<RouteDTO> getRoutes() throws ObjectNotFoundException {
		List<RouteDTO> routeDTOList = null;
		List<Route> routeList = Query.from(Route.class).selectAll();
		RouteDTO routeDTO = null;
		if (null != routeList && !routeList.isEmpty()) {
			routeDTOList = new ArrayList<>();
			for (Route route : routeList) {
				if (!Objects.isNull(route.getOrigin()) && !Objects.isNull(route.getDestination())) {
					routeDTO = new RouteDTO();

					routeDTO.setRouteId(route.getId().toString());
					routeDTO.setName(route.getName().toString());

					AirportDTO airportDTOOrig = new AirportDTO();
					Airport origin = route.getOrigin();
					airportDTOOrig.setAirportId(origin.getId().toString());
					airportDTOOrig.setAirportIATA(origin.getAirportIATA());
					airportDTOOrig.setAirportICAO(origin.getAirportICAO());
					airportDTOOrig.setAirportName(origin.getAirportName());

					Airport airportDest = route.getDestination();
					AirportDTO destinationDTO = new AirportDTO();
					destinationDTO.setAirportId(airportDest.getId().toString());
					destinationDTO.setAirportIATA(airportDest.getAirportIATA());
					destinationDTO.setAirportICAO(airportDest.getAirportICAO());
					destinationDTO.setAirportName(airportDest.getAirportName());

					routeDTO.setOrigin(airportDTOOrig);
					routeDTO.setDestination(destinationDTO);
					routeDTOList.add(routeDTO);
				}
			}
		} else {

			// This object is null- throws an Exception
			logger.error("Object is null");
			throw new ObjectNotFoundException(new ErrorResponse("Object is null", HttpStatus.NOT_FOUND));

		}
		return routeDTOList;
	}

	/**
	 * Service Method to delete a route
	 * 
	 * @param routeId
	 * @throws ObjectNotFoundException
	 */

	public String deleteRoute(String routeId) throws ObjectNotFoundException {
		Route route = Query.from(Route.class).where("id = ?", routeId).first();
		if (null != route) {
			route.delete();
			return "Route deleted successfully";
		} else {

			// This Route is not found- throws an Exception
			logger.error("Route not found");
			throw new ObjectNotFoundException(new ErrorResponse("Route not found", HttpStatus.NOT_FOUND));
		}
	}

	/**
	 * Service Method to update a Route
	 * 
	 * @param routeId
	 * @throws ObjectNotFoundException,BadRequestException
	 * @return routeDTO
	 */
	public RouteDTO updateRoute(String routeId, RouteDTO routeDetails)
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException {
		RouteDTO routeDTO = null;
		Route route = null;
		Airport airportOrigin = null;
		Airport airportDestination = null;
		
		String airportOriginId = null;
		String airportDestinationId = null;
		
		
		AirportDTO airportOriginDTO = routeDetails.getOrigin();
		if(airportOriginDTO != null){
			airportOriginId = airportOriginDTO.getAirportId();
			if(StringUtils.isNotBlank(airportOriginId)){
				airportOrigin = Query.from(Airport.class).where("id = ?",airportOriginId).first();
				if(airportOrigin == null){
					throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.AIRPORT}, Locale.US), HttpStatus.NOT_FOUND));
				}
			}
			else{
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}
		}
		
		AirportDTO airportDestinationDTO = routeDetails.getDestination();
		if(airportDestinationDTO != null){
			airportDestinationId = airportDestinationDTO.getAirportId();
			if(StringUtils.isNotBlank(airportDestinationId)){
				if(StringUtils.isNotBlank(airportOriginId) && airportOriginId.equalsIgnoreCase(airportDestinationId)){
					throw new BadRequestException(new ErrorResponse("Bad Request - Origin and Destination cannot be the same!", HttpStatus.BAD_REQUEST));
				}
				airportDestination = Query.from(Airport.class).where("id = ?",airportDestinationId).first();
				if(airportDestination == null){
					throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.AIRPORT}, Locale.US), HttpStatus.NOT_FOUND));
				}
			}
			else{
				throw new BadRequestException(new ErrorResponse(messageSource.getMessage(ECMSConstants.BAD_REQUEST, null, Locale.US), HttpStatus.BAD_REQUEST));
			}
		}
		
		route = Query.from(Route.class).where("id = ?",routeId).first();
		if(route != null){
			
			String routeName = routeDetails.getName();
			if(StringUtils.isNotBlank(routeName)){
				if(Query.from(Route.class).where("name = ?",routeName).first() == null){
					route.setName(routeName);
				}
				else{
					logger.error("Route with the same name already exists");
					throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS, new Object[]{ECMSConstants.ROUTE, ECMSConstants.NAME}, Locale.US), HttpStatus.FORBIDDEN));
				}
			}
			
			Route duplicateRoute = Query.from(Route.class).where("origin = ?",airportOrigin).and("destination = ?",airportDestination).first();
			if(duplicateRoute == null){
				if(airportOrigin != null){
					if(route.getDestination().getId().toString().equalsIgnoreCase(airportOrigin.getId().toString())){
						throw new BadRequestException(new ErrorResponse("Bad Request - Origin and Destination cannot be the same!", HttpStatus.BAD_REQUEST));
					}
					route.setOrigin(airportOrigin);
				}
				if(airportDestination != null){
					if(route.getOrigin().getId().toString().equalsIgnoreCase(airportDestination.getId().toString())){
						throw new BadRequestException(new ErrorResponse("Bad Request - Origin and Destination cannot be the same!", HttpStatus.BAD_REQUEST));
					}
					route.setDestination(airportDestination);
				}
				route.save();
				
				routeDTO = new RouteDTO();

				Airport origin = route.getOrigin();
				AirportDTO originDTO = new AirportDTO();
				originDTO.setAirportId(origin.getId().toString());
				originDTO.setAirportIATA(origin.getAirportIATA());
				originDTO.setAirportICAO(origin.getAirportICAO());
				originDTO.setAirportName(origin.getAirportName());

				Airport airportDest = route.getDestination();
				AirportDTO destinationDTO = new AirportDTO();
				destinationDTO.setAirportId(airportDest.getId().toString());
				destinationDTO.setAirportIATA(airportDest.getAirportIATA());
				destinationDTO.setAirportICAO(airportDest.getAirportICAO());
				destinationDTO.setAirportName(airportDest.getAirportName());

				routeDTO.setOrigin(originDTO);
				routeDTO.setDestination(destinationDTO);
				routeDTO.setRouteId(route.getId().toString());
				routeDTO.setName(route.getName().toString());
			}
			else{
				logger.error("Route with the same origin and destination already exists");
				throw new ObjectExistsException(new ErrorResponse("Route with the same origin and destination already exists", HttpStatus.FORBIDDEN));
			}
		}
		else{
			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.ROUTE}, Locale.US), HttpStatus.NOT_FOUND));
		}

//		if (routeDetails.getDestination() != null && routeDetails.getOrigin() != null
//				&& !routeDetails.getDestination().getAirportId().equals(routeDetails.getOrigin().getAirportId())) {
//			
//			airportOrigin = Query.from(Airport.class).where("id = ?", routeDetails.getOrigin().getAirportId()).first();
//			if(airportOrigin == null){
//				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.AIRPORT}, Locale.US), HttpStatus.NOT_FOUND));
//			}
//
//			airportDestination = Query.from(Airport.class).where("id = ?", routeDetails.getDestination().getAirportId()).first();
//			if(airportDestination == null){
//				throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.AIRPORT}, Locale.US), HttpStatus.NOT_FOUND));
//			}
////			if(null==airportDestination || null==airportOrigin){
////				throw new BadRequestException(
////						new ErrorResponse("Bad Request -Please fill all the required fields correctly", HttpStatus.BAD_REQUEST));
////			}
//			// Route routeQuery = Query.from(Route.class).where("origin = ? ",
//			// airportOrigin).and("destination = ?",
//			// airportDestination).first();
//		} else {
//			if (null == routeDetails.getDestination()) {
//				// This destination Airport not present- throws an Exception
//				logger.error("Destination airport not present");
//				throw new BadRequestException(
//						new ErrorResponse("Bad Request - destination airport not present", HttpStatus.FORBIDDEN));
//			} else if (null == routeDetails.getOrigin()) {
//				// This origin Airport not present- throws an Exception
//				logger.error("Origin airport not present");
//				throw new BadRequestException(
//						new ErrorResponse("Bad Request - origin airport not present", HttpStatus.FORBIDDEN));
//			} else if (routeDetails.getDestination().getAirportId().equals(routeDetails.getOrigin().getAirportId())) {
//				// Origin and destination can not be same- throws an Exception
//				logger.error("Origin and destination can not be same");
//				throw new BadRequestException(
//						new ErrorResponse("Wrong input - origin and destination cannot be same", HttpStatus.FORBIDDEN));
//			}
//		}
//
//		route = Query.from(Route.class).where("id = ?", routeId).first();
//		if (null != route) {
//			Route dupRoute = Query.from(Route.class).where("name = ?", routeDetails.getName()).first();
//			if (null == dupRoute) {
//				route.setName(routeDetails.getName().trim());
//				Route routeQuery = Query.from(Route.class).where("origin = ? ", airportOrigin)
//						.and("destination = ?", airportDestination).first();
//
//				if (null != routeQuery) {
//
//					if (routeDetails.getOrigin().getAirportId().equals(route.getOrigin().getId().toString())
//							&& routeDetails.getDestination().getAirportId()
//									.equals(route.getDestination().getId().toString())) {
//						route.setOrigin(airportOrigin);
//						route.setDestination(airportDestination);
//						route.setName(routeDetails.getName().trim());
//						route.as(Site.ObjectModification.class).setGlobal(true);
//						logger.debug("Route instance saved successfully! " + route.toString());
//						route.save();
//					} else {
//						// This Route already exists - throw an Exception
//						logger.error("Route is already present");
//						throw new ObjectExistsException(
//								new ErrorResponse("Route is already present", HttpStatus.FORBIDDEN));
//					}
//
//				}else{
//					route.setOrigin(airportOrigin);
//					route.setDestination(airportDestination);
//					route.setName(routeDetails.getName().trim());
//					route.as(Site.ObjectModification.class).setGlobal(true);
//					logger.debug("Route instance saved successfully! " + route.toString());
//					route.save();
//				}
//			}
//			else{
//				logger.error("Route with the same name already exists");
//				throw new ObjectExistsException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_EXISTS,new Object[]{ECMSConstants.ROUTE, ECMSConstants.NAME} ,Locale.US), HttpStatus.FORBIDDEN));
//			}
////			} else {
////				if (routeDetails.getName().equals(route.getName())) {
////					Route routeQuery = Query.from(Route.class).where("origin = ? ", airportOrigin)
////							.and("destination = ?", airportDestination).first();
////
////					if (null != routeQuery) {
////
////						if (routeDetails.getOrigin().getAirportId().equals(route.getOrigin().getId().toString())
////								&& routeDetails.getDestination().getAirportId()
////										.equals(route.getDestination().getId().toString())) {
////							route.setOrigin(airportOrigin);
////							route.setDestination(airportDestination);
////							route.setName(routeDetails.getName().trim());
////							route.as(Site.ObjectModification.class).setGlobal(true);
////							logger.debug("Route instance saved successfully! " + route.toString());
////							route.save();
////						} else {
////							// This Route already exists - throw an Exception
////							logger.error("Route is already present");
////							throw new ObjectExistsException(
////									new ErrorResponse("Route is already present", HttpStatus.FORBIDDEN));
////						}
////
////					}else{
////						route.setOrigin(airportOrigin);
////						route.setDestination(airportDestination);
////						route.setName(routeDetails.getName().trim());
////						route.as(Site.ObjectModification.class).setGlobal(true);
////						logger.debug("Route instance saved successfully! " + route.toString());
////						route.save();
////					}
////				} else {
////
////					// Route with this routeName is already present- throws an
////					// Exception
////					logger.error("Route with this routeName is already present.");
////					throw new BadRequestException(new ErrorResponse(
////							"Bad Request -Route with this routeName is already present", HttpStatus.BAD_REQUEST));
////				}
////			}
//		} else {
//			// This route is not found- throws an Exception
//			logger.error("Route not found while updating the route for routeId " + routeId);
//			throw new ObjectNotFoundException(new ErrorResponse(messageSource.getMessage(ECMSConstants.OBJECT_NOT_FOUND, new Object[]{ECMSConstants.ROUTE}, Locale.US), HttpStatus.NOT_FOUND));
//		}

		return routeDTO;
	}

	/**
	 * Service Method to retrieve route by its routeID
	 * 
	 * @param routeId
	 * @throws ObjectNotFoundException
	 */
	public RouteDTO getRouteById(String routeId) throws ObjectNotFoundException {

		RouteDTO routeDTO = null;

		Route route = Query.from(Route.class).where("id = ?", routeId).first();
		if (null != route) {
			routeDTO = new RouteDTO();
			routeDTO.setRouteId(route.getId().toString());
			routeDTO.setName(route.getName());
			AirportDTO originDTO = new AirportDTO();
			Airport origin = route.getOrigin();
			originDTO.setAirportId(origin.getId().toString());
			originDTO.setAirportIATA(origin.getAirportIATA());
			originDTO.setAirportICAO(origin.getAirportICAO());
			originDTO.setAirportName(origin.getAirportName());

			Airport airportDest = route.getDestination();
			AirportDTO destinationDTO = new AirportDTO();
			destinationDTO.setAirportId(airportDest.getId().toString());
			destinationDTO.setAirportIATA(airportDest.getAirportIATA());
			destinationDTO.setAirportICAO(airportDest.getAirportICAO());
			destinationDTO.setAirportName(airportDest.getAirportName());
			routeDTO.setDestination(destinationDTO);
			routeDTO.setOrigin(originDTO);
		} else {

			// This object is null- throws an Exception
			logger.error("Object is null");
			throw new ObjectNotFoundException(new ErrorResponse("Object is null", HttpStatus.NOT_FOUND));

		}
		return routeDTO;
	}

}