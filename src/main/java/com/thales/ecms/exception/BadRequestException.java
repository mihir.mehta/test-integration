package com.thales.ecms.exception;

public class BadRequestException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final String DEFAULT_MESSAGE = "The request is syntactically incorrect";

	private ErrorResponse errorResponse;

	public BadRequestException() {
		super(DEFAULT_MESSAGE);
	}

	public BadRequestException(String message){
		super(message);
	}

	public BadRequestException(ErrorResponse errorResponse){
		super(errorResponse.getErrorMessage());
		this.errorResponse = errorResponse;
	}

	public ErrorResponse getErrorResponse() {
		return this.errorResponse;
	}
}
