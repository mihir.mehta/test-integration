package com.thales.ecms.exception;

public class ObjectAssociationExistsException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final String DEFAULT_MESSAGE = "The object is associated with other objects.";
	
	private ErrorResponse errorResponse;
	
	public ObjectAssociationExistsException() {
		super(DEFAULT_MESSAGE);
	}
	
	public ObjectAssociationExistsException(String message){
		super(message);
	}
	
	public ObjectAssociationExistsException(ErrorResponse errorResponse){
		super(errorResponse.getErrorMessage());
		this.errorResponse = errorResponse;
	}

	public ErrorResponse getErrorResponse() {
		return this.errorResponse;
	}
	
}
