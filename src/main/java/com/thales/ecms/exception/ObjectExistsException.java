package com.thales.ecms.exception;

public class ObjectExistsException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final String DEFAULT_MESSAGE = "The object already exists empty";
	
	private ErrorResponse errorResponse;
	
	public ObjectExistsException(){
		super(DEFAULT_MESSAGE);
	}
	
	public ObjectExistsException(String message){
		super(message);
	}
	
	public ObjectExistsException(ErrorResponse errorResponse){
		super(errorResponse.getErrorMessage());
		this.errorResponse = errorResponse;
	}

	public ErrorResponse getErrorResponse() {
		return this.errorResponse;
	}
	
}
