package com.thales.ecms.exception;

public class DuplicateEntryException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final String DEFAULT_MESSAGE = "The response returned was empty";
	
	private ErrorResponse errorResponse;
	
	public DuplicateEntryException() {
		// TODO Auto-generated constructor stub
	}
	
	public DuplicateEntryException(String message){
		super(message);
	}
	
	public DuplicateEntryException(ErrorResponse errorResponse){
		super(errorResponse.getErrorMessage());
		this.errorResponse = errorResponse;
	}

	public ErrorResponse getErrorResponse() {
		return this.errorResponse;
	}
	
}
