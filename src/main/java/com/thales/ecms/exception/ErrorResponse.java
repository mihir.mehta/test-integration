package com.thales.ecms.exception;

import org.springframework.http.HttpStatus;

public class ErrorResponse {

	private String errorMessage;
	private HttpStatus errorStatusCode;
	
	public ErrorResponse(String errorMessage, HttpStatus errorStatusCode){
		this.errorMessage = errorMessage;
		this.errorStatusCode = errorStatusCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public HttpStatus getErrorStatusCode() {
		return errorStatusCode;
	}
	
}
