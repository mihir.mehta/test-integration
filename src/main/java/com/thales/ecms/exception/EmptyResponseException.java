package com.thales.ecms.exception;

public class EmptyResponseException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final String DEFAULT_MESSAGE = "The response returned was empty";
	
	private ErrorResponse errorResponse;
	
	public EmptyResponseException(){
		super(DEFAULT_MESSAGE);
	}
	
	public EmptyResponseException(String message){
		super(message);
	}
	
	public EmptyResponseException(ErrorResponse errorResponse){
		super(errorResponse.getErrorMessage());
		this.errorResponse = errorResponse;
	}

	public ErrorResponse getErrorResponse() {
		return this.errorResponse;
	}
	
}
