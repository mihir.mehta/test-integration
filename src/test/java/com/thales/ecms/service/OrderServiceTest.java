package com.thales.ecms.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.NoSuchMessageException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.EmptyResponseException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.model.dto.ConfigurationDTO;
import com.thales.ecms.model.dto.ConfigurationRequestDTO;
import com.thales.ecms.model.dto.ContentTypeDTO;
import com.thales.ecms.model.dto.OrderRequestDTO;
import com.thales.ecms.model.dto.OrderRequestDTO.DisplayGroupRequestDTO;

import junit.framework.Assert;

import com.thales.ecms.model.dto.OrderResponseDTO;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })
public class OrderServiceTest {

	
	@Autowired
	private AirlineService airlineService;
	
	@Autowired
	private  ConfigurationService  configurationService;
	
	@Autowired
	private ContentTypeService contentTypeService;
	
	@Autowired
	private OrderService orderService;
	
	@Before
	public void setup() {
	  
	}
	
	
	@Test
	public void createOrderTest() throws BadRequestException, ObjectExistsException, 
	                DuplicateEntryException, NoSuchMessageException, ObjectNotFoundException, EmptyResponseException {
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Siceje4qt1121111");
		airlineDetails.setWebsite("https://www.Sic4eq1jet111121.com");
		airlineDetails.setCompanyName("Siceje4t11q1121");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("90368941q12111");
		airlineDetails.setIataCode("BBj11141q211");
		airlineDetails.setIcaoCode("AABui141q11211");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
        OrderRequestDTO orderDetails = new OrderRequestDTO();
		
		orderDetails.setLastModifiedDate(null);
		orderDetails.setOrderDescription("My yOreer1");
		orderDetails.setOrderEndDate(1487808000000L);
		orderDetails.setOrderName("First_yorder1");
		orderDetails.setOrderStartDate(1486944000000L);
		orderDetails.setOrderStatus("COMPLETE");
		
		List<DisplayGroupRequestDTO> displayGroupList = new ArrayList<OrderRequestDTO.DisplayGroupRequestDTO>();
		DisplayGroupRequestDTO displayGroupRequestDTO = new DisplayGroupRequestDTO();
		displayGroupRequestDTO.setStartDate(new Date());
		displayGroupRequestDTO.setMonth("February");
		displayGroupRequestDTO.setEndDate(new Date());
		displayGroupList.add(displayGroupRequestDTO);
		orderDetails.setDisplayGroupList(displayGroupList);
		
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Configure_1Si4cejet12");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejet1412");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		List<String> configurationIdList = new ArrayList<>();
		configurationIdList.add(configurationRequestDTO.getConfigurationId());
		orderDetails.setConfigurationIdList(configurationIdList);
		OrderResponseDTO orderResponseDTO = orderService.createOrder(airlineDTO.getAirlineInternalId(), orderDetails);
		Assert.assertNotNull(orderResponseDTO);
		String response = orderService.deleteOrder(airlineDTO.getAirlineInternalId(), orderResponseDTO.getOrderId());
		Assert.assertEquals(response, "order deleted succesfully");
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}
	
	@Test
	public void getOrerListTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException, 
	                  NoSuchMessageException, ObjectNotFoundException, EmptyResponseException{
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Siceje4qt1121111");
		airlineDetails.setWebsite("https://www.Sic4eq1jet111121.com");
		airlineDetails.setCompanyName("Siceje4t11q1121");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("90368941q12111");
		airlineDetails.setIataCode("BBj11141q211");
		airlineDetails.setIcaoCode("AABui141q11211");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
        OrderRequestDTO orderDetails = new OrderRequestDTO();
		
		orderDetails.setLastModifiedDate(null);
		orderDetails.setOrderDescription("My yOreer1");
		orderDetails.setOrderEndDate(1487808000000L);
		orderDetails.setOrderName("First_yorder1");
		orderDetails.setOrderStartDate(1486944000000L);
		orderDetails.setOrderStatus("COMPLETE");
		
		List<DisplayGroupRequestDTO> displayGroupList = new ArrayList<OrderRequestDTO.DisplayGroupRequestDTO>();
		DisplayGroupRequestDTO displayGroupRequestDTO = new DisplayGroupRequestDTO();
		displayGroupRequestDTO.setStartDate(new Date());
		displayGroupRequestDTO.setMonth("February");
		displayGroupRequestDTO.setEndDate(new Date());
		displayGroupList.add(displayGroupRequestDTO);
		orderDetails.setDisplayGroupList(displayGroupList);
		
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Configure_1Si4cejet12");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejet1412");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		List<String> configurationIdList = new ArrayList<>();
		configurationIdList.add(configurationRequestDTO.getConfigurationId());
		orderDetails.setConfigurationIdList(configurationIdList);
		OrderResponseDTO orderResponseDTO = orderService.createOrder(airlineDTO.getAirlineInternalId(), orderDetails);
		List<OrderResponseDTO> responseDTOs = orderService.getOrerList(airlineDTO.getAirlineInternalId());
		Assert.assertNotNull(responseDTOs);
		for(OrderResponseDTO dto : responseDTOs){
			String response = orderService.deleteOrder(airlineDTO.getAirlineInternalId(), dto.getOrderId());
			Assert.assertEquals(response, "order deleted succesfully");
		}
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}
	
	
	@Test
	public void getOrderByIdTest() throws NoSuchMessageException, BadRequestException, ObjectExistsException, 
	               ObjectNotFoundException, EmptyResponseException, DuplicateEntryException{
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Siceje4qt1121111");
		airlineDetails.setWebsite("https://www.Sic4eq1jet111121.com");
		airlineDetails.setCompanyName("Siceje4t11q1121");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("90368941q12111");
		airlineDetails.setIataCode("BBj11141q211");
		airlineDetails.setIcaoCode("AABui141q11211");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
        OrderRequestDTO orderDetails = new OrderRequestDTO();
		
		orderDetails.setLastModifiedDate(null);
		orderDetails.setOrderDescription("My yOreer1");
		orderDetails.setOrderEndDate(1487808000000L);
		orderDetails.setOrderName("First_yorder1");
		orderDetails.setOrderStartDate(1486944000000L);
		orderDetails.setOrderStatus("COMPLETE");
		
		List<DisplayGroupRequestDTO> displayGroupList = new ArrayList<OrderRequestDTO.DisplayGroupRequestDTO>();
		DisplayGroupRequestDTO displayGroupRequestDTO = new DisplayGroupRequestDTO();
		displayGroupRequestDTO.setStartDate(new Date());
		displayGroupRequestDTO.setMonth("February");
		displayGroupRequestDTO.setEndDate(new Date());
		displayGroupList.add(displayGroupRequestDTO);
		orderDetails.setDisplayGroupList(displayGroupList);
		
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Configure_1Si4cejet12");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejet1412");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		List<String> configurationIdList = new ArrayList<>();
		configurationIdList.add(configurationRequestDTO.getConfigurationId());
		orderDetails.setConfigurationIdList(configurationIdList);
		OrderResponseDTO orderResponseDTO = orderService.createOrder(airlineDTO.getAirlineInternalId(), orderDetails);
		OrderResponseDTO responseDTO = orderService.getOrderById(airlineDTO.getAirlineInternalId(), orderResponseDTO.getOrderId());
		Assert.assertNotNull(responseDTO);
		String response = orderService.deleteOrder(airlineDTO.getAirlineInternalId(), orderResponseDTO.getOrderId());
		Assert.assertEquals(response, "order deleted succesfully");
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}
	
	
	@Test
	public void updateOrderTest() throws NoSuchMessageException, BadRequestException, ObjectExistsException, 
	                     ObjectNotFoundException, DuplicateEntryException, EmptyResponseException{
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Siceje4qt1121111");
		airlineDetails.setWebsite("https://www.Sic4eq1jet111121.com");
		airlineDetails.setCompanyName("Siceje4t11q1121");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("90368941q12111");
		airlineDetails.setIataCode("BBj11141q211");
		airlineDetails.setIcaoCode("AABui141q11211");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
        OrderRequestDTO orderDetails = new OrderRequestDTO();
		
		orderDetails.setLastModifiedDate(null);
		orderDetails.setOrderDescription("My yOreer1");
		orderDetails.setOrderEndDate(1487808000000L);
		orderDetails.setOrderName("First_yorder1");
		orderDetails.setOrderStartDate(1486944000000L);
		orderDetails.setOrderStatus("COMPLETE");
		
		List<DisplayGroupRequestDTO> displayGroupList = new ArrayList<OrderRequestDTO.DisplayGroupRequestDTO>();
		DisplayGroupRequestDTO displayGroupRequestDTO = new DisplayGroupRequestDTO();
		displayGroupRequestDTO.setStartDate(new Date());
		displayGroupRequestDTO.setMonth("February");
		displayGroupRequestDTO.setEndDate(new Date());
		displayGroupList.add(displayGroupRequestDTO);
		orderDetails.setDisplayGroupList(displayGroupList);
		
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Configure_1Si4cejet12");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejet1412");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		List<String> configurationIdList = new ArrayList<>();
		configurationIdList.add(configurationRequestDTO.getConfigurationId());
		orderDetails.setConfigurationIdList(configurationIdList);
		OrderResponseDTO orderResponseDTO = orderService.createOrder(airlineDTO.getAirlineInternalId(), orderDetails);
		OrderRequestDTO orderRequestDTO = new OrderRequestDTO();
		orderRequestDTO.setConfigurationIdList(configurationIdList);
		orderRequestDTO.setDisplayGroupList(displayGroupList);
		orderRequestDTO.setLastModifiedDate(orderResponseDTO.getLastModifiedDate());
		orderRequestDTO.setOrderName(orderResponseDTO.getOrderName());
		orderRequestDTO.setOrderStatus("Complete");
		orderRequestDTO.setOrderDescription("update order");
		OrderResponseDTO orderResDTO = orderService.updateOrder(airlineDTO.getAirlineInternalId(), orderResponseDTO.getOrderId(), orderRequestDTO);
		Assert.assertNotNull(orderResDTO);
		String response = orderService.deleteOrder(airlineDTO.getAirlineInternalId(), orderResponseDTO.getOrderId());
		Assert.assertEquals(response, "order deleted succesfully");
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}
	
	
	@Test
	public void searchOrderTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException, 
	                       NoSuchMessageException, ObjectNotFoundException, EmptyResponseException{
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Siceje4qt1121111");
		airlineDetails.setWebsite("https://www.Sic4eq1jet111121.com");
		airlineDetails.setCompanyName("Siceje4t11q1121");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("90368941q12111");
		airlineDetails.setIataCode("BBj11141q211");
		airlineDetails.setIcaoCode("AABui141q11211");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
        OrderRequestDTO orderDetails = new OrderRequestDTO();
		
		orderDetails.setLastModifiedDate(null);
		orderDetails.setOrderDescription("My yOreer1");
		orderDetails.setOrderEndDate(1487808000000L);
		orderDetails.setOrderName("First_yorder1");
		orderDetails.setOrderStartDate(1486944000000L);
		orderDetails.setOrderStatus("COMPLETE");
		
		List<DisplayGroupRequestDTO> displayGroupList = new ArrayList<OrderRequestDTO.DisplayGroupRequestDTO>();
		DisplayGroupRequestDTO displayGroupRequestDTO = new DisplayGroupRequestDTO();
		displayGroupRequestDTO.setStartDate(new Date());
		displayGroupRequestDTO.setMonth("February");
		displayGroupRequestDTO.setEndDate(new Date());
		displayGroupList.add(displayGroupRequestDTO);
		orderDetails.setDisplayGroupList(displayGroupList);
		
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Configure_1Si4cejet12");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejet1412");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		List<String> configurationIdList = new ArrayList<>();
		configurationIdList.add(configurationRequestDTO.getConfigurationId());
		orderDetails.setConfigurationIdList(configurationIdList);
		OrderResponseDTO orderResponseDTO = orderService.createOrder(airlineDTO.getAirlineInternalId(), orderDetails);
		OrderRequestDTO orderRequestDTO = new OrderRequestDTO();
		orderRequestDTO.setConfigurationIdList(configurationIdList);
		orderRequestDTO.setDisplayGroupList(displayGroupList);
		orderRequestDTO.setLastModifiedDate(orderResponseDTO.getLastModifiedDate());
		orderRequestDTO.setOrderName(orderResponseDTO.getOrderName());
		orderRequestDTO.setOrderStatus("Complete");
		orderRequestDTO.setOrderDescription(orderResponseDTO.getOrderDescription());
		List<OrderResponseDTO> responseDTOs = orderService.searchOrder(airlineDTO.getAirlineInternalId(), orderRequestDTO);
		Assert.assertNotNull(responseDTOs);
		for(OrderResponseDTO dto : responseDTOs){
			String response = orderService.deleteOrder(airlineDTO.getAirlineInternalId(), dto.getOrderId());
			Assert.assertEquals(response, "order deleted succesfully");
		}
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
		
	}
	
	
	@Test
	public void getCongifurationByAirlineTest() throws NoSuchMessageException, BadRequestException, ObjectExistsException, ObjectNotFoundException, DuplicateEntryException{
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Siceje4qt1121111");
		airlineDetails.setWebsite("https://www.Sic4eq1jet111121.com");
		airlineDetails.setCompanyName("Siceje4t11q1121");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("90368941q12111");
		airlineDetails.setIataCode("BBj11141q211");
		airlineDetails.setIcaoCode("AABui141q11211");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
        OrderRequestDTO orderDetails = new OrderRequestDTO();
		
		orderDetails.setLastModifiedDate(null);
		orderDetails.setOrderDescription("My yOreer1");
		orderDetails.setOrderEndDate(1487808000000L);
		orderDetails.setOrderName("First_yorder1");
		orderDetails.setOrderStartDate(1486944000000L);
		orderDetails.setOrderStatus("COMPLETE");
		
		List<DisplayGroupRequestDTO> displayGroupList = new ArrayList<OrderRequestDTO.DisplayGroupRequestDTO>();
		DisplayGroupRequestDTO displayGroupRequestDTO = new DisplayGroupRequestDTO();
		displayGroupRequestDTO.setStartDate(new Date());
		displayGroupRequestDTO.setMonth("February");
		displayGroupRequestDTO.setEndDate(new Date());
		displayGroupList.add(displayGroupRequestDTO);
		orderDetails.setDisplayGroupList(displayGroupList);
		
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Configure_1Si4cejet12");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejet1412");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		List<ConfigurationDTO> list = orderService.getCongifurationByAirline(airlineDTO.getAirlineInternalId());
		Assert.assertNotNull(list);
		for(ConfigurationDTO dto : list){
			Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), dto.getConfigurationId());
			Assert.assertTrue(deleteStatus);
		}
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}

}
