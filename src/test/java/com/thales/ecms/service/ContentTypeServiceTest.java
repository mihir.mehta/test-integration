package com.thales.ecms.service;


import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.thales.ecms.model.dto.ContentTypeDTO;

import junit.framework.Assert;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })
public class ContentTypeServiceTest {

	@Autowired
	private ContentTypeService contentTypeService;
	
	@Before
	public void setup() {
	  
	}
	
	@Test
	public void getContentTypesTest() {
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		Assert.assertNotNull(contentTypeDTOs);
	}

}
