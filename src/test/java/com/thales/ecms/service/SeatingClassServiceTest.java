package com.thales.ecms.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.NoSuchMessageException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ObjectAssociationExistsException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.SeatingClass;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.model.dto.SeatingClassDTO;
import com.thales.ecms.service.SeatingClassService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })

public class SeatingClassServiceTest {



@Autowired
 private AirlineService airlineService;
@Autowired
 private SeatingClassService seatingClassService;
	@Before
	public void setup() {
		
	}
	
@Test
public void addClassTest() throws ObjectNotFoundException, ObjectExistsException, BadRequestException, DuplicateEntryException{
	
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sice34x567fdf1dzdsfsf1111122");
	airlineDetails.setWebsite("https://wwwx.go5sdff7161122.com");
	airlineDetails.setCompanyName("Sicxes111sdf1f1212");
	airlineDetails.setCodeShare("90903x37s11sdff111122");
	airlineDetails.setIataCode("Bytx7s1111sdf1f212");
	airlineDetails.setIcaoCode("Ahjx37s11sdf112f112");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
	SeatingClassDTO classDetails=new SeatingClassDTO();
    classDetails.setName("Business23xdsf4f15371");
	SeatingClassDTO seatingClassDTO =seatingClassService.addClass(airlineDTO.getAirlineInternalId(), classDetails) ;
    Assert.assertEquals(seatingClassDTO.getName(), classDetails.getName());
	Assert.assertNotNull(airlineDTO.getAirlineInternalId());
	Query.from(SeatingClass.class).where("id = ?",  seatingClassDTO.getSeatingClassInternalId()).first().delete();
     Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first().delete();
	
	
}
 @Test (expected = ObjectNotFoundException.class)
public void addClassExceptionTest() throws ObjectExistsException, ObjectNotFoundException, BadRequestException {
		SeatingClassDTO seatingClassDTO  = new SeatingClassDTO() ;
		seatingClassDTO.setName("MyClass");
		SeatingClassDTO seatingClass = seatingClassService.addClass("airlineid", seatingClassDTO);
		 Query.from(SeatingClass.class).where("id = ?",  seatingClass.getSeatingClassInternalId()).first().delete();
}

@Test
public void getClassesTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	
	
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sice34x5164sfdfg33345345");
	airlineDetails.setWebsite("https://wwwx.go15f761sdfg11221.com");
	airlineDetails.setCompanyName("Sicxes111111f122dfg1");
	airlineDetails.setCodeShare("90903x37s1dfgffdg1111111212");
	airlineDetails.setIataCode("Bytx7s11111fdg11f2121");
	airlineDetails.setIcaoCode("Ahjx37s1111dfg11f12112");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
	
	SeatingClassDTO classDetails=new SeatingClassDTO();
    classDetails.setName("Business23xdfg4f5113711");
	SeatingClassDTO seatingClassDTO =seatingClassService.addClass(airlineDTO.getAirlineInternalId(), classDetails) ;
	
	List<SeatingClassDTO> seatingClassDTOList = seatingClassService.getClasses(airlineDTO.getAirlineInternalId());
    Assert.assertTrue(seatingClassDTOList.size()>0);
    
    
    Query.from(SeatingClass.class).where("id = ?",  seatingClassDTO.getSeatingClassInternalId()).first().delete();
    Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first().delete();
	
}

@Test
public void getClassByIdTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sice34x516f4fxxg33345345");
	airlineDetails.setWebsite("https://wwwx.gofxxdfg1576111221.com");
	airlineDetails.setCompanyName("Sicxes11111xfxfdg11221");
	airlineDetails.setCodeShare("90903x37s111xxf1fdg1111212");
	airlineDetails.setIataCode("Bytx7s111111xx1f2fg121");
	airlineDetails.setIcaoCode("Ahjx37s1111xx11ffdg12112");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
	
	SeatingClassDTO classDetails=new SeatingClassDTO();
    classDetails.setName("Business23x45zzdfgf113711");
	SeatingClassDTO seatingClassDTO =seatingClassService.addClass(airlineDTO.getAirlineInternalId(), classDetails) ;
	
	SeatingClassDTO seatingClassDTO1 = seatingClassService.getClassById(airlineDTO.getAirlineInternalId(), seatingClassDTO.getSeatingClassInternalId());
    Assert.assertNotNull(seatingClassDTO1);
    
    Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first().delete();
    Query.from(SeatingClass.class).where("id = ?",  seatingClassDTO1.getSeatingClassInternalId()).first().delete();
    
}

@Test
public void updateClassTest() throws NoSuchMessageException, ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException, ObjectAssociationExistsException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Siced34x5f5g345");
	airlineDetails.setWebsite("https://wwwx.gof1dtgf1.com");
	airlineDetails.setCodeShare("90903x37dsg1tf111212");
	airlineDetails.setIataCode("Bytx7sd1ethgf121");
	airlineDetails.setIcaoCode("Ahjx37sdrt1tfgfh2112");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
	
	SeatingClassDTO classDetails=new SeatingClassDTO();
    classDetails.setName("Busin1gf13d711");
	SeatingClassDTO seatingClassDTO =seatingClassService.addClass(airlineDTO.getAirlineInternalId(), classDetails) ;
	classDetails.setName("Businf1371d1");
    SeatingClassDTO seatingClassDTO1 = seatingClassService.updateClass(airlineDTO.getAirlineInternalId(), seatingClassDTO.getSeatingClassInternalId(), classDetails);
    Assert.assertEquals(seatingClassDTO.getName(), seatingClassDTO.getName());
    Query.from(SeatingClass.class).where("id = ?",  seatingClassDTO1.getSeatingClassInternalId()).first().delete();
    
    Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first().delete();
}

@Test
public void deleteClassTest() throws ObjectNotFoundException, ObjectExistsException, ObjectAssociationExistsException, BadRequestException, DuplicateEntryException{
	
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sice34x5f67df1dsergxf1111122");
	airlineDetails.setWebsite("https://wwwx.gfo5716dfgx1122.com");
	airlineDetails.setCompanyName("Sicxes11xfdfg111212");
	airlineDetails.setCodeShare("90903x37xsedrfg11111122");
	airlineDetails.setIataCode("Bytx7s111x1dfg1f212");
	airlineDetails.setIcaoCode("Ahjx37s111dfgx1f2112");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
	SeatingClassDTO classDetails=new SeatingClassDTO();
    classDetails.setName("Businesxs23x415371");
	SeatingClassDTO seatingClassDTO =seatingClassService.addClass(airlineDTO.getAirlineInternalId(), classDetails) ;
	
	Boolean result=seatingClassService.deleteClass(airlineDTO.getAirlineInternalId(), seatingClassDTO.getSeatingClassInternalId());
	Assert.assertTrue(result);
    
    Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first().delete();
}

@Test (expected = ObjectNotFoundException.class)
public void deleteClassExceptionTest() throws ObjectExistsException, ObjectNotFoundException, BadRequestException, ObjectAssociationExistsException {
		
		 boolean result = seatingClassService.deleteClass("airlineId", "seatingClassId");
			Assert.assertTrue(result);
}

	
 	
	
}
