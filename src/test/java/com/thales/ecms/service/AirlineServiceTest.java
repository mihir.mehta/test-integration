package com.thales.ecms.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.NoSuchMessageException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.airline.AirlinePortal.AirlineStatus;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.service.AirlineService;

/**
 * @author Sushma
 * Service class for JUIT test cases on Aircraft
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })

public class AirlineServiceTest {
	@Autowired
	private AirlineService airlineService;
	public void setup() {
		
	} 
	 
	 /**
	 * Add airline
	 * @param AirlineDTO
	 * @return AirlineDTO
     */
 @Test
 public void addAirlineTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sice1jet1yy");
	airlineDetails.setWebsite("https://www.go1yntu.com");
	airlineDetails.setCompanyName("Sicejet11ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("9031689");
	airlineDetails.setIataCode("BB1j");
	airlineDetails.setIcaoCode("AA1Bui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
    Assert.assertEquals(airlineDTO.getAirlineName(), airlineDetails.getAirlineName());
 //   Assert.assertEquals(airlineDTO.getAirlineStatus(), airlineDetails.getAirlineStatus());
    Assert.assertEquals(airlineDTO.getCodeShare(), airlineDetails.getCodeShare());
    Assert.assertEquals(airlineDTO.getCompanyName(), airlineDetails.getCompanyName());
    Assert.assertEquals(airlineDTO.getIataCode(), airlineDetails.getIataCode());
    Assert.assertEquals(airlineDTO.getIcaoCode(), airlineDetails.getIcaoCode());
    Assert.assertEquals(airlineDTO.getWebsite(), airlineDetails.getWebsite());
    Assert.assertNotNull(airlineDTO.getAirlineInternalId());
	Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
	airline.delete();
}

/**
 * Add airline
 * @param AirlineDTO
 * @return BadRequestException
 */

@Test (expected = BadRequestException.class)
public void addAirlineExceptionTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException{
   AirlineDTO airlineDetails=new AirlineDTO();
   airlineService.addAirline(airlineDetails);
}

/**
 * Get airline
 * @return List of AirlineDTO
 */

@Test
public void getAirlinesTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sice1jet1yy");
	airlineDetails.setWebsite("https://www.go1yntu.com");
	airlineDetails.setCompanyName("Sicejet11ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("9031689");
	airlineDetails.setIataCode("BB1j");
	airlineDetails.setIcaoCode("AA1Bui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
	List<AirlineDTO> airlineDTOList= airlineService.getAirlines();
    Assert.assertTrue(airlineDTOList.size()>0);
 	Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
 	airline.delete();
}

/**
 * Get airline by id
 * @param airlineId
 * @return  AirlineDTO
 */

@Test
public void getAirlineByIdTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sice1jet1yy");
	airlineDetails.setWebsite("https://www.go1yntu.com");
	airlineDetails.setCompanyName("Sicejet11ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("9031689");
	airlineDetails.setIataCode("BB1j");
	airlineDetails.setIcaoCode("AA1Bui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
    AirlineDTO airlineDTO1= airlineService.getAirlineById(airlineDTO.getAirlineInternalId());
	Assert.assertNotNull(airlineDTO1);
 	Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
 	airline.delete();
   }

/**
 * Get airline by id
 * @param airlineId
 * @return  ObjectNotFoundException
 */

@Test (expected = ObjectNotFoundException.class)
public void getAirlineByIdExceptionTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
     airlineService.getAirlineById("airlineId");
    }

/**
 * Update airline 
 * @param airlineId
 * @param AirlineDTO
 * @return  AirlineDTO
 */

@Test
public void updateAirlineTest() throws ObjectNotFoundException, ObjectExistsException, BadRequestException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sice1jet1yy");
	airlineDetails.setWebsite("https://www.go1yntu.com");
	airlineDetails.setCompanyName("Sicejet11ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("9031689");
	airlineDetails.setIataCode("BB1j");
	airlineDetails.setIcaoCode("AA1Bui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
    airlineDetails.setAirlineName("Spicejet2167");
	AirlineDTO airlineDTO1=airlineService.updateAirline(airlineDTO.getAirlineInternalId(), airlineDetails);
    Assert.assertEquals(airlineDTO1.getAirlineName(), airlineDetails.getAirlineName());
	Assert.assertNotNull(airlineDTO);
	Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
	airline.delete();

}

/**
 * Update airline 
 * @param airlineId
 * @param AirlineDTO
 * @return  ObjectNotFoundException
 */

@Test (expected = ObjectNotFoundException.class)
public void updateAirlineExceptionTest() throws ObjectNotFoundException, ObjectExistsException, BadRequestException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
	airlineDetails.setAirlineName("jet2");
	airlineService.updateAirline("airlineId", airlineDetails);

}

/**
 * Delete airline 
 * @param airlineId
 * @return Message
 */

@Test 
public void deleteAirlineTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sice1jet1yy");
	airlineDetails.setWebsite("https://www.go1yntu.com");
	airlineDetails.setCompanyName("Sicejet11ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("9031689");
	airlineDetails.setIataCode("BB1j");
	airlineDetails.setIcaoCode("AA1Bui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
    String message=airlineService.deleteAirline(airlineDTO.getAirlineInternalId());
    Assert.assertEquals(message, "Airline deleted successfully");

}

/**
 * Delete airline 
 * @param airlineId
 * @return ObjectNotFoundException
 * @throws ObjectExistsException 
 * @throws BadRequestException 
 * @throws NoSuchMessageException 
 */


@Test (expected = ObjectNotFoundException.class)
public void deleteAirlineExceptionTest() throws ObjectNotFoundException, NoSuchMessageException, BadRequestException, ObjectExistsException{
	String message=airlineService.deleteAirline("airlineId");
    Assert.assertEquals(message, "Airline deleted successfully");
}

/**
 * Change airline Status
 * @param airlineId
 * @param status
 * @return true
 */

@Test
public void changeAirlineStatus() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
	AirlineDTO airlineDetails=new AirlineDTO();
    airlineDetails.setAirlineName("Sice1jet1yy");
	airlineDetails.setWebsite("https://www.go1yntu.com");
	airlineDetails.setCompanyName("Sicejet11ui");
	//airlineDetails.setAirlineStatus("ACTIVE");
	airlineDetails.setCodeShare("9031689");
	airlineDetails.setIataCode("BB1j");
	airlineDetails.setIcaoCode("AA1Bui");
	airlineDetails.setActivate(true);
	Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetStartDate(startDate);
	Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
	airlineDetails.setAssetEndDate(endDate);
	Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
	Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
    airlineDetails.setContentStartDate(contentstartDate);
	airlineDetails.setContentEndDate(contentEndDate);
	airlineDetails.setShipmentStartdate(contentEndDate);
	airlineDetails.setShipmentEndDate(contentEndDate);
	AirlineDTO airlineDTO =airlineService.addAirline(airlineDetails);
    AirlineStatus airlineStatus = AirlineStatus.INACTIVE;
	Boolean result=airlineService.changeAirlineStatus(airlineDTO.getAirlineInternalId(), airlineStatus);
    Assert.assertTrue(result);
	Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
	airline.delete();
}

/**
 * Change airline Status
 * @param airlineId
 * @param status
 * @return ObjectNotFoundException
 */

@Test (expected = ObjectNotFoundException.class)
public void changeAirlineExceptionStatus() throws ObjectNotFoundException, BadRequestException, ObjectExistsException, DuplicateEntryException{
    AirlineStatus airlineStatus = AirlineStatus.INACTIVE;
	airlineService.changeAirlineStatus("airlineId", airlineStatus);
}

}
