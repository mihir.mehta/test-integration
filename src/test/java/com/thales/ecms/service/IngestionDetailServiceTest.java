package com.thales.ecms.service;


import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.NoSuchMessageException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.cms.db.Site;
import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.DuplicateEntryException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.IngestionDetail;
import com.thales.ecms.model.dto.AirlineDTO;
import com.thales.ecms.model.dto.ConfigurationRequestDTO;
import com.thales.ecms.model.dto.ContentTypeDTO;
import com.thales.ecms.model.dto.IngestionDetailDTO;

import junit.framework.Assert;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })
public class IngestionDetailServiceTest {

	
	 @Autowired
	 private  IngestionDetailService ingestionDetailService;
	 
	 @Autowired
	 private  AirlineService airlineService;
	 
	 @Autowired
	 private  ContentTypeService contentTypeService;
	 
	 @Autowired
	 private  ConfigurationService configurationService;
	 
	 @Before
		public void setup() {
		  
		}
	 
	 
	@Test
	public void saveIngestionDetailsByAirlineIdTest() throws BadRequestException, ObjectExistsException, 
	                       DuplicateEntryException, NoSuchMessageException, ObjectNotFoundException {
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Sicej5qetwtqe111232111w1");
		airlineDetails.setWebsite("https://www.Sicqtq5ew12we1jet1113121.com");
		airlineDetails.setCompanyName("Sicejet113q1tweq512w121");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("90368915e11qq2tw31w2111");
		airlineDetails.setIataCode("BBj111153e2qqw1w21t1");
		airlineDetails.setIcaoCode("AABui1135eqq1w1w212t11");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Configure_Sic2qe1wqjet5wt1e23");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejweet2wq511tq23");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		@SuppressWarnings("unused")
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		IngestionDetailDTO ingestionDetailsDTO = new IngestionDetailDTO();
		ingestionDetailsDTO.setConfiguration(configurationDTO.getConfigurationName());
		ingestionDetailsDTO.setFileName("newMetaDataw1qetIm1p2qort - Copy.xlsx");
		ingestionDetailsDTO.setImportStatus(null);
		ingestionDetailsDTO.setIngestedDate(new Date());
		ingestionDetailsDTO.setIngestionTitleDTOList(null);
		ingestionDetailsDTO.setMode("Uploaded");
		ingestionDetailsDTO.setStatus("in progress");
		ingestionDetailsDTO.setTitleCount(0);
		ingestionDetailsDTO.setConfigurationId(configurationDTO.getConfigurationId());
		IngestionDetailDTO dto = ingestionDetailService.saveIngestionDetailsByAirlineId(airlineDTO.getAirlineInternalId(), ingestionDetailsDTO);
		Assert.assertNotNull(dto);
		IngestionDetail ingestionDetail = Query.from(IngestionDetail.class).where("id = ?", dto.getIngestionDetailInternalId()).first();
		ingestionDetail.delete();
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}
	
	@Test
	public void getIngestionDetailsTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException, NoSuchMessageException, ObjectNotFoundException{
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Sicej5qetwtqe111232111w1");
		airlineDetails.setWebsite("https://www.Sicqtq5ew12we1jet1113121.com");
		airlineDetails.setCompanyName("Sicejet113q1tweq512w121");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("90368915e11qq2tw31w2111");
		airlineDetails.setIataCode("BBj111153e2qqw1w21t1");
		airlineDetails.setIcaoCode("AABui1135eqq1w1w212t11");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Configure_Sic2qe1wqjet5wt1e23");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejweet2wq511tq23");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		@SuppressWarnings("unused")
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		IngestionDetailDTO ingestionDetailsDTO = new IngestionDetailDTO();
		ingestionDetailsDTO.setConfiguration(configurationDTO.getConfigurationName());
		ingestionDetailsDTO.setFileName("newMetaDataw1qetIm1p2qort - Copy.xlsx");
		ingestionDetailsDTO.setImportStatus(null);
		ingestionDetailsDTO.setIngestedDate(new Date());
		ingestionDetailsDTO.setIngestionTitleDTOList(null);
		ingestionDetailsDTO.setMode("Uploaded");
		ingestionDetailsDTO.setStatus("in progress");
		ingestionDetailsDTO.setTitleCount(0);
		ingestionDetailsDTO.setConfigurationId(configurationDTO.getConfigurationId());
		IngestionDetailDTO dto = ingestionDetailService.saveIngestionDetailsByAirlineId(airlineDTO.getAirlineInternalId(), ingestionDetailsDTO);
		List<IngestionDetailDTO> list = ingestionDetailService.getIngestionDetails(airlineDTO.getAirlineInternalId(), "Uploaded");
		Assert.assertNotNull(list);
		for(IngestionDetailDTO ingestionDetailDTO : list){
			IngestionDetail ingestionDetail = Query.from(IngestionDetail.class).where("id = ?", ingestionDetailDTO.getIngestionDetailInternalId()).first();
			ingestionDetail.delete();
		}
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}
	
	@Test
	public void getIngestionDetailsByIdTest() throws NoSuchMessageException, BadRequestException, ObjectExistsException, ObjectNotFoundException, DuplicateEntryException{
		
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Sicej5qetwtqe111232111w1");
		airlineDetails.setWebsite("https://www.Sicqtq5ew12we1jet1113121.com");
		airlineDetails.setCompanyName("Sicejet113q1tweq512w121");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("90368915e11qq2tw31w2111");
		airlineDetails.setIataCode("BBj111153e2qqw1w21t1");
		airlineDetails.setIcaoCode("AABui1135eqq1w1w212t11");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Configure_Sic2qe1wqjet5wt1e23");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejweet2wq511tq23");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		@SuppressWarnings("unused")
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		IngestionDetailDTO ingestionDetailsDTO = new IngestionDetailDTO();
		ingestionDetailsDTO.setConfiguration(configurationDTO.getConfigurationName());
		ingestionDetailsDTO.setFileName("newMetaDataw1qetIm1p2qort - Copy.xlsx");
		ingestionDetailsDTO.setImportStatus(null);
		ingestionDetailsDTO.setIngestedDate(new Date());
		ingestionDetailsDTO.setIngestionTitleDTOList(null);
		ingestionDetailsDTO.setMode("Uploaded");
		ingestionDetailsDTO.setStatus("in progress");
		ingestionDetailsDTO.setTitleCount(0);
		ingestionDetailsDTO.setConfigurationId(configurationDTO.getConfigurationId());
		IngestionDetailDTO dto = ingestionDetailService.saveIngestionDetailsByAirlineId(airlineDTO.getAirlineInternalId(), ingestionDetailsDTO);
		IngestionDetailDTO ingestionDetailDTO = ingestionDetailService.getIngestionDetailsById(dto.getIngestionDetailInternalId());
		Assert.assertNotNull(ingestionDetailDTO);
		IngestionDetail ingestionDetail = Query.from(IngestionDetail.class).where("id = ?", ingestionDetailDTO.getIngestionDetailInternalId()).first();
		ingestionDetail.delete();
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}
	
	@Test
	public void updateIngestionDetailsByAirlineIdTest() throws NoSuchMessageException, BadRequestException, ObjectExistsException, ObjectNotFoundException, DuplicateEntryException{
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Sicej5qetwtqe111232111w1");
		airlineDetails.setWebsite("https://www.Sicqtq5ew12we1jet1113121.com");
		airlineDetails.setCompanyName("Sicejet113q1tweq512w121");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("90368915e11qq2tw31w2111");
		airlineDetails.setIataCode("BBj111153e2qqw1w21t1");
		airlineDetails.setIcaoCode("AABui1135eqq1w1w212t11");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Configure_Sic2qe1wqjet5wt1e23");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejweet2wq511tq23");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		@SuppressWarnings("unused")
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		IngestionDetailDTO ingestionDetailsDTO = new IngestionDetailDTO();
		ingestionDetailsDTO.setConfiguration(configurationDTO.getConfigurationName());
		ingestionDetailsDTO.setFileName("newMetaDataw1qetIm1p2qort - Copy.xlsx");
		ingestionDetailsDTO.setImportStatus(null);
		ingestionDetailsDTO.setIngestedDate(new Date());
		ingestionDetailsDTO.setIngestionTitleDTOList(null);
		ingestionDetailsDTO.setMode("Uploaded");
		ingestionDetailsDTO.setStatus("in progress");
		ingestionDetailsDTO.setTitleCount(0);
		ingestionDetailsDTO.setConfigurationId(configurationDTO.getConfigurationId());
		IngestionDetailDTO dto = ingestionDetailService.saveIngestionDetailsByAirlineId(airlineDTO.getAirlineInternalId(), ingestionDetailsDTO);
		dto.setStatus("completed");
		IngestionDetailDTO ingestionDetailDTO = ingestionDetailService.updateIngestionDetailsByAirlineId(airlineDTO.getAirlineInternalId(), dto.getIngestionDetailInternalId(), dto);
		Assert.assertNotNull(ingestionDetailDTO);
		IngestionDetail ingestionDetail = Query.from(IngestionDetail.class).where("id = ?", ingestionDetailDTO.getIngestionDetailInternalId()).first();
		ingestionDetail.delete();
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}
	
	@Test
	public void getIngestionDetailsByFileNameTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException, NoSuchMessageException, ObjectNotFoundException{
		
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Sicej5qetwtqe111232111w1");
		airlineDetails.setWebsite("https://www.Sicqtq5ew12we1jet1113121.com");
		airlineDetails.setCompanyName("Sicejet113q1tweq512w121");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("90368915e11qq2tw31w2111");
		airlineDetails.setIataCode("BBj111153e2qqw1w21t1");
		airlineDetails.setIcaoCode("AABui1135eqq1w1w212t11");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Configure_Sic2qe1wqjet5wt1e23");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejweet2wq511tq23");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		@SuppressWarnings("unused")
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		IngestionDetailDTO ingestionDetailsDTO = new IngestionDetailDTO();
		ingestionDetailsDTO.setConfiguration(configurationDTO.getConfigurationName());
		ingestionDetailsDTO.setFileName("newMetaDataw1qetIm1p2qort - Copy.xlsx");
		ingestionDetailsDTO.setImportStatus(null);
		ingestionDetailsDTO.setIngestedDate(new Date());
		ingestionDetailsDTO.setIngestionTitleDTOList(null);
		ingestionDetailsDTO.setMode("Uploaded");
		ingestionDetailsDTO.setStatus("in progress");
		ingestionDetailsDTO.setTitleCount(0);
		ingestionDetailsDTO.setConfigurationId(configurationDTO.getConfigurationId());
		IngestionDetailDTO dto = ingestionDetailService.saveIngestionDetailsByAirlineId(airlineDTO.getAirlineInternalId(), ingestionDetailsDTO);
		IngestionDetail ingestionDetail = ingestionDetailService.getIngestionDetailsByFileName(dto.getFileName());
		Assert.assertNotNull(ingestionDetail);
		ingestionDetail.delete();
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}
	
	@Test
	public void getIngestionDetailByIdTest() throws BadRequestException, ObjectExistsException, DuplicateEntryException, NoSuchMessageException, ObjectNotFoundException{
		AirlineDTO airlineDetails=new AirlineDTO();
	    airlineDetails.setAirlineName("Sicej5qetwtqe111232111w1");
		airlineDetails.setWebsite("https://www.Sicqtq5ew12we1jet1113121.com");
		airlineDetails.setCompanyName("Sicejet113q1tweq512w121");
		//airlineDetails.setAirlineStatus("ACTIVE");
		airlineDetails.setCodeShare("90368915e11qq2tw31w2111");
		airlineDetails.setIataCode("BBj111153e2qqw1w21t1");
		airlineDetails.setIcaoCode("AABui1135eqq1w1w212t11");
		airlineDetails.setActivate(true);
		Date startDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetStartDate(startDate);
		Date endDate=new Date(Calendar.getInstance().getTimeInMillis());
		airlineDetails.setAssetEndDate(endDate);
		Date contentstartDate=new Date(Calendar.getInstance().getTimeInMillis());
		Date contentEndDate=new Date(Calendar.getInstance().getTimeInMillis());
	    airlineDetails.setContentStartDate(contentstartDate);
		airlineDetails.setContentEndDate(contentEndDate);
		airlineDetails.setShipmentStartdate(contentEndDate);
		airlineDetails.setShipmentEndDate(contentEndDate);
		AirlineDTO airlineDTO = airlineService.addAirline(airlineDetails);
		ConfigurationRequestDTO configurationDTO = new ConfigurationRequestDTO();
		configurationDTO.setConfigurationName("Configure_Sic2qe1wqjet5wt1e23");
		configurationDTO.setVersion("1.0.2");
		configurationDTO.setPlatform("Sicejweet2wq511tq23");
		configurationDTO.setConfigurationStatus("ACTIVE");
		configurationDTO.setPackagingType("CIL");
		configurationDTO.setLopaIdList(null);
		configurationDTO.setInterfaceLanguagesId(null);
		configurationDTO.setLrusId(null);
		configurationDTO.setLruTypesId(null);
		configurationDTO.setServerGroupList(null);
		Set<String> contentTypeIdList = new HashSet<>();
		List<ContentTypeDTO> contentTypeDTOs = contentTypeService.getContentTypes();
		contentTypeIdList.add(contentTypeDTOs.get(0).getContentTypeInternalId());
		configurationDTO.setContentTypeIdList(contentTypeIdList);
		configurationDTO.setTitleList(null);
		configurationDTO.setRulesId(null);
		@SuppressWarnings("unused")
		ConfigurationRequestDTO configurationRequestDTO = configurationService.addConfiguration(airlineDTO.getAirlineInternalId(), configurationDTO);
		IngestionDetailDTO ingestionDetailsDTO = new IngestionDetailDTO();
		ingestionDetailsDTO.setConfiguration(configurationDTO.getConfigurationName());
		ingestionDetailsDTO.setFileName("newMetaDataw1qetIm1p2qort - Copy.xlsx");
		ingestionDetailsDTO.setImportStatus(null);
		ingestionDetailsDTO.setIngestedDate(new Date());
		ingestionDetailsDTO.setIngestionTitleDTOList(null);
		ingestionDetailsDTO.setMode("Uploaded");
		ingestionDetailsDTO.setStatus("in progress");
		ingestionDetailsDTO.setTitleCount(0);
		ingestionDetailsDTO.setConfigurationId(configurationDTO.getConfigurationId());
		IngestionDetailDTO dto = ingestionDetailService.saveIngestionDetailsByAirlineId(airlineDTO.getAirlineInternalId(), ingestionDetailsDTO);
		IngestionDetail ingestionDetail = ingestionDetailService.getIngestionDetailById(dto.getIngestionDetailInternalId());
		Assert.assertNotNull(ingestionDetail);
		ingestionDetail.delete();
		Boolean deleteStatus = configurationService.deleteconfigurationById(airlineDTO.getAirlineInternalId(), configurationDTO.getConfigurationId());
		Assert.assertTrue(deleteStatus);
		Site airline = Query.from(Site.class).where("id = ?", airlineDTO.getAirlineInternalId()).first();
		airline.delete();
	}

}
