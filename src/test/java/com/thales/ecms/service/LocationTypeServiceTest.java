package com.thales.ecms.service;

import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.psddev.dari.db.Query;
import com.thales.ecms.exception.BadRequestException;
import com.thales.ecms.exception.ObjectAssociationExistsException;
import com.thales.ecms.exception.ObjectExistsException;
import com.thales.ecms.exception.ObjectNotFoundException;
import com.thales.ecms.model.LocationType;
import com.thales.ecms.model.dto.LocationTypeDTO;
import com.thales.ecms.service.LocationTypeService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath*:test-servlet.xml" })

public class LocationTypeServiceTest {

	@Autowired
	LocationTypeService locationTypeService;

	@Before
	public void setup() {

	}

	/**
	 * Add LocationType
	 * 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	@Test
	public void addLocationTypeTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException {
		LocationTypeDTO locationDetails = new LocationTypeDTO();
		locationDetails.setLocationType("Loading Dock");
		LocationTypeDTO locationTypeDTO = locationTypeService.addLocationType(locationDetails);
		Assert.assertEquals(locationTypeDTO.getLocationType(), locationDetails.getLocationType());
		Assert.assertNotNull(locationTypeDTO.getLocationTypeInternalId());
		LocationType locationType = Query.from(LocationType.class)
				.where("id = ?", locationTypeDTO.getLocationTypeInternalId()).first();
		locationType.delete();
	}


	/**
	 * Get location type
	 * @throws ObjectExistsException 
	 * @throws BadRequestException 
	 * @throws ObjectNotFoundException 
	 */
	@Test
	public void getlocationTypeListTest() throws ObjectNotFoundException, BadRequestException, ObjectExistsException {
		LocationTypeDTO locationTypeDTO =new LocationTypeDTO();
		locationTypeDTO.setLocationType("Loading Dock222");
		LocationTypeDTO locationType=locationTypeService.addLocationType(locationTypeDTO);
		List<LocationTypeDTO> locationTypeDTOList = locationTypeService.getlocationTypeList();
		Assert.assertNotNull(locationTypeDTOList);
		Assert.assertTrue(locationTypeDTOList.size() > 0);
		Query.from(LocationType.class).where("id = ?", locationType.getLocationTypeInternalId()).first().delete();
	}

	/**
	 * delete Location type
	 * 
	 * @throws ObjectNotFoundException
	 * @throws BadRequestException
	 * @throws ObjectExistsException
	 */
	@Test
	public void deleteLocationTypeByIdTest()
			throws ObjectNotFoundException, BadRequestException, ObjectExistsException {
		LocationTypeDTO locationTypeDTO = new LocationTypeDTO();
		locationTypeDTO.setLocationType("Loading Dock");
		LocationTypeDTO locationType = locationTypeService.addLocationType(locationTypeDTO);
		String result = locationTypeService.deleteLocationTypeById(locationType.getLocationTypeInternalId());
		Assert.assertEquals(result, "LocationType deleted successfully");
	}

	/**
	 * delete Location type for thrown exception
	 * 
	 * @throws ObjectNotFoundException
	 * @throws ObjectAssociationExistsException
	 * @throws ObjectExistsException
	 */
	@Test(expected = ObjectNotFoundException.class)
	public void deleteLocationTypeExceptionTest()
			throws ObjectNotFoundException, ObjectAssociationExistsException, ObjectExistsException {
		String result = locationTypeService.deleteLocationTypeById("id");
		Assert.assertEquals(result, "LocationType not exist");
	}

	/**
	 * 
	 * @throws ObjectNotFoundException
	 * @throws ObjectExistsException
	 * @throws BadRequestException
	 */
	@Test
	public void updateLocationTypeByIdTest()
			throws ObjectNotFoundException, ObjectExistsException, BadRequestException {
		LocationTypeDTO locationTypeDetails = new LocationTypeDTO();
		locationTypeDetails.setLocationType("lOADING Dock");
		LocationTypeDTO locationType = locationTypeService.addLocationType(locationTypeDetails);
		locationTypeDetails.setLocationType("Loadind Doc");
		LocationTypeDTO locationTypeDTO = locationTypeService
				.updateLocationTypeById(locationType.getLocationTypeInternalId(), locationTypeDetails);
		Assert.assertEquals(locationTypeDTO.getLocationType(), locationTypeDetails.getLocationType());
		LocationType locationType1 = Query.from(LocationType.class)
				.where("id = ?", locationType.getLocationTypeInternalId()).first();
		locationType1.delete();

	}
	/**
	 * 
	 * @throws ObjectNotFoundException
	 * @throws ObjectExistsException
	 */
	@Test(expected = ObjectNotFoundException.class)
	public void updateLocationTypeExceptionTest() throws ObjectNotFoundException, ObjectExistsException {
		LocationTypeDTO locationTypeDTO = new LocationTypeDTO();
		locationTypeDTO.setLocationType("Loading Dock");
		locationTypeService.updateLocationTypeById("location type", locationTypeDTO);

	}

}
