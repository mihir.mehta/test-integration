import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { IOrder, IConfigureDTOSet } from '../common/interfaces';
import { HttpService } from '../ecmsservice.service';

// Import RxJs required methods
import 'rxjs/add/operator/map';

const AIRLINE_ID: string = "00000159-f382-da8b-ab79-fbaf78a30000";

@Injectable()
export class OrderManagementService {

  constructor(private http: HttpService) { }


  getOrders(airlineId:string): Observable<IOrder[]> {
    return this.http.get("apis/order/airlines/"+airlineId+"/orderList")
      .map((res: Response) => res.json())
  }

  getOrder(orderId, airlineId:string): Observable<IOrder> {
    return this.http.get("apis/order/airlines/"+airlineId+"/orderById/" + orderId)
      .map((res: Response) => res.json())
  }

  postOrder(data: IOrder, airlineId: string): Observable<IOrder> {
    airlineId = airlineId||AIRLINE_ID;
    return this.http.post("apis/order/createOrder/airlines/" + airlineId, data)
      .map((res: Response) => res.json())
  }

  getConfigurationByAirline(airlineId:string): Observable<IConfigureDTOSet[]> {
    airlineId = airlineId||AIRLINE_ID;
    this.getWorkflows().subscribe(res=>{
      console.log(res);
    })
    return this.http.get("apis/order/configurationByAirline/" + airlineId)
      .map((res: Response) => res.json())
  }

  getWorkflows(){    
    return this.http.get("api/workflow/search?size=100&freeText=*")
      .map((res: Response) => res.json())    
  }

  updateOrder(data:IOrder, airlineId):Observable<IOrder>{
    airlineId = airlineId||AIRLINE_ID;
    let orderId = data.orderId;
    return this.http.put("apis/order/airlines/"+airlineId+"/updateOrderById/"+ orderId, data)
      .map((res: Response) => res.json())
  }

  searchOrder(data:IOrder, airlineId):Observable<IOrder[]>{
    airlineId = airlineId||AIRLINE_ID;
    return this.http.post("apis/order/airlines/"+airlineId+"/searchOrder", data)
      .map((res: Response) => res.json())

  }
}
