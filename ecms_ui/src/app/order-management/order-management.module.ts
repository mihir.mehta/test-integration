import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderManagementRoutingModule } from './order-management-routing.module';
import { OrderManagement, Order } from './order-management.component';
import { CreateOrder } from './create-order/create-order';
import { ViewOrderComponent } from './view-order/view-order';
import { AddTitle } from './component/add-title/add-title';
import { searchOrderCmp } from './search-order/search-order';
import { OrderDetails } from './component/order-details/order-details.component';
import { DisplayGroup } from './component/display-group/display-group';
import { EcmsFeatureModule } from '../common/ecms-feature/ecms-feature.module';
import { FormsModule } from '@angular/forms';
import { OrderManagementService } from './order-management.service';
import { ViewTitle } from './component/view-title/view-title';
import { FilterOrderPipe } from './filters/filter.pipe';
import { FilterConfigPipe, FilterTitlePipe } from './filters/filter.title';


@NgModule({
  imports: [
    CommonModule,
    OrderManagementRoutingModule,
    EcmsFeatureModule,
    FormsModule,
    NgbModule
  ],
  providers: [OrderManagementService],
  declarations: [FilterConfigPipe, OrderManagement, CreateOrder, ViewOrderComponent, AddTitle, searchOrderCmp, OrderDetails, DisplayGroup, Order, ViewTitle, FilterOrderPipe, FilterTitlePipe]
})
export class OrderManagementModule { }
