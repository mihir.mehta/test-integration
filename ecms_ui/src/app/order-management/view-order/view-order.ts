import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderManagementService } from '../order-management.service';
import { IOrder } from '../../common/interfaces';
import { AppService } from '../../app-service.service';


@Component({
	selector: 'order',
	templateUrl: './view-order.html',
	styleUrls: ['./view-order.scss']
})
export class ViewOrderComponent implements OnInit, OnDestroy {
	private subscribe: any;

	constructor(private activatedRoute: ActivatedRoute, private orderManagementService: OrderManagementService, private appSrvice: AppService) {}

	order: IOrder;

	orderId: string

	ngOnInit() {
		this.subscribe = this.activatedRoute.params.subscribe(params => {
			this.orderId = params['orderId'];

			if (this.orderId) {
				this.orderManagementService.getOrder(this.orderId, this.appSrvice.getAirline().airlineInternalId).subscribe(response => {
					this.order = response;
				});
			}

		});
	}

	ngOnDestroy() {
		this.subscribe.unsubscribe();
	}
}
