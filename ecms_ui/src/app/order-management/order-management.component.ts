import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order-management',
  templateUrl: './order-management.component.html'
})
export class OrderManagement {
	isActiveCopyOrder:boolean = false;
	isActiveEditOrder:boolean = false;
	currentRoute:string;
	private routeSub:any;

	constructor(private router:Router)
	{
		this.routeSub = this.router.events.subscribe(path => {
		  this.currentRoute =  path.url;		 
		  this.currentRoute.indexOf("copy") > 0 ? this.isActiveCopyOrder = true : this.isActiveCopyOrder = false;
		  this.currentRoute.indexOf("edit") > 0 ? this.isActiveEditOrder = true : this.isActiveEditOrder = false;
		 console.log(path);	
		  
		});
	}

	ngOnDestroy(){
		this.routeSub.unsubscribe();
	}
}

@Component({
  selector: 'order-management',
  template:`<router-outlet></router-outlet>`
})
export class Order{}
