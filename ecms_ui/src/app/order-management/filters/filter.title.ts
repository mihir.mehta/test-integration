import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterTitle'
})
export class FilterTitlePipe implements PipeTransform {

  transform(value: any, titleName: string): any {
    titleName = titleName ? titleName.toLowerCase() : "";
    return value && value.filter((value) => {
      if (value.contentName.toLowerCase().indexOf(titleName) >= 0) {
        return true
      }
      return false;
    });

  }
}

@Pipe({
  name: 'filterConfig'
})
export class FilterConfigPipe implements PipeTransform {
  transform(value: any, config: string): any {
    config = config ? config.toLowerCase() : "";
    return value && value.filter((value) => {
      if (value.configurationId.toLowerCase().indexOf(config) >= 0) {
        return true
      }
      return false;
    });
  }
}
