import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { IOrder, IDisplayGroup, IConfigureDTOSet } from '../../../common/interfaces';

@Component({
  selector: 'order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetails implements OnInit {

  orderDetail: IOrder;
  monthsArr: IDisplayGroup[];


  configurationList: string[];
  configurationSelected: IConfigureDTOSet

  @Input() availableItems: any[]

  @Output() orderChange = new EventEmitter();

  @Input()
  get order(): IOrder {
    return this.orderDetail;
  }

  set order(order: IOrder) {
    this.orderDetail = order;
    this.orderChange.emit(this.orderDetail);
    this.setSelectedConfiguration();
  }

  ngOnInit() {
    this.order.displayGroupList = this.order.displayGroupList || []
  }

  handlerDateChange() {
    this.getDisplayGroup(this.order.orderStartDate, this.order.orderEndDate)
  }

  getDisplayGroup(fromDate: number, toDate: number) {
    if (fromDate < toDate) {
      var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
      this.order.displayGroupList = [];
      var d1 = new Date(fromDate);
      var d2 = new Date(toDate);
      this.order.displayGroupList.push(
        {
          month: monthNames[d1.getMonth()],
          startDate: fromDate,
          endDate: toDate
        } as IDisplayGroup);
    }
  }

  setSelectedConfiguration() {
    if (this.order && this.order.configureDTOSet) {
      let configs: string[] = [];
      this.order.configureDTOSet.forEach(obj => {
        configs.push(obj.configurationId);
      })
      this.configurationList = configs;
    }
  }

  handlerConfigChange() {
    if (this.availableItems && this.configurationList) {
      this.order.configureDTOSet = [];
      if (this.configurationSelected && this.configurationList.indexOf(this.configurationSelected.configurationId) == -1) {
        this.configurationSelected = {} as IConfigureDTOSet;
      }

      this.availableItems.forEach(item => {
        if (this.configurationList.indexOf(item.value) >= 0) {
          this.order.configureDTOSet.push(item.data);
        }
      })
    }
  }
}
