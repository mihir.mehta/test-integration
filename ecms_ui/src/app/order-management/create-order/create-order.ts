import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, DoCheck, ElementRef, OnChanges, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IConfigureDTOSet, IListItem, IMultiSelectOption, IOrder } from '../../common/interfaces';
import { OrderManagementService } from '../order-management.service';
import { validate } from '../../common/validations';
import { AppService } from '../../app-service.service';

@Component({
	selector: 'create-order',
	templateUrl: './create-order.html',
	styleUrls: ['./create-order.scss']
})
export class CreateOrder implements OnInit {

	order: IOrder = {} as IOrder;
	availableItems: IMultiSelectOption[];
	selectedItems: IMultiSelectOption[] = [];
	validateForm: any;
	errMsg: boolean = true;
	errMsgText: string = "";

	@ViewChild('confirmDialog') confirmDialog: ElementRef;

	constructor(private router: Router, private route: ActivatedRoute, private orderService: OrderManagementService, private appService: AppService, private modalService: NgbModal) {

	}

	ngOnInit() {
		this.orderService.getConfigurationByAirline(this.appService.getAirline().airlineInternalId).subscribe(response => {
			response.forEach(order => {
				order.titleList.forEach(title => {
					title.destinationList = title.destinationList || []
				})
			});
			this.availableItems = this.prepareDataItemSelection(response);
			if (this.route.snapshot.params["orderId"]) {
				this.orderService.getOrder(this.route.snapshot.params["orderId"], this.appService.getAirline().airlineInternalId).subscribe(response => {
					this.order = response;
					this.selectedItems = this.prepareDataItemSelection(response.configureDTOSet);
				});
			}
		})
		this.errMsg = false;
	}


	handlerSubmit() {
		this.validateForm = new validate(this.order);
		if (this.validateForm.form().status) {
			delete this.order.dirty;
			if (!this.route.snapshot.params["orderId"]) {
				this.orderService.postOrder(this.prepareDataCreateOrder(this.order), this.appService.getAirline().airlineInternalId).subscribe(response => {
					this.router.navigate(['../search'], { relativeTo: this.route });
				});
			} else {
				this.orderService.updateOrder(this.prepareDataCreateOrder(this.order), this.appService.getAirline().airlineInternalId).subscribe(response => {
					this.router.navigate(['../../search'], { relativeTo: this.route });
				});
			}
		} else {
			this.errMsg = true;
			this.errMsgText = this.validateForm.form().message;
			setTimeout(() => {
				console.log('timeout', this.errMsg);
				this.errMsg = false;
			}, 7000)
		}
	}

	prepareDataItemSelection(data: IConfigureDTOSet[]): IMultiSelectOption[] {
		let items: IMultiSelectOption[] = [];
		data.forEach(x => {
			items.push({ label: x.configurationName, id: x.configurationId, value: x.configurationId, data: x } as IMultiSelectOption);
		})
		return items;
	}

	getDataItemSelection(): IConfigureDTOSet[] {
		let configDTO: IConfigureDTOSet[] = [];
		this.selectedItems.forEach(obj => {
			configDTO.push(obj.data);
		})
		return configDTO;
	}

	prepareDataCreateOrder(order: IOrder): any {
		let _order: any = {};
		_order = order;
		_order.configurationIdList = [];
		if (order.configureDTOSet) {
			order.configureDTOSet.forEach(con => {
				_order.configurationIdList.push(con.configurationId);
			})
		}
		delete _order.configureDTOSet;
		return _order;
	}

	hasChanges() {
		return this.order.dirty;
	}

	openConfirmDialog() {			
		const modalRef = this.modalService.open(this.confirmDialog)			
		return modalRef.result;
	}
}
