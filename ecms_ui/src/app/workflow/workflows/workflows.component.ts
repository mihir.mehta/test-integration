import { IWFMetadata } from '../workflow.interfaces';
import { WFService } from '../workflow.service';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
@Component({
    templateUrl: './workflows.component.html'
})
export class Workflows implements OnInit {
    private sub: any;
    private params: any;
    public sortField = "";

    public availableTypes: any[];
    public availableStatus: any[] = [{ label: 'RUNNING' }, { label: 'COMPLETED' }, { label: 'TERMINATED' }, { label: 'PAUSED' }, { label: 'FAILED' }, { label: 'TIMED_OUT' }];

    public filter: any = {};
    public workflows: any[];

    constructor(private route: Router, private activatedRoute: ActivatedRoute, private wfService: WFService) {
        const pramas: any = this.activatedRoute.snapshot.queryParams;
        this.workflows = [];
        if (!pramas.status) {
            if (this.route.url.indexOf('workflow/running') >= 0) {
                this.route.navigate([], { queryParams: { status: "RUNNING" }, relativeTo: this.activatedRoute });
            } else if (this.route.url.indexOf('workflow/failed') >= 0) {
                this.route.navigate([], { queryParams: { status: "FAILED" }, relativeTo: this.activatedRoute });
            } else if (this.route.url.indexOf('workflow/timed-out') >= 0) {
                this.route.navigate([], { queryParams: { status: "TIMED_OUT" }, relativeTo: this.activatedRoute });
            } else if (this.route.url.indexOf('workflow/terminated') >= 0) {
                this.route.navigate([], { queryParams: { status: "TERMINATED" }, relativeTo: this.activatedRoute });
            }
        }

    }

    ngOnInit() {
        this.sub = this.route.events.filter(event => event instanceof NavigationEnd).subscribe(event => {
            let params: any = Object.assign({}, this.activatedRoute.snapshot.queryParams);
            if (Object.keys(params).length || this.route.url.indexOf('workflow/all') >= 0) {
                params.status = params.status ? params.status.split(",") : [];
                params.workflowTypes = params.workflowTypes ? params.workflowTypes.split(",") : [];
                this.filter = params;
                this.searchWorkflows();
            }
        })
        this.wfService.getWFMetadata().subscribe(res => {
            this.availableTypes = this.prepareDataForWFTypesMulti(res);
        })
    }

    prepareDataForWFTypesMulti(data: IWFMetadata[]): any[] {
        let options: any[] = [];
        data.forEach(d => {
            options.push({
                label: d.name,
                data: d,
                value: d.name
            })
        })
        return options;
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    changeSortField(field) {
        //this.route.navigate([], { queryParams: { status: "ALL,COMPLETED" }, relativeTo: this.activatedRoute });
    }

    handlerFilterChange() {
        this.route.navigate([], { queryParams: this.filter, relativeTo: this.activatedRoute });
    }

    searchWorkflows() {
        this.wfService.searchWorkflow(this.filter).subscribe(res => {
            this.workflows = res.results;
        })
    }
}