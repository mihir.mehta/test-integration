import { ActivatedRoute } from '@angular/router';
import { WFService } from '../workflow.service';
import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';

@Component({
    templateUrl: './workflow-detail.component.html',
    styleUrls:['./workflow-detail.component.scss']
})
export class WFDetail {
    workflowDetail: any;
    workflowMetadata: any;
    constructor(private wfService: WFService, private route: ActivatedRoute) {

    }

    ngOnInit() {
        console.log(this.route);
        this.wfService.getWorkflowDetail(this.route.snapshot.params['id'])
            .mergeMap(workflow => {
                this.workflowDetail = workflow;
                return this.wfService.getWFMetaDefinition(workflow.workflowType, workflow.version)
            })
            .subscribe(result => {
                this.workflowMetadata = result;
            })
    }

    getTotalSec(end: number, start: number): string {
        if (end == null || end == 0) {
            return "";
        }
        var total = end - start;
        var sec = total / 1000;
        return sec.toString();
    }
}