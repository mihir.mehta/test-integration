import { Graph } from './component/graph.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WFDetail } from './wf-detail/workflow-detail.component';
import { EcmsFeatureModule } from '../common/ecms-feature/ecms-feature.module';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { WFService } from './workflow.service';
import { Workflows } from './workflows/workflows.component';
import { WFDefs} from './workflowDefs/wf-defs.component';
import { Tasks } from './tasks/tasks.component';
import { WorkflowComponent, WorkflowHome } from './workflow.component';
import { WorkflowRoute } from './workflow.routing.module';
import { NgModule } from '@angular/core';
import { D3Service } from 'd3-ng2-service';

@NgModule({
    imports: [WorkflowRoute, CommonModule, EcmsFeatureModule, NgbModule, FormsModule],
    declarations: [WorkflowComponent, WorkflowHome, Workflows, WFDetail, WFDefs, Tasks, Graph],
    providers: [WFService, D3Service]
})
export class WorkflowModule { }