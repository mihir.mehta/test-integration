import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PackagingComponent, PackagingRoot } from './packaging.component';
import { ViewPackagingComponent } from './view-packaging/view-packaging';

const routes: Routes = [
	{
		path: 'packaging', component: PackagingRoot, children: [
			{ path: '', redirectTo: 'list', pathMatch: "full" },
			{ path: 'list', component: PackagingComponent },
			{
				path: 'view', component: ViewPackagingComponent, data: {
					breadcrumb: "View"
				}
			}
		], data: {
			breadcrumb: "Packaging"
		}
	},

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: []
})
export class PackagingRoutingModule { }
