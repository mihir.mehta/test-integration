import {Component,OnInit} from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import {Validators,FormGroup,FormArray,FormBuilder} from '@angular/forms';
import {ProfileService} from '../../profile.service';
import {IContentTypeForProfile,IConetntTypeFields} from '../../../common/interfaces';
import { Observable } from 'rxjs/Rx';

@Component({
    selector: 'view-contentType',
    templateUrl: './view-content-type.html',
    styleUrls: ['./view-content-type.scss']
})

export class ViewContentType implements OnInit {

    contentTypeList:IContentTypeForProfile = {} as IContentTypeForProfile// = {} as IContentTypeForProfile// = {contentTypeName:"",fields:[this.contentTypeFields()]};
    fieldTypes:string[];
    private subscribe:any;
    contentTypeId:string;
    constructor(private _router: Router, 
                private profileService: ProfileService,
                private activatedRoute:ActivatedRoute) { }

    ngOnInit() {
        //debugger;
       this.subscribe = this.activatedRoute.params.subscribe(params => {          
           this.contentTypeId = params['contentTypeId'];       
           if(this.contentTypeId){
               this.profileService.getGenericContentType( this.contentTypeId).subscribe(res=>{
                    this.contentTypeList = res;
                });             
           } 
        });       
       
     }
   
    editContentType = function(){
        this.subscribe = this.activatedRoute.params.subscribe(params => {          
            this.contentTypeId = params['contentTypeId'];      
            this._router.navigate(['profile/content-type/edit-content-type',this.contentTypeId], { relativeTo: this.route });
        });
    }
    goBack = function(){
        this._router.navigate(['profile/content-type/content-type-list'], { relativeTo: this.route });
    }

}