import {Component,OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Validators,FormGroup,FormArray,FormBuilder} from '@angular/forms';
import {ProfileService} from '../../profile.service';
import {IContentTypeForProfile,IConetntTypeFields} from '../../../common/interfaces';

@Component({
    selector: 'create-contentType',
    templateUrl: './create-content.html',
    styleUrls: ['./create-content.scss']
})

export class CreateContentType implements OnInit {

    contentTypeList:IContentTypeForProfile = {} as IContentTypeForProfile// = {contentTypeName:"",fields:[this.contentTypeFields()]};

    /*contentTypeFields():IConetntTypeFields
    {
        return {displayName:"",fieldId:"",fieldType:"",selectionListName:""}
    }*/

    fieldTypes:string[];

    constructor(private _router: Router, private profileService: ProfileService) { }

    ngOnInit() {
        this.profileService.getFieldTypes().subscribe(res=>{
            this.fieldTypes = res;
        });
        this.contentTypeList.fields = this.contentTypeList.fields || [];
        this.contentTypeList.fields.push({} as IConetntTypeFields); 
     }

    addBaseFields = function() {          
        this.contentTypeList.fields.push({} as IConetntTypeFields);
    }

    deleteBaseFields = function(index) {
         this.contentTypeList.fields.splice(index,1);
    }
    submitContentType =function(){
         this.profileService.addGenericContentType(this.contentTypeList).subscribe(response=>{

             this._router.navigate(['profile/content-type/content-type-list'], { relativeTo: this.route });
        });
    }
    goBack = function(){
        this._router.navigate(['profile/content-type/content-type-list'], { relativeTo: this.route });
    }

}