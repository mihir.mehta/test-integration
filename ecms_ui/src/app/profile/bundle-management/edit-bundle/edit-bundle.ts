import { Component,OnInit,OnDestroy} from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { IBundle,ISeatingClass,ISeatingClassPriceMappings} from '../../../common/interfaces';
import { ProfileService } from '../../profile.service';
import { Observable } from 'rxjs/Rx';
import { AppService } from '../../../app-service.service';

@Component({
	selector: 'edit-bundle',
	templateUrl: './edit-bundle.html',
	styleUrls: ['./edit-bundle.scss']
})
export class EditBundle implements OnInit{	
	bundleId:string;	
	private subscribe:any;
	seatingClasses:any;
	bundle:IBundle = {seatingClassPriceMappings:[this.initSeatingClassPriceMappings()],bundleId:"",bundleName:""};
	
	initSeatingClassPriceMappings():ISeatingClassPriceMappings{
		return {ppa:"", ppv:"", seatingClassId:""};
	}
	addSeatingClassPriceMappings(){
		this.bundle.seatingClassPriceMappings.push(this.initSeatingClassPriceMappings());
	}
	constructor(private _router: Router,    
				private activatedRoute:ActivatedRoute, 
				private profileService:ProfileService,
				private appService:AppService){	}
	
	ngOnInit()
	{
	   this.subscribe = this.activatedRoute.params.subscribe(params => {          
	       this.bundleId = params['bundleId'];       
	       if(this.bundleId)
	       {

				this.profileService.getBundle(this.bundleId, this.appService.getAirline().airlineInternalId).subscribe(response=>{ 
					this.bundle = response;
					this.bundle.seatingClassPriceMappings = response.seatingClassPriceMappings;					
				});
	       } 
    	});

	   this.profileService.getSeatingClass(this.appService.getAirline().airlineInternalId).subscribe(response=>{ 
       	 	this.seatingClasses = response;
        }); 
	}

	ngOnDestroy() {
    this.subscribe.unsubscribe();
  }
  removeSeatingClassPriceMappings = function(index)
  {  		
  		this.bundle.seatingClassPriceMappings.splice(index,1);
  }
  goBack = function()
  {
  	this._router.navigate(['profile/bundle/bundle-list'], { relativeTo: this.route });
  }
  updateBundle = function(bundleId)
  {
  	if(bundleId){
		this.profileService.updateBundle(this.bundle, this.appService.getAirline().airlineInternalId).subscribe(response => {
			this._router.navigate(['profile/bundle/bundle-list'], { relativeTo: this.route });
		});
  	}
  	
  }
	
}
