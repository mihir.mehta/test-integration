import { Component,OnInit,OnDestroy} from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { IBundle,ISeatingClass,ISeatingClassPriceMappings} from '../../../common/interfaces';
import { ProfileService } from '../../profile.service';
import { Observable } from 'rxjs/Rx';
import { AppService } from '../../../app-service.service';

@Component({
	selector: 'view-bundle',
	templateUrl: './view-bundle.html',
	styleUrls: ['./view-bundle.scss']
})
export class ViewBundle implements OnInit{	
	bundleId:string;	
	private subscribe:any;
	bundle:IBundle;
	constructor(private _router: Router,    
				private activatedRoute:ActivatedRoute, 
				private profileService:ProfileService,
				private appService:AppService){	}
	
	ngOnInit()
	{
	 
	   this.subscribe = this.activatedRoute.params.subscribe(params => {          
	       this.bundleId = params['bundleId'];       
	       if(this.bundleId)
	       {
				this.profileService.getBundle(this.bundleId, this.appService.getAirline().airlineInternalId).subscribe(response=>{ 
					this.bundle = response;
					this.bundle.seatingClassPriceMappings = response.seatingClassPriceMappings;
				});
	       } 
    	});
	}

	ngOnDestroy() {
    this.subscribe.unsubscribe();
  }
	
}
