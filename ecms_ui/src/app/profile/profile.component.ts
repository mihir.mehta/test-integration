import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class Profile {}

/*@Component({
  selector: 'configuration-root',
  template:`<router-outlet></router-outlet>`
})
export class ConfigurationRoot{}*/
