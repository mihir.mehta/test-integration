import { Component, OnInit } from '@angular/core';
import {ProfileService} from '../profile.service';
import { AppService } from '../../app-service.service';

@Component({
  selector: 'configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['configuration.component.scss']
})
export class Configuration {
  filterConfig:any;
  configurations:any[];
  constructor(private profileService:ProfileService,private appService: AppService){
    profileService.getConfigurations().subscribe(res=>{
      this.configurations = res;
    });
  }
  
  editConfiguration(id:string){
    
  }
  deleteConfiguration(index){
    debugger;
    let configurationId = this.configurations[index].configurationId;    
      this.profileService.deleteConfiguration(configurationId,this.appService.getAirline().airlineInternalId).subscribe(res => {
        //this.bundles = res;  
        this.configurations.splice(index,1);
      });
    
  }
}

