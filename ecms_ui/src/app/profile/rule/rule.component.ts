import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileService } from '../profile.service';
import { IRule } from '../../common/interfaces';
import { AppService } from '../../app-service.service';

@Component({
  selector: 'rule',
  templateUrl: './rule.component.html',
  styleUrls: ['rule.component.scss']
})
export class Rule implements OnInit{  
  rules: any[] = [];
  filterRule:any;

  constructor(private _router: Router,private profileService: ProfileService, private appService: AppService)
  {
  	if (this.appService.getAirline()) {
      this.profileService.getRules(this.appService.getAirline().airlineInternalId).subscribe(res => this.rules = res);
    }
  }
 
  deleteRule = function(ruleId)
  {
      if (this.appService.getAirline()) {
      this.profileService.deleteRule(ruleId,this.appService.getAirline().airlineInternalId).subscribe(res => {
        this.rules = res;        
      });
     
    }
    window.location.reload();
  }
  ngOnInit() {}
}

