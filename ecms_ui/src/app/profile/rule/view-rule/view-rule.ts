import { Component, OnInit,ElementRef, OnDestroy} from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { ProfileService } from '../../profile.service';
import { Observable } from 'rxjs/Rx';
import { IRule,IRuleContentTypeMappingDTOSet} from '../../../common/interfaces';
import { AppService } from '../../../app-service.service';

@Component({
  selector: 'view-rule',
  templateUrl: './view-rule.html',
  styleUrls: ['./view-rule.scss']
})
export class ViewRule implements OnInit {  
  Rule:IRule;
  ruleId:string
  responseDTOSet:any;
  resposeDTOFileds:any;
  private subscribe:any;
  goBack()
  {
    this._router.navigate(['profile/rule']);
  }
  constructor(private _router: Router,
              private activatedRoute:ActivatedRoute, 
              private profileService:ProfileService, 
              private appSrvice:AppService) { }

  ngOnInit()
  {
    this.subscribe = this.activatedRoute.params.subscribe(params => {          
       this.ruleId = params['ruleId'];
       
       if(this.ruleId)
       {

          this.profileService.getRule(this.ruleId, this.appSrvice.getAirline().airlineInternalId).subscribe(response=>{        
            this.Rule = response;  
            this.responseDTOSet = response;   

          }); 
       } 
    });
   
  }

  ngOnDestroy() {
    this.subscribe.unsubscribe();
  }  
  editRoute = function()
  {
    this.subscribe = this.activatedRoute.params.subscribe(params => {          
       this.ruleId = params['ruleId'];     
       this._router.navigate(['profile/rule/edit-rule',this.ruleId ], { relativeTo: this.route });
    });    
  } 
}


