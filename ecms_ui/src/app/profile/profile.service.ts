import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {IBundle,IContentTypeListForRule,IRule,IRuleContentTypeMappingDTOSet,IContentTypeForProfile} from '../common/interfaces';
import { HttpService } from '../ecmsservice.service';
import { AppService } from '../app-service.service';


// Import RxJs required methods
import 'rxjs/add/operator/map';

const AIRLINE_ID: string = "00000159-f382-da8b-ab79-fbaf78a30000";

@Injectable()
export class ProfileService {

  constructor(private http: HttpService, private appService:AppService) { }
    
    /* Rule Services */
    getRules(airlineId: string): Observable < IRule[] > {
        return this.http.get("apis/airlines/" + airlineId + "/rule/getRules")
            .map((res: Response) => res.json());
    }

    getRule(ruleId, airlineId: string): Observable < IRule > {
        return this.http.get("apis/rule/getRule/" + ruleId + "/" + airlineId)
            .map((res: Response) => res.json());
    }

    deleteRule(ruleId, airlineId: string): Observable < IRule > {
        return this.http.delete("apis/rule/deleteRule/" + ruleId + "/" + airlineId)
            .map((res: Response) => res.json());
    }
    addRule(data: IRule, airlineId: string): Observable < IRule > {
        airlineId = airlineId || AIRLINE_ID;
        return this.http.post("apis/airlines/" + airlineId + "/rule/addRule", data)
            .map((res: Response) => res.json());
    }
    getContentTypes(): Observable < IContentTypeListForRule[] > {
        return this.http.get("apis/rule/contentTypeList")
            .map((res: Response) => res.json());
    }

    getContentType(ruleId): Observable < IContentTypeListForRule[] > {
        return this.http.get("apis/rule/getFieldByContentTypeId/" + ruleId)
            .map((res: Response) => res.json());
    }
     addAirlineContentType(data: IContentTypeListForRule, airlineId: string): Observable < IContentTypeListForRule > {
        airlineId = airlineId || AIRLINE_ID;
        return this.http.post("apis/airlines/" + airlineId + "/rule/addRule", data)
            .map((res: Response) => res.json());
    }

    /* content type Services */
    /* generic*/ 

    deleteContentType(contentTypeId): Observable < IContentTypeForProfile > {
        return this.http.delete("apis/genericContentTypes/" + contentTypeId)
            .map((res: Response) => res.json());
    }

    addGenericContentType(data: IContentTypeListForRule): Observable < IContentTypeListForRule > {
        return this.http.post("apis/genericContentTypes", data)
            .map((res: Response) => res.json());
    }
     getGenericContentType(contentTypeId): Observable < IContentTypeForProfile> {
        return this.http.get("apis/genericContentTypes/" + contentTypeId)
            .map((res: Response) => res.json());
    }
    /* Airline Specific */
   



    /* get all fields */

    getFieldTypes(): Observable < any[] > {
        return this.http.get("apis/get/fieldTypes")
            .map((res: Response) => res.json());
    }



    /* configuration services */

    getConfigurations(): Observable < any[] > {
        let airlineId = this.appService.getAirline().airlineInternalId || AIRLINE_ID;
        return this.http.get("apis/configuration/configurationList/" + airlineId)
            .map((res: Response) => res.json());
    }

    getConfigurationById(configurationId: string): Observable < any > {
        let airlineId = this.appService.getAirline().airlineInternalId || AIRLINE_ID;
        return this.http.get("apis/configuration/get/" + airlineId + "/" + configurationId)
            .map((res: Response) => res.json());
    }

    updateConfiguration(configurationId: string, data: any): Observable < any > {
        let airlineId = this.appService.getAirline().airlineInternalId || AIRLINE_ID;
        return this.http.put("apis/configuration/updateConfiguration/" + airlineId + "/" + configurationId, data)
            .map((res: Response) => res.json());
    }
     deleteConfiguration(configurationId, airlineId: string): Observable < any > {
        return this.http.delete("apis/configuration/deleteConfiguration/" + airlineId + "/" + configurationId)
            .map((res: Response) => res.json());
    }

    getContentTypesList(): Observable < any > {
        let airlineId = this.appService.getAirline().airlineInternalId;
        airlineId = airlineId || AIRLINE_ID;
        return this.http.get("apis/genericContentTypes")
            .map((res: Response) => res.json());
    }
    createContentType(data: any) {
        let airlineId = this.appService.getAirline().airlineInternalId;
        airlineId = airlineId || AIRLINE_ID;

        return this.http.post("apis/genericContentTypes", data)
            .map((res: Response) => res.json());
    }

    createConfiguration(data: any): Observable < any > {
        let airlineId = this.appService.getAirline().airlineInternalId || AIRLINE_ID;
        return this.http.post("apis/configuration/addConfiguration/" + airlineId, data)
            .map((res: Response) => res.json());
    }

    getLopas(): Observable < any[] > {
        let airlineId = this.appService.getAirline().airlineInternalId || AIRLINE_ID;
        return this.http.get("apis/airlines/" + airlineId + "/lopas")
            .map((res: Response) => res.json());
    }

    getPaxguilanguages(): Observable < any[] > {
        let airlineId = this.appService.getAirline().airlineInternalId || AIRLINE_ID;
        return this.http.get("apis/airlines/" + airlineId + "/paxguilanguages")
            .map((res: Response) => res.json());
    }

    getLrus(): Observable < any[] > {
        let airlineId = this.appService.getAirline().airlineInternalId || AIRLINE_ID;
        return this.http.get("apis/airlines/" + airlineId + "/lrus")
            .map((res: Response) => res.json());
    }

    getLruTypes(): Observable < any[] > {
        return this.http.get("apis/lruTypes")
            .map((res: Response) => res.json());
    }

    /* Bundle Services */
    addBundle(data: IBundle, airlineId: string): Observable < IBundle > {
        airlineId = airlineId || AIRLINE_ID;
        return this.http.post("apis/bundle/add/" + airlineId, data)
            .map((res: Response) => res.json());
    }
    getBundles(airlineId: string): Observable < IBundle > {
        return this.http.get("apis/bundle/getBundles/" + airlineId)
            .map((res: Response) => res.json());
    }

    deleteBundle(bundleId, airlineId: string): Observable < IBundle > {
        return this.http.delete("apis/bundle/delete/" + airlineId + "/" + bundleId)
            .map((res: Response) => res.json());
    }
    getBundle(bundleId, airlineId: string): Observable < IBundle > {
        return this.http.get("apis/bundle/getBundle/" + airlineId + "/" + bundleId)
            .map((res: Response) => res.json());
    }
    updateBundle(data: IBundle, airlineId): Observable < IBundle > {
        airlineId = airlineId || AIRLINE_ID;
        let bundleId = data.bundleId;
        return this.http.put("apis/bundle/update/" + airlineId + "/" + bundleId, data)
            .map((res: Response) => res.json())
    }

    /* rule Services */
    updateRule(data: IRule, airlineId): Observable < IRule > {
        airlineId = airlineId || AIRLINE_ID;
        let ruleId = data.ruleId;
        return this.http.put("apis/updateRule/" + airlineId + "/" + ruleId, data)
            .map((res: Response) => res.json())
    }
    getSeatingClass(airlineId: string): Observable < IRule[] > {
        return this.http.get("apis/airlines/" + airlineId + "/classes")
            .map((res: Response) => res.json());
    }
}