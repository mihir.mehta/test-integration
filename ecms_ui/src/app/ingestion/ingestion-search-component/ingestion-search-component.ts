import { AfterViewChecked } from '@angular/core/src/metadata/lifecycle_hooks';
import {Component, EventEmitter, Output, AfterViewInit} from '@angular/core';


@Component({
	selector: 'ingestion-search-component',
	templateUrl: './ingestion-search-component.html',
	styleUrls: ['./ingestion-search-component.scss']
})
export class IngestionSearchComponent implements AfterViewInit
{
	fields:any;
	flag:boolean=false;

	@Output() onSearch = new EventEmitter();

	constructor(){
		this.fields = {jobName:"", jobStartDate:"", jobEndDate:"", jobStatus:""};
	}

	hidePopup(){
		this.flag = false;
	}

	openSearchPopup = function()
	{
		this.flag = true;
	}

	serachResults = function() {
		this.onSearch.emit(this.fields);
		this.flag = false;
	}

	ngAfterViewInit(){
		this.onSearch.emit(this.fields);
	}

	parseDatetoTime(date:any):number{
		return (new Date(date.year,date.month,date.day)).getTime();
	}
}
