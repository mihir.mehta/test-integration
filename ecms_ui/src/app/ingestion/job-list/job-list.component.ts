import { Component, OnInit } from '@angular/core';
import {FileUploader} from 'ng2-file-upload';
import {Ingestion} from '../models/ingestion';
import {IngestionService} from '../services/ingestion.service';
import {IngestionViewService} from "../services/ingestion-view.service";
import {AppService} from "../../app-service.service";

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.scss']
})
export class JobListComponent implements OnInit {
    public data;
    public filterQuery = "";
    public rowsOnPage = 5;
    public sortBy = "email";
    public sortOrder = "asc";

  public uploader:FileUploader= new FileUploader({url: URL});

  jobList:any[]=[];
  uploadedContent:any[]=[];
  ingestionData: Ingestion[];
  sortField = "";

  filter:any = {};
  constructor(private ingestionService: IngestionService,private appService:AppService, private ingestionViewService:IngestionViewService) {

    // setInterval(() => {
    //   this.ngOnInit();
    // }, 25000);
  }

  ngOnInit(): void {

    this.ingestionViewService.getJobList(this.appService.getAirline().airlineInternalId)
      .subscribe(
        data =>{
          //console.log(data);
          const myJobList=[];
          for (let key in data){
            myJobList.push(data[key]);
          }
          this.jobList=myJobList;
        }
      )

    this.ingestionViewService.getUploadedContent(this.appService.getAirline().airlineInternalId)
      .subscribe(
        data =>{
          //console.log(data);
          const myUploadedContent=[];
          for (let key in data){
            myUploadedContent.push(data[key]);
          }
          this.uploadedContent=myUploadedContent;
        }
      )
  }

  startImport(fileName:string,config:string,ingestionId:string){
    let airlineId=this.appService.getAirline().airlineInternalId;

    this.ingestionService.importJob({airlineId:airlineId,configurationId:config,fileName:fileName,ingestionDetailId:ingestionId})
      .subscribe(
        data => console.log(data)
      );

    this.ingestionViewService.getJobList(this.appService.getAirline().airlineInternalId)
        .subscribe(
            data =>{
              //console.log(data);
              const myJobList=[];
              for (let key in data){
                myJobList.push(data[key]);
              }
              this.jobList=myJobList;
            }
        )

    this.ingestionViewService.getUploadedContent(this.appService.getAirline().airlineInternalId)
        .subscribe(
            data =>{
              //console.log(data);
              const myUploadedContent=[];
              for (let key in data){
                myUploadedContent.push(data[key]);
              }
              this.uploadedContent=myUploadedContent;
            }
        )
  }

  handlerSearch(fields){
    this.filter = fields;
    //this.orderService.searchOrder(fields as IOrder, this.appService.getAirline().airlineInternalId).subscribe(res=>this.orders = res);
  }

  changeSortField(field) {
    if (this.sortField.indexOf(field) == -1) {
      this.sortField = field;
    }else{
      if(this.sortField.indexOf('!')==-1)
        this.sortField = '!'+field;
      else
        this.sortField = field
    }
  }

  showDialog = false;

  closeDialog(event: boolean) {
    //console.log(event);
    this.showDialog = event;
  }

  showUploadedData(data: any) {


    this.uploader.queue.push(data);
    // data.queue.forEach((q: any) => {
    //   console.log(q.some.name);
    //   console.log(q.some.size);
    // })
  }

  flag1:boolean= false;
  showJobDetails(){
    //this.flag1 = true;
    if(this.flag1==true){
      this.flag1=false;
      return;
    }
    if(this.flag1==false){
      this.flag1=true;
    }
  }

    public toInt(num: string) {
        return +num;
    }

    public sortByWordLength = (a: any) => {
        return a.city.length;
    }


}
