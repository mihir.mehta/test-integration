import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IngestionCmp } from './ingestion/ingestion.component';
import { JobDetailsComponent } from './job-details/job-details.component';
import {JobListComponent} from './job-list/job-list.component'
import {IngestionViewComponent} from './ingestion-view/ingestion-view.component'
import {RouteImportComponent} from './route-import/route-import.component'
import {RouteImportListComponent} from './route-import/route-import-list/route-import-list.component'

const routes: Routes = [
  {
    path: 'ingestion', component: IngestionCmp,  children: [
    { path: '', redirectTo: 'ingestion-view', pathMatch: 'full' },
    {
      path: 'job-details/:jobId', component: JobDetailsComponent, data: {
      breadcrumb: "Job-Details"
    }
    },
    {
      path: 'route-import', component: RouteImportListComponent, data: {
      breadcrumb: "Route-import"
    }
    },
    {
      path: 'ingestion-view', component: IngestionViewComponent, children:[
      { path: '', redirectTo: 'job-list', pathMatch: 'full' },
      {
        path: 'job-list', component: JobListComponent, data:{
        //breadcrumb: "Job-List"
      }
      }
    ],
      data: {
        breadcrumb: "Ingestion-view"
      }
    }
  ], data:{
    breadcrumb:"Ingestion"
  }
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class IngestionRoutingModule { }
