import { Component, OnInit } from '@angular/core';
import {JobDetailsService} from "../job-details.service";
import {IngestionViewService} from '../services/ingestion-view.service'
import {Response} from '@angular/http'
import {AppService} from "../../app-service.service";
import {ActivatedRoute} from "@angular/router"

@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.scss']
})
export class JobDetailsComponent implements OnInit {

  constructor(private jobDetailsVar:JobDetailsService,private appService:AppService, private ingestionViewService:IngestionViewService,
  private route:ActivatedRoute) { }


  jobDetail:any[]=[];

  //job1:any;
  private id;
  private sub: any;

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.id = params['jobId']; // (+) converts string 'id' to a number
      //console.log(this.id);


    this.ingestionViewService.getJobDetails(this.id)
      .subscribe(
        data =>{
          console.log(data);
          const myJobList=[];

            myJobList.push(data);

          this.jobDetail=myJobList;
          //this.job1=this.jobDetail[0];
        }
      )
    })
}
}
