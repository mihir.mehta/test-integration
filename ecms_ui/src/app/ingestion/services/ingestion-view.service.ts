import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from 'rxjs/Rx';

@Injectable()
export class IngestionViewService {

  constructor(private http: Http) { }

  getJobList(airlineId){
    //console.log("Hello " + tempID);

    return this.http.get("apis/airlines/"+airlineId+"/ingestionDetails?mode=Imported")
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || "Server Error"));
  }

  getJobDetails(id){
    return this.http.get("apis/ingestionDetails/"+id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || "Server Error"));
  }

  getUploadedContent(airlineId){
    return this.http.get("apis/airlines/"+airlineId+"/ingestionDetails?mode=Uploaded")
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || "Server Error"));
  }
}
