import { NgModule } from '@angular/core';
import {CommonModule, NgStyle, NgClass} from '@angular/common';
import { IngestionRoutingModule } from './ingestion-routing.module';
import { IngestionCmp } from './ingestion/ingestion.component';
import { UploadLocalComponent } from './upload-local/upload-local.component';
import { FileDropDirective, FileSelectDirective} from "ng2-file-upload";
import { IngestionService } from './services/ingestion.service';
import {EcmsFeatureModule} from '../common/ecms-feature/ecms-feature.module';

import { JobDetailsComponent } from './job-details/job-details.component';
import {JobDetailsService} from "./job-details.service";
import { JobListComponent } from './job-list/job-list.component';
import {IngestionSearchComponent} from '../ingestion/ingestion-search-component/ingestion-search-component';
import {FormsModule} from '@angular/forms';
import { IngestionViewComponent } from './ingestion-view/ingestion-view.component';
import {IngestionViewService} from "./services/ingestion-view.service";
import {JobListFilter} from './filters/job-list-Filter.pipe';
import {RouteImportComponent} from "./route-import/route-import.component";
import {RouteImportListComponent} from "./route-import/route-import-list/route-import-list.component";
import {DataFilterPipe} from './filters/data-table-filter';
import {DataTableModule} from "angular2-datatable";

@NgModule({
  imports: [
    CommonModule,
    IngestionRoutingModule,
    EcmsFeatureModule,
    FormsModule,
    DataTableModule
  ],
  providers:[IngestionService,JobDetailsService,IngestionViewService],
  declarations: [RouteImportListComponent,UploadLocalComponent, FileDropDirective, FileSelectDirective, IngestionCmp, JobDetailsComponent,JobListComponent,IngestionSearchComponent, IngestionViewComponent,JobListFilter,DataFilterPipe]
})
export class IngestionModule { }
