import { Component, OnInit } from '@angular/core';
import {Ingestion} from '../models/ingestion';
import {IngestionService} from '../services/ingestion.service';
import { FileUploader } from 'ng2-file-upload';
import {AppService} from '../../app-service.service'
import {IngestionViewService} from '../services/ingestion-view.service'
import { Airline } from '../../common/interfaces';
import {Output, EventEmitter} from '@angular/core';


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-ingestion-view',
  templateUrl: './ingestion-view.component.html',
  styleUrls: ['./ingestion-view.component.scss']
})
export class IngestionViewComponent implements OnInit {

  public uploader:FileUploader= new FileUploader({url: URL});
  public testSelectedUrl: string;
  ingestionData: Ingestion[];

  selectedAirline:Airline;

  constructor(private ingestionService: IngestionService, private appService: AppService, private ingestionViewService:IngestionViewService) {
    this.testSelectedUrl = '';
  }



  ngOnInit(): void {

    this.selectedAirline = this.appService.getAirline();
    //console.log(this.selectedAirline.airlineName);

    //upload request will not go here

  }

  showDialog = false;

  closeDialog(event: boolean) {
    //console.log(event);
    this.showDialog = event;
  }

  showUploadedData(data: any) {


    this.uploader.queue.push(data);
    // data.queue.forEach((q: any) => {
    //   console.log(q.some.name);
    //   console.log(q.some.size);
    // })
  }


  showConfigRes:any[]=[];
  getMyConfig(){
    this.ingestionService.getConfig(this.appService.getAirline().airlineInternalId)
      .subscribe(
        data =>{
          //console.log(data);
          const myArray=[];
          for (let key in data){
            myArray.push(data[key]);
          }
          this.showConfigRes=myArray;
        }
      )
  }


  val:any;
  callType(value){
    this.testSelectedUrl = value;
    //console.log(value);
    //this.ingestionService.postSelectedConfig(value);
    if(value!=0){
      document.getElementById('showFileSelector').style.display="block";
    }
  }

}
