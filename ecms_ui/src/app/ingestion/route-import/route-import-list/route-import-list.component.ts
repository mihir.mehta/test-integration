import {Component} from '@angular/core';


@Component({
    selector: 'route-import-view',
    templateUrl:'route-import-list.html',
    styleUrls: ['route-import-list.less']
})

export class RouteImportListComponent{

    showDialog:boolean = false;

    closeDialog(event: boolean) {
        //console.log(event);
        this.showDialog = event;
    }
}
