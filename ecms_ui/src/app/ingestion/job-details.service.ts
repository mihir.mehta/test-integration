import { Injectable } from '@angular/core';
import {Http} from "@angular/http";

@Injectable()
export class JobDetailsService {

  constructor(private http:Http) { }

  getData(){
    return this.http.get('https://myfirstajax.firebaseio.com/title.json');
  }
}
