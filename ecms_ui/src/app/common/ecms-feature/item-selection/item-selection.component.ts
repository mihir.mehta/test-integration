import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ItemListComponent, ItemClickedEvent } from '../item-list/item-list.component' ;
import { IListItem } from '../../interfaces';



@Component({
  selector: 'item-selection',
  templateUrl: './item-selection.component.html',
  styleUrls: ['./item-selection.component.scss']
})
export class ItemSelectionComponent implements OnInit {

  @Input() viewOnlyMode:boolean = false;

  @Input() availableItems:IListItem[];

  selectedItems:IListItem[];

  @Output() selectedChange = new EventEmitter();

  @Input()
  get selected():IListItem[]{
    return this.selectedItems;
  }

  set selected(val:IListItem[]){
    this.selectedItems = val;
    this.selectedChange.emit(this.selectedItems);
  }

  constructor() {}  
  
  ngOnInit() {
    	
  }

  handlerMove(){
  	this.availableItems = this.availableItems.filter((item)=>{
  		if(item.selected){
  			item.selected = false;
        let items = this.selected;
        items.push(item);
  			this.selected = items;
  			return false
  		}
  		return true;
  	});
  }

  handlerRemove(){
  	this.selected = this.selected.filter((item)=>{
  		if(item.selected){
  			item.selected = false;
  			this.availableItems.push(item);
  			return false
  		}
  		return true;
  	});
  }

  handlerMoveAll(){
  	this.availableItems.forEach((item)=>{
  		item.selected = false;
  	})
  	this.selected = this.selected.concat(this.availableItems);
  	this.availableItems = [];
  }

  handlerRemoveAll(){
  	this.selected.forEach((item)=>{
  		item.selected = false;
  	})
  	this.availableItems = this.availableItems.concat(this.selected);
  	this.selected = [];
  }

}
