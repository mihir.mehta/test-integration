import { OrderByPipe } from './pipes/order-by.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ItemSelectionComponent } from './item-selection/item-selection.component';
import { ItemListComponent } from './item-list/item-list.component';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { DialogComponent } from './dialog/dialog.component';
import { SearchComponent } from './search-component/search-component';
import { Steps, Step } from './steps/steps.component';
import { EcmsAccordionComponent, Accordion } from './ecms-accordion/ecms-accordion.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { RouterModule } from '@angular/router';
import { MultiSelectComponent } from './multi-select/multi-select.component';
import { SwitchBoxComponent } from './switchbox/switchbox';


@Component({
  selector: 'router-outlet-component',
  template: '<router-outlet></router-outlet>'
})
export class RouterOutletComponent{

}

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgbModule
  ],
  declarations: [OrderByPipe, ItemSelectionComponent, ItemListComponent, DatePickerComponent, DialogComponent, SearchComponent, Steps, Step, EcmsAccordionComponent, Accordion, BreadcrumbComponent, MultiSelectComponent, SwitchBoxComponent, RouterOutletComponent],
  exports: [OrderByPipe, ItemSelectionComponent, ItemListComponent, DatePickerComponent, DialogComponent, SearchComponent, Steps, Step, EcmsAccordionComponent, Accordion, BreadcrumbComponent, MultiSelectComponent, SwitchBoxComponent]
})
export class EcmsFeatureModule { }
