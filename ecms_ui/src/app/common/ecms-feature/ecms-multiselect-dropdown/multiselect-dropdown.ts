import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

declare var jQuery:any;

@Component({
  selector: 'multiselect-dropdown',
  template: `<input #input type="text" class="span2">`
})
export class MultiselectDropdownComponent implements OnInit, AfterViewInit {

  @ViewChild('input') input: ElementRef;

  ngOnInit() {
  }

  ngAfterViewInit(){
  	jQuery(this.input.nativeElement).fdatepicker({
		initialDate: '',
		format: 'mm-dd-yyyy',
		disableDblClickSelection: true,
		leftArrow:'<<',
		rightArrow:'>>',
		closeIcon:'X',
		closeButton: false
	});
  }

}
