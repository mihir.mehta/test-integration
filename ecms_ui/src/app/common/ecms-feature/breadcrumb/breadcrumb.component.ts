import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, Params, PRIMARY_OUTLET, RouterModule } from "@angular/router";
import "rxjs/add/operator/filter";

interface IBreadcrumb {
  label: string;
  params?: Params;
  url: string;
}

@Component({
  selector: "breadcrumb",
  template: `
    <!--<nav class="ecms-breadcrumb" aria-label="You are here:" role="navigation">
      <ul class="breadcrumbs nav">
        <li class="disabled nav-item">Home</li>
        <li class="nav-item" *ngFor="let breadcrumb of breadcrumbs">        
          <a class="nav-link" [routerLink]="[breadcrumb.url, breadcrumb.params]">{{breadcrumb.label}}</a>
        </li>
      </ul>
    </nav>-->
    <nav class="ecms-breadcrumb">    
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item" *ngFor="let breadcrumb of breadcrumbs"><a [routerLink]="[breadcrumb.url, breadcrumb.params]">{{breadcrumb.label}}</a></li>      
      </ol>
    </nav>
  `,
  styleUrls:['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  public breadcrumbs: IBreadcrumb[];

  private sub:any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.breadcrumbs = [];
  }

  ngOnInit() {
    const ROUTE_DATA_BREADCRUMB: string = "breadcrumb";
    //subscribe to the NavigationEnd event
    this.sub = this.router.events.filter(event => event instanceof NavigationEnd).subscribe(event => {
      
      //set breadcrumbs
      let root: ActivatedRoute = this.activatedRoute.root;
      this.breadcrumbs = this.getBreadcrumbs(root);
    });
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }
  
  private getBreadcrumbs(route: ActivatedRoute, url: string="", breadcrumbs: IBreadcrumb[]=[]): IBreadcrumb[] {
    const ROUTE_DATA_BREADCRUMB: string = "breadcrumb";

    //get the child routes
    let children: ActivatedRoute[] = route.children;

    //return if there are no more children
    if (children.length === 0) {
      return breadcrumbs;
    }

    //iterate over each children
    for (let child of children) {
      
      //verify primary route
      if (child.outlet !== PRIMARY_OUTLET) {
        continue;
      }
      //console.log(child.snapshot.data);
      //verify the custom data property "breadcrumb" is specified on the route
      if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
        continue;
      }

      //get the route's URL segment
      let routeURL: string = child.snapshot.url.map(segment => segment.path).join("/");
      
      //append route URL to URL
      url += `/${routeURL}`;

      //add breadcrumb
      let breadcrumb: IBreadcrumb = {
        label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
        params: child.snapshot.params,
        url: url
      };
      breadcrumbs.push(breadcrumb);
      
      //recursive
      return this.getBreadcrumbs(child, url, breadcrumbs);
    }
    
    //we should never get here, but just in case
    return breadcrumbs;
  }

}
