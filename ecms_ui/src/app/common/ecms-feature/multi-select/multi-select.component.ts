import { IMultiSelectOption } from '../../interfaces';
import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { 
    ControlValueAccessor, 
    NG_VALUE_ACCESSOR, 
    NG_VALIDATORS, 
    FormControl, 
    Validator 
} from '@angular/forms';


declare var jQuery: any;
@Component({
  selector: 'multi-select',
  template: `
    <select [disabled]="disabled=='yes'" #multiSelect multiple="multiple" class="w300">
      <option *ngFor="let option of options" value="{{option.value||option.label}}" selected>{{option.label}}</option>
    </select>
  `,
  styleUrls: ['./multi-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultiSelectComponent),
      multi: true
    }]
})

export class MultiSelectComponent implements OnInit, AfterViewInit, ControlValueAccessor, Validator {
  @ViewChild('multiSelect') select: ElementRef;

  _options: IMultiSelectOption[];

  @Input()
  get options(): IMultiSelectOption[] {
    return this._options;
  };

  set options(val) {
    //console.log("Multiselect > options", val);
    this._options = val;
    this.initMultiSelect();
  }

  public validate(c: FormControl) {
      return null;
  }

  private propagateChange = (_: any) => { };

  // this is the initial value set to the component
  public writeValue(obj: any) {
      if (obj) {
          this.value = obj;
      }
  }

  // registers 'fn' that will be fired when changes are made
  // this is how we emit the changes back to the form
  public registerOnChange(fn: any) {
      this.propagateChange = fn;
  }
  // not used, used for touch input
  public registerOnTouched() { }
  

  public disableMe: boolean;

  multi: boolean;
  config: any;
  value: any = [];

  private selectInstace: any;

  @Output() valuesChange = new EventEmitter();

  @Input()
  get values() {
    return this.value;
  }

  set values(val) {
    this.value = val || [];
    this.setSelects(this.value);
    this.valuesChange.emit(this.value);
  }

  @Input()
  set multiple(val) {
    this.multi = val;
    this.config.single = !this.multi;
  }

  @Input()
  set disabled(val) {
    this.disableMe = val;
    if (this.selectInstace) {
      this.toggleDisable()
    }
  }

  constructor() {
    this.options = [];
    this.config = {
      placeholder: "Select",
      width: 150
    };
    this.value = [];
  }

  ngOnInit() {
    this.config.onClick = this.config.onCheckAll = this.config.onUncheckAll = (() => {
      this.values = this.selectInstace.multipleSelect("getSelects");
      this.propagateChange(this.value);
    })
  }

  ngAfterViewInit() {
    this.selectInstace = jQuery(this.select.nativeElement).multipleSelect(this.config);
    this.setSelects(this.values);
    //this.toggleDisable();
  }

  initMultiSelect() {
    var self = this;
    setTimeout(function () {
      jQuery(self.select.nativeElement).multipleSelect("refresh");
      self.setSelects(self.values);
      //self.toggleDisable();
    }, 500);
  }

  setSelects(val: string[]) {
    if (this.selectInstace && val) {
      this.selectInstace.multipleSelect("setSelects", val);
    }
  }

  toggleDisable() {
    if (this.disableMe) {
      this.selectInstace.multipleSelect("disable");
    } else {
      this.selectInstace.multipleSelect("enable");
    }
  }

}
