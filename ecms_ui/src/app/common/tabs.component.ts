import { Component, Input  } from '@angular/core';

@Component({
  selector: 'tabs',
  styleUrls: ['../app.component.scss'],
  template: `
    <ul class="abc">
      <li *ngFor="let tab of tabs" (click)="selectTab(tab)" class="abcd" [ngClass]="{'active': tab.active}">
        {{tab.tabTitle}}
      </li>
    </ul>
    <ng-content></ng-content>
  `,
})
export class Tabs {
  tabs: Tab[] = [];

  selectTab(tab: Tab) {
    this.tabs.forEach((tab) => {
      tab.active = false;
      
    });
    tab.active = true;
  }

  addTab(tab: Tab) {
    if (this.tabs.length === 0) {
      tab.active = true;
    }
    this.tabs.push(tab);

  }
}

@Component({
  selector: 'tab',
  styleUrls: ['../app.component.scss'],
  template: `
    <div [hidden]="!active">
      <ng-content></ng-content>
    </div>
  `
})
export class Tab {

  @Input() tabTitle: string;
  active:boolean = false;
  constructor(tabs:Tabs) {
    tabs.addTab(this);
    
  }
}