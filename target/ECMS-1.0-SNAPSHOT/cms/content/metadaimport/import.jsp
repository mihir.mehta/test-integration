<%@page import="java.util.List"%>
<%@page import="com.psddev.dari.db.ObjectType"%>
<%@page import="com.psddev.dari.db.ObjectField"%>
<%@page import="com.psddev.dari.db.Database"%>
<%@page import="com.psddev.cms.db.Site"%>
<%@page import="com.psddev.cms.tool.ToolPageContext"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>MetaData Import</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="../../../css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../../css/demo.css">
<script src="../../../js/jquery.js"></script>
<script src="../../../js/bootstrap.js"></script>
<script>
var contextPath = '<%=request.getContextPath()%>';
	var url = window.location.href;
	var arr = url.split("/");
	var result = arr[0] + "//" + arr[2]
	
	var applicationURL =result + contextPath; 
	
	///////////////////////
	
	$(document).ready(function() {
		var file = $('[name="file"]');
	 

		$('#btnUpload').on('click', function() {
			var filename = $.trim(file.val());
			var airlineId= $("#airlineId").val();
			var configurationId= $("#configurationId").val();
			
			$.ajax({
				url : applicationURL+'/apis/metadata/uploadfiles/'+airlineId+'/'+configurationId,
				type : "POST",
				data : new FormData(document.getElementById("fileForm")),
				enctype : 'multipart/form-data',
				processData : false,
				contentType : false
			}).done(function(response) {
			  var divData;
			  var html = "<tr><td>File Name</td><td>Status</td><td>Reason</td><td>Action</td></tr>";
		      $('#myTable').append(html);
		      
			   $.each (response, function (key, value) {
			     divData = value; //gives me the value of div 1
			     var html = "<tr><td>" + divData.fileName + "</td><td>" + divData.status+ "</td><td>" + divData.reason+ "</td><td><button onclick=\"javascript:startImport('"+divData.fileName+"','"+ divData.ingestionDetailInternalId+"');\">Start Import</button></td></tr>";
			      $('#myTable').append(html);
			   });
			}).fail(function(jqXHR, textStatus) {
				alert('File upload failed ...');
			});
		});
		
	});
	
function startImport(fileName,ingestionDetailInternalId){
	
	var airlineId= $("#airlineId").val();
	var configurationId= $("#configurationId").val();
	
	var json = { "airlineId": airlineId,"configurationId" : configurationId,"ingestionDetailId":ingestionDetailInternalId,"fileName" : fileName};
	
	console.log("json = ",json);
	$.ajax({
		url : applicationURL+"/apis/metadata/importcontenttype/",
		type : "POST",
		contentType : "application/json",
		dataType : "json",
		processData : false,
		data : JSON.stringify(json),
		success : function (result) {
			console.log("Added success", result);
			showIngestionReport(result);
		},
		error : function (error) {
			console.log("error occured", error);
		}
	})
	
}


function showIngestionReport(result){
	var str = JSON.stringify(result);
	output(syntaxHighlight(str));
}

function output(inp) {
    document.body.appendChild(document.createElement('pre')).innerHTML = inp;
}

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}	
	
	
</script>

<link type="text/css" rel="stylesheet"
	href="/assets/css/less/svod/thalesHCL/landingAirlinePortal.css" />
<style type="text/css">
pre {
	outline: 1px solid #ccc;
	padding: 5px;
	margin: 5px;
}

.string {
	color: green;
}

.number {
	color: darkorange;
}

.boolean {
	color: blue;
}

.null {
	color: magenta;
}

.key {
	color: red;
}
</style>
<%
	ToolPageContext wp = new ToolPageContext(pageContext);

	wp.writeHeader();
%>
</head>
<body>
	<!-- Using this to pass the airlineId to rule.js. Will pass null if the Site is Global -->
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-9 ruleClass">
				<div class="panel panel-default">
					<div class="panel-heading">MetaDataImport</div>
					<div class="panel-body">
						configurationId: <input type="text" name="configurationId"
							id="configurationId"> <br> airlineId: <input
							type="text" name="airlineId" id="airlineId">
						<form id="fileForm">
							<div>
								<input type="file" name="file" multiple="multiple" />
							</div>
							<br />
							<div class="row">
								<button id="btnUpload" type="button">Upload file</button>
							</div>
						</form>
					</div>
					<br />
					<table id="myTable">
					</table>
					<p>&nbsp;</p>
				</div>
			</div>
		</div>
	</div>
	<%
		wp.writeFooter();
	%>
</body>
</html>