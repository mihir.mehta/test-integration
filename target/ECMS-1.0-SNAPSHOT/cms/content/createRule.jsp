<%@page import="java.util.List"%>
<%@page import="com.psddev.dari.db.ObjectType"%>
<%@page import="com.psddev.dari.db.ObjectField"%>
<%@page import="com.psddev.dari.db.Database"%>
<%@page import="com.psddev.cms.db.Site"%>
<%@page import="com.psddev.cms.tool.ToolPageContext"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Rule Management</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../css/demo.css">
<script src="../../js/jquery.js"></script>
<script src="../../js/bootstrap.js"></script>
<script src="../../js/rule.js"></script>
<link type="text/css" rel="stylesheet"
	href="/assets/css/less/svod/thalesHCL/landingAirlinePortal.css" />
<script></script>
<%
	ToolPageContext toolPageContext = new ToolPageContext(pageContext);
    Site airline = toolPageContext.getSite();
    String airlineId = null;
    if(airline != null){
    	airlineId = airline.getId().toString();
    }
%>
</head>
<body>
	<%
		ToolPageContext wp = new ToolPageContext(pageContext);

		wp.writeHeader();
		
	%>
	<!-- Using this to pass the airlineId to rule.js. Will pass null if the Site is Global -->
	<input type = "hidden" id="airlineId" value="<%=airlineId%>">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="panel panel-default">
					<div class="panel-heading">Rule Management</div>
					<div class="panel-body">
						<form action="">
							<!-- Trigger the modal with a button -->
							<div>
								<ul class="thalesNavBar">
									<li id="createAnOrder"><a href="ruleEngine.jsp">List
											Rules</a></li>
									<li id="createAnOrder"><a href="createRule.jsp">Create
											Rule</a></li>
								</ul>
							</div>
							<!-- Modal Ends -->
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-9 ruleClass">
				<div class="panel panel-default">
					<div class="panel-heading">Create Rule</div>
					<div class="panel-body">
						<div>
							<input type="text" name="ruleName" id="ruleName" class="txtBox"
								placeholder="Rule Name">
						</div>
						<br />
						
               
						<div class="row">
						<div id="dynamic-ConfigDropdwon-area" class="col-md-12"></div>
						<div id="dynamic-dropdwon-area" class="col-md-12"></div>
						
						</div>
					</div>
					<br />
					<button type="button" class="btn btn-primary confirm-btn"
						id="submitValidation" style="display: none; margin-left:20px;">Submit</button>
					<p>&nbsp;</p>
				</div>
			</div>
		</div>
	</div>
	<%
		wp.writeFooter();
	%>
</body>
</html>
