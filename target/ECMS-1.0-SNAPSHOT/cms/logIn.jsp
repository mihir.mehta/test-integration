	<%@ page session="false" import="
   com.psddev.cms.db.ToolAuthenticationPolicy,
   com.psddev.cms.db.ToolUser,
   com.psddev.cms.tool.AuthenticationFilter,
   com.psddev.cms.tool.ToolPageContext,

   com.psddev.dari.db.Query,
   com.psddev.dari.util.AuthenticationException,
   com.psddev.dari.util.AuthenticationPolicy,
   com.psddev.dari.util.HtmlWriter,
   com.psddev.dari.util.JspUtils,
   com.psddev.dari.util.ObjectUtils,
   com.psddev.dari.util.Settings,
   com.psddev.dari.util.StringUtils,
   com.psddev.dari.util.UrlBuilder,

   java.net.MalformedURLException,
   java.net.URL,
   java.util.UUID
   " %><%
   // --- Logic ---

   ToolPageContext wp = new ToolPageContext(pageContext);

   if (wp.getUser() != null) {
       AuthenticationFilter.Static.logOut(response);
       response.sendRedirect(new UrlBuilder(request).
               currentPath().
               currentParameters().
               toString());
       return;
   }

   AuthenticationException authError = null;
   String username = wp.param("username");
   String returnPath = wp.param(AuthenticationFilter.RETURN_PATH_PARAMETER);
   ToolUser user = ToolUser.Static.getByTotpToken(wp.param(String.class, "totpToken"));

   if (wp.isFormPost()) {
       try {

           if (user != null) {
               if (!user.verifyTotp(wp.param(int.class, "totpCode"))) {
                   throw new AuthenticationException("The code you've entered is either invalid or has already been used.");
               }

           } else {
               AuthenticationPolicy authPolicy = AuthenticationPolicy.Static.getInstance(Settings.get(String.class, "cms/tool/authenticationPolicy"));

               if (authPolicy == null) {
                   authPolicy = new ToolAuthenticationPolicy();
               }

               user = (ToolUser) authPolicy.authenticate(username, wp.param(String.class, "password"));

               if (user.isTfaEnabled()) {
                   String totpToken = UUID.randomUUID().toString();

                   user.setTotpToken(totpToken);
                   user.save();
                   wp.redirect("", "totpToken", totpToken);
                   return;
               }
           }

           if (user.isChangePasswordOnLogIn()) {
               String changePasswordToken = UUID.randomUUID().toString();
               user.setChangePasswordToken(changePasswordToken);
               user.save();
               wp.redirect("change-password.jsp", "changePasswordToken", changePasswordToken, AuthenticationFilter.RETURN_PATH_PARAMETER, returnPath);
               return;
           }

           if (user.getChangePasswordToken() != null) {
               user.setChangePasswordToken(null);
               user.save();
           }

           AuthenticationFilter.Static.logIn(request, response, user);

           if (!StringUtils.isBlank(returnPath)) {
               try {
                   wp.redirect(new URL(JspUtils.getAbsoluteUrl(request, returnPath)).toString());
               } catch (MalformedURLException e) {
                   wp.redirect("/");
               }
           } else {
               wp.redirect("/");
           }

           return;

       } catch (AuthenticationException error) {
           authError = error;
       }
   }

   // --- Presentation ---

   %><% wp.include("/WEB-INF/header.jsp"); %>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<style type="text/css">
		.toolHeader {
		background-color: transparent;
		border-style: none;
		display: none;
		}
		.toolTitle {
		float: none;
		height: 100px;
		margin: 30px 0 0 0;
		text-align: center;
		}
		.toolFooter {
		border-style: none;
		text-align: center;
		display: none !important;
		}
		.toolFooter .build {
		background-position: top center;
		text-align: center;
		}
		.toolContent
		{
		background: transparent;
		padding: 0px;
		margin: 0px;
		}
		body {
		margin-top: 170px;
		background-position: center;
		background-size: cover;
		background-image: url("http://staticviewlift-a.akamaihd.net/dims4/default/fe77e6a/2147483647/thumbnail/1000x563%3E/quality/90/?url=http%3A%2F%2Fsnagfilms-a.akamaihd.net%2F9a%2F63%2Fc980983446cb9a3d47bf86b4e598%2Fbg.png");
		}
		body.hasToolBroadcast {
		margin-top: 195px;
		}
		input[type=password], input[type=text]
		{
		border-radius: 2px;
		width: 100%;
		height: 30px;
		}
		.loginmsg,.login-foooter{
		color: #FFF;
		text-align: center;
		
		}

		.login-foooter
		{
		margin-top: 89px !important;
		}
		.inputContainer
		{
		margin-bottom: 15px;
		}
		.inputLabel
		{
		display: none;
		}
		.action-logIn-new
		{
		border-radius: 37px;
		width: 50px;
		height: 50px;
		font-size: 31px;
		font-weight: bold;
		}
		.submitbtn
		{
		text-align: center;
		}
		.img-container {
			text-align: center !important;
			margin-bottom: 30px !important;
		}
		</style>
		<div class="container">
		<div class="row">
		<div class="col-md-12 img-container">
		<h1 class="loginmsg">CMS</h1>
		<%--<img  class="login-logo" src= "https://snagfilms-a.akamaihd.net/fd/4b/2001daf54c129d28fde73e893a9f/inflytcloud-logo.png">--%>
		</div>
		</div>
		<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<%
         if (wp.param(boolean.class, "forced")) {
             wp.writeStart("div", "class", "message message-warning");
                 wp.writeHtml("You've been inactive for too long or logged out from a different page. Please log in again.");
             wp.writeEnd();
         }

         if (authError != null) {
             new HtmlWriter(wp.getWriter()).object(authError);
         }
         %>
			<% if (!Query.from(ToolUser.class).hasMoreThan(0)) { %>
		<div class="message message-info">
		<p>Welcome! You're our first user. Give us your email or
		username and password and we'll make you an administrator.
		</p>
		</div>
			<% }
         %>
		<form action="<%= wp.url("", "forced", null) %>" method="post">
			<% if (user == null) { %>
		<div class="inputContainer">
		<div class="inputLabel">
		<label for="<%= wp.createId() %>">Username</label>
		</div>
		<div class="">
		<input class="autoFocus" id="<%= wp.getId() %>" name="username" type="text" value="<%= wp.h(username) %>" placeholder="UserName">
		</div>
		</div>
		<div class="inputContainer">
		<div class="inputLabel">
		<label for="<%= wp.createId() %>">Password</label>
		</div>
		<div class="">
		<input id="<%= wp.getId() %>" name="password" type="password" placeholder="Password">
		</div>
		</div>
			<% } else { %>
		<div class="inputContainer">
		<div class="inputLabel">
		<label for="<%= wp.createId() %>">Code</label>
		</div>
		<div class="inputSmall">
		<input class="autoFocus" id="<%= wp.getId() %>" name="totpCode" type="text">
		</div>
		</div>
			<% } %>
		<br/>
		<div class="submitbtn">
		<button class="action action-logIn-new"> > </button>
			<% if (!StringUtils.isBlank(Settings.get(String.class, "cms/tool/forgotPasswordEmailSender")) && user == null) {%>
		<a href="<%= wp.url("forgot-password.jsp", AuthenticationFilter.RETURN_PATH_PARAMETER, returnPath) %>">Forgot Password?</a>
			<% } %>
		</div>
		<div class="login-foooter">
		<%--<h2 style="border-bottom: none"><img src="https://snagfilms-a.akamaihd.net/81/c0/76ac0086484eabe6518b566a3e65/thales.png"></h2>--%>
		</div>
		</form>
		</div>
		<div class="col-md-4"></div>
		</div>
			<% wp.include("/WEB-INF/footer.jsp"); %>
